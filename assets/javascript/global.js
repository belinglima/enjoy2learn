// script de loading global
const lodingGeral = () => {
	$('body').append('<div class="bgLoadingMaster"><div class="loadingMaster"></div><p class="mgs">Aguarde carregamento.</p></div>');
	// finalizaLoading();
}

// finaliza carregamento 
const finalizaLoading = () => {
	setTimeout(function(){ 
		$('.bgLoadingMaster').toggle('hide');
		$('.bgLoadingMaster').remove();
	}, 3000);
}

$(document).ready(function(){

	$.get(base_url('Dashboard/atualizaCarrinho'), function(data){
	 	if (data > 0) {
	 		$('.totalCarrinho').html(data);
	 	}
	});
	

	// mostrar notificações e esconder ao clicar fora
	var box = $('.notificacoes');
	var notify = $('.notify');
	var img = base_url('assets/images/2.svg');
	var texto = '<li><img src='+img+' width="60" style="margin-left: 50%; transform: translate(-50%);"></li><a href="javaScript:void(0);" class="btt-notificacoes-lista">Aguarde...</a>';

	notify.click(function() {
	    box.removeClass('hide'); 
	    $('.lista-notificacoes').html(texto);

	   	setTimeout(function(){
	   		$.get(base_url('Dashboard/getNotification'), function(data){
			 	if (data) {
			 		$('.lista-notificacoes').html(JSON.parse(data));
			 	}
			});	
	   	},5000)

	    return false;
	});

	$(document).click(function() {
	    box.addClass('hide');
	    $('.lista-notificacoes').html('');
	});

	box.click(function(e) {
	    e.stopPropagation();
	});
});
