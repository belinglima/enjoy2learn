const validarSenha = () => {
	let senha1 = $('.senha1').val();
	let senha2 = $('.senha2').val();
	let url = base_url('motoristas/alteraSenha');

	if (senha1 == "") {
		$('.text01').removeAttr('data-error')
		$('.text01').attr('data-error', 'Digite à senha...');
		$('.senha1').focus();
		return;
	}

	if (senha1.length < 6) {
		$('.text01').removeAttr('data-error')
		$('.text01').attr('data-error', 'Tamanho inválido');
		return;
	}

	if (senha2 == "") {
		$('.text02').removeAttr('data-error')
		$('.text02').attr('data-error', 'Digite à senha...');
		$('.senha2').focus();
		return;
	}

	if (senha2.length < 6) {
		$('.text02').removeAttr('data-error')
		$('.text02').attr('data-error', 'Tamanho inválido');
		return;
	}

	if (senha1 === senha2) {
		$('form.link').submit();
	} else {
		$('.text02').removeAttr('data-success')
		$('.text02').attr('data-success', 'Senha não confere...');
		return;
	}
}