// function consultaGEO(logradouro) {
//     let lo = logradouro + '+' + $("#numero").val();
//     let end = logradouro.replace(/\s/g, "+");
//     let url = 'https://nominatim.openstreetmap.org/search.php?q='+ end +'&format=json&limit=1';
    
//     $.get(url, function(data, status){
//         if(data.length != 0 && status == "success"){
//             $("#lat").val(data[0].lat);
//             $("#lon").val(data[0].lon);
//         } else {
//             swal({
//                title: "Aviso!",
//                text: "Geo Refência não encontrada \n Necessário informar referência!",
//                closeOnConfirm: true
//             });
//             $(".materialize-textarea").focus();
//         }
//     });
// }
    
function limpa_formulário_cep() { 
    //Limpa valores do formulário de cep.
    $("#logradouro").val("");
    $("#bairro").val("");
    $("#localidade").val("");
    // $("#uf").val("");
}
    
function meu_callback(conteudo) {
    if (!("erro" in conteudo)) {
        //Atualiza os campos com os valores.
        $("#logradouro").val(conteudo.logradouro);
        $("#bairro").val(conteudo.bairro);
        $("#localidade").val(conteudo.localidade);
        getCidadeMapa(conteudo.ibge);
    } else {
        //CEP não Encontrado.
        limpa_formulário_cep();
        swal({
            title: "Aviso!",
            text: "CEP não encontrado, favor digitar os dados com referência",
            closeOnConfirm: true
        });
    }
}

function getCidadeMapa(ibge) {
    let urlMapa = base_url('clientes/getCidade/' + ibge);
    $.get(urlMapa, function(data){
        if (data != "") {
            let cidade = JSON.parse(data);
            $('#cidade_latitude').val(cidade.latitude);
            $('#cidade_longitude').val(cidade.longitude);
        }
    });
}
    
function pesquisacep(valor) {
    // $(".labelRua, .labelNumero, .labelBairro, .labelLocalidade, .labelUf").addClass('active');
    //Nova variável "cep" somente com dígitos.
    var cep = valor.replace(/\D/g, '');
    //Verifica se campo cep possui valor informado.
    if (cep != "") {
        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;
        //Valida o formato do CEP.
        if (validacep.test(cep)) {
            //Preenche os campos com "..." enquanto consulta webservice.
            $("#rua").val("...");
            $("#bairro").val("...");
            $("#localidade").val("...");
            //Cria um elemento javascript.
            var script = document.createElement('script');
            //Sincroniza com o callback.
            script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';
            //Insere script no documento e carrega o conteúdo.
            document.body.appendChild(script);
        } else {
            //cep é inválido.
            limpa_formulário_cep();
            swal({
                title: "Aviso!",
                text: "Formato de CEP inválido.",
                closeOnConfirm: true
            });
        }
    } else {
        //cep sem valor, limpa formulário.
        limpa_formulário_cep();
    }
};
