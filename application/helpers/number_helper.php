<?php 

function convert_value_to_cents($value)
{
    return number_format($value, 2, "", "");
}

function convert_cents_to_double($value)
{
    return number_format(($value)/100, 2, ".", "");
}

function convert_double_to_BRL($value)
{
    return number_format($value, 2, ",", "");
}

function convert_BRL_to_double($value)
{
    return number_format($value, 2, ".", "");
}

function convert_Real($value)
{
    return number_format($value, 2, ",", ".");
}

function convert_CnpjCpf($value)
{
    $cnpj_cpf = preg_replace("/\D/", '', $value);
    
    if (strlen($cnpj_cpf) === 11) {
        return preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cnpj_cpf);
    } 
    
    return preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
}

function formataTelefone($numero){
    if(strlen($numero) == 11){
        $novo = substr_replace($numero, '(', 0, 0);
        $novo = substr_replace($novo, '-', 8, 0);
        $novo = substr_replace($novo, ') ', 3, 0);
    }else{
        $novo = substr_replace($numero, '(', 0, 0);
        $novo = substr_replace($novo, '-', 7, 0);
        $novo = substr_replace($novo, ') ', 3, 0);
    }
    return $novo;
}

function geraToken($tamanho = 10, $maiusculas = true, $numeros = true, $simbolos = false)
{
    // Caracteres de cada tipo
    $lmin = 'abcdefghijklmnopqrstuvwxyz';
    $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $num = '1234567890';
    $simb = '!@#$%*-';

    // Variáveis internas
    $retorno = '';
    $caracteres = '';

    // Agrupamos todos os caracteres que poderão ser utilizados
    $caracteres .= $lmin;
    if ($maiusculas) $caracteres .= $lmai;
    if ($numeros) $caracteres .= $num;
    if ($simbolos) $caracteres .= $simb;

    // Calculamos o total de caracteres possíveis
    $len = strlen($caracteres);

    for ($n = 1; $n <= $tamanho; $n++) {
    // Criamos um número aleatório de 1 até $len para pegar um dos caracteres
    $rand = mt_rand(1, $len);
    // Concatenamos um dos caracteres na variável $retorno
    $retorno .= $caracteres[$rand-1];
    }

    return $retorno;
}


function trataNumero($tel) {
 echo strlen($tel).'<br>';
 // seria melhor cirar uma white list.
 // tratando manualmente
 $tel = str_replace("-", "", $tel);
 $tel = str_replace("(", "", $tel);
 $tel = str_replace(")", "", $tel);
 $tel = str_replace("_", "", $tel);
 $tel = str_replace(" ", "", $tel);
 $tel = str_replace("+", "", $tel);
 //---------------------

 // Se nao tiver DDD e 9 digito
 if (strlen($tel) == 8) {

  $tel = '9'.$tel;

 };

 // Se nao tiver DDD
 if (strlen($tel) == 9) {

  $tel = '11'.$tel;

 };

 // Se tiver DDD mas nao tiver o 9 digito
 if (strlen($tel) == 10) {

  $inicio = substr($tel, 0, 2);
  $fim =  substr($tel, 2, 10);
  $tel = $inicio.'9'.$fim;

 };

 //verificando se é celular
 $celReal = array ("9","8","7","6","5","4");

 // retirando espaços
 $tel = trim($tel);

 // Valida se esta com 55
 $ddi = strripos($tel, '55');
 $val_ddi = strlen($ddi);

 if ($val_ddi != 1) {

        $tel = '55'.$tel;

 }

 // Verifica se e celular mesmo
 if (strlen($tel) == 13) {

        $validaCel = substr($tel,5,1);
        if (in_array($validaCel, $celReal)){

                return $tel;

        } else {

                return false;

        }
 }

return trataNumero($tel);
}
