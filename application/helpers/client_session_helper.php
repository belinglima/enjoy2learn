<?php 

function set_user_session($data='')
{
	$CI = & get_instance();
	$CI->load->library('session');
    
	$CI->session->set_userdata($data);
}

function get_user_session()
{
	$CI = & get_instance();
	$CI->load->library('session');
    
    return $CI->session->userdata('id');
}

function get_user_nome()
{
	$CI = & get_instance();
	$CI->load->library('session');
    
    return $CI->session->userdata('nome');
}

function set_system_user_session($data='')
{
	$CI = & get_instance();
	$CI->load->library('session');
    
    $CI->session->set_userdata($data);
}

function get_system_user_session()
{
	$CI = & get_instance();
	$CI->load->library('session');
    
    return $CI->session->userdata('id');
}

function get_nivel()
{
	$CI = & get_instance();
	$CI->load->library('session');
	
	return $CI->session->userdata('nivel');
}

function set_nivel($nivel='')
{
	$CI = & get_instance();
	$CI->load->library('session');
	$CI->session->set_userdata(
		[
			'nivel' => $nivel
		]
	);
}

function get_foto()
{
	$CI = & get_instance();
	$CI->load->library('session');
	
	return $CI->session->userdata('foto');
}

function set_foto($foto='')
{
	$CI = & get_instance();
	$CI->load->library('session');
	$CI->session->set_userdata(
		[
			'foto' => $foto
		]
	);
}