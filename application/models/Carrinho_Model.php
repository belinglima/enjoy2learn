<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carrinho_Model extends CI_Model
{
    protected $aluno_id;
    protected $status = [
        0 => 'AGUARDANDO', 
        1 => 'EFETIVADA', 
        2 => 'CANCELADA'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->aluno_id = get_user_session();
    }

    public function getVendasAguardando()
    {
        $this->db->select('a.total, a.id_venda, c.id , c.nome, c.foto, c.preco, c.descricao')
            ->from('vendas as a')
            ->join('vendas_curso AS b', 'a.id_venda = b.venda_id', 'left')
            ->join('cursos AS c', 'c.id = b.curso_id', 'left')
            ->where('a.status', $this->status[0])
            ->where('a.aluno_id', $this->aluno_id);

        return $this->db->get();
    }

    public function verificaExistencia($venda_id, $curso_id)
    {
        $this->db->select('*')
            ->from('vendas_curso')
            ->where('venda_id', $venda_id)
            ->where('curso_id', $curso_id);

        return $this->db->get()->num_rows();
    }

    public function insereAtividade($venda_id, $data)
    {
        $curso['venda_id'] = $venda_id;
        $curso['curso_id'] = $data['id'];
        $curso['valor'] = $data['preco'];

        $this->db->insert('vendas_curso', $curso);
        return ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();
    }

    public function atualizaTotal($id, $total)
    {
        $this->db->where('id_venda', $id);
        return $this->db->update('vendas', array('total' => $total));
    }

    public function verificaCodigo()
    {
        $this->db->select('b.porcentagem')
            ->from('vendas as a')
            ->join('cupom AS b', 'a.cupom_id = b.id')
            ->where('a.status', $this->status[0])
            ->where('a.aluno_id', $this->aluno_id);

        return $this->db->get();
    }

    // public function updateStatusVenda($id_venda, $status)
    // {
    //     $this->db->where('id_venda', $id_venda);
    //     return $this->db->update('vendas', array('status' => $status));
    // }

    public function getCursosVenda($venda_id)
    {
        $this->db->select(
            'a.codigo AS idVenda, 
            b.valor, 
            b.id, 
            c.*')
            ->from('vendas AS a')
            ->join('vendas_curso AS b', 'a.id_venda = b.venda_id')
            ->join('cursos AS c', 'b.curso_id = c.id')
            ->where('a.id_venda', $venda_id)
            ->where('a.aluno_id', $this->aluno_id);

        return $this->db->get();
    }

    // public function getVendaByPdfId($id)
    // {
    //     $this->db->select('a.*, b.valor')
    //         ->from('vendas AS a')
    //         ->join('vendas_pdf AS b', 'b.venda_id = a.id_venda')
    //         ->where('b.id', $id);

    //     return $this->db->get();
    // }
    
    public function getIdByCodigo($codigo)
    {
        $this->db->select('id_venda')
            ->from('vendas')
            ->where('codigo', $codigo)
            ->where('aluno_id', $this->aluno_id);
        
        return $this->db->get()->row()->id_venda;
    }

    // public function minhasAtividades()
    // {
    //     $this->db->select('a.codigo AS venda, d.*, e.*')
    //         ->from('vendas AS a')
    //         ->join('vendas_pdf AS b', 'a.id_venda = b.venda_id')
    //         ->join('cielo AS c', 'b.venda_id = c.venda_id')
    //         ->join('pdfs AS d', 'b.pdf_id = d.id_pdf')
    //         ->join('categorias AS e', 'd.categoria_id = e.id_categoria', 'LEFT')
    //         ->where('a.cliente_id', $this->cliente_id)
    //         ->where('c.payment_status', 2);

    //     return $this->db->get();
    // }

    // public function removeAtividade($id)
    // {
    //     $this->db->where('id', $id);
    //     return $this->db->delete('vendas_pdf');
    // }

    // public function delete($id)
    // {
    //     $this->db->where('id_venda', $id);
    //     $this->db->where('cliente_id', $this->cliente_id);

    //     if ($this->db->delete('vendas')) 
    //     {
    //         $this->db->where('venda_id', $id);
    //         $this->db->delete('cielo');

    //         $this->db->where('venda_id', $id);
    //         $this->db->delete('vendas_pdf');

    //         return true;
    //     }

    //     return false;
    // }

}