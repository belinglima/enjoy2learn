<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contato_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function save($data)
    {
        $this->db->insert('contato', $data);
        return ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();
    }
}
