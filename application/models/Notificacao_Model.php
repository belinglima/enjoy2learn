<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificacao_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function all()
    {
        $this->db->select('*');
        $this->db->from('notificacoes');
        $this->db->where('data >=', date('Y-m-d H:i:s'));
        $this->db->order_by('id', 'DESC');    
                  
        return $this->db->get();
    }

    public function pegaUrlNotificacao($id)
    {
        $this->db->select('url');
        $this->db->from('notificacoes');
        $this->db->where('id', $id);

        return $this->db->get()->row()->url;
    }
}