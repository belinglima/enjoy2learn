<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cursos_Model extends CI_Model
{
    protected $aluno_id;
    protected $status = [
      0 => 'AGUARDANDO', 
      1 => 'EFETIVADA', 
      2 => 'CANCELADA'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->aluno_id = get_user_session();
    }

    public function all($num)
    {
        $this->db->select('a.*');
        $this->db->from('cursos as a');

        // todos os cursos sem distinção de usuario
        if($num == 0){
          $this->db->where('status', 'ATIVO'); 
        } 

        // meus cursos que estou cursando aqueles 
        // que eu ja paguei e que ainda nao foram 
        // concluidos e eu ainda nao peguei o certificado
        if($num == 1){ 
            $this->db->join('vendas_curso as b','b.curso_id = a.id');
            $this->db->join('vendas as c','c.id_venda = b.venda_id');
            $this->db->where('b.start', 'INICIADO');
            $this->db->where('c.status', 'EFETIVADA');
        } 

        // meu cursos que eu ja paguei 
        // e que au ja peguei o certificado
        if($num == 2){ 
            $this->db->join('vendas_curso as b','b.curso_id = a.id');
            $this->db->join('vendas as c','c.id_venda = b.venda_id');
            $this->db->where('b.start', 'FINALIZADO');
            $this->db->where('c.status', 'EFETIVADA');
        }
                  
        return $this->db->get();
    }

    public function find($id)
    {
        $this->db->select('*');
        $this->db->from('cursos');
        $this->db->where('id', $id);
                
        return $this->db->get();
    }

    public function aulasCurso($idCurso)
    {
        $this->db->select('*');
        $this->db->from('aulas');
        $this->db->where('id_curso', $idCurso);
                
        return $this->db->get();   
    }

    public function find_by_code($id)
    {
        $this->db->select('*')
            ->from('cursos')
            ->where('id', $id);
        
        return $this->db->get();
    }

    public function getVendasAguardando()
    {
        $this->db->select('b.curso_id')
            ->from('vendas as a')
            ->join('vendas_curso AS b', 'a.id_venda = b.venda_id')
            ->where('status', $this->status[0])
            ->where('aluno_id', $this->aluno_id);

        return $this->db->get();
    }

    public function search($data)
    {
        $this->db->select('*');
        $this->db->from('cursos');
        $this->db->like('nome', $data);
                
        return $this->db->get(); 
    }

    public function duvidas($num)
    {
        $this->db->select('d.*');
        $this->db->from('duvidas as d');
        
        if($num === '1'){
            $this->db->where('d.tipo', 'GERAL');
        }

        if($num === '2'){
            $this->db->where('d.tipo', 'ALUNOS');
        }

        if($num === '3'){
            $this->db->where('d.tipo', 'PAGAMENTOS');
        }

        return $this->db->get();
    }

    public function verificaAvalicao($id)
    {
        $this->db->select('*');
        $this->db->from('avaliacao');
        $this->db->where('id_curso', $id);

        if($this->db->get()->num_rows() > 0){
            return 'true';
        } 

        return 'false';
    }

    public function saveEvaluation($dados)
    {
        $data['id_curso'] = $dados['hash'];
        $data['id_aluno'] = $this->aluno_id;
        $data['conteudo'] = $dados['conteudo'];
        $data['audio'] = $dados['audio'];
        $data['video'] = $dados['video'];
        $data['material'] = $dados['material'];
        $data['dinamica'] = $dados['dinamica'];
        $data['dificuldade'] = $dados['dificuldade'];
        $data['professor'] = $dados['professor'];
        $data['nota'] = $dados['nota'];

        if($dados['obs']){
            $data['obs'] = $dados['obs'];
        }

        $this->db->insert('avaliacao', $data);
        return ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();
    }

    public function getAvaliados()
    {
        $this->db->select('id_curso as idCurso');
        $this->db->from('avaliacao');

        return $this->db->get();
    }

    public function getCertificado($id)
    {
        $this->db->select('e.foto, d.nome, f.nome as nomeAluno, f.email, f.id as idUsuario, b.id as idCursoVendas, d.id as idCurso');
        $this->db->from('vendas AS a');
        $this->db->join('vendas_curso AS b', 'a.id_venda = b.venda_id');
        $this->db->join('cielo AS c', 'a.id_venda = c.venda_id');
        $this->db->join('cursos AS d', 'd.id = b.curso_id');
        $this->db->join('certificados AS e', 'e.id = d.id_certificado');
        $this->db->join('usuarios as f','a.aluno_id = f.id');

        $this->db->where('a.aluno_id', $this->aluno_id);
        $this->db->where('c.payment_status', '2');
        $this->db->where('b.start', 'FINALIZADO');
        $this->db->where('d.id', $id);

        return $this->db->get();
    }

    public function getMaterialApoio($idCurso, $idAula)
    {
        $this->db->select('a.*');
        $this->db->from('material_apoio as a');
        $this->db->where('a.status', 'SIM');
        $this->db->where('a.id_aula', $idAula);
        $this->db->where('a.id_curso', $idCurso);

        return $this->db->get();
    }

    public function getAulas($idCurso)
    {
        $this->db->select('a.*, b.aula_id as idAula');
        $this->db->from('aulas as a');
        $this->db->join('usuario_curso_aula as b','a.id = b.aula_id', 'left');
        $this->db->where('a.id_curso', $idCurso);
        
        return $this->db->get();
    }

    public function verificaVisualizacao($idAula, $idCurso)
    {
        $this->db->select('*');
        $this->db->from('usuario_curso_aula');
        $this->db->where('aula_id', $idAula);
        $this->db->where('curso_id', $idCurso);
        $this->db->where('usuario_id', $this->aluno_id);

        return $this->db->get()->num_rows();
    }

    public function saveVisualizacao($idAula, $idCurso)
    {
        if($this->verificaVisualizacao($idAula, $idCurso) == 0){
            $data['usuario_id'] = $this->aluno_id;
            $data['curso_id'] = $idCurso;
            $data['aula_id'] = $idAula;
            $data['data'] = date('Y-m-d');

            $this->db->insert('usuario_curso_aula', $data);
            return ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();
        } 
    }
}