<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendas_Model extends CI_Model
{
    protected $aluno_id;
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->aluno_id = get_user_session();
    }

    public function allFrom()
    {
        $this->db->select('d.*, a.status, b.curso_id, a.codigo, c.payment_status');
        $this->db->from('vendas AS a');
        $this->db->join('vendas_curso AS b', 'a.id_venda = b.venda_id');
        $this->db->join('cielo AS c', 'a.id_venda = c.venda_id');
        $this->db->join('cursos AS d', 'd.id = b.curso_id');
        $this->db->where('a.aluno_id', $this->aluno_id);
        $this->db->where('a.status', 'EFETIVADA');
        $this->db->order_by('b.id', 'ASC');

        return $this->db->get();
    }

    public function all()
    {
        $this->db->select('a.id_venda, a.data, a.total, c.payment_status, c.venda_id as IdTransaction, d.nome, d.descricao');
        $this->db->from('vendas AS a');
        $this->db->join('vendas_curso AS b', 'a.id_venda = b.venda_id');
        $this->db->join('cielo AS c', 'a.id_venda = c.venda_id');
        $this->db->join('cursos AS d', 'd.id = b.curso_id');
        $this->db->where('a.aluno_id', $this->aluno_id);
        $this->db->group_by('a.id_venda');

        return $this->db->get();
    }

    public function allMovimentos()
    {
        $this->db->select('a.id_venda, a.data, a.total as totalGeral, d.preco as total, c.payment_status as situacao, c.venda_id as IdTransaction, d.nome, d.descricao');
        $this->db->from('vendas AS a');
        $this->db->join('vendas_curso AS b', 'a.id_venda = b.venda_id');
        $this->db->join('cielo AS c', 'a.id_venda = c.venda_id');
        $this->db->join('cursos AS d', 'd.id = b.curso_id');
        $this->db->where('a.aluno_id', $this->aluno_id);
        // $this->db->group_by('a.id_venda');

        return $this->db->get();
    }

    // public function findEbookInLibrary($codigo, $cliente_id)
    // {
    //     $this->db->select('a.*')
    //         ->from('vendas AS a')
    //         ->join('pdfs AS b', 'a.pdf_id = b.id_pdf')
    //         ->where('b.codigo', $codigo)
    //         ->where('a.cliente_id', $cliente_id);
        
    //     return $this->db->get()->num_rows();
    // }

    // public function getVenda($codigo)
    // {
    //     $this->db->select('a.*, b.*, c.cupom, c.porcentagem');
    //     $this->db->from('vendas as a');
    //     $this->db->join('vendas_curso AS b', 'a.id_venda = b.venda_id');
    //     $this->db->join('cupom as c','c.id = a.cupom_id', 'left');
    //     $this->db->where('a.codigo', $codigo);
    //     $this->db->where('a.aluno_id', $this->aluno_id);
        
    //     return $this->db->get();
    // }

    // public function download($codigo)
    // {
    //     $pdf = $this->Pdf_Model->meuEbook($codigo, $this->cliente_id)->row_array();

    //     if (! $pdf['pdf']) {
    //         return $this->redirect->url('portal/welcome')
    //             ->withError('Atividade não encontrada!')
    //             ->send();
    //     }

    //     $DOCUMENT_ROOT = "./assets/uploads/".$pdf['pdf'];

    //     if (file_exists($DOCUMENT_ROOT)) {
    //         header('Content-Description: File Transfer');
    //         header('Content-Type: application/octet-stream');
    //         header("Content-Type: application/force-download");
    //         header('Content-Disposition: attachment; filename=' . urlencode(basename($DOCUMENT_ROOT)));
    //         header('Expires: 0');
    //         header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    //         header('Pragma: public');
    //         header('Content-Length: ' . filesize($DOCUMENT_ROOT));

    //         ob_clean();
    //         flush();

    //         readfile($DOCUMENT_ROOT);

    //         exit;
    //     }
    // }

    ////////// FUNÇÕES DO SISTEMA //////////

    // public function filter($data)
    // {
    //     $this->db->select('a.*, b.nome, c.payment_status')
    //         ->from('vendas AS a')
    //         ->join('clientes AS b', 'a.cliente_id = b.id_cliente')
    //         ->join('cielo AS c', 'a.id_venda = c.venda_id', 'LEFT');;

    //     if ($data['datai']) {
    //         $this->db->where('a.data >=', format_date_to_bd($data['datai']));
    //     }

    //     if ($data['dataf']) {
    //         $this->db->where('a.data <=', format_date_to_bd($data['dataf']));
    //     }

    //     if ($data['status']) {
    //         $this->db->where('c.payment_status', $data['status']);
    //     }
        
    //     return $this->db->get();
    // }

    public function find($id)
    {
        $this->db->select('a.*, b.nome')
            ->from('vendas AS a')
            ->join('usuarios AS b', 'a.aluno_id = b.id', 'left')
            ->where('a.id_venda', $id);
        
        return $this->db->get();
    }

    public function cursos()
    {
        $this->db->select('v.*');
        $this->db->from('vendas AS v');
        $this->db->join('cielo AS c', 'c.venda_id = v.id_venda');
        $this->db->where('v.aluno_id', $this->aluno_id);
        $this->db->where('c.payment_status', 2);
        
        return $this->db->get();
    }

    public function aulas()
    {
        $this->db->select('a.*');
        $this->db->from('aulas AS a');
        $this->db->join('cursos AS c', 'c.id = a.id_curso');
        $this->db->join('vendas_curso AS vc', 'vc.curso_id = c.id');
        $this->db->join('vendas AS v', 'v.id_venda = vc.venda_id');
        $this->db->join('cielo AS ci', 'ci.venda_id = v.id_venda');
        $this->db->where('v.aluno_id', $this->aluno_id);
        $this->db->where('ci.payment_status', 2);
                
        return $this->db->get();
    }

    public function save($data)
    {
        $this->db->insert('vendas', $data);
        return ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();
    }

    // public function update($id = '', $data)
    // {
    //     $this->db->where('id_venda', $id);
    //     return $this->db->update('vendas', $data);
    // }

    public function deleteCurso($id)
    {
        $this->db->where('curso_id', $id);
        $this->db->delete('vendas_curso');
        
        if ($this->db->affected_rows() > 0){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function deletaVenda($idVenda)
    {
        $this->db->where('id_venda', $idVenda);
        $this->db->delete('vendas');
        
        if ($this->db->affected_rows() > 0){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function verificaCursos($idVenda)
    {
        $this->db->select('*')
            ->from('vendas_curso')
            ->where('venda_id', $idVenda);

        return $this->db->get();
    }

    public function verificaCupom($cupom)
    {
        $this->db->select('validade, id');
        $this->db->from('cupom');
        $this->db->where('cupom', $cupom);

        return $this->db->get();
    }

    public function insereCupom($idCupom, $idVenda)
    {
        $data['cupom_id'] = $idCupom;
        $this->db->where('id_venda', $idVenda);
        return $this->db->update('vendas', $data);
    }

    // public function getCursosVenda($venda_id)
    // {
    //     $this->db->select('c.*, a.*')
    //         ->from('vendas AS a')
    //         ->join('vendas_curso AS b', 'a.id_venda = b.venda_id')
    //         ->join('cursos AS c', 'b.curso_id = c.id')
    //         ->where('a.id_venda', $venda_id);

    //     return $this->db->get();
    // }

    // public function removePedido($id)
    // {
    //     $this->db->where('id_venda', $id);

    //     if ($this->db->delete('vendas')) 
    //     {
    //         $this->db->where('venda_id', $id);
    //         $this->db->delete('cielo');

    //         $this->db->where('venda_id', $id);
    //         $this->db->delete('vendas_pdf');

    //         return true;
    //     }

    //     return false;
    // }

}