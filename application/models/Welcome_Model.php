<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function todasglobal($nome = NULL)
    {
        $this->db->select('*');
        $this->db->from('cargas AS c');
        $this->db->join('empresas AS e', 'c.empresa_id = e.id');

        if (isset($nome)) {
        	if ($nome == 'ATIVAS') {
                $this->db->where('c.status', 'PUBLICA');
        		$this->db->where('c.statusCarga', '0');
               
        	}

	        if ($nome == 'TRANSPORTE') {
	        	$this->db->where('c.status', 'TRANSPORTE');
	        	$this->db->where('c.statusCarga', '0');
	        }

	        if ($nome == 'FINALIZADA') {
	        	$this->db->where('c.status', 'FINALIZADA');
	            $this->db->where('c.statusCarga', '0');
	        }
        }
        
        $this->db->where('e.id', get_empresa_id());
        $this->db->order_by('c.codigo', 'DESC');

        return $this->db->get();
    }

    public function veiculosEmpresa() {
        $this->db->select('*');
        $this->db->from('veiculos as v ');
        $this->db->join('empresas AS e', 'v.empresa_id = e.id');
        //$this->db->where('e.id', get_empresa_id());
       
        return $this->db->get();
    }
}