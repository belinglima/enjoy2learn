<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes_Model extends CI_Model
{
    protected $empresa_id;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->empresa_id = get_empresa_id();
    }

    public function all($start = 0, $data = '')
    {
        $this->db->select('cli.*, est.id AS idEstado, est.sigla, est.descricao');
        $this->db->from('clientes AS cli');
        $this->db->join('estados AS est', 'est.id = cli.estado_id', 'LEFT');
        if (isset($data)) {
            if (isset($data['buscar'])) {
                $this->db->like('cli.nome', $data['buscar']);
                $this->db->or_like('cli.cpfcnpj', $data['buscar']);
            }
        }
        $this->db->where('empresa_id', $this->empresa_id);  
        $this->db->limit(20, $start);
        
        return $this->db->get();
    }

    public function countAll()
    {
        $this->db->select('cli.*, est.id AS idEstado, est.sigla, est.descricao');
        $this->db->from('clientes AS cli');
        $this->db->join('estados AS est', 'est.id = cli.estado_id', 'LEFT');
        $this->db->where('empresa_id', $this->empresa_id);  
                
        return $this->db->get();
    }

    public function allSemEmpresa()
    {
        $this->db->select('cli.*, est.id AS idEstado, est.sigla, est.descricao');
        $this->db->from('clientes AS cli');
        $this->db->join('estados AS est', 'est.id = cli.estado_id', 'LEFT');
                    
        return $this->db->get();
    }

    public function find($id, $diferente = '')
    {
        $this->db->select('*');
        $this->db->from('clientes');

        if ($diferente) {
            $this->db->where('id !=', $id);
        } else {
            $this->db->where('id', $id);
        }
        
        $this->db->where('empresa_id', $this->empresa_id);
        
        return $this->db->get();
    }

    public function buscaCNPJ($id) 
    {
        $this->db->select('cli.cpfcnpj');
        $this->db->from('clientes AS cli');
        $this->db->where('cli.id', $id);

        $data = $this->db->get()->row()->cpfcnpj;

        if ($data) {
            return $data;
        }

        return false;
    }

    public function findCpfCnpj($cpfCnpj, $id = '')
    {
        $this->db->select("c.id, c.cpfcnpj");
        $this->db->from('clientes AS c');
        $this->db->where('c.cpfcnpj', $cpfCnpj);
        $this->db->where('c.empresa_id', get_empresa_id());

        if ($id) {
            $this->db->where('c.id !=', $id);            
        }

        $result = $this->db->get();

        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
    }

    public function contatos($id)
    {
        $this->db->select('*');
        $this->db->from('contato_clientes AS cc');
        $this->db->where('cc.id_cliente', $id);
        $this->db->order_by('cc.id', 'ASC');
          
        return $this->db->get();
    }

    public function whatsEmbarcador($id)
    {
        $this->db->select('*');
        $this->db->from('contato_clientes AS cc');
        $this->db->where('cc.tipo', 'WHATSAPP');
        $this->db->where('cc.view', '1');
        $this->db->where('cc.id_cliente', $id);
        $this->db->limit(1);

        return $this->db->get();
    }

    public function whatsDestinatario($id)
    {
        $this->db->select('*');
        $this->db->from('contato_clientes AS cc');
        $this->db->where('cc.tipo', 'WHATSAPP');
        $this->db->where('cc.view', '1');
        $this->db->where('cc.id_cliente', $id);
        $this->db->limit(1);        
               
        return $this->db->get();
    }

    public function save($data)
    {
        $data['empresa_id'] = $this->empresa_id;
        $this->db->insert('clientes', $data);
        return ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();
    }

    public function saveContato($id, $contato)
    {
        for ($i = 0; $i < count($contato); $i++) {
            if( $contato['nome'][$i] != "" ){
                $data['id_cliente'] = $id;
                $data['nome'] = $contato['nome'][$i];
                $data['telefone'] = $contato['telefone'][$i];
                $data['tipo'] = $contato['tipo'][$i];
                $data['view'] = $contato['view'][$i];
                
                $this->db->insert('contato_clientes', $data);
            }
        }

        return ( !$this->db->affected_rows() ) ? false : true;
    }

    public function update($id = '', $data)
    {
        $this->db->where('id', $id);
        $this->db->where('empresa_id', $this->empresa_id);
        return $this->db->update('clientes', $data);
    }

    public function updateContato($id, $contato)
    {
        for ($i = 0; $i < count($contato); $i++) {
            if( $contato['nome'][$i] != "" ){
                $this->db->set('id_cliente', $id);
                $this->db->set('nome', $contato['nome'][$i]);
                $this->db->set('telefone', $contato['telefone'][$i]);
                $this->db->set('tipo', $contato['tipo'][$i]);
                $this->db->set('view', $contato['view'][$i]);
                $this->db->insert('contato_clientes');
            }
        }

        return ( !$this->db->affected_rows() ) ? false : true;
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->where('empresa_id', $this->empresa_id);
        return $this->db->delete('clientes');
    }

    public function deleteContato($id)
    {
        $this->db->where('id_cliente', $id);
        return $this->db->delete('contato_clientes');
    }

    public function deleteCarga($id)
    {
        $this->db->where('embarcador_id', $id);
        $this->db->where('status !=', 'TRANSPORTE');
        return $this->db->delete('cargas');
    }

    public function pegaNome($id)
    {
        $this->db->select('nome');
        $this->db->from('clientes');
        $this->db->where('id', $id);

        return $this->db->get();
    }

    public function carregaClientes($nome)
    {
        $this->db->select('id, nome');
        $this->db->from('clientes');
        $this->db->like('nome', $nome);
        $this->db->where('empresa_id', $this->empresa_id);

        return $this->db->get();
    }

}