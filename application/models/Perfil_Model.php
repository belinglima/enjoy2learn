<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil_Model extends CI_Model
{
    protected $aluno_id;
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->aluno_id = get_user_session();
    }

    public function update($data)
    {
        $this->db->where('id', $this->aluno_id);
        return $this->db->update('usuarios', $data);
    }

    public function estadosAll()
    {
        $this->db->select('*');
        $this->db->from('estados');
        $this->db->order_by('sigla', 'ASC');
        
        return $this->db->get();
    }

    public function getMunicipios($sigla)
    {
        $this->db->select('m.nome, m.codigo_uf, m.codigo_ibge, e.sigla');
        $this->db->from('municipios AS m');
        $this->db->join('estados AS e', 'm.codigo_uf = e.id');
        $this->db->where('e.sigla', $sigla);
        $this->db->order_by('m.nome', 'ASC');
        $this->db->group_by('m.codigo_ibge');

        return $this->db->get();
    }

    public function findUser()
    {
        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where('id', $this->aluno_id);

        return $this->db->get();
    }

    public function getCursos()
    {
        $this->db->select('d.id_certificado, d.id as idCurso, d.foto, d.nome');
        $this->db->from('vendas AS a');
        $this->db->join('vendas_curso AS b', 'a.id_venda = b.venda_id');
        $this->db->join('cielo AS c', 'a.id_venda = c.venda_id');
        $this->db->join('cursos AS d', 'd.id = b.curso_id');
        // $this->db->join('certificados AS e', 'e.id = d.id_certificado');
        $this->db->where('a.aluno_id', $this->aluno_id);
        $this->db->where('c.payment_status', '2');
        $this->db->where('b.start', 'FINALIZADO');

        return $this->db->get();
    }
}