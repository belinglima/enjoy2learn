<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Certificados_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function all($start = 0, $data = '')
    {
        $this->db->select('c.*');
        $this->db->from('certificados AS c');
        if (isset($data)) {
            if (isset($data['buscar'])) {
                $this->db->like('c.nome', $data['buscar']);
            }    
        }    
        $this->db->limit(10, $start);
                  
        return $this->db->get();
    }

    public function countAll()
    {
        $this->db->select('c.*');
        $this->db->from('certificados AS c');
                        
        return $this->db->get();
    }

    public function find($id)
    {
        $this->db->select('*');
        $this->db->from('certificados');
        $this->db->where('id', $id);
                
        return $this->db->get();
    }

    public function save($data)
    {
        $data['data'] = date('Y-m-d H:i:s');
        $this->db->insert('certificados', $data);
        return ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();
    }

    public function update($id = '', $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('certificados', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('certificados');
    }

    public function delete_imagem($id)
    {
        $data['foto'] = null;
        $this->db->where('id', $id);
        return $this->db->update('certificados', $data);
    }

   public function totalRegistros()
    {
        $this->db->select('*');
        $this->db->from('certificados');
        
        return $this->db->get()->num_rows();
    }

}