<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendas_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function all($start = 0)
    {
        $this->db->select('v.*, u.nome, c.payment_status');
        $this->db->from('vendas AS v');
        $this->db->join('usuarios AS u', 'v.aluno_id = u.id');
        $this->db->join('cielo AS c', 'v.id_venda = c.venda_id', 'LEFT');
        
        $this->db->limit(10, $start);
                  
        return $this->db->get();
    }

    public function countAll()
    {
        $this->db->select('v.*, u.nome, c.payment_status');
        $this->db->from('vendas AS v');
        $this->db->join('usuarios AS u', 'v.aluno_id = u.id');
        $this->db->join('cielo AS c', 'v.id_venda = c.venda_id', 'LEFT');
                        
        return $this->db->get();
    }
    
    public function totalRegistros()
    {
        $this->db->select('v.*, u.nome, c.payment_status');
        $this->db->from('vendas AS v');
        $this->db->join('usuarios AS u', 'v.aluno_id = u.id');
        $this->db->join('cielo AS c', 'v.id_venda = c.venda_id', 'LEFT');
        
        return $this->db->get()->num_rows();
    }
    
    public function findEbookInLibrary($codigo, $cliente_id)
    {
        $this->db->select('a.*')
            ->from('vendas AS a')
            ->join('pdfs AS b', 'a.pdf_id = b.id_pdf')
            ->where('b.codigo', $codigo)
            ->where('a.cliente_id', $cliente_id);
        
        return $this->db->get()->num_rows();
    }

    public function getVenda($id)
    {
        $this->db->select('*')
            ->from('vendas')
            ->where('id_venda', $id)
            ->where('cliente_id', $this->cliente_id);
        
        return $this->db->get();
    }

    public function getRelatorioVendas($dados)
    {

        $this->db->select('v.*, u.nome, c.payment_status');
        $this->db->from('vendas AS v');
        $this->db->join('usuarios AS u', 'v.aluno_id = u.id');
        $this->db->join('cielo AS c', 'v.id_venda = c.venda_id','LEFT');

        if ($dados['datai']) {
            $this->db->where('v.data >=', format_date_to_bd($dados['datai']));
        }

        if ($dados['dataf']) {
            $this->db->where('v.data <=', format_date_to_bd($dados['dataf']));
        }

        if ($dados['status']) {
            $this->db->where('c.payment_status', $dados['status']);
        }
        
        $this->db->limit(900);

        return $this->db->get();
    }

    public function find($id)
    {
        $this->db->select('a.*, b.nome')
            ->from('vendas AS a')
            ->join('clientes AS b', 'a.cliente_id = b.id_cliente')
            ->where('a.id_venda', $id);
        
        return $this->db->get();
    }

    public function save($data)
    {
        $this->db->insert('vendas', $data);
        return ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();
    }

    public function update($id = '', $data)
    {
        $this->db->where('id_venda', $id);
        return $this->db->update('vendas', $data);
    }

    public function delete($id)
    {
        $this->db->where('id_venda', $id);
        return $this->db->delete('vendas');
    }

    public function getPdfsVenda($venda_id)
    {
        $this->db->select(
            'a.codigo AS idVenda, 
            b.valor, 
            b.id, 
            c.titulo, 
            c.capa, 
            c.codigo, 
            c.pdf, 
            c.link_youtube, 
            d.categoria, 
            d.cor')
            ->from('vendas AS a')
            ->join('vendas_pdf AS b', 'a.id_venda = b.venda_id')
            ->join('pdfs AS c', 'b.pdf_id = c.id_pdf')
            ->join('categorias AS d', 'c.categoria_id = d.id_categoria', 'LEFT')
            ->where('a.id_venda', $venda_id);

        return $this->db->get();
    }

    public function removePedido($id)
    {
        $this->db->where('id_venda', $id);

        if ($this->db->delete('vendas')) 
        {
            $this->db->where('venda_id', $id);
            $this->db->delete('cielo');

            $this->db->where('venda_id', $id);
            $this->db->delete('vendas_pdf');

            return true;
        }

        return false;
    }

}
