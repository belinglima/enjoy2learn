<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contatos_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function all($start = 0, $data = '')
    {
        $this->db->select('c.*');
        $this->db->from('contato AS c');
        if (isset($data)) {
            if (isset($data['buscar'])) {
                $this->db->like('c.msg', $data['buscar']);
            }    
        }    
        $this->db->limit(10, $start);
                  
        return $this->db->get();
    }

    public function countAll()
    {
        $this->db->select('c.*');
        $this->db->from('contato AS c');
                        
        return $this->db->get();
    }

    public function getMensagem($id)
    {
        $this->db->select('*');
        $this->db->from('contato AS c');
        $this->db->where('c.id', $id);
                     
        return $this->db->get();
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('contato');
    }

    public function totalRegistros()
    {
        $this->db->select('*');
        $this->db->from('contato AS c');
               
        return $this->db->get()->num_rows();
    }

}