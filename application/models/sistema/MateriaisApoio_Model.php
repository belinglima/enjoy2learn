<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MateriaisApoio_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function all($start = 0, $data = '')
    {
        $this->db->select('ma.*, c.id AS idCurso, c.nome AS curso, a.id AS idAula, a.nome');
        $this->db->from('material_apoio AS ma');
        $this->db->join('cursos AS c', 'c.id = ma.id_curso');
        $this->db->join('aulas AS a', 'a.id = ma.id_aula');
        if (isset($data)) {
            if (isset($data['buscar'])) {
                $this->db->like('a.nome', $data['buscar']);
            }    
        }    
        $this->db->group_by('ma.id');
        $this->db->limit(10, $start);
                  
        return $this->db->get();
    }

    public function countAll()
    {
        $this->db->select('ma.*');
        $this->db->from('material_apoio AS ma');
                        
        return $this->db->get();
    }

    public function find($id)
    {
        $this->db->select('*');
        $this->db->from('material_apoio');
        $this->db->where('id', $id);
                
        return $this->db->get();
    }

    public function save($data)
    {
        $this->db->insert('material_apoio', $data);
        return ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();
    }

    public function update($id = '', $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('material_apoio', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('material_apoio');
    }

    public function delete_imagem($id)
    {
        $data['url_imagem'] = null;
        $this->db->where('id', $id);
        return $this->db->update('material_apoio', $data);
    }

   public function totalRegistros()
    {
        $this->db->select('*');
        $this->db->from('material_apoio');
        
        return $this->db->get()->num_rows();
    }

}