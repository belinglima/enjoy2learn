<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cupons_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function all($start = 0)
    {
        $this->db->select('c.*');
        $this->db->from('cupom AS c');
       
        $this->db->limit(10, $start);
                  
        return $this->db->get();
    }

    public function countAll()
    {
        $this->db->select('c.*');
        $this->db->from('cupom AS c');
                                
        return $this->db->get();
    }

    public function find($id)
    {
        $this->db->select('*');
        $this->db->from('cupom');
        $this->db->where('id', $id);
                
        return $this->db->get();
    }

    public function save($data)
    {
        $this->db->insert('cupom', $data);
        return ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();
    }

    public function update($id = '', $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('cupom', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('cupom');
    }

    public function totalRegistros()
    {
        $this->db->select('*');
        $this->db->from('cupom as c');
             
        return $this->db->get()->num_rows();
    }
}