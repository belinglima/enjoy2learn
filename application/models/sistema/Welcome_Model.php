<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function agendas()
    {
        $this->db->select('a.*');
        $this->db->from('agendas AS a');
                
        return $this->db->get();
    }

    public function alunos()
    {
        $this->db->select('u.*');
        $this->db->from('usuarios AS u');
        $this->db->where('u.nivel', 'Usuario');
        
        return $this->db->get();
    }

    public function aulas()
    {
        $this->db->select('a.*');
        $this->db->from('aulas AS a');
                
        return $this->db->get();
    }

    public function blog()
    {
        $this->db->select('b.*');
        $this->db->from('blog AS b');
                
        return $this->db->get();
    }

    public function cursos()
    {
        $this->db->select('c.*');
        $this->db->from('cursos AS c');
                
        return $this->db->get();
    }

    public function vendas()
    {
        $this->db->select('*')
            ->from('cielo')
            ->where('payment_status', 2);

        return $this->db->get();
    }

    public function duvidas()
    {
        $this->db->select('*')
            ->from('duvidas');

        return $this->db->get();
    }

    public function certificados()
    {
        $this->db->select('*')
            ->from('certificados');

        return $this->db->get();
    }

    public function notificacoes()
    {
        $this->db->select('*')
            ->from('notificacoes');

        return $this->db->get();
    }

    public function contato()
    {
        $this->db->select('*')
            ->from('contato');

        return $this->db->get();
    }

    public function cupom()
    {
        $this->db->select('*')
            ->from('cupom');

        return $this->db->get();
    }
}