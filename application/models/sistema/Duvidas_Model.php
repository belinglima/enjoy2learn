<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Duvidas_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function all($start = 0, $data = '')
    {
        $this->db->select('d.*');
        $this->db->from('duvidas AS d');
        if (isset($data)) {
             if (isset($data['buscar'])) {
                 $this->db->like('d.pergunta', $data['buscar']);
             }    
        }    
        $this->db->limit(10, $start);
                  
        return $this->db->get();
    }

    public function countAll()
    {
        $this->db->select('d.*');
        $this->db->from('duvidas AS d');
                              
        return $this->db->get();
    }

    public function find($id)
    {
        $this->db->select('*');
        $this->db->from('duvidas');
        $this->db->where('id', $id);
                
        return $this->db->get();
    }

    public function save($data)
    {
         $this->db->insert('duvidas', $data);
         return ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();
    }

    public function update($id = '', $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('duvidas', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('duvidas');
    }

    public function totalRegistros()
    {
        $this->db->select('d.*');
        $this->db->from('duvidas AS d');
            
        return $this->db->get()->num_rows();
    }
}