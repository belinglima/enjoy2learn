<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agendas_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function all($start = 0, $data = '')
    {
        $this->db->select('a.*, u.nome');
        $this->db->from('agendas AS a');
        $this->db->join('usuarios AS u', 'u.id = a.aluno_id');
        if (isset($data)) {
            if (isset($data['buscar'])) {
                $this->db->like('u.nome', $data['buscar']);
            }    
        }    
        $this->db->order_by('a.data, a.horainicio','ASC');
        $this->db->limit(10, $start);
                  
        return $this->db->get();
    }

    public function countAll()
    {
        $this->db->select('a.*, u.nome');
        $this->db->from('agendas AS a');
        $this->db->join('usuarios AS u', 'u.id = a.aluno_id');
        $this->db->order_by('a.data, a.horainicio','ASC');
                        
        return $this->db->get();
    }

    public function find($id)
    {
        $this->db->select('a.*, u.nome');
        $this->db->from('agendas AS a');
        $this->db->join('usuarios AS u', 'u.id = a.aluno_id');
        $this->db->where('a.id', $id);
        $this->db->order_by('a.data, a.horainicio','ASC');
                
        return $this->db->get();
    }

    public function save($data)
    {
        $this->db->insert('agendas', $data);
        return ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();
    }

    public function update($id = '', $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('agendas', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('agendas');
    }

    public function verificaAgenda($data)
    {
        $this->db->select('a.data, a.horainicio a, a.horafim');
        $this->db->from('agendas AS a');
        $this->db->where('a.data', $data['data']);
        $this->db->where('a.horainicio >=', $data['horainicio']);
        $this->db->where('a.horafim <=', $data['horafim']);
       
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return true;
        }

        return false;
    }

    public function verificaAgendaAluno($data)
    {
        $this->db->select('a.data, a.horainicio a, a.horafim');
        $this->db->from('agendas AS a');
        $this->db->where('a.data', $data['data']);
        $this->db->where('a.horainicio >=', $data['horainicio']);
        $this->db->where('a.horafim <=', $data['horafim']);
        $this->db->where('a.aluno_id', $data['aluno_id']);

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return true;
        }

        return false;
    }

    public function totalRegistros()
    {
        $this->db->select('a.*, u.nome');
        $this->db->from('agendas AS a');
        $this->db->join('usuarios AS u', 'u.id = a.aluno_id');
        $this->db->order_by('a.data, a.horainicio','ASC');
        
        return $this->db->get()->num_rows();
    }
}