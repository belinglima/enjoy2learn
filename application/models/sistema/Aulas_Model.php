<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aulas_Model extends CI_Model
{
    protected $empresa_id;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        
    }

    public function find($id)
    {
        $this->db->select('*');
        $this->db->from('aulas');        
        $this->db->where('id', $id);
   
        return $this->db->get();
    }

    public function findCurso($id, $start = 0, $data = '') 
    {
        $this->db->select('a.*, c.nome AS nome_curso');
        $this->db->from('aulas AS a');
        $this->db->join('cursos AS c', 'c.id = a.id_curso');  
        if (isset($data)) {
            if (isset($data['buscar'])) {
                $this->db->like('a.nome', $data['buscar']);
                $this->db->or_like('a.descricao', $data['buscar']);
               
            }
        }
        $this->db->where('a.id_curso', $id);
        $this->db->limit(10, $start);
                   
        return $this->db->get();
    }

    public function countFindCurso($id) 
    {
        $this->db->select('a.*, c.nome AS nome_curso');
        $this->db->from('aulas AS a');
        $this->db->join('cursos AS c', 'c.id = a.id_curso');  
        $this->db->where('a.id_curso', $id);
                           
        return $this->db->get();
    }

    public function save($data = null)
    {

        $this->db->insert('aulas', $data);
        $id = ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();

        return $id;
    }

    public function update($id = '', $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('aulas', $data);
    }
    
    public function totalRegistros($id)
    {
        $this->db->select('a.*');
        $this->db->from('aulas AS a');
        $this->db->where('a.id_curso', $id);

        return $this->db->get()->num_rows();
    }

    public function delete($id)
    {
        return $this->db->where('id', $id)->delete('aulas');
    }

    public function deleteAulas($id_curso)
    {
        return $this->db->where('id_curso', $id_curso)->delete('aulas');
    }

}