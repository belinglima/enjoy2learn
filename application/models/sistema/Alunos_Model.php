<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alunos_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function all($start = 0, $data = '')
    {
        $this->db->select('u.*');
        $this->db->from('usuarios AS u');
        $this->db->where('u.nivel', 'Usuario');
        if (isset($data)) {
            if (isset($data['buscar'])) {
                $this->db->like('u.nome', $data['buscar']);
            }    
        }    
        $this->db->limit(10, $start);
                  
        return $this->db->get();
    }

    public function countAll()
    {
        $this->db->select('u.*');
        $this->db->from('usuarios AS u');
        $this->db->where('u.nivel', 'Usuario');
                        
        return $this->db->get();
    }

    public function find($id)
    {
        $this->db->select('*');
        $this->db->from('usuarios AS u');
        $this->db->where('u.nivel', 'Usuario');
        $this->db->where('id', $id);
                
        return $this->db->get();
    }

    public function save($data)
    {
        $this->db->insert('usuarios', $data);
        return ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();
    }

    public function update($id = '', $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('usuarios', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('usuarios');
    }

    public function totalRegistros()
    {
        $this->db->select('*');
        $this->db->from('usuarios AS u');
        $this->db->where('u.nivel', 'Usuario');
        
        return $this->db->get()->num_rows();
    }

    public function getRelatorioAlunos($dados)
    {
        $this->db->select('u.*');
        $this->db->from('usuarios AS u');
        $this->db->where('u.nivel', 'Usuario');

        if ($dados['datai']) {
            $this->db->where('u.criacao >=', format_date_to_bd($dados['datai']));
        }

        if ($dados['dataf']) {
            $this->db->where('u.criacao <=', format_date_to_bd($dados['dataf']));
        }

        $this->db->limit(900);

        return $this->db->get();
    }

}
