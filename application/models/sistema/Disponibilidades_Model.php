<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disponibilidades_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function all($start = 0, $data = '')
    {
        $this->db->select('d.*');
        $this->db->from('disponibilidades AS d');
        if (isset($data)) {
            if (isset($data['buscar'])) {
                $this->db->where('d.dia', $data['buscar']);
            }    
        }    
        $this->db->order_by('d.dia','ASC');
        $this->db->limit(10, $start);
                  
        return $this->db->get();
    }

    public function countAll()
    {
        $this->db->select('d.*');
        $this->db->from('disponibilidades AS d');
        $this->db->order_by('d.dia','ASC');
                        
        return $this->db->get();
    }

    public function find($id)
    {
        $this->db->select('d.*');
        $this->db->from('disponibilidades AS d');
        $this->db->where('d.id', $id);
        $this->db->order_by('d.dia','ASC');
                
        return $this->db->get();
    }

    public function save($data)
    {
        return $this->db->insert_batch('disponibilidades', $data); 
    }

    public function update($id = '', $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('disponibilidades', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('disponibilidades');
    }

    public function totalRegistros()
    {
        $this->db->select('d.*');
        $this->db->from('disponibilidades AS d');
        $this->db->order_by('d.dia, d.horainicio','ASC');
        
        return $this->db->get()->num_rows();
    }
}