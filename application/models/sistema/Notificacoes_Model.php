<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificacoes_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function all($start = 0, $data = '')
    {
        $this->db->select('n.*');
        $this->db->from('notificacoes AS n');
        if (isset($data)) {
            if (isset($data['buscar'])) {
                $this->db->like('n.titulo', $data['buscar']);
            }    
        }    
        $this->db->limit(10, $start);
                  
        return $this->db->get();
    }

    public function countAll()
    {
        $this->db->select('n.*');
        $this->db->from('notificacoes AS n');
                              
        return $this->db->get();
    }

    public function find($id)
    {
        $this->db->select('*');
        $this->db->from('notificacoes');
        $this->db->where('id', $id);
                
        return $this->db->get();
    }

    public function save($data)
    {
        $data['data'] = date('Y-m-d H:i:s', strtotime("+1 day"));

        $this->db->insert('notificacoes', $data);
        return ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();
    }

    public function update($id = '', $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('notificacoes', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('notificacoes');
    }

    public function totalRegistros()
    {
        $this->db->select('*');
        $this->db->from('notificacoes AS n');
            
        return $this->db->get()->num_rows();
    }
}
