<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cursos_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function all($start = 0, $data = '')
    {
        $this->db->select('c.*');
        $this->db->from('cursos AS c');
        if (isset($data)) {
            if (isset($data['buscar'])) {
                $this->db->like('c.nome', $data['buscar']);
                $this->db->or_like('c.descricao', $data['buscar']);
            }    
        }    
        $this->db->limit(10, $start);
                  
        return $this->db->get();
    }

    public function countAll()
    {
        $this->db->select('c.*');
        $this->db->from('cursos AS c');
                        
        return $this->db->get();
    }

    public function find($id)
    {
        $this->db->select('*');
        $this->db->from('cursos');
        $this->db->where('id', $id);
                
        return $this->db->get();
    }

    public function getAulaByCurso($id)
    {
        $this->db->select('a.id, a.nome, c.id AS idCurso');
        $this->db->from('aulas AS a');
        $this->db->join('cursos AS c', 'a.id_curso = c.id');
        $this->db->where('c.id', $id);
        $this->db->order_by('a.nome', 'ASC');
        $this->db->group_by('a.id');

        return $this->db->get();
    }

    public function getRelatorioAvaliacoesCursos($dados)
    {
        $this->db->select('a.id as idAvaliacao, a.nota, count(u.id) AS qtdAlunos');
        $this->db->from('avaliacao  AS a');
        $this->db->join('usuarios AS u', 'a.id_aluno = u.id', 'LEFT');
        $this->db->group_by('a.id');

        if ($dados['id_curso']) {
            $this->db->where('a.id_curso', $dados['id_curso']);
        }
        
        if ($dados['datai']) {
            $this->db->where('a.data >=', format_date_to_bd($dados['datai']));
        }

        if ($dados['dataf']) {
            $this->db->where('a.data <=', format_date_to_bd($dados['dataf']));
        }

        $this->db->limit(900);

        return $this->db->get();
    }

    public function getRelatorioCursosMaisVendidos($dados)
    {
        $this->db->select('count(*) as qtd_vendido, c.id AS idCurso, c.nome');
        $this->db->from('cursos AS c');
        $this->db->join('vendas_curso AS vc', 'vc.curso_id = c.id');
        $this->db->join('vendas AS v', 'v.id_venda = vc.venda_id');
        $this->db->join('cielo AS ci', 'ci.venda_id = v.id_venda');
        $this->db->where('ci.payment_status', 2);
        $this->db->group_by('c.id');
        $this->db->order_by('qtd_vendido', 'DESC');

        if ($dados['datai']) {
            $this->db->where('ci.data >=', format_date_to_bd($dados['datai']));
        }

        if ($dados['dataf']) {
            $this->db->where('ci.data <=', format_date_to_bd($dados['dataf']));
        }

        $this->db->limit(900);

        return $this->db->get();
    }

    public function save($data)
    {
        $this->db->insert('cursos', $data);
        return ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();
    }

    public function update($id = '', $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('cursos', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('cursos');
    }

    public function delete_imagem($id)
    {
        $data['foto'] = null;
        $this->db->where('id', $id);
        return $this->db->update('cursos', $data);
    }

   public function totalRegistros()
    {
        $this->db->select('*');
        $this->db->from('cursos');
        
        return $this->db->get()->num_rows();
    }

}