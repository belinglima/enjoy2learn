<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function all($start = 0, $data = '')
    {
        $this->db->select('b.*');
        $this->db->from('blog AS b');
        if (isset($data)) {
            if (isset($data['buscar'])) {
                $this->db->like('b.descricao', $data['buscar']);
            }    
        }    
        $this->db->limit(10, $start);
                  
        return $this->db->get();
    }

    public function countAll()
    {
        $this->db->select('b.*');
        $this->db->from('blog AS b');
                        
        return $this->db->get();
    }

    public function find($id)
    {
        $this->db->select('*');
        $this->db->from('blog');
        $this->db->where('id', $id);
                
        return $this->db->get();
    }

    public function save($data)
    {
        $data['created_at'] = date('Y-m-d H:i:s');

        $this->db->insert('blog', $data);
        return ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();
    }

    public function update($id = '', $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('blog', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('blog');
    }

    public function delete_imagem($id)
    {
        $data['foto'] = null;
        $this->db->where('id', $id);
        return $this->db->update('blog', $data);
    }

   public function totalRegistros()
    {
        $this->db->select('*');
        $this->db->from('blog');
        
        return $this->db->get()->num_rows();
    }
}