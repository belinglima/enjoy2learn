<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_Model extends CI_Model
{
    protected $aluno_id;
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->aluno_id = get_user_session();
    }

    public function all($start = 0)
    {
        $this->db->select('u.*');
        $this->db->from('usuarios AS u');
        $this->db->where('u.nivel', 'Administrador');
        
        $this->db->limit(10, $start);
                  
        return $this->db->get();
    }

    public function countAll()
    {
        $this->db->select('u.*');
        $this->db->from('usuarios AS u');
        $this->db->where('u.nivel', 'Administrador');
                        
        return $this->db->get();
    }

    public function find($id)
    {
        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where('id', $id);
                
        return $this->db->get();
    }

    public function findUser()
    {
        $this->db->select('*');
        $this->db->from('usuarios');
                     
        return $this->db->get();
    }

    public function getFoto()
    {
        $this->db->select('u.foto');
        $this->db->from('usuarios AS u');
        $this->db->where('u.id', get_system_user_session());
        
        return $this->db->get()->row()->foto;
    }

    public function getNivel()
    {
        $this->db->select('u.*');
        $this->db->from('usuarios AS u');
        $this->db->where('u.id', get_system_user_session());
        
        return $this->db->get()->row()->nivel;
    }

    public function getUsuarioPorEmail($email)
    {
        $this->db->select("u.*");
        $this->db->from('usuarios AS u');
        $this->db->where('u.email', $email);
     
        $result = $this->db->get();
       
        if ($result) {
            if ($result->num_rows() > 0) {
                return $result->row_array();
            } 
        } 
    }

    public function save($data)
    {
        $data['criacao'] = date('Y-m-d');
        
        $this->db->insert('usuarios', $data);
        return ( !$this->db->affected_rows() ) ? false : $this->db->insert_id();
    }

    public function update($id = '', $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('usuarios', $data);
    }

    public function updatePerfil($data)
    {
        $this->db->where('id', $this->aluno_id);
        return $this->db->update('usuarios', $data);
    }

    public function updateSenha($id, $email, $senha)
    {
        $this->db->set('hash_senha', $senha);
        $this->db->where('id', $id);
        $this->db->where('email', $email);
        $this->db->update('usuarios');

        return ( !$this->db->affected_rows() ) ? false : true;
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('usuarios');
    }

    public function findLogin($login, $id = '')
    {
        $this->db->select("u.id, u.nome, u.hash_senha, u.nivel");
        $this->db->from('usuarios AS u');
        $this->db->where('u.login', $login);

        if ($id) {
            $this->db->where('u.id !=', $id);            
        }

        $result = $this->db->get();

        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
    }

    public function findLoginEmail($email)
    {
        $this->db->select("u.id, u.nome, u.hash_senha, u.nivel");
        $this->db->from('usuarios AS u');
        $this->db->where('u.email', $email);
        $result = $this->db->get();

        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
    }

    public function totalRegistros()
    {
        $this->db->select('*');
        $this->db->from('usuarios as u');
        $this->db->where('u.nivel', 'Administrador');
        
        return $this->db->get()->num_rows();
    }

    public function findEmail($email)
    {
        $this->db->select('*')
            ->from('usuarios')
            ->where('email', $email);

        return $this->db->get();
    }

    public function deletarFoto()
    {
        $data['foto'] = null;
        $this->db->where('id', $this->aluno_id);
        return $this->db->update('usuarios', $data);
    }

    public function getUltimoIdTermo()
    {
        $this->db->select('MAX(id) as ultimoID');
        $this->db->from('termos');

        return $this->db->get()->row()->ultimoID; 
    }
}