<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cielo_Model extends CI_Model
{
    protected $aluno_id;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->aluno_id = get_user_session();
    }

    public function find($id)
    {
        $this->db->select('*')
            ->from('cielo')
            ->where('venda_id', $id);
            
        return $this->db->get();
    }
    
    public function save($data)
    {
        $this->db->insert('cielo', $data);
        return (!$this->db->affected_rows()) ? false : true;
    }

    public function update($id = '', $data)
    {
        $this->db->where('product_id', $id);
        return $this->db->update('cielo', $data);
    }

    public function updateByOrderNumber($id = '', $data)
    {
        $this->db->where('order_number', $id);
        return $this->db->update('cielo', $data);
    }

    public function updateByIdVenda($id = '', $data)
    {
        $this->db->where('venda_id', $id);
        return $this->db->update('cielo', $data);
    }

    public function updateStatusVenda($order_number)
    {
        $this->db->select('*')
            ->from('cielo')
            ->where('order_number', $order_number);

        $order = $this->db->get()->row();

        switch ($order->payment_status) {
            case 2:
                $update['status'] = "EFETIVADA";
                break;
            case 8:
                $update['status'] = "CANCELADA";
                break;
            default:
                $update['status'] = "PENDENTE";
                break;
        }

        $this->db->where('id_venda', $order->venda_id);
        return $this->db->update('vendas', $update);
    }

    public function updateStatusByProductId($product_id)
    {
        $this->db->select('*')
            ->from('cielo')
            ->where('product_id', $product_id);

        $order = $this->db->get()->row();

        switch ($order->payment_status) {
            case 2:
                $update['status'] = "EFETIVADA";
                break;
            case 8:
                $update['status'] = "CANCELADA";
                break;
            default:
                $update['status'] = "PENDENTE";
                break;
        }

        $this->db->where('id_venda', $order->venda_id);
        return $this->db->update('vendas', $update);
    }

    public function delete($id)
    {
        $this->db->where('venda_id', $id);
        return $this->db->delete('cielo');
    }
}