<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mother_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function deleteNotificacao()
    {
        $this->db->where('data <=', date('Y-m-d H:i:s'));
        return $this->db->delete('notificacoes');
    }
}