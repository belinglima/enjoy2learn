<div style="width: 98%; border: solid 1px #ccc; padding: 0px; margin: 0px;">
	<div style="width: 100%; background-color: #2196F3;">
		<img src="{{ base_url('assets/images/site/logo.png') }}" style="width: 100px; margin: 12px;">
	</div>
	<div style="padding: 12px;">
		<h2>Conta Enjoy2learn</h2>
		<b style='font-size: 12px;'>Data {{ date('d/m/Y') }}</b>
		<h4>Olá {{ $nome }}</h4>
		<p>Recebemos uma solicitação de mudança de senha em nosso sistema</p>
		<p>Abaixo segue o link para mudar sua senha</p>
		<a style="font-weight: bold; color: #336699; font-size: 14px; text-decoration: underline;" href="{{ $endereco }}">Desejo mudar minha senha</a>
		<br/><br/>
		Email automático, não responda esta mensagem.
	</div>
</div>