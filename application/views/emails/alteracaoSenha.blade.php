<div style="width: 98%; border: solid 1px #ccc; padding: 0px; margin: 0px;">
	<div style="width: 100%; background-color: #2196F3;">
		<img src="{{ base_url('assets/images/logo.png') }}" style="width: 100px; margin: 12px;">
	</div>
	<div style="padding: 12px;">
		<h2>Alteração de senha</h2>
		<b style='font-size: 12px;'>Data {{ date('d/m/Y') }}</b>
		<h4>Olá {{ $nome }}</h4>
		<p>Sua senha foi modificada com sucesso!</p>
		<p>Caso você não tenha solicitado a mudança de senha, entre em contato com o suporte imediatamente!</p>
		<p><b>idiomas.enjoy2learn@gmail.com</b> ou <b>(11) 95789-8152</b>.</p>
		<p>Atenciosamente <b>Enjoy2learn</b></p>
	</div>
</div>
