<div style="background-color: #e6f2ff; width: 100%; padding: 20px; margin: 0px;">
	<div style="margin: 0px auto; width: 460px; background-color: #fff; padding: 20px; border: solid 1px #eee; border-radius: 6px; font-family: 'Arial';">
		<img src="<?=base_url('assets/images/logo.png');?>" style="width: 200px; margin-bottom: 14px;"> <br/>
		<b style="font-size: 20px;">Email de ativação</b> <br/><br/>
		Olá <b><?=$nome;?></b>, <br/>
		Bem vindo(a) ao sistema <b>Enjoy2Learn</b>, clique no link abaixo para ativar seu cadastro: <br/><br/>
		<a href="<?=base_url('ativacao/'.base64_encode($idcliente));?>" style="padding: 6px; color: #0083CB;">
			Ativar conta
		</a>
		<br/><br/>
		Email automático, não responda esta mensagem.
	</div>
</div>