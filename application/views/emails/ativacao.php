<div style="background-color: #6372ff; width: 100%; padding: 20px; margin: 0px;">
	<div style="margin: 0px auto; width: 460px; background-color: #fff; padding: 20px; border: solid 1px #eee; border-radius: 6px; font-family: 'Arial';">
		<img src="<?=base_url('assets/images/logo.png');?>" style="width: 200px; margin-bottom: 14px;"> <br/>
		<b style="font-size: 20px;">Email de ativação</b> <br/><br/>
		Olá <b><?=$nome;?></b>, <br/><br/>

		Link para ativar seu cadastro: <b><?=$link;?></b> <br/><br/>

		Bem vindo(a) ao sistema <b>Enjoy2Learn</b>, seus dados para entrar no sistema são: <br/><br/>
		E-Mail: <b><?=$email;?></b> <br/>
		Senha: <b><?=$senha;?></b> <br/><br/>
		Email automático, não responda esta mensagem.
	</div>
</div>