@extends('template.base-sistema')

@section('content')

<main>

	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h1 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Editar
			</h1>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('sistema/usuarios/update/'.$usuario['id']) }}" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Usúario</h5>
						</div>	
					</div>
					
					<div class="row">	
						<div class="col s12 m6">
				          	<label>Nome <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="nome" name="nome" type="text" class="validate browser-default" value="{{ $usuario['nome'] }}" required="required">
				        </div>
				        <div class="col s12 m6">
				          	<label>Login <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="login" name="login" data-id="" type="text" class="validate browser-default" value="{{ $usuario['login'] }}" required="required">
				        </div>
				     </div>
				     <div class="row">   
				        <div class="col s12 m6">
							<label>Senha</label>
							<input id="hash_senha" name="hash_senha" class="validate browser-default" type="password">
							<span>Senha criptografada, é necessário gerar uma nova</span>
						</div>
						<div class="col s12 m6">
							<label>Nível <span style="font-weight: bold; color: red;">*</span></label>
							<select name="nivel" id="nivel" class="browser-default" required="required">
								@if($usuario['nivel'] == 'Administrador')
									<option value="Administrador" selected>Administrador</option>
									<option value="Suporte">Suporte</option>
								@else
									<option value="Administrador" >Administrador</option>
									<option value="Suporte" selected>Suporte</option>
								@endif
							</select>
						</div>
				    </div>
										
					<div class="row">	
						<div class="col s12">
							<button class="btn blue darken-1 add-usuario-sistema full-width" type="submit">
								Salvar usuário
								<i class="material-icons right">chevron_right</i>
							</button>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>

	<br><br><br><br>
</main>

@stop

@section('extra-javascript')

<script type="text/javascript">
	
	$('#login').on('input', function(){
	   $(this).val($(this).val().toLowerCase());
	});

</script>

@stop
