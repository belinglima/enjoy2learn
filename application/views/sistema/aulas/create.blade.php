@extends('template.base-sistema')

@section('content')

<main>
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h1 class="header center blue-text truncate">
				<a href="{{ base_url('sistema/aulas/lista/'.$idCurso) }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
				Novo
				@if ($curso)
					/ {{ $curso['nome'] }}
				@endif
				
			</h1>
		</div>
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('sistema/aulas/insert/'.$idCurso) }}" enctype="multipart/form-data" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Aula</h5>
						</div>
					</div>
					<div class="row">	
				    	<div class="col s12">
				          	<label>Nome <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="nome" name="nome" type="text" class="validate browser-default" required="required">
				        </div>
				    </div>
					<div class="row">    
				        <div class="col s12">
							<label>Descrição <span style="font-weight: bold; color: red;">*</span></label>
							<textarea data-ls-module="charCounter" maxlength="300" id="descricao" name="descricao" class="validate browser-default" type="text" required="required" rows="20" cols="33"></textarea>
					    </div>
				    </div>
					<div class="row">    
				        <div class="col s12">
							<label>Tipo de URL <span style="font-weight: bold; color: red;">*</span></label>
							<select name="tipo_url" id="tipo_url" class="browser-default" required="required">
								<option value="" disabled selected>Escolha um tipo</option>
								<option value="YOUTUBE">Youtube</option>
								<option value="VIMEO">Vimeo</option>
							</select>
						</div>
     			    </div>
				   	<div class="row">
						<div class="col s12">
							<label>URL <span style="font-weight: bold; color: red;">*</span></label>
							<input id="url" name="url" type="text" class="validate browser-default" placeholder="Usar a hash da url do vimeo. Ex: se a URL for https://vimeo.com/34534040, usar somente o seu hash 34534040" required="required">
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<label>URL apresentação</label>
							<input id="url_apresentacao" name="url_apresentacao" type="text" class="validate browser-default" placeholder="Usar a hash da url do youtube. Ex: se a URL for https://youtube.com/y3454ee4, usar somente o seu hash y3454ee4">
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<label>URL áudio <span style="font-weight: bold; color: red;">*</span></label>
							<input id="url_audio" name="url_audio" type="text" class="validate browser-default" placeholder="Usar a hash da url do vimeo do áudio. Ex: se a URL for https://vimeo.com/34534040, usar somente o seu hash 34534040" required="required">
						</div>
					</div>
					<div class="row">
				    	<div class="col s12">
							<div class="file-field input-field">
								<div class="btn blue darken-1">
									<span>FOTO</span>
									<input type="file" id="foto" name="foto">
								</div>
								<div class="file-path-wrapper">
									<input class="file-path validate" type="text">
								</div>
							</div>
						</div>
					</div>
					
				    <div class="row">    
				        <div class="col s12">
							<button class="btn blue darken-1 add-aula-sistema full-width" type="submit">
								Salvar aula
								<i class="material-icons right">chevron_right</i>
							</button>
				        </div>
					</div>
				</form>

			</div>
		</div>
	</div>
	
</main>

@stop

@section('extra-javascript')

<script type="text/javascript">

	$(document).ready(function() {
		$('input#input_text, textarea#descricao').characterCounter();
	});
	
</script>

@stop