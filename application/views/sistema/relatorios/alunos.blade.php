@extends('template.base-sistema')

@section('content')

<main>	
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h3 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Relatório
			</h3>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">
				
				<form name="relatorio" action="{{ base_url('sistema/relatorios/alunos') }}" method="POST">
					<div class="row" style="margin-top: -10px;">
						<div class="col s12">
							<h5>Relatório de alunos</h5>
						</div>	
					</div>
					
                    <div class="row">
                        <div class="col s12 m6">
                            <label>Data final <span style="font-weight: bold; color: red;">*</span></label>
                            <input 
                              type="text" 
                              name="datai" 
                              id="datai" 
                              class="datepicker browser-default" 
                              autocomplete="off"
                              value="{{ (isset($dados['datai'])) ? $dados['datai'] : '' }}" 
                              required="required">
                        </div>
                        <div class="col s12 m6">
                            <label>Data final <span style="font-weight: bold; color: red;">*</span></label>
                            <input 
                              type="text" 
                              name="dataf" 
                              id="dataf" 
                              class="datepicker browser-default" 
                              autocomplete="off"
                              value="{{ (isset($dados['dataf'])) ? $dados['dataf'] : '' }}" 
                              required="required">
                        </div>
						<div class="col s12">
							<div class="progress loading hide">
								<div class="indeterminate"></div>
							</div>
						</div>	
						<div class="col s12">
							<button class="btn full-width blue darken-1">
								Buscar
								<i class="material-icons right">chevron_right</i>
							</button>
						</div>
					</div>

				</form>

				@if (isset($alunos))

					<div class="col s12" id="print_div">
						<div class="card-panel white">
							<table class="responsive-table" id="relatorio-alunos">
								@if ($msg)
									<tbody>
										  <tr>
											<td colspan="4">
												<i class="material-icons left">folder_open</i> 
												<b>Sem alunos!</b>
											  </td>
										  </tr>
									</tbody>  	
								@else
									<thead>
										  <tr>
											<th>ID</th>
											<th>NOME</th>
											<th>LOGIN</th>
											<th>E-MAIL</th>
										  </tr>
									</thead>
									<tbody>
										@foreach($alunos AS $aluno)
											<tr>
												<td>{{ $aluno['id'] }}</td>
                                            <td>{{$aluno['nome'] }}</td>
                                            <td>{{ strtolower($aluno['login']) }}</td>
                                            <td>{{ strtolower($aluno['email']) }}</td>
											</tr>
										@endforeach
									</tbody>
								 @endif
							</table>		
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<br><br><br><br> 
</main>

@stop

@section('extra-javascript')

<script type="text/javascript">
	
</script>

@stop