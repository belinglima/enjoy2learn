@extends('template.base-sistema')

@section('content')

@php
  const STATUS_PAGAMENTO = [
    0 => 'AGUARDANDO',
    1 => 'PENDENTE',
    2 => 'PAGO',
    3 => 'NEGADO',
    4 => 'EXPIRADO',
    5 => 'CANCELADO',
    6 => 'NÃO FINALIZADO',
    7 => 'AUTORIZADO',
    8 => 'CHARGEBACK'
  ];

  const TIPO_PAGAMENTO = [
    1 => 'CARTÃO DE CRÉDITO',
    2 => 'BOLETO',
    3 => 'DÉBITO ONLINE',
    4 => 'CARTÃO DE DÉBITO',
  ];

  const BANDEIRAS = [
    1 => 'VISA',
    2 => 'MASTERCARD',
    3 => 'AMERCICANEXPRESS',
    4 => 'DINERS',
    5 => 'ELO',
    6 => 'AURA',
    7 => 'JCB',
    8 => 'DISCOVER',
    9 => 'HIPERCARD',
  ];
@endphp

<main>	
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h3 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Relatório
			</h3>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">
				
				<form name="relatorio" action="{{ base_url('sistema/relatorios/vendas') }}" method="POST">
					<div class="row" style="margin-top: -10px;">
						<div class="col s12">
							<h5>Relatório de vendas</h5>
						</div>	
					</div>
					
                    <div class="row">
						  <div class="col s12 m4">
							 <label>Data inicial <span style="font-weight: bold; color: red;">*</span></label>
							 <input 
								type="text" 
								name="datai" 
								id="datai" 
								class="datepicker browser-default" 
								autocomplete="off" 
								value="{{ (isset($dados['datai'])) ? $dados['datai'] : '' }}" 
								required="required">
						  </div>
						  <div class="col s12 m4">
							  <label>Data final <span style="font-weight: bold; color: red;">*</span></label>
							  <input 
								type="text" 
								name="dataf" 
								id="dataf" 
								class="datepicker browser-default" 
								autocomplete="off"
								value="{{ (isset($dados['dataf'])) ? $dados['dataf'] : '' }}" 
								required="required">
						  </div>
						  <div class="col s12 m4">
							<label>Status <span style="font-weight: bold; color: red;">*</span></label>
							<select name="status" id="status" class="browser-default">
								<option value="all">TODOS</option>
								@foreach(STATUS_PAGAMENTO AS $key => $value)
								<option 
									value="{{ $key }}" 
									{{ (isset($dados['status']) && $dados['status'] == $key) ? 'selected' : '' }}>
									{{ $value }}
								</option>
								@endforeach
							</select>	
						</div>
						<div class="col s12">
							<div class="progress loading hide">
								<div class="indeterminate"></div>
							</div>
						</div>	
						<div class="col s12">
							<button class="btn full-width blue darken-1">
								Buscar
								<i class="material-icons right">chevron_right</i>
							</button>
						</div>
					</div>

				</form>

				@if (isset($vendas))
					<div class="col s12" id="print_div">
						<div class="card-panel white">
							<table class="responsive-table" id="relatorio-vendas">
								@if ($msg)
									<tbody>
										  <tr>
											<td colspan="5">
												<i class="material-icons left">folder_open</i> 
												<b>Sem vendas!</b>
											  </td>
										  </tr>
									</tbody>  	
								@else
									<thead>
										  <tr>
											<th>ID</th>
											<th>ALUNO</th>
											<th>DATA</th>
											<th>STATUS</th>
											<th>TOTAL</th>
										  </tr>
									</thead>
									<tbody>
										@foreach($vendas AS $venda)			          			
										<tr>
                                            <td>{{ $venda['id_venda'] }}</td>
											<td>{{ $venda['nome'] }}</td>
                                            <td>{{ format_date_to_show($venda['data']) }}</td>
                                            <td>
                                                {{ STATUS_PAGAMENTO[($venda['payment_status']) ? $venda['payment_status'] : 0] }}
                                            </td>
                                            <td>R$ {{ convert_double_to_BRL($venda['total']) }}</td> 
										</tr>
									@endforeach
									</tbody>
								 @endif
							</table>	
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>	

	<br><br><br><br> 
</main>

@stop

@section('extra-javascript')

<script type="text/javascript">
	
</script>

@stop