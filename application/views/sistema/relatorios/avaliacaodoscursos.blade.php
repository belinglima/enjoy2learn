@extends('template.base-sistema')

@section('content')

@php
	$soma = 0.00;
    $numeroalunos = 0;
@endphp


<main>	
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h3 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Relatório
			</h3>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">
				
				<form name="relatorio" action="{{ base_url('sistema/relatorios/avaliacaodoscursos') }}" method="POST">
					<div class="row" style="margin-top: -10px;">
						<div class="col s12">
							<h5>Relatório de avaliação dos cursos</h5>
						</div>	
					</div>
					
                    <div class="row">
                        <div class="col s12 m4">
                            <label>Curso <span style="font-weight: bold; color: red;">*</span></label>
                            <select class="browser-default curso" name="id_curso" required="required">
								<option value="" disabled selected>Escolher um curso</option>
								@foreach($cursos AS $curso)
                                    @if($dados['id_curso'] == $curso['id'])
									    <option value="{{ $curso['id'] }}" selected>{{ $curso['nome'] }}</option>
                                    @else
                                        <option value="{{ $curso['id'] }}">{{ $curso['nome'] }}</option>
                                    @endif
                                @endforeach
							</select>
                        </div>
                        <div class="col s12 m4">
                            <label>Data final <span style="font-weight: bold; color: red;">*</span></label>
                            <input 
                              type="text" 
                              name="datai" 
                              id="datai" 
                              class="datepicker browser-default" 
                              autocomplete="off"
                              value="{{ (isset($dados['datai'])) ? $dados['datai'] : '' }}" 
                              required="required">
                        </div>
                        <div class="col s12 m4">
                            <label>Data final <span style="font-weight: bold; color: red;">*</span></label>
                            <input 
                              type="text" 
                              name="dataf" 
                              id="dataf" 
                              class="datepicker browser-default" 
                              autocomplete="off"
                              value="{{ (isset($dados['dataf'])) ? $dados['dataf'] : '' }}" 
                              required="required">
                        </div>
						<div class="col s12">
							<div class="progress loading hide">
								<div class="indeterminate"></div>
							</div>
						</div>	
						<div class="col s12">
							<button class="btn full-width blue darken-1">
								Buscar
								<i class="material-icons right">chevron_right</i>
							</button>
						</div>
					</div>

				</form>

				@if (isset($avaliacoes))

					<div class="col s12" id="print_div">
						<div class="card-panel white">
							<table class="responsive-table" id="relatorio-alunos">
								@if ($msg)
									<tbody>
										  <tr>
											<td colspan="2">
												<i class="material-icons left">folder_open</i> 
												<b>Sem avaliações!</b>
											  </td>
										  </tr>
									</tbody>  	
								@else
									<thead>
										  <tr>
											<th>ID</th>
											<th>NOTA</th>
										  </tr>
									</thead>
									<tbody>
										@foreach($avaliacoes AS $avaliacao)
											<tr>
												<tr>
													<td>{{ $avaliacao['idAvaliacao'] }}</td>
													<td>{{ $avaliacao['nota'] }}</td>
												</tr>
												@php
													$numeroalunos += $avaliacao['qtdAlunos'];
													$soma += $avaliacao['nota'];
												@endphp
											</tr>
										@endforeach
									</tbody>
								 @endif
							</table>		
						    @if($soma > 0)
                                <div class="row">
                                    <div class="right-align" style="margin-right: 10px;">
                                        <b class="grande">Média {{ $soma/$numeroalunos }}</b>
                                    </div>
                                </div>
                            @endif
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>	

	<br><br><br><br> 
</main>

@stop

@section('extra-javascript')

<script type="text/javascript">
	
</script>

@stop