@extends('template.base-sistema')

@section('content')

<main>	
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h3 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20p;">chevron_left</i>
				</a>
				Cupons({{ $total }})
			</h3>
		</div> 
	</div>

	<a href="{{ base_url('sistema/cupons/create') }}" class="btn-floating btn-large waves-effect waves-light blue darken-1 right " style="margin-top: -50px; margin-right: 20px;">
	    <i class="material-icons">add</i>
	</a>
	
	<div class="container">
		<div class="card">
			<div class="card-content">
				<table class="responsive-table" id="lista">
					@if (! $cupons)
						<tbody>
							  <tr>
								  <td colspan="5">
									 <i class="material-icons left">folder_open</i>
									 <b>Nenhum cupom encontrado!</b>
								  </td>
							  </tr>
						</tbody>  	
					@else
						<thead>
							  <tr>
								<th>ID</th>
		              			<th>CUPOM</th>
								<th>VALIDADE</th>
								<th>PORCENTAGEM DE DESCONTO</th>
								<th class="right">AÇÕES</th>
							  </tr>
						</thead>
						<tbody>
							@foreach($cupons AS $cupom)
								<tr>
									<td>{{ $cupom['id'] }}</td>
									<td>{{ $cupom['cupom'] }}</td>
									<td>{{ format_date_to_show($cupom['validade']) }}</td>
									<td>{{ convert_Real($cupom['porcentagem']) }}</td>
									<td class="right">
										<div style="margin-top: -6px;">
											<a href="{{ base_url('sistema/cupons/edit/'.$cupom['id'])}}">
												<div class="btn blue darken-1 tooltipped" data-position="top" data-tooltip="Editar">
													<i class="material-icons">edit</i>
												</div>
											</a>
											&nbsp;
											<div class="btn blue darken-1 remove-cupom-sistema tooltipped" data-id="{{ $cupom ['id'] }}" data-position="top" data-tooltip="Excluir">
												<i class="material-icons">delete</i>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					 @endif
				</table>
			</div>
		</div>
	</div>
	<div class="center">
		@php
			echo $links;
		@endphp
  	</div>
	
	<br><br><br><br>	  
</main>

@stop

@section('extra-javascript')
<script type="text/javascript">
	$(".remove-cupom-sistema").on('click', function(){
		var id = $(this).data('id');
		swal({
			title: 'Tem certeza?',
			text: "Esta ação não poderá ser desfeita!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#b71c1c ',
			cancelButtonColor: '#ccc',
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não'
		}, function() {
			location.href = base_url('sistema/cupons/delete/'+id);
		});
	});
</script>
@stop
