@extends('template.base-sistema')

@section('content')

<main>
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h1 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Novo
			</h1>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('sistema/cupons/insert') }}" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Cupom</h5>
						</div>	
					</div>
					
					<div class="row">	
						<div class="col s12">
				          	<label>Cupom <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="cupom" name="cupom" type="text" class="validate browser-default" required="required">
						</div>
					</div>
					<div class="row">
						<div class="col s12 m6">
							<label>Validade <span style="font-weight: bold; color: red;">*</span></label>
							<input id="validade" name="validade" type="text" class="datepicker browser-default" required="required">
						</div>
						<div class="col s12 m6">
							<label>Porcentagem de desconto <span style="font-weight: bold; color: red;">*</span></label>
							<input id="porcentagem" name="porcentagem" type="text" class="real validate browser-default" required="required">
						</div>
					</div>
																    
				    <div class="row">    
				        <div class="col s12">
							<button class="btn blue darken-1 add-cupom-sistema full-width" type="submit">
								Salvar cupom
								<i class="material-icons right">chevron_right</i>	
						    </button>
				        </div>
					</div>
				</form>

			</div>
		</div>
	</div>
	
</main>

@stop

@section('extra-javascript')

<script type="text/javascript">
	$('.datepicker').datepicker("setDate", new Date());
</script>

@stop

