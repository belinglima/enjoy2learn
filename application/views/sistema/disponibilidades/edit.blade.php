@extends('template.base-sistema')

@section('content')

<main>

	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h1 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Editar
			</h1>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('sistema/disponibilidades/update/'.$disponibilidade['id']) }}" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Disponibilidade</h5>
						</div>	
					</div>
					
					<div class="row">	
						<div class="col s12 m4">
				          	<label>Dia <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="dia" name="dia" type="text" class="validate browser-default" value="{{ $disponibilidade['dia'] }}" required="required" readonly="readonly">
				        </div>
				        <div class="col s12 m4">
				          	<label>Hora início <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="horainicio" name="horainicio" type="time" class="validate browser-default" value="{{ $disponibilidade['horainicio'] }}" required="required">
				        </div>
                        <div class="col s12 m4">
                            <label>Hora fim <span style="font-weight: bold; color: red;">*</span></label>
                            <input id="horafim" name="horafim" type="time" class="validate browser-default" value="{{ $disponibilidade['horafim'] }}" required="required">
                        </div>
				    </div>
				     
                  	<div class="row">	
						<div class="col s12">
							<button class="btn blue darken-1 add-disponibilidade-sistema full-width" type="submit">
								Salvar disponibilidade
								<i class="material-icons right">chevron_right</i>
							</button>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>

</main>

@stop

@section('extra-javascript')

@stop
