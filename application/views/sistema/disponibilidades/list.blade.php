@extends('template.base-sistema')

@section('content')

<main>	
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h3 class="header center blue-text">
				@if(isset($buscar))
					<a href="{{ base_url('sistema/disponibilidades') }}">
						<i class="material-icons medium left blue-text" style="margin-right: -20px">chevron_left</i>
					</a>
				@else
					<a href="{{ base_url('sistema/welcome') }}">
						<i class="material-icons medium left bluee-text" style="margin-right: -20p;">chevron_left</i>
					</a>
				@endif
				@if(isset($buscar))
					Disponibilidade
				@else
					Disponibilidade({{ $total }})
				@endif
			</h3>
			@if(isset($buscar))
				<span class="blue-text" style="font-weight: bold;">Filtrado por: <u>{{ $buscar }}</u></span>
			@endif	
		</div> 
	</div>

	<a href="{{ base_url('sistema/disponibilidades/create') }}" class="btn-floating btn-large waves-effect waves-light blue darken-1 right" style="margin-top: -50px; margin-right: 20px;">
	    <i class="material-icons">add</i>
	</a>
	
	<div class="container">
		<div class="card">
			<div class="card-content">
				<form class="form-horizontal" method="post" action="{{ base_url('sistema/disponibilidades/index') }}">    
					<div class="row" style="margin-top: -10px;">
						<div class="col s12 m6">
							<input id="buscar" name="buscar" type="text" class="input-field validate" required="required" placeholder="Pesquisa por dia" autocomplete="off">
				        </div>
				        <div class="col s12 m6">
					        <button class="btn full-width blue darken-1">
					        	Buscar
					        	<i class="material-icons right">chevron_right</i>
					        </button>
				    	</div>
					</div>
										
			    </form>		
				<table class="responsive-table" id="lista">
					@if (! $disponibilidades)
						<tbody>
							  <tr>
								  <td colspan="4">
									<i class="material-icons left">folder_open</i>
									@if(isset($buscar))
										<b>Nenhum disponibilidade encontrada!</b>
									  @else
										  <b>Nenhum disponibilidade cadastrada!</b>
									  @endif  
								</td>
							  </tr>
						</tbody>  	
					@else
						<thead>
							  <tr>
								<th>DIA</th>
								<th>HORA INÍCIO</th>
								<th>HORA FIM</th>
								<th class="right">AÇÕES</th>
							  </tr>
						</thead>
						<tbody>
							@foreach($disponibilidades AS $disponibilidade)
								<tr>
									<td>{{ $disponibilidade['dia'] }}</td>
									<td>{{ $disponibilidade['horainicio'] }}</td>
									<td>{{ $disponibilidade['horafim'] }}</td>
									<td class="right">
										<div style="margin-top: -6px;">
											<a href="{{ base_url('sistema/disponibilidades/edit/'.$disponibilidade['id'])}}">
												<div class="btn blue darken-1 tooltipped" data-position="top" data-tooltip="Editar">
													<i class="material-icons">edit</i>
												</div>
											</a>
											&nbsp;
											<div class="btn blue darken-1 remove-disponibilidade-sistema tooltipped" data-id="{{ $disponibilidade['id'] }}" data-position="top" data-tooltip="Excluir">
												<i class="material-icons">delete</i>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					 @endif
				</table>	
			</div>
		</div>
	</div>
	<div class="center">
		@php
			echo $links;
		@endphp
  	</div>
	
	<br><br><br><br>
</main>

@stop

@section('extra-javascript')
<script type="text/javascript">
	$(".remove-disponibilidade-sistema").on('click', function(){
		var id = $(this).data('id');
		swal({
			title: 'Tem certeza?',
			text: "Esta ação não poderá ser desfeita!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#b71c1c ',
			cancelButtonColor: '#ccc',
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não'
		}, function() {
			location.href = base_url('sistema/disponibilidades/delete/'+id);
		});
	});
</script>
@stop
