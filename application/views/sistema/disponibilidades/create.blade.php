@extends('template.base-sistema')

@section('content')

<main>
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h1 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Novo
			</h1>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('sistema/disponibilidades/insert') }}" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Disponibilidade</h5>
						</div>	
					</div>
					<div class="row">
						<div class="col s12 m4">
							<label>
								<input id="dom" name="dia[]" type="checkbox" class="dom with-gap" value="DOM">
								<span>Domingo</span>
							</label>
						</div>
						<div class="col s12 m4">
							<label>Hora início</label>
							<select name="horainicio[]" id="horainiciodom" class="horainiciodom browser-default" disabled>
								<option value="" disabled selected>Escolha uma hora</option>
								<option value="23:45">23:45</option>
								<option value="23:30">23:30</option>
								<option value="23:15">23:15</option>
								<option value="23:00">23:00</option>
								<option value="22:45">22:45</option>
								<option value="22:30">22:30</option>
								<option value="22:15">22:15</option>
								<option value="22:00">22:00</option>
								<option value="21:45">21:45</option>
								<option value="21:30">21:30</option>
								<option value="21:15">21:15</option>
								<option value="21:00">21:00</option>
								<option value="20:45">20:45</option>
								<option value="20:30">20:30</option>
								<option value="20:15">20:15</option>
								<option value="20:00">20:00</option>
								<option value="19:45">19:45</option>
								<option value="19:30">19:30</option>
								<option value="19:15">19:15</option>
								<option value="19:00">19:00</option>
								<option value="18:45">18:45</option>
								<option value="18:30">18:30</option>
								<option value="18:15">18:15</option>
								<option value="18:00">18:00</option>
								<option value="17:45">17:45</option>
								<option value="17:30">17:30</option>
    							<option value="17:15">17:15</option>
								<option value="17:00">17:00</option>
								<option value="16:45">16:45</option>
								<option value="16:30">16:30</option>
								<option value="16:15">16:15</option>
								<option value="16:00">16:00</option>
								<option value="15:45">15:45</option>
								<option value="15:30">15:30</option>
								<option value="15:15">15:15</option>
								<option value="15:00">15:00</option>
								<option value="14:45">14:45</option>
								<option value="14:30">14:30</option>
								<option value="14:15">14:15</option>
								<option value="14:00">14:00</option>
								<option value="13:45">13:45</option>
								<option value="13:30">13:30</option>
								<option value="13:15">13:15</option>
								<option value="13:00">13:00</option>
								<option value="12:45">12:45</option>
								<option value="12:30">12:30</option>
								<option value="12:15">12:15</option>
								<option value="12:00">12:00</option>
								<option value="11:45">11:45</option>
								<option value="11:30">11:30</option>
								<option value="11:15">11:15</option>
								<option value="11:00">11:00</option>
								<option value="10:45">10:45</option>
								<option value="10:30">10:30</option>
								<option value="10:15">10:15</option>
								<option value="10:00">10:00</option>
								<option value="09:45">09:45</option>
								<option value="09:30">09:30</option>
								<option value="09:15">09:15</option>
								<option value="09:00">09:00</option>
								<option value="08:45">08:45</option>
								<option value="08:30">08:30</option>
								<option value="08:15">08:15</option>
								<option value="08:00">08:00</option>
								<option value="07:45">07:45</option>
								<option value="07:30">07:30</option>
								<option value="07:15">07:15</option>
								<option value="07:00">07:00</option>
								<option value="06:45">06:45</option>
								<option value="06:30">06:30</option>
								<option value="06:15">06:15</option>
								<option value="06:00">06:00</option>
								<option value="05:45">05:45</option>
								<option value="05:30">05:30</option>
								<option value="05:15">05:15</option>
								<option value="05:00">05:00</option>
								<option value="04:45">04:45</option>
								<option value="04:30">04:30</option>
								<option value="04:15">04:15</option>
								<option value="04:00">04:00</option>
								<option value="03:45">03:45</option>
								<option value="03:30">03:30</option>
								<option value="03:15">03:15</option>
								<option value="03:00">03:00</option>
								<option value="02:45">02:45</option>
								<option value="02:30">02:30</option>
								<option value="02:15">02:15</option>
								<option value="02:00">02:00</option>
								<option value="01:45">01:45</option>
								<option value="01:30">01:30</option>
								<option value="01:15">01:15</option>
								<option value="01:00">01:00</option>
								<option value="00:45">00:45</option>
								<option value="00:30">00:30</option>
								<option value="00:15">00:15</option>
								<option value="00:00">00:00</option>
							</select>
						</div>
						<div class="col s12 m4">
							<label>Hora fim</label>
							<select name="horafim[]" id="horafimdom" class="horafimdom browser-default" disabled>
								<option value="" disabled selected>Escolha uma hora</option>
								<option value="23:45">23:45</option>
								<option value="23:30">23:30</option>
								<option value="23:15">23:15</option>
								<option value="23:00">23:00</option>
								<option value="22:45">22:45</option>
								<option value="22:30">22:30</option>
								<option value="22:15">22:15</option>
								<option value="22:00">22:00</option>
								<option value="21:45">21:45</option>
								<option value="21:30">21:30</option>
								<option value="21:15">21:15</option>
								<option value="21:00">21:00</option>
								<option value="20:45">20:45</option>
								<option value="20:30">20:30</option>
								<option value="20:15">20:15</option>
								<option value="20:00">20:00</option>
								<option value="19:45">19:45</option>
								<option value="19:30">19:30</option>
								<option value="19:15">19:15</option>
								<option value="19:00">19:00</option>
								<option value="18:45">18:45</option>
								<option value="18:30">18:30</option>
								<option value="18:15">18:15</option>
								<option value="18:00">18:00</option>
								<option value="17:45">17:45</option>
								<option value="17:30">17:30</option>
								<option value="17:15">17:15</option>
								<option value="17:00">17:00</option>
								<option value="16:45">16:45</option>
								<option value="16:30">16:30</option>
								<option value="16:15">16:15</option>
								<option value="16:00">16:00</option>
								<option value="15:45">15:45</option>
								<option value="15:30">15:30</option>
								<option value="15:15">15:15</option>
								<option value="15:00">15:00</option>
								<option value="14:45">14:45</option>
								<option value="14:30">14:30</option>
								<option value="14:15">14:15</option>
								<option value="14:00">14:00</option>
								<option value="13:45">13:45</option>
								<option value="13:30">13:30</option>
								<option value="13:15">13:15</option>
								<option value="13:00">13:00</option>
								<option value="12:45">12:45</option>
								<option value="12:30">12:30</option>
								<option value="12:15">12:15</option>
								<option value="12:00">12:00</option>
								<option value="11:45">11:45</option>
								<option value="11:30">11:30</option>
								<option value="11:15">11:15</option>
								<option value="11:00">11:00</option>
								<option value="10:45">10:45</option>
								<option value="10:30">10:30</option>
								<option value="10:15">10:15</option>
								<option value="10:00">10:00</option>
								<option value="09:45">09:45</option>
								<option value="09:30">09:30</option>
								<option value="09:15">09:15</option>
								<option value="09:00">09:00</option>
								<option value="08:45">08:45</option>
								<option value="08:30">08:30</option>
								<option value="08:15">08:15</option>
								<option value="08:00">08:00</option>
								<option value="07:45">07:45</option>
								<option value="07:30">07:30</option>
								<option value="07:15">07:15</option>
								<option value="07:00">07:00</option>
								<option value="06:45">06:45</option>
								<option value="06:30">06:30</option>
								<option value="06:15">06:15</option>
								<option value="06:00">06:00</option>
								<option value="05:45">05:45</option>
								<option value="05:30">05:30</option>
								<option value="05:15">05:15</option>
								<option value="05:00">05:00</option>
								<option value="04:45">04:45</option>
								<option value="04:30">04:30</option>
								<option value="04:15">04:15</option>
								<option value="04:00">04:00</option>
								<option value="03:45">03:45</option>
								<option value="03:30">03:30</option>
								<option value="03:15">03:15</option>
								<option value="03:00">03:00</option>
								<option value="02:45">02:45</option>
								<option value="02:30">02:30</option>
								<option value="02:15">02:15</option>
								<option value="02:00">02:00</option>
								<option value="01:45">01:45</option>
								<option value="01:30">01:30</option>
								<option value="01:15">01:15</option>
								<option value="01:00">01:00</option>
								<option value="00:45">00:45</option>
								<option value="00:30">00:30</option>
								<option value="00:15">00:15</option>
								<option value="00:00">00:00</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m4">
							<label>
								<input id="sab" name="dia[]" type="checkbox" class="sab with-gap" value="SAB">
								<span>Sábado</span>
							</label>
						</div>
						<div class="col s12 m4">
							<label>Hora início</label>
							<select name="horainicio[]" id="horainiciosab" class="horainiciosab browser-default" disabled>
								<option value="" disabled selected>Escolha uma hora</option>
								<option value="23:45">23:45</option>
								<option value="23:30">23:30</option>
								<option value="23:15">23:15</option>
								<option value="23:00">23:00</option>
								<option value="22:45">22:45</option>
								<option value="22:30">22:30</option>
								<option value="22:15">22:15</option>
								<option value="22:00">22:00</option>
								<option value="21:45">21:45</option>
								<option value="21:30">21:30</option>
								<option value="21:15">21:15</option>
								<option value="21:00">21:00</option>
								<option value="20:45">20:45</option>
								<option value="20:30">20:30</option>
								<option value="20:15">20:15</option>
								<option value="20:00">20:00</option>
								<option value="19:45">19:45</option>
								<option value="19:30">19:30</option>
								<option value="19:15">19:15</option>
								<option value="19:00">19:00</option>
								<option value="18:45">18:45</option>
								<option value="18:30">18:30</option>
								<option value="18:15">18:15</option>
								<option value="18:00">18:00</option>
								<option value="17:45">17:45</option>
								<option value="17:30">17:30</option>
								<option value="17:15">17:15</option>
								<option value="17:00">17:00</option>
								<option value="16:45">16:45</option>
								<option value="16:30">16:30</option>
								<option value="16:15">16:15</option>
								<option value="16:00">16:00</option>
								<option value="15:45">15:45</option>
								<option value="15:30">15:30</option>
								<option value="15:15">15:15</option>
								<option value="15:00">15:00</option>
								<option value="14:45">14:45</option>
								<option value="14:30">14:30</option>
								<option value="14:15">14:15</option>
								<option value="14:00">14:00</option>
								<option value="13:45">13:45</option>
								<option value="13:30">13:30</option>
								<option value="13:15">13:15</option>
								<option value="13:00">13:00</option>
								<option value="12:45">12:45</option>
								<option value="12:30">12:30</option>
								<option value="12:15">12:15</option>
								<option value="12:00">12:00</option>
								<option value="11:45">11:45</option>
								<option value="11:30">11:30</option>
								<option value="11:15">11:15</option>
								<option value="11:00">11:00</option>
								<option value="10:45">10:45</option>
								<option value="10:30">10:30</option>
								<option value="10:15">10:15</option>
								<option value="10:00">10:00</option>
								<option value="09:45">09:45</option>
								<option value="09:30">09:30</option>
								<option value="09:15">09:15</option>
								<option value="09:00">09:00</option>
								<option value="08:45">08:45</option>
								<option value="08:30">08:30</option>
								<option value="08:15">08:15</option>
								<option value="08:00">08:00</option>
								<option value="07:45">07:45</option>
								<option value="07:30">07:30</option>
								<option value="07:15">07:15</option>
								<option value="07:00">07:00</option>
								<option value="06:45">06:45</option>
								<option value="06:30">06:30</option>
								<option value="06:15">06:15</option>
								<option value="06:00">06:00</option>
								<option value="05:45">05:45</option>
								<option value="05:30">05:30</option>
								<option value="05:15">05:15</option>
								<option value="05:00">05:00</option>
								<option value="04:45">04:45</option>
								<option value="04:30">04:30</option>
								<option value="04:15">04:15</option>
								<option value="04:00">04:00</option>
								<option value="03:45">03:45</option>
								<option value="03:30">03:30</option>
								<option value="03:15">03:15</option>
								<option value="03:00">03:00</option>
								<option value="02:45">02:45</option>
								<option value="02:30">02:30</option>
								<option value="02:15">02:15</option>
								<option value="02:00">02:00</option>
								<option value="01:45">01:45</option>
								<option value="01:30">01:30</option>
								<option value="01:15">01:15</option>
								<option value="01:00">01:00</option>
								<option value="00:45">00:45</option>
								<option value="00:30">00:30</option>
								<option value="00:15">00:15</option>
								<option value="00:00">00:00</option>
							</select>
						</div>
						<div class="col s12 m4">
							<label>Hora fim</label>
							<select name="horafim[]" id="horafimsab" class="horafimsab browser-default" disabled>
								<option value="" disabled selected>Escolha uma hora</option>
								<option value="23:45">23:45</option>
								<option value="23:30">23:30</option>
								<option value="23:15">23:15</option>
								<option value="23:00">23:00</option>
								<option value="22:45">22:45</option>
								<option value="22:30">22:30</option>
								<option value="22:15">22:15</option>
								<option value="22:00">22:00</option>
								<option value="21:45">21:45</option>
								<option value="21:30">21:30</option>
								<option value="21:15">21:15</option>
								<option value="21:00">21:00</option>
								<option value="20:45">20:45</option>
								<option value="20:30">20:30</option>
								<option value="20:15">20:15</option>
								<option value="20:00">20:00</option>
								<option value="19:45">19:45</option>
								<option value="19:30">19:30</option>
								<option value="19:15">19:15</option>
								<option value="19:00">19:00</option>
								<option value="18:45">18:45</option>
								<option value="18:30">18:30</option>
								<option value="18:15">18:15</option>
								<option value="18:00">18:00</option>
								<option value="17:45">17:45</option>
								<option value="17:30">17:30</option>
								<option value="17:15">17:15</option>
								<option value="17:00">17:00</option>
								<option value="16:45">16:45</option>
								<option value="16:30">16:30</option>
								<option value="16:15">16:15</option>
								<option value="16:00">16:00</option>
								<option value="15:45">15:45</option>
								<option value="15:30">15:30</option>
								<option value="15:15">15:15</option>
								<option value="15:00">15:00</option>
								<option value="14:45">14:45</option>
								<option value="14:30">14:30</option>
								<option value="14:15">14:15</option>
								<option value="14:00">14:00</option>
								<option value="13:45">13:45</option>
								<option value="13:30">13:30</option>
								<option value="13:15">13:15</option>
								<option value="13:00">13:00</option>
								<option value="12:45">12:45</option>
								<option value="12:30">12:30</option>
								<option value="12:15">12:15</option>
								<option value="12:00">12:00</option>
								<option value="11:45">11:45</option>
								<option value="11:30">11:30</option>
								<option value="11:15">11:15</option>
								<option value="11:00">11:00</option>
								<option value="10:45">10:45</option>
								<option value="10:30">10:30</option>
								<option value="10:15">10:15</option>
								<option value="10:00">10:00</option>
								<option value="09:45">09:45</option>
								<option value="09:30">09:30</option>
								<option value="09:15">09:15</option>
								<option value="09:00">09:00</option>
								<option value="08:45">08:45</option>
								<option value="08:30">08:30</option>
								<option value="08:15">08:15</option>
								<option value="08:00">08:00</option>
								<option value="07:45">07:45</option>
								<option value="07:30">07:30</option>
								<option value="07:15">07:15</option>
								<option value="07:00">07:00</option>
								<option value="06:45">06:45</option>
								<option value="06:30">06:30</option>
								<option value="06:15">06:15</option>
								<option value="06:00">06:00</option>
								<option value="05:45">05:45</option>
								<option value="05:30">05:30</option>
								<option value="05:15">05:15</option>
								<option value="05:00">05:00</option>
								<option value="04:45">04:45</option>
								<option value="04:30">04:30</option>
								<option value="04:15">04:15</option>
								<option value="04:00">04:00</option>
								<option value="03:45">03:45</option>
								<option value="03:30">03:30</option>
								<option value="03:15">03:15</option>
								<option value="03:00">03:00</option>
								<option value="02:45">02:45</option>
								<option value="02:30">02:30</option>
								<option value="02:15">02:15</option>
								<option value="02:00">02:00</option>
								<option value="01:45">01:45</option>
								<option value="01:30">01:30</option>
								<option value="01:15">01:15</option>
								<option value="01:00">01:00</option>
								<option value="00:45">00:45</option>
								<option value="00:30">00:30</option>
								<option value="00:15">00:15</option>
								<option value="00:00">00:00</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m4">
							<label>
								<input id="sex" name="dia[]" type="checkbox" class="sex with-gap" value="SEX">
								<span>Sexta</span>
							</label>
						</div>
						<div class="col s12 m4">
							<label>Hora início</label>
							<select name="horainicio[]" id="horainiciosex" class="horainiciosex browser-default" disabled>
								<option value="" disabled selected>Escolha uma hora</option>
								<option value="23:45">23:45</option>
								<option value="23:30">23:30</option>
								<option value="23:15">23:15</option>
								<option value="23:00">23:00</option>
								<option value="22:45">22:45</option>
								<option value="22:30">22:30</option>
								<option value="22:15">22:15</option>
								<option value="22:00">22:00</option>
								<option value="21:45">21:45</option>
								<option value="21:30">21:30</option>
								<option value="21:15">21:15</option>
								<option value="21:00">21:00</option>
								<option value="20:45">20:45</option>
								<option value="20:30">20:30</option>
								<option value="20:15">20:15</option>
								<option value="20:00">20:00</option>
								<option value="19:45">19:45</option>
								<option value="19:30">19:30</option>
								<option value="19:15">19:15</option>
								<option value="19:00">19:00</option>
								<option value="18:45">18:45</option>
								<option value="18:30">18:30</option>
								<option value="18:15">18:15</option>
								<option value="18:00">18:00</option>
								<option value="17:45">17:45</option>
								<option value="17:30">17:30</option>
    							<option value="17:15">17:15</option>
								<option value="17:00">17:00</option>
								<option value="16:45">16:45</option>
								<option value="16:30">16:30</option>
								<option value="16:15">16:15</option>
								<option value="16:00">16:00</option>
								<option value="15:45">15:45</option>
								<option value="15:30">15:30</option>
								<option value="15:15">15:15</option>
								<option value="15:00">15:00</option>
								<option value="14:45">14:45</option>
								<option value="14:30">14:30</option>
								<option value="14:15">14:15</option>
								<option value="14:00">14:00</option>
								<option value="13:45">13:45</option>
								<option value="13:30">13:30</option>
								<option value="13:15">13:15</option>
								<option value="13:00">13:00</option>
								<option value="12:45">12:45</option>
								<option value="12:30">12:30</option>
								<option value="12:15">12:15</option>
								<option value="12:00">12:00</option>
								<option value="11:45">11:45</option>
								<option value="11:30">11:30</option>
								<option value="11:15">11:15</option>
								<option value="11:00">11:00</option>
								<option value="10:45">10:45</option>
								<option value="10:30">10:30</option>
								<option value="10:15">10:15</option>
								<option value="10:00">10:00</option>
								<option value="09:45">09:45</option>
								<option value="09:30">09:30</option>
								<option value="09:15">09:15</option>
								<option value="09:00">09:00</option>
								<option value="08:45">08:45</option>
								<option value="08:30">08:30</option>
								<option value="08:15">08:15</option>
								<option value="08:00">08:00</option>
								<option value="07:45">07:45</option>
								<option value="07:30">07:30</option>
								<option value="07:15">07:15</option>
								<option value="07:00">07:00</option>
								<option value="06:45">06:45</option>
								<option value="06:30">06:30</option>
								<option value="06:15">06:15</option>
								<option value="06:00">06:00</option>
								<option value="05:45">05:45</option>
								<option value="05:30">05:30</option>
								<option value="05:15">05:15</option>
								<option value="05:00">05:00</option>
								<option value="04:45">04:45</option>
								<option value="04:30">04:30</option>
								<option value="04:15">04:15</option>
								<option value="04:00">04:00</option>
								<option value="03:45">03:45</option>
								<option value="03:30">03:30</option>
								<option value="03:15">03:15</option>
								<option value="03:00">03:00</option>
								<option value="02:45">02:45</option>
								<option value="02:30">02:30</option>
								<option value="02:15">02:15</option>
								<option value="02:00">02:00</option>
								<option value="01:45">01:45</option>
								<option value="01:30">01:30</option>
								<option value="01:15">01:15</option>
								<option value="01:00">01:00</option>
								<option value="00:45">00:45</option>
								<option value="00:30">00:30</option>
								<option value="00:15">00:15</option>
								<option value="00:00">00:00</option>
							</select>
						</div>
						<div class="col s12 m4">
							<label>Hora fim</label>
							<select name="horafim[]" id="horafimsex" class="horafimsex browser-default" disabled>
								<option value="" disabled selected>Escolha uma hora</option>
								<option value="23:45">23:45</option>
								<option value="23:30">23:30</option>
								<option value="23:15">23:15</option>
								<option value="23:00">23:00</option>
								<option value="22:45">22:45</option>
								<option value="22:30">22:30</option>
								<option value="22:15">22:15</option>
								<option value="22:00">22:00</option>
								<option value="21:45">21:45</option>
								<option value="21:30">21:30</option>
								<option value="21:15">21:15</option>
								<option value="21:00">21:00</option>
								<option value="20:45">20:45</option>
								<option value="20:30">20:30</option>
								<option value="20:15">20:15</option>
								<option value="20:00">20:00</option>
								<option value="19:45">19:45</option>
								<option value="19:30">19:30</option>
								<option value="19:15">19:15</option>
								<option value="19:00">19:00</option>
								<option value="18:45">18:45</option>
								<option value="18:30">18:30</option>
								<option value="18:15">18:15</option>
								<option value="18:00">18:00</option>
								<option value="17:45">17:45</option>
								<option value="17:30">17:30</option>
								<option value="17:15">17:15</option>
								<option value="17:00">17:00</option>
								<option value="16:45">16:45</option>
								<option value="16:30">16:30</option>
								<option value="16:15">16:15</option>
								<option value="16:00">16:00</option>
								<option value="15:45">15:45</option>
								<option value="15:30">15:30</option>
								<option value="15:15">15:15</option>
								<option value="15:00">15:00</option>
								<option value="14:45">14:45</option>
								<option value="14:30">14:30</option>
								<option value="14:15">14:15</option>
								<option value="14:00">14:00</option>
								<option value="13:45">13:45</option>
								<option value="13:30">13:30</option>
								<option value="13:15">13:15</option>
								<option value="13:00">13:00</option>
								<option value="12:45">12:45</option>
								<option value="12:30">12:30</option>
								<option value="12:15">12:15</option>
								<option value="12:00">12:00</option>
								<option value="11:45">11:45</option>
								<option value="11:30">11:30</option>
								<option value="11:15">11:15</option>
								<option value="11:00">11:00</option>
								<option value="10:45">10:45</option>
								<option value="10:30">10:30</option>
								<option value="10:15">10:15</option>
								<option value="10:00">10:00</option>
								<option value="09:45">09:45</option>
								<option value="09:30">09:30</option>
								<option value="09:15">09:15</option>
								<option value="09:00">09:00</option>
								<option value="08:45">08:45</option>
								<option value="08:30">08:30</option>
								<option value="08:15">08:15</option>
								<option value="08:00">08:00</option>
								<option value="07:45">07:45</option>
								<option value="07:30">07:30</option>
								<option value="07:15">07:15</option>
								<option value="07:00">07:00</option>
								<option value="06:45">06:45</option>
								<option value="06:30">06:30</option>
								<option value="06:15">06:15</option>
								<option value="06:00">06:00</option>
								<option value="05:45">05:45</option>
								<option value="05:30">05:30</option>
								<option value="05:15">05:15</option>
								<option value="05:00">05:00</option>
								<option value="04:45">04:45</option>
								<option value="04:30">04:30</option>
								<option value="04:15">04:15</option>
								<option value="04:00">04:00</option>
								<option value="03:45">03:45</option>
								<option value="03:30">03:30</option>
								<option value="03:15">03:15</option>
								<option value="03:00">03:00</option>
								<option value="02:45">02:45</option>
								<option value="02:30">02:30</option>
								<option value="02:15">02:15</option>
								<option value="02:00">02:00</option>
								<option value="01:45">01:45</option>
								<option value="01:30">01:30</option>
								<option value="01:15">01:15</option>
								<option value="01:00">01:00</option>
								<option value="00:45">00:45</option>
								<option value="00:30">00:30</option>
								<option value="00:15">00:15</option>
								<option value="00:00">00:00</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m4">
							<label>
								<input id="qui" name="dia[]" type="checkbox" class="qui with-gap" value="QUI">
								<span>Quinta</span>
							</label>
						</div>
						<div class="col s12 m4">
							<label>Hora início</label>
							<select name="horainicio[]" id="horainicioqui" class="horainicioqui browser-default" disabled>
								<option value="" disabled selected>Escolha uma hora</option>
								<option value="23:45">23:45</option>
								<option value="23:30">23:30</option>
								<option value="23:15">23:15</option>
								<option value="23:00">23:00</option>
								<option value="22:45">22:45</option>
								<option value="22:30">22:30</option>
								<option value="22:15">22:15</option>
								<option value="22:00">22:00</option>
								<option value="21:45">21:45</option>
								<option value="21:30">21:30</option>
								<option value="21:15">21:15</option>
								<option value="21:00">21:00</option>
								<option value="20:45">20:45</option>
								<option value="20:30">20:30</option>
								<option value="20:15">20:15</option>
								<option value="20:00">20:00</option>
								<option value="19:45">19:45</option>
								<option value="19:30">19:30</option>
								<option value="19:15">19:15</option>
								<option value="19:00">19:00</option>
								<option value="18:45">18:45</option>
								<option value="18:30">18:30</option>
								<option value="18:15">18:15</option>
								<option value="18:00">18:00</option>
								<option value="17:45">17:45</option>
								<option value="17:30">17:30</option>
								<option value="17:15">17:15</option>
								<option value="17:00">17:00</option>
								<option value="16:45">16:45</option>
								<option value="16:30">16:30</option>
								<option value="16:15">16:15</option>
								<option value="16:00">16:00</option>
								<option value="15:45">15:45</option>
								<option value="15:30">15:30</option>
								<option value="15:15">15:15</option>
								<option value="15:00">15:00</option>
								<option value="14:45">14:45</option>
								<option value="14:30">14:30</option>
								<option value="14:15">14:15</option>
								<option value="14:00">14:00</option>
								<option value="13:45">13:45</option>
								<option value="13:30">13:30</option>
								<option value="13:15">13:15</option>
								<option value="13:00">13:00</option>
								<option value="12:45">12:45</option>
								<option value="12:30">12:30</option>
								<option value="12:15">12:15</option>
								<option value="12:00">12:00</option>
								<option value="11:45">11:45</option>
								<option value="11:30">11:30</option>
								<option value="11:15">11:15</option>
								<option value="11:00">11:00</option>
								<option value="10:45">10:45</option>
								<option value="10:30">10:30</option>
								<option value="10:15">10:15</option>
								<option value="10:00">10:00</option>
								<option value="09:45">09:45</option>
								<option value="09:30">09:30</option>
								<option value="09:15">09:15</option>
								<option value="09:00">09:00</option>
								<option value="08:45">08:45</option>
								<option value="08:30">08:30</option>
								<option value="08:15">08:15</option>
								<option value="08:00">08:00</option>
								<option value="07:45">07:45</option>
								<option value="07:30">07:30</option>
								<option value="07:15">07:15</option>
								<option value="07:00">07:00</option>
								<option value="06:45">06:45</option>
								<option value="06:30">06:30</option>
								<option value="06:15">06:15</option>
								<option value="06:00">06:00</option>
								<option value="05:45">05:45</option>
								<option value="05:30">05:30</option>
								<option value="05:15">05:15</option>
								<option value="05:00">05:00</option>
								<option value="04:45">04:45</option>
								<option value="04:30">04:30</option>
								<option value="04:15">04:15</option>
								<option value="04:00">04:00</option>
								<option value="03:45">03:45</option>
								<option value="03:30">03:30</option>
								<option value="03:15">03:15</option>
								<option value="03:00">03:00</option>
								<option value="02:45">02:45</option>
								<option value="02:30">02:30</option>
								<option value="02:15">02:15</option>
								<option value="02:00">02:00</option>
								<option value="01:45">01:45</option>
								<option value="01:30">01:30</option>
								<option value="01:15">01:15</option>
								<option value="01:00">01:00</option>
								<option value="00:45">00:45</option>
								<option value="00:30">00:30</option>
								<option value="00:15">00:15</option>
								<option value="00:00">00:00</option>
							</select>
						</div>
						<div class="col s12 m4">
							<label>Hora fim</label>
							<select name="horafim[]" id="horafimqui" class="horafimqui browser-default" disabled>
								<option value="" disabled selected>Escolha uma hora</option>
								<option value="23:45">23:45</option>
								<option value="23:30">23:30</option>
								<option value="23:15">23:15</option>
								<option value="23:00">23:00</option>
								<option value="22:45">22:45</option>
								<option value="22:30">22:30</option>
								<option value="22:15">22:15</option>
								<option value="22:00">22:00</option>
								<option value="21:45">21:45</option>
								<option value="21:30">21:30</option>
								<option value="21:15">21:15</option>
								<option value="21:00">21:00</option>
								<option value="20:45">20:45</option>
								<option value="20:30">20:30</option>
								<option value="20:15">20:15</option>
								<option value="20:00">20:00</option>
								<option value="19:45">19:45</option>
								<option value="19:30">19:30</option>
								<option value="19:15">19:15</option>
								<option value="19:00">19:00</option>
								<option value="18:45">18:45</option>
								<option value="18:30">18:30</option>
								<option value="18:15">18:15</option>
								<option value="18:00">18:00</option>
								<option value="17:45">17:45</option>
								<option value="17:30">17:30</option>
								<option value="17:15">17:15</option>
								<option value="17:00">17:00</option>
								<option value="16:45">16:45</option>
								<option value="16:30">16:30</option>
								<option value="16:15">16:15</option>
								<option value="16:00">16:00</option>
								<option value="15:45">15:45</option>
								<option value="15:30">15:30</option>
								<option value="15:15">15:15</option>
								<option value="15:00">15:00</option>
								<option value="14:45">14:45</option>
								<option value="14:30">14:30</option>
								<option value="14:15">14:15</option>
								<option value="14:00">14:00</option>
								<option value="13:45">13:45</option>
								<option value="13:30">13:30</option>
								<option value="13:15">13:15</option>
								<option value="13:00">13:00</option>
								<option value="12:45">12:45</option>
								<option value="12:30">12:30</option>
								<option value="12:15">12:15</option>
								<option value="12:00">12:00</option>
								<option value="11:45">11:45</option>
								<option value="11:30">11:30</option>
								<option value="11:15">11:15</option>
								<option value="11:00">11:00</option>
								<option value="10:45">10:45</option>
								<option value="10:30">10:30</option>
								<option value="10:15">10:15</option>
								<option value="10:00">10:00</option>
								<option value="09:45">09:45</option>
								<option value="09:30">09:30</option>
								<option value="09:15">09:15</option>
								<option value="09:00">09:00</option>
								<option value="08:45">08:45</option>
								<option value="08:30">08:30</option>
								<option value="08:15">08:15</option>
								<option value="08:00">08:00</option>
								<option value="07:45">07:45</option>
								<option value="07:30">07:30</option>
								<option value="07:15">07:15</option>
								<option value="07:00">07:00</option>
								<option value="06:45">06:45</option>
								<option value="06:30">06:30</option>
								<option value="06:15">06:15</option>
								<option value="06:00">06:00</option>
								<option value="05:45">05:45</option>
								<option value="05:30">05:30</option>
								<option value="05:15">05:15</option>
								<option value="05:00">05:00</option>
								<option value="04:45">04:45</option>
								<option value="04:30">04:30</option>
								<option value="04:15">04:15</option>
								<option value="04:00">04:00</option>
								<option value="03:45">03:45</option>
								<option value="03:30">03:30</option>
								<option value="03:15">03:15</option>
								<option value="03:00">03:00</option>
								<option value="02:45">02:45</option>
								<option value="02:30">02:30</option>
								<option value="02:15">02:15</option>
								<option value="02:00">02:00</option>
								<option value="01:45">01:45</option>
								<option value="01:30">01:30</option>
								<option value="01:15">01:15</option>
								<option value="01:00">01:00</option>
								<option value="00:45">00:45</option>
								<option value="00:30">00:30</option>
								<option value="00:15">00:15</option>
								<option value="00:00">00:00</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m4">
							<label>
								<input id="qua" name="dia[]" type="checkbox" class="qua with-gap" value="QUA">
								<span>Quarta</span>
							</label>
						</div>
						<div class="col s12 m4">
							<label>Hora início</label>
							<select name="horainicio[]" id="horainicioqua" class="horainicioqua browser-default" disabled>
								<option value="" disabled selected>Escolha uma hora</option>
								<option value="23:45">23:45</option>
								<option value="23:30">23:30</option>
								<option value="23:15">23:15</option>
								<option value="23:00">23:00</option>
								<option value="22:45">22:45</option>
								<option value="22:30">22:30</option>
								<option value="22:15">22:15</option>
								<option value="22:00">22:00</option>
								<option value="21:45">21:45</option>
								<option value="21:30">21:30</option>
								<option value="21:15">21:15</option>
								<option value="21:00">21:00</option>
								<option value="20:45">20:45</option>
								<option value="20:30">20:30</option>
								<option value="20:15">20:15</option>
								<option value="20:00">20:00</option>
								<option value="19:45">19:45</option>
								<option value="19:30">19:30</option>
								<option value="19:15">19:15</option>
								<option value="19:00">19:00</option>
								<option value="18:45">18:45</option>
								<option value="18:30">18:30</option>
								<option value="18:15">18:15</option>
								<option value="18:00">18:00</option>
								<option value="17:45">17:45</option>
								<option value="17:30">17:30</option>
    							<option value="17:15">17:15</option>
								<option value="17:00">17:00</option>
								<option value="16:45">16:45</option>
								<option value="16:30">16:30</option>
								<option value="16:15">16:15</option>
								<option value="16:00">16:00</option>
								<option value="15:45">15:45</option>
								<option value="15:30">15:30</option>
								<option value="15:15">15:15</option>
								<option value="15:00">15:00</option>
								<option value="14:45">14:45</option>
								<option value="14:30">14:30</option>
								<option value="14:15">14:15</option>
								<option value="14:00">14:00</option>
								<option value="13:45">13:45</option>
								<option value="13:30">13:30</option>
								<option value="13:15">13:15</option>
								<option value="13:00">13:00</option>
								<option value="12:45">12:45</option>
								<option value="12:30">12:30</option>
								<option value="12:15">12:15</option>
								<option value="12:00">12:00</option>
								<option value="11:45">11:45</option>
								<option value="11:30">11:30</option>
								<option value="11:15">11:15</option>
								<option value="11:00">11:00</option>
								<option value="10:45">10:45</option>
								<option value="10:30">10:30</option>
								<option value="10:15">10:15</option>
								<option value="10:00">10:00</option>
								<option value="09:45">09:45</option>
								<option value="09:30">09:30</option>
								<option value="09:15">09:15</option>
								<option value="09:00">09:00</option>
								<option value="08:45">08:45</option>
								<option value="08:30">08:30</option>
								<option value="08:15">08:15</option>
								<option value="08:00">08:00</option>
								<option value="07:45">07:45</option>
								<option value="07:30">07:30</option>
								<option value="07:15">07:15</option>
								<option value="07:00">07:00</option>
								<option value="06:45">06:45</option>
								<option value="06:30">06:30</option>
								<option value="06:15">06:15</option>
								<option value="06:00">06:00</option>
								<option value="05:45">05:45</option>
								<option value="05:30">05:30</option>
								<option value="05:15">05:15</option>
								<option value="05:00">05:00</option>
								<option value="04:45">04:45</option>
								<option value="04:30">04:30</option>
								<option value="04:15">04:15</option>
								<option value="04:00">04:00</option>
								<option value="03:45">03:45</option>
								<option value="03:30">03:30</option>
								<option value="03:15">03:15</option>
								<option value="03:00">03:00</option>
								<option value="02:45">02:45</option>
								<option value="02:30">02:30</option>
								<option value="02:15">02:15</option>
								<option value="02:00">02:00</option>
								<option value="01:45">01:45</option>
								<option value="01:30">01:30</option>
								<option value="01:15">01:15</option>
								<option value="01:00">01:00</option>
								<option value="00:45">00:45</option>
								<option value="00:30">00:30</option>
								<option value="00:15">00:15</option>
								<option value="00:00">00:00</option>
							</select>
						</div>
						<div class="col s12 m4">
							<label>Hora fim</label>
							<select name="horafim[]" id="horafimqua" class="horafimqua browser-default" disabled>
								<option value="" disabled selected>Escolha uma hora</option>
								<option value="23:45">23:45</option>
								<option value="23:30">23:30</option>
								<option value="23:15">23:15</option>
								<option value="23:00">23:00</option>
								<option value="22:45">22:45</option>
								<option value="22:30">22:30</option>
								<option value="22:15">22:15</option>
								<option value="22:00">22:00</option>
								<option value="21:45">21:45</option>
								<option value="21:30">21:30</option>
								<option value="21:15">21:15</option>
								<option value="21:00">21:00</option>
								<option value="20:45">20:45</option>
								<option value="20:30">20:30</option>
								<option value="20:15">20:15</option>
								<option value="20:00">20:00</option>
								<option value="19:45">19:45</option>
								<option value="19:30">19:30</option>
								<option value="19:15">19:15</option>
								<option value="19:00">19:00</option>
								<option value="18:45">18:45</option>
								<option value="18:30">18:30</option>
								<option value="18:15">18:15</option>
								<option value="18:00">18:00</option>
								<option value="17:45">17:45</option>
								<option value="17:30">17:30</option>
								<option value="17:15">17:15</option>
								<option value="17:00">17:00</option>
								<option value="16:45">16:45</option>
								<option value="16:30">16:30</option>
								<option value="16:15">16:15</option>
								<option value="16:00">16:00</option>
								<option value="15:45">15:45</option>
								<option value="15:30">15:30</option>
								<option value="15:15">15:15</option>
								<option value="15:00">15:00</option>
								<option value="14:45">14:45</option>
								<option value="14:30">14:30</option>
								<option value="14:15">14:15</option>
								<option value="14:00">14:00</option>
								<option value="13:45">13:45</option>
								<option value="13:30">13:30</option>
								<option value="13:15">13:15</option>
								<option value="13:00">13:00</option>
								<option value="12:45">12:45</option>
								<option value="12:30">12:30</option>
								<option value="12:15">12:15</option>
								<option value="12:00">12:00</option>
								<option value="11:45">11:45</option>
								<option value="11:30">11:30</option>
								<option value="11:15">11:15</option>
								<option value="11:00">11:00</option>
								<option value="10:45">10:45</option>
								<option value="10:30">10:30</option>
								<option value="10:15">10:15</option>
								<option value="10:00">10:00</option>
								<option value="09:45">09:45</option>
								<option value="09:30">09:30</option>
								<option value="09:15">09:15</option>
								<option value="09:00">09:00</option>
								<option value="08:45">08:45</option>
								<option value="08:30">08:30</option>
								<option value="08:15">08:15</option>
								<option value="08:00">08:00</option>
								<option value="07:45">07:45</option>
								<option value="07:30">07:30</option>
								<option value="07:15">07:15</option>
								<option value="07:00">07:00</option>
								<option value="06:45">06:45</option>
								<option value="06:30">06:30</option>
								<option value="06:15">06:15</option>
								<option value="06:00">06:00</option>
								<option value="05:45">05:45</option>
								<option value="05:30">05:30</option>
								<option value="05:15">05:15</option>
								<option value="05:00">05:00</option>
								<option value="04:45">04:45</option>
								<option value="04:30">04:30</option>
								<option value="04:15">04:15</option>
								<option value="04:00">04:00</option>
								<option value="03:45">03:45</option>
								<option value="03:30">03:30</option>
								<option value="03:15">03:15</option>
								<option value="03:00">03:00</option>
								<option value="02:45">02:45</option>
								<option value="02:30">02:30</option>
								<option value="02:15">02:15</option>
								<option value="02:00">02:00</option>
								<option value="01:45">01:45</option>
								<option value="01:30">01:30</option>
								<option value="01:15">01:15</option>
								<option value="01:00">01:00</option>
								<option value="00:45">00:45</option>
								<option value="00:30">00:30</option>
								<option value="00:15">00:15</option>
								<option value="00:00">00:00</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m4">
							<label>
								<input id="ter" name="dia[]" type="checkbox" class="ter with-gap" value="TER">
								<span>Terça</span>
							</label>
						</div>
						<div class="col s12 m4">
							<label>Hora início</label>
							<select name="horainicio[]" id="horainicioter" class="horainicioter browser-default" disabled>
								<option value="" disabled selected>Escolha uma hora</option>
								<option value="23:45">23:45</option>
								<option value="23:30">23:30</option>
								<option value="23:15">23:15</option>
								<option value="23:00">23:00</option>
								<option value="22:45">22:45</option>
								<option value="22:30">22:30</option>
								<option value="22:15">22:15</option>
								<option value="22:00">22:00</option>
								<option value="21:45">21:45</option>
								<option value="21:30">21:30</option>
								<option value="21:15">21:15</option>
								<option value="21:00">21:00</option>
								<option value="20:45">20:45</option>
								<option value="20:30">20:30</option>
								<option value="20:15">20:15</option>
								<option value="20:00">20:00</option>
								<option value="19:45">19:45</option>
								<option value="19:30">19:30</option>
								<option value="19:15">19:15</option>
								<option value="19:00">19:00</option>
								<option value="18:45">18:45</option>
								<option value="18:30">18:30</option>
								<option value="18:15">18:15</option>
								<option value="18:00">18:00</option>
								<option value="17:45">17:45</option>
								<option value="17:30">17:30</option>
								<option value="17:15">17:15</option>
								<option value="17:00">17:00</option>
								<option value="16:45">16:45</option>
								<option value="16:30">16:30</option>
								<option value="16:15">16:15</option>
								<option value="16:00">16:00</option>
								<option value="15:45">15:45</option>
								<option value="15:30">15:30</option>
								<option value="15:15">15:15</option>
								<option value="15:00">15:00</option>
								<option value="14:45">14:45</option>
								<option value="14:30">14:30</option>
								<option value="14:15">14:15</option>
								<option value="14:00">14:00</option>
								<option value="13:45">13:45</option>
								<option value="13:30">13:30</option>
								<option value="13:15">13:15</option>
								<option value="13:00">13:00</option>
								<option value="12:45">12:45</option>
								<option value="12:30">12:30</option>
								<option value="12:15">12:15</option>
								<option value="12:00">12:00</option>
								<option value="11:45">11:45</option>
								<option value="11:30">11:30</option>
								<option value="11:15">11:15</option>
								<option value="11:00">11:00</option>
								<option value="10:45">10:45</option>
								<option value="10:30">10:30</option>
								<option value="10:15">10:15</option>
								<option value="10:00">10:00</option>
								<option value="09:45">09:45</option>
								<option value="09:30">09:30</option>
								<option value="09:15">09:15</option>
								<option value="09:00">09:00</option>
								<option value="08:45">08:45</option>
								<option value="08:30">08:30</option>
								<option value="08:15">08:15</option>
								<option value="08:00">08:00</option>
								<option value="07:45">07:45</option>
								<option value="07:30">07:30</option>
								<option value="07:15">07:15</option>
								<option value="07:00">07:00</option>
								<option value="06:45">06:45</option>
								<option value="06:30">06:30</option>
								<option value="06:15">06:15</option>
								<option value="06:00">06:00</option>
								<option value="05:45">05:45</option>
								<option value="05:30">05:30</option>
								<option value="05:15">05:15</option>
								<option value="05:00">05:00</option>
								<option value="04:45">04:45</option>
								<option value="04:30">04:30</option>
								<option value="04:15">04:15</option>
								<option value="04:00">04:00</option>
								<option value="03:45">03:45</option>
								<option value="03:30">03:30</option>
								<option value="03:15">03:15</option>
								<option value="03:00">03:00</option>
								<option value="02:45">02:45</option>
								<option value="02:30">02:30</option>
								<option value="02:15">02:15</option>
								<option value="02:00">02:00</option>
								<option value="01:45">01:45</option>
								<option value="01:30">01:30</option>
								<option value="01:15">01:15</option>
								<option value="01:00">01:00</option>
								<option value="00:45">00:45</option>
								<option value="00:30">00:30</option>
								<option value="00:15">00:15</option>
								<option value="00:00">00:00</option>
							</select>
						</div>
						<div class="col s12 m4">
							<label>Hora fim</label>
							<select name="horafim[]" id="horafimter" class="horafimter browser-default" disabled>
								<option value="" disabled selected>Escolha uma hora</option>
								<option value="23:45">23:45</option>
								<option value="23:30">23:30</option>
								<option value="23:15">23:15</option>
								<option value="23:00">23:00</option>
								<option value="22:45">22:45</option>
								<option value="22:30">22:30</option>
								<option value="22:15">22:15</option>
								<option value="22:00">22:00</option>
								<option value="21:45">21:45</option>
								<option value="21:30">21:30</option>
								<option value="21:15">21:15</option>
								<option value="21:00">21:00</option>
								<option value="20:45">20:45</option>
								<option value="20:30">20:30</option>
								<option value="20:15">20:15</option>
								<option value="20:00">20:00</option>
								<option value="19:45">19:45</option>
								<option value="19:30">19:30</option>
								<option value="19:15">19:15</option>
								<option value="19:00">19:00</option>
								<option value="18:45">18:45</option>
								<option value="18:30">18:30</option>
								<option value="18:15">18:15</option>
								<option value="18:00">18:00</option>
								<option value="17:45">17:45</option>
								<option value="17:30">17:30</option>
								<option value="17:15">17:15</option>
								<option value="17:00">17:00</option>
								<option value="16:45">16:45</option>
								<option value="16:30">16:30</option>
								<option value="16:15">16:15</option>
								<option value="16:00">16:00</option>
								<option value="15:45">15:45</option>
								<option value="15:30">15:30</option>
								<option value="15:15">15:15</option>
								<option value="15:00">15:00</option>
								<option value="14:45">14:45</option>
								<option value="14:30">14:30</option>
								<option value="14:15">14:15</option>
								<option value="14:00">14:00</option>
								<option value="13:45">13:45</option>
								<option value="13:30">13:30</option>
								<option value="13:15">13:15</option>
								<option value="13:00">13:00</option>
								<option value="12:45">12:45</option>
								<option value="12:30">12:30</option>
								<option value="12:15">12:15</option>
								<option value="12:00">12:00</option>
								<option value="11:45">11:45</option>
								<option value="11:30">11:30</option>
								<option value="11:15">11:15</option>
								<option value="11:00">11:00</option>
								<option value="10:45">10:45</option>
								<option value="10:30">10:30</option>
								<option value="10:15">10:15</option>
								<option value="10:00">10:00</option>
								<option value="09:45">09:45</option>
								<option value="09:30">09:30</option>
								<option value="09:15">09:15</option>
								<option value="09:00">09:00</option>
								<option value="08:45">08:45</option>
								<option value="08:30">08:30</option>
								<option value="08:15">08:15</option>
								<option value="08:00">08:00</option>
								<option value="07:45">07:45</option>
								<option value="07:30">07:30</option>
								<option value="07:15">07:15</option>
								<option value="07:00">07:00</option>
								<option value="06:45">06:45</option>
								<option value="06:30">06:30</option>
								<option value="06:15">06:15</option>
								<option value="06:00">06:00</option>
								<option value="05:45">05:45</option>
								<option value="05:30">05:30</option>
								<option value="05:15">05:15</option>
								<option value="05:00">05:00</option>
								<option value="04:45">04:45</option>
								<option value="04:30">04:30</option>
								<option value="04:15">04:15</option>
								<option value="04:00">04:00</option>
								<option value="03:45">03:45</option>
								<option value="03:30">03:30</option>
								<option value="03:15">03:15</option>
								<option value="03:00">03:00</option>
								<option value="02:45">02:45</option>
								<option value="02:30">02:30</option>
								<option value="02:15">02:15</option>
								<option value="02:00">02:00</option>
								<option value="01:45">01:45</option>
								<option value="01:30">01:30</option>
								<option value="01:15">01:15</option>
								<option value="01:00">01:00</option>
								<option value="00:45">00:45</option>
								<option value="00:30">00:30</option>
								<option value="00:15">00:15</option>
								<option value="00:00">00:00</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m4">
							<label>
								<input id="seg" name="dia[]" type="checkbox" class="seg with-gap" value="SEG">
								<span>Segunda</span>
							</label>
						</div>
						<div class="col s12 m4">
							<label>Hora início</label>
							<select name="horainicio[]" id="horainicioseg" class="horainicioseg browser-default" disabled>
								<option value="" disabled selected>Escolha uma hora</option>
								<option value="23:45">23:45</option>
								<option value="23:30">23:30</option>
								<option value="23:15">23:15</option>
								<option value="23:00">23:00</option>
								<option value="22:45">22:45</option>
								<option value="22:30">22:30</option>
								<option value="22:15">22:15</option>
								<option value="22:00">22:00</option>
								<option value="21:45">21:45</option>
								<option value="21:30">21:30</option>
								<option value="21:15">21:15</option>
								<option value="21:00">21:00</option>
								<option value="20:45">20:45</option>
								<option value="20:30">20:30</option>
								<option value="20:15">20:15</option>
								<option value="20:00">20:00</option>
								<option value="19:45">19:45</option>
								<option value="19:30">19:30</option>
								<option value="19:15">19:15</option>
								<option value="19:00">19:00</option>
								<option value="18:45">18:45</option>
								<option value="18:30">18:30</option>
								<option value="18:15">18:15</option>
								<option value="18:00">18:00</option>
								<option value="17:45">17:45</option>
								<option value="17:30">17:30</option>
								<option value="17:15">17:15</option>
								<option value="17:00">17:00</option>
								<option value="16:45">16:45</option>
								<option value="16:30">16:30</option>
								<option value="16:15">16:15</option>
								<option value="16:00">16:00</option>
								<option value="15:45">15:45</option>
								<option value="15:30">15:30</option>
								<option value="15:15">15:15</option>
								<option value="15:00">15:00</option>
								<option value="14:45">14:45</option>
								<option value="14:30">14:30</option>
								<option value="14:15">14:15</option>
								<option value="14:00">14:00</option>
								<option value="13:45">13:45</option>
								<option value="13:30">13:30</option>
								<option value="13:15">13:15</option>
								<option value="13:00">13:00</option>
								<option value="12:45">12:45</option>
								<option value="12:30">12:30</option>
								<option value="12:15">12:15</option>
								<option value="12:00">12:00</option>
								<option value="11:45">11:45</option>
								<option value="11:30">11:30</option>
								<option value="11:15">11:15</option>
								<option value="11:00">11:00</option>
								<option value="10:45">10:45</option>
								<option value="10:30">10:30</option>
								<option value="10:15">10:15</option>
								<option value="10:00">10:00</option>
								<option value="09:45">09:45</option>
								<option value="09:30">09:30</option>
								<option value="09:15">09:15</option>
								<option value="09:00">09:00</option>
								<option value="08:45">08:45</option>
								<option value="08:30">08:30</option>
								<option value="08:15">08:15</option>
								<option value="08:00">08:00</option>
								<option value="07:45">07:45</option>
								<option value="07:30">07:30</option>
								<option value="07:15">07:15</option>
								<option value="07:00">07:00</option>
								<option value="06:45">06:45</option>
								<option value="06:30">06:30</option>
								<option value="06:15">06:15</option>
								<option value="06:00">06:00</option>
								<option value="05:45">05:45</option>
								<option value="05:30">05:30</option>
								<option value="05:15">05:15</option>
								<option value="05:00">05:00</option>
								<option value="04:45">04:45</option>
								<option value="04:30">04:30</option>
								<option value="04:15">04:15</option>
								<option value="04:00">04:00</option>
								<option value="03:45">03:45</option>
								<option value="03:30">03:30</option>
								<option value="03:15">03:15</option>
								<option value="03:00">03:00</option>
								<option value="02:45">02:45</option>
								<option value="02:30">02:30</option>
								<option value="02:15">02:15</option>
								<option value="02:00">02:00</option>
								<option value="01:45">01:45</option>
								<option value="01:30">01:30</option>
								<option value="01:15">01:15</option>
								<option value="01:00">01:00</option>
								<option value="00:45">00:45</option>
								<option value="00:30">00:30</option>
								<option value="00:15">00:15</option>
								<option value="00:00">00:00</option>
							</select>
						</div>
						<div class="col s12 m4">
							<label>Hora fim</label>
							<select name="horafim[]" id="horafimseg" class="horafimseg browser-default" disabled>
								<option value="" disabled selected>Escolha uma hora</option>
								<option value="23:45">23:45</option>
								<option value="23:30">23:30</option>
								<option value="23:15">23:15</option>
								<option value="23:00">23:00</option>
								<option value="22:45">22:45</option>
								<option value="22:30">22:30</option>
								<option value="22:15">22:15</option>
								<option value="22:00">22:00</option>
								<option value="21:45">21:45</option>
								<option value="21:30">21:30</option>
								<option value="21:15">21:15</option>
								<option value="21:00">21:00</option>
								<option value="20:45">20:45</option>
								<option value="20:30">20:30</option>
								<option value="20:15">20:15</option>
								<option value="20:00">20:00</option>
								<option value="19:45">19:45</option>
								<option value="19:30">19:30</option>
								<option value="19:15">19:15</option>
								<option value="19:00">19:00</option>
								<option value="18:45">18:45</option>
								<option value="18:30">18:30</option>
								<option value="18:15">18:15</option>
								<option value="18:00">18:00</option>
								<option value="17:45">17:45</option>
								<option value="17:30">17:30</option>
								<option value="17:15">17:15</option>
								<option value="17:00">17:00</option>
								<option value="16:45">16:45</option>
								<option value="16:30">16:30</option>
								<option value="16:15">16:15</option>
								<option value="16:00">16:00</option>
								<option value="15:45">15:45</option>
								<option value="15:30">15:30</option>
								<option value="15:15">15:15</option>
								<option value="15:00">15:00</option>
								<option value="14:45">14:45</option>
								<option value="14:30">14:30</option>
								<option value="14:15">14:15</option>
								<option value="14:00">14:00</option>
								<option value="13:45">13:45</option>
								<option value="13:30">13:30</option>
								<option value="13:15">13:15</option>
								<option value="13:00">13:00</option>
								<option value="12:45">12:45</option>
								<option value="12:30">12:30</option>
								<option value="12:15">12:15</option>
								<option value="12:00">12:00</option>
								<option value="11:45">11:45</option>
								<option value="11:30">11:30</option>
								<option value="11:15">11:15</option>
								<option value="11:00">11:00</option>
								<option value="10:45">10:45</option>
								<option value="10:30">10:30</option>
								<option value="10:15">10:15</option>
								<option value="10:00">10:00</option>
								<option value="09:45">09:45</option>
								<option value="09:30">09:30</option>
								<option value="09:15">09:15</option>
								<option value="09:00">09:00</option>
								<option value="08:45">08:45</option>
								<option value="08:30">08:30</option>
								<option value="08:15">08:15</option>
								<option value="08:00">08:00</option>
								<option value="07:45">07:45</option>
								<option value="07:30">07:30</option>
								<option value="07:15">07:15</option>
								<option value="07:00">07:00</option>
								<option value="06:45">06:45</option>
								<option value="06:30">06:30</option>
								<option value="06:15">06:15</option>
								<option value="06:00">06:00</option>
								<option value="05:45">05:45</option>
								<option value="05:30">05:30</option>
								<option value="05:15">05:15</option>
								<option value="05:00">05:00</option>
								<option value="04:45">04:45</option>
								<option value="04:30">04:30</option>
								<option value="04:15">04:15</option>
								<option value="04:00">04:00</option>
								<option value="03:45">03:45</option>
								<option value="03:30">03:30</option>
								<option value="03:15">03:15</option>
								<option value="03:00">03:00</option>
								<option value="02:45">02:45</option>
								<option value="02:30">02:30</option>
								<option value="02:15">02:15</option>
								<option value="02:00">02:00</option>
								<option value="01:45">01:45</option>
								<option value="01:30">01:30</option>
								<option value="01:15">01:15</option>
								<option value="01:00">01:00</option>
								<option value="00:45">00:45</option>
								<option value="00:30">00:30</option>
								<option value="00:15">00:15</option>
								<option value="00:00">00:00</option>
							</select>
						</div>
					</div>				
						
					<div class="row">    
				        <div class="col s12">
							<button class="btn blue darken-1 add-disponibilidade-sistema full-width" type="submit">
								Salvar disponibilidade
								<i class="material-icons right">chevron_right</i>	
						    </button>
				        </div>
					</div>
				</form>

			</div>
		</div>
	</div>
	
</main>

@stop

@section('extra-javascript')
<script type="text/javascript">
	$('input[type=checkbox]').click(function() {
		if ($('.dom').is(':checked')) {
			$(".horainiciodom").attr('disabled', false);
			$(".horafimdom").attr('disabled', false);
			$(".horainiciodom").attr('required', true);
			$(".horafimdom").attr('required', true);
		} else {
			$(".horainiciodom").attr('disabled', true);
			$(".horafimdom").attr('disabled', true);
			$(".horainiciodom").val('');
			$(".horafimdom").val('');
		}
		if ($('.sab').is(':checked')) {
			$(".horainiciosab").attr('disabled', false);
			$(".horafimsab").attr('disabled', false);
			$(".horainiciosab").attr('required', true);
			$(".horafimsab").attr('required', true);
		} else {
			$(".horainiciosab").attr('disabled', true);
			$(".horafimsab").attr('disabled', true);
			$(".horainiciosab").val('');
			$(".horafimsab").val('');
		}
		if ($('.sex').is(':checked')) {
			$(".horainiciosex").attr('disabled', false);
			$(".horafimsex").attr('disabled', false);
			$(".horainiciosex").attr('required', true);
			$(".horafimsex").attr('required', true);
		} else {
			$(".horainiciosex").attr('disabled', true);
			$(".horafimsex").attr('disabled', true);
			$(".horainiciosex").val('');
			$(".horafimsex").val('');
		}
		if ($('.qui').is(':checked')) {
			$(".horainicioqui").attr('disabled', false);
			$(".horafimqui").attr('disabled', false);
			$(".horainicioqui").attr('required', true);
			$(".horafimqui").attr('required', true);
		} else {
			$(".horainicioqui").attr('disabled', true);
			$(".horafimqui").attr('disabled', true);
			$(".horainicioqui").val('');
			$(".horafimqui").val('');
		}
		if ($('.qua').is(':checked')) {
			$(".horainicioqua").attr('disabled', false);
			$(".horafimqua").attr('disabled', false);
			$(".horainicioqua").attr('required', true);
			$(".horafimqua").attr('required', true);
		} else {
			$(".horainicioqua").attr('disabled', true);
			$(".horafimqua").attr('disabled', true);
			$(".horainicioqua").val('');
			$(".horafimqua").val('');
		}
		if ($('.ter').is(':checked')) {
			$(".horainicioter").attr('disabled', false);
			$(".horafimter").attr('disabled', false);
			$(".horainicioter").attr('required', true);
			$(".horafimter").attr('required', true);
		} else {
			$(".horainicioter").attr('disabled', true);
			$(".horafimter").attr('disabled', true);
			$(".horainicioter").val('');
			$(".horafimter").val('');
		}
		if ($('.seg').is(':checked')) {
			$(".horainicioseg").attr('disabled', false);
			$(".horafimseg").attr('disabled', false);
			$(".horainicioseg").attr('required', true);
			$(".horafimseg").attr('required', true);
		} else {
			$(".horainicioseg").attr('disabled', true);
			$(".horafimseg").attr('disabled', true);
			$(".horainicioseg").val('');
			$(".horafimseg").val('');
		}
	});
</script>
@stop

