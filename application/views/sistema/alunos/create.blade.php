@extends('template.base-sistema')

@section('content')

<main>
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h1 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Novo
			</h1>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('sistema/alunos/insert') }}" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Aluno</h5>
						</div>	
					</div>
					
					<div class="row">	
						<div class="col s12 m6">
				          	<label>Nome <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="nome" name="nome" type="text" class="validate browser-default" required="required">
				        </div>
				        <div class="col s12 m6">
				          	<label>E-mail <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="email" name="email" data-id="" type="text" class="validate browser-default" required="required">
				        </div>
				     </div>
				     <div class="row">   
				        <div class="col s12 m6">
				          	<label>Senha <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="hash_senha" name="hash_senha" class="validate browser-default" type="password" required="required">
				        </div>
						<div class="col s12 m6">
							<label>Status <span style="font-weight: bold; color: red;">*</span></label>
							<select name="status" id="status" class="browser-default" required="required">
								<option value="0" selected>Inativo</option>
								<option value="1">Ativo</option>
							</select>
						</div>
				    </div>
					<div class="row">   
				        <div class="col s12 m6">
							<label>Nível <span style="font-weight: bold; color: red;">*</span></label>
							<select name="nivel" id="nivel" class="browser-default" required="required">
								<option value="Administrador" selected>Administrador</option>
								<option value="Usuario">Usuário</option>
							</select>
						</div>
				    </div>
				    
				    <div class="row">    
				        <div class="col s12">
							<button class="btn blue darken-1 add-aluno-sistema full-width" type="submit">
								Salvar aluno
								<i class="material-icons right">chevron_right</i>	
						    </button>
				        </div>
					</div>
				</form>

			</div>
		</div>
	</div>
	
	<br><br><br><br>
</main>

@stop

@section('extra-javascript')

<script type="text/javascript">
	
		
</script>

@stop

