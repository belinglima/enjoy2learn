@extends('template.base-sistema')

@section('content')

<main>	
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h3 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20p;">chevron_left</i>
				</a>
				Blog({{ $total }})
			</h3>
		</div> 
	</div>

	<a href="{{ base_url('sistema/blog/create') }}" class="btn-floating btn-large waves-effect waves-light blue darken-1 right" style="margin-top: -50px; margin-right: 20px;">
	    <i class="material-icons">add</i>
	</a>
	
	<div class="container">
		<div class="card">
			<div class="card-content">
				<form class="form-horizontal" method="post" action="{{ base_url('sistema/blog/index') }}">    
					<div class="row" style="margin-top: -10px;">
						<div class="col s12 m6">
							<input id="buscar" name="buscar" type="text" class="input-field validate" required="required" placeholder="Pesquisa por descrição" autocomplete="off">
				        </div>
				        <div class="col s12 m6">
					        <button class="btn full-width blue darken-1">
					        	Buscar
					        	<i class="material-icons right">chevron_right</i>
					        </button>
				    	</div>
					</div>
										
			    </form>		
				<table class="responsive-table" id="lista">
					@if (! $blogs)
						<tbody>
							  <tr>
								  <td colspan="6">
									 <i class="material-icons left">folder_open</i>
									 <b>Nenhum blog encontrado!</b>
								  </td>
							  </tr>
						</tbody>  	
					@else
						<thead>
							  <tr>
								<th>ID</th>
		              			<th>TIPO</th>
								<th>TÍTULO</th>
								<th>URL YOUTUBE</th>
								<th>CRIADO EM</th>
								<th class="right">AÇÕES</th>
							  </tr>
						</thead>
						<tbody>
							@foreach($blogs AS $blog)
								<tr>
									<td>{{ $blog['id'] }}</td>
									<td>{{ ucfirst(strtolower($blog['tipo'])) }}</td>
									<td>{{ $blog['titulo'] }}</td>
									@if($blog['url_youtube'])
										<td>{{ $blog['url_youtube'] }}</td>									
									@else
										<td> </td>
									@endif
									<td>{{ format_datetime_to_show($blog['created_at']) }}</td>
								    <td class="right">
										<div style="margin-top: -6px;">
											<a href="{{ base_url('sistema/blog/edit/'.$blog['id'])}}">
												<div class="btn blue darken-1 tooltipped" data-position="top" data-tooltip="Editar">
													<i class="material-icons">edit</i>
												</div>
											</a>
											&nbsp;
											<div class="btn blue darken-1 remove-blog-sistema tooltipped" data-id="{{ $blog ['id'] }}" data-position="top" data-tooltip="Excluir">
												<i class="material-icons">delete</i>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					 @endif
				</table>
			</div>
		</div>
	</div>
	<div class="center">
		@php
			echo $links;
		@endphp
  	</div>
	
	<br><br><br><br>	  
</main>

@stop

@section('extra-javascript')
<script type="text/javascript">
	$(".remove-blog-sistema").on('click', function(){
		var id = $(this).data('id');
		swal({
			title: 'Tem certeza?',
			text: "Esta ação não poderá ser desfeita!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#b71c1c ',
			cancelButtonColor: '#ccc',
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não'
		}, function() {
			location.href = base_url('sistema/blog/delete/'+id);
		});
	});
</script>
@stop
