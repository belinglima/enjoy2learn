@extends('template.base-sistema')

@section('content')

<main>

	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h1 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Editar
			</h1>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('sistema/blog/update/'.$blog['id']) }}" enctype="multipart/form-data" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Blog</h5>
						</div>	
					</div>
					
					<div class="row">
						<div class="col s12">
							<label>Tipo <span style="font-weight: bold; color: red;">*</span></label>
							<select name="tipo" id="tipo" class="browser-default" required>
								@if ($blog['tipo'] == 'PUBLICO')
									<option value="PUBLICO" selected>Público</option>
									<option value="ALUNOS">Alunos</option>
								@else
									<option value="PUBLICO">Público</option>
									<option value="ALUNOS" selected>Alunos</option>
								@endif
							</select>
						</div> 
					</div>
					<div class="row">	
						<div class="col s12">
				          	<label>Título <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="titulo" name="titulo" type="text" class="validate browser-default" value="{{ $blog['titulo'] }}" required="required">
				        </div>
				    </div>
				    <div class="row">   
				        <div class="col s12">
							<label>Descrição <span style="font-weight: bold; color: red;">*</span></label>
							<textarea data-ls-module="charCounter" maxlength="2000" id="descricao" name="descricao" class="validate browser-default" type="text" required="required" rows="20" cols="33">{{ $blog['descricao'] }}</textarea>
					    </div>
					</div>
					<div class="row">
				    	<div class="col s12 center">
							<div class="col s4">
								@if (! $blog['foto'])
								   <div class="file-field blue-field">
										<div class="btn blue darken-1">
											<span>FOTO</span>
											<input type="file" id="foto" name="foto">
										</div>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text">
										</div>
									</div>
	
								@else
									<img src="{{ base_url('assets/uploads/blog/'.$blog['foto']) }}" class="responsive-img" style="width: 200px; height: 200px; padding: 2px; border: solid 1px #eee;">
									<div class="btn-floating blue darken-1 right remove-blog-sistema-image" data-id="{{ $blog['id'] }}">
										<i class="material-icons">delete</i>
									</div>
								@endif
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<label>URL youtube</label>
							<input id="url_youtube" name="url_youtube" type="text" class="validate browser-default" value="{{ $blog['url_youtube'] }}" placeholder="Usar toda a URL do youtube.">
						</div>
					</div>
					<div class="row">	
						<div class="col s12">
							<button class="btn blue darken-1 add-blog-sistema full-width" type="submit">
								Salvar blog
								<i class="material-icons right">chevron_right</i>
							</button>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>

	<br><br><br><br>
</main>

@stop

@section('extra-javascript')

<script type="text/javascript">

$(document).ready(function() {
	$('input#input_text, textarea#descricao').characterCounter();
});

$(".remove-blog-sistema-image").on('click', function(){
	let id = $(this).data('id');
		swal({
			title: 'Deseja excluir esta imagem?',
			text: "Esta ação não poderá ser desfeita!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#b71c1c ',
			cancelButtonColor: '#ccc',
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não'
		}, function() {
			location.href = base_url('sistema/blog/delete_image/'+id);
		});
});

</script>

@stop
