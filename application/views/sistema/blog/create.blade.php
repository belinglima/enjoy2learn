@extends('template.base-sistema')

@section('content')

<main>
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h1 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Novo
			</h1>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('sistema/blog/insert') }}" enctype="multipart/form-data" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Blog</h5>
						</div>	
					</div>

					<div class="row">
						<div class="col s12">
							<label>Tipo <span style="font-weight: bold; color: red;">*</span></label>
							<select name="tipo" id="tipo" class="browser-default" required>
								<option value="PUBLICO" selected>Público</option>
								<option value="ALUNOS">Alunos</option>
							</select>
						</div>
					</div>
					<div class="row">	
						<div class="col s12">
				          	<label>Título <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="titulo" name="titulo" type="text" class="validate browser-default" required="required">
				        </div>
				    </div>
				    <div class="row">   
				        <div class="col s12">
				          	<label>Descrição <span style="font-weight: bold; color: red;">*</span></label>
				          	<textarea data-ls-module="charCounter" maxlength="2000" id="descricao" name="descricao" class="validate browser-default" type="text" required="required" rows="20" cols="33"></textarea>
				        </div>
					</div>
					<div class="row">
				    	<div class="col s12">
							<div class="file-field input-field">
								<div class="btn blue darken-1">
									<span>FOTO</span>
									<input type="file" id="foto" name="foto">
								</div>
								<div class="file-path-wrapper">
									<input class="file-path validate" type="text">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<label>URL youtube</label>
							<input id="url_youtube" name="url_youtube" type="text" class="validate browser-default" placeholder="Usar toda a URL do youtube.">
						</div>
					</div>
					<div class="row">    
				        <div class="col s12">
							<button class="btn blue darken-1 add-blog-sistema full-width" type="submit">
								Salvar blog
								<i class="material-icons right">chevron_right</i>	
						    </button>
				        </div>
					</div>
				</form>

			</div>
		</div>
	</div>
	
	<br><br><br><br>
</main>

@stop

@section('extra-javascript')

<script type="text/javascript">

	$(document).ready(function() {
		$('input#input_text, textarea#descricao').characterCounter();
	});
	
</script>

@stop

