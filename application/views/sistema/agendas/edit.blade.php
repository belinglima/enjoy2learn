@extends('template.base-sistema')

@section('content')

<main>

	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h1 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Editar
			</h1>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('sistema/agendas/update/'.$agenda['id']) }}" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Agenda</h5>
						</div>	
					</div>
					
					<div class="row">	
						<div class="col s12 m4">
				          	<label>Data <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="data" name="data" type="text" class="datepicker validate browser-default" value="{{ format_date_to_show($agenda['data']) }}" required="required">
				        </div>
				        <div class="col s12 m4">
				          	<label>Hora início <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="horainicio" name="horainicio" type="time" class="validate browser-default" value="{{ $agenda['horainicio'] }}" required="required">
				        </div>
                        <div class="col s12 m4">
                            <label>Hora fim <span style="font-weight: bold; color: red;">*</span></label>
                            <input id="horafim" name="horafim" type="time" class="validate browser-default" value="{{ $agenda['horafim'] }}" required="required">
                        </div>
				    </div>
				     
                    <div class="row">
						<div class="input-field col s12">
							<p style="color:#c0c0c0">Aluno <span style="font-weight: bold; color: red;">*</span></p>
							<input id="aluno" name="nome" type="text" value="[{{ $agenda['aluno_id'] }}]-{{ ucwords(strtolower($agenda['nome'])) }}" class="autocomplete browser-default" autofocus autocomplete="off" required="required">
                            @if(empty($agenda['aluno_id']))
							        <input type="hidden" id="idAluno" name="aluno_id" value="NULL" />
						    @else
							<input type="hidden" id="idAluno" name="aluno_id" value="{{ $agenda['aluno_id'] }}" />		
						    @endif
                        </div>
                    </div>
					
					<div class="row">	
						<div class="col s12">
							<button class="btn blue darken-1 add-agenda-sistema full-width" type="submit">
								Salvar agenda
								<i class="material-icons right">chevron_right</i>
							</button>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>

</main>

@stop

@section('extra-javascript')

<script type="text/javascript">

	$('input.autocomplete').autocomplete({
      	data: {
        	@foreach ($alunos as $aluno)
				"{{ ' ['. $aluno['id'] .']'.'-'. ucwords(strtolower($aluno['nome'])) }}" : null,
			@endforeach
			
      	},
      	onAutocomplete(){
			let aluno = $("#aluno").val();
			aluno = aluno.split("-");
			idAluno = aluno[0];

			let aluno0 = idAluno.split("[");
            let aluno1 = aluno0[1];
			let aluno2 = aluno1.split("]");
			let aluno3 = aluno2[0];

			$("#idAluno").val(aluno3);
      	},
	});

</script>

@stop
