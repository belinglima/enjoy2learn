@extends('template.base-sistema')

@section('content')

<main>

	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h1 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Editar
			</h1>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('sistema/certificados/update/'.$certificado['id']) }}" enctype="multipart/form-data" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Certificado</h5>
						</div>	
					</div>
					
					<div class="row">	
						<div class="col s12">
				          	<label>Nome <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="nome" name="nome" type="text" class="validate browser-default" value="{{ $certificado['nome'] }}" required="required">
				        </div>
				    </div>
				    <div class="row">
				    	<div class="col s12 center">
							<div class="col s4">
								@if (! $certificado['foto'])
								   <div class="file-field blue-field">
										<div class="btn blue darken-1">
											<span>FOTO</span>
											<input type="file" id="foto" name="foto">
										</div>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text">
										</div>
									</div>
	
								@else
									<img src="{{ base_url('assets/uploads/certificados/'.$certificado['foto']) }}" class="responsive-img" style="height: 150px;"; padding: 2px; border: solid 1px #eee;">
									<div class="btn-floating blue darken-1 right remove-certificado-sistema-image" data-id="{{ $certificado['id'] }}">
										<i class="material-icons">delete</i>
									</div>
								@endif
	
							</div>
	
						</div>
					</div>
															
					<div class="row">	
						<div class="col s12">
							<button class="btn blue darken-1 add-certificado-sistema full-width" type="submit">
								Salvar certificado
								<i class="material-icons right">chevron_right</i>
							</button>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>

</main>

@stop

@section('extra-javascript')

<script type="text/javascript">

$(".remove-certificado-sistema-image").on('click', function(){
	let id = $(this).data('id');
		swal({
			title: 'Deseja excluir esta imagem?',
			text: "Esta ação não poderá ser desfeita!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#b71c1c ',
			cancelButtonColor: '#ccc',
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não'
		}, function() {
			location.href = base_url('sistema/certificados/delete_image/'+id);
		});
});

</script>

@stop
