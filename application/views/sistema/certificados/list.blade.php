@extends('template.base-sistema')

@section('content')

<main>	
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h3 class="header center blue-text">
				@if(isset($buscar))
					<a href="{{ base_url('sistema/certificados') }}">
						<i class="material-icons medium left blue-text" style="margin-right: -20px">chevron_left</i>
					</a>
				@else
					<a href="{{ base_url('sistema/welcome') }}">
						<i class="material-icons medium left blue-text" style="margin-right: -20p;">chevron_left</i>
					</a>
				@endif
				@if(isset($buscar))
					Certificados
				@else
				    Certificados({{ $total }})
				@endif
			</h3>
			@if(isset($buscar))
				<span class="blue-text" style="font-weight: bold;">Filtrado por: <u>{{ $buscar }}</u></span>
			@endif	
		</div> 
	</div>

	<a href="{{ base_url('sistema/certificados/create') }}" class="btn-floating btn-large waves-effect waves-light blue darken-1 right" style="margin-top: -50px; margin-right: 20px;">
	    <i class="material-icons">add</i>
	</a>

	<div class="container">
		<div class="card">
			<div class="card-content">
				<form class="form-horizontal" method="post" action="{{ base_url('sistema/certificados/index') }}">    
					<div class="row">
						<div class="col s12 m6">
							<input id="buscar" name="buscar" type="text" class="input-field validate" required="required" placeholder="Pesquisa por nome" autocomplete="off">
				        </div>
				        <div class="col s12 m6">
					        <button class="btn full-width blue darken-1">
					        	Buscar
					        	<i class="material-icons right">chevron_right</i>
					        </button>
				    	</div>
					</div>
										
			    </form>	
			</div>
		</div>
								
		@if (! $certificados)
			<div class="col s12">
				<div class="card horizontal">
					<div class="card-content">
						<div class="row">	
							<div class="col s12 valign-wrapper">
								<i class="material-icons left">folder_open</i>
								@if(isset($buscar))
									<b>Nenhum certificado encontrado!</b>
								@else
									<b>Nenhum certificado cadastrado!</b>
								@endif 
							</div>
						</div>		
					</div>
				</div>
			</div>	
		@else 
		
		<div class="row">
			@foreach($certificados AS $certificado)
					<div class="col s12 m4">		
						<div class="card" style="z-index: 9999 !important;">
							<div class="card-image">
								@if(isset($certificado['foto']))
									<img class="activator" 
									src="{{ base_url('assets/uploads/certificados/'.$certificado['foto']) }}" style="height: 150px;">
								@else
								<img class="activator" 
									src="{{base_url('assets/uploads/semfotoCinza.png')
									}}" style="height: 150px;">
								@endif
							</div>
							<div class="card-content center-align" style="height: 130px !important;">
								<p style="margin-top: 0px;">
									{{ $certificado['nome'] }}
								</p>
								<p style="font-weight: bold;">{{ format_datetime_to_show($certificado['data']) }}</p> <br/>
							</div>
							<div class="card-action center-align">
								<a href="{{ base_url('sistema/certificados/edit/'.$certificado['id'])}}">
									<div class="btn blue darken-1 tooltipped" data-position="top" data-tooltip="Editar">
										<i class="material-icons">edit</i>
									</div>
								</a>
								&nbsp;
								<div class="btn blue darken-1 remove-certificado-sistema tooltipped" data-id="{{ $certificado['id'] }}" data-position="top" data-tooltip="Excluir">
									<i class="material-icons">delete</i>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			@endif
		</div>
	</div>	
	<div class="center">
		@php
			echo $links;
		@endphp
  	</div>
    
	<br><br><br><br>
</main>

@stop

@section('extra-javascript')
<script type="text/javascript">
	$(".remove-certificado-sistema").on('click', function(){
		var id = $(this).data('id');
		swal({
			title: 'Tem certeza?',
			text: "Esta ação não poderá ser desfeita!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#b71c1c ',
			cancelButtonColor: '#ccc',
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não'
		}, function() {
			location.href = base_url('sistema/certificados/delete/'+id);
		});
	});
</script>
@stop