@extends('template.base-sistema')

@section('content')

<main>
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h1 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Novo
			</h1>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('sistema/certificados/insert') }}" enctype="multipart/form-data" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Certificado</h5>
						</div>	
					</div>
					
					<div class="row">	
						<div class="col s12">
				          	<label>Nome <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="nome" name="nome" type="text" class="validate browser-default" required="required">
				        </div>
				    </div>
				   	<div class="row">
				    	<div class="col s12">
							<div class="file-field input-field">
								<div class="btn blue darken-1">
									<span>FOTO</span>
									<input type="file" id="foto" name="foto">
								</div>
								<div class="file-path-wrapper">
									<input class="file-path validate" type="text">
								</div>
							</div>
						</div>
					</div>
														    
				    <div class="row">    
				        <div class="col s12">
							<button class="btn blue darken-1 add-certificado-sistema full-width" type="submit">
								Salvar certificado
								<i class="material-icons right">chevron_right</i>	
						    </button>
				        </div>
					</div>
				</form>

			</div>
		</div>
	</div>
	
</main>

@stop

@section('extra-javascript')

<script type="text/javascript">
	
		
</script>

@stop

