@extends('template.base-sistema')

@php
  const STATUS_PAGAMENTO = [
    0 => 'AGUARDANDO',
    1 => 'PENDENTE',
    2 => 'PAGO',
    3 => 'NEGADO',
    4 => 'EXPIRADO',
    5 => 'CANCELADO',
    6 => 'NÃO FINALIZADO',
    7 => 'AUTORIZADO',
    8 => 'CHARGEBACK'
  ];

  const TIPO_PAGAMENTO = [
    1 => 'CARTÃO DE CRÉDITO',
    2 => 'BOLETO',
    3 => 'DÉBITO ONLINE',
    4 => 'CARTÃO DE DÉBITO',
  ];

  const BANDEIRAS = [
    1 => 'VISA',
    2 => 'MASTERCARD',
    3 => 'AMERCICANEXPRESS',
    4 => 'DINERS',
    5 => 'ELO',
    6 => 'AURA',
    7 => 'JCB',
    8 => 'DISCOVER',
    9 => 'HIPERCARD',
  ];
@endphp

@section('content')

<main>	
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h3 class="header center blue-text">
				@if(isset($buscar))
					<a href="{{ base_url('sistema/vendas') }}">
						<i class="material-icons medium left blue-text" style="margin-right: -20px">chevron_left</i>
					</a>
				@else
					<a href="{{ base_url('sistema/welcome') }}">
						<i class="material-icons medium left blue-text" style="margin-right: -20p;">chevron_left</i>
					</a>
				@endif
				@if(isset($buscar))
					Vendas
				@else
					Vendas({{ $total }})
				@endif
			</h3>
			@if(isset($buscar))
				<span class="blue-text" style="font-weight: bold;">Filtrado por: <u>{{ $buscar }}</u></span>
			@endif	
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">
				<table class="responsive-table" id="lista">
					@if (! $vendas)
						<tbody>
							  <tr>
								  <td colspan="5">
									<i class="material-icons left">folder_open</i>
									<b>Nenhuma venda cadastrada!</b>
								</td>
							  </tr>
						</tbody>  	
					@else
						<thead>
							  <tr>
								<th>ID</th>
                            	<th>ALUNO</th>
                            	<th>DATA</th>
                            	<th>STATUS</th>
                            	<th>TOTAL</th>
							  </tr>
						</thead>
						<tbody>
							@foreach($vendas AS $venda)
								<tr>
									<td>{{ $venda['id_venda'] }}</td>
									<td>{{ $venda['nome'] }}</td>
									<td>{{ format_date_to_show($venda['data']) }}</td>
                                	<td>
                                    	{{ STATUS_PAGAMENTO[($venda['payment_status']) ? $venda['payment_status'] : 0] }}
                                	</td>
                                	<td>R$ {{ convert_double_to_BRL($venda['total']) }}</td>
								</tr>
							@endforeach
						</tbody>
					 @endif
				</table>				
			</div>
		</div>
	</div>
	<div class="center">
		@php
			echo $links;
		@endphp
  	</div>
    
	<br><br><br><br> 
</main>

@stop

@section('extra-javascript')
<script type="text/javascript">
	
</script>
@stop
