@extends('template.base-sistema')

@section('content')

<main>

	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h1 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Editar
			</h1>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('sistema/notificacoes/update/'.$notificacao['id']) }}" enctype="multipart/form-data" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Notificação</h5>
						</div>	
					</div>
					
					<div class="row">	
						<div class="col s12">
				          	<label>Título <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="titulo" name="titulo" type="text" class="validate browser-default" value="{{ $notificacao['titulo'] }}" required="required">
				        </div>
				    </div>
					<div class="row">
				    	<div class="col s12">
							<label for="texto">Texto <span style="font-weight: bold; color: red;">*</span></label>
							<textarea data-ls-module="charCounter" maxlength="180" id="texto" name="texto" class="validate browser-default" type="text" rows="20" cols="33" placeholder="Um breve texto" required="required">{{ $notificacao['texto'] }}</textarea>				       
						</div>
					</div>
				   	<div class="row">
						<div class="col s12">
							<label>URL <span style="font-weight: bold; color: red;">*</span></label>
							<input id="url" name="url" type="text" class="validate browser-default" value="{{ $notificacao['url'] }}" placeholder="Usar a hash da url do vimeo. Ex: se a URL for https://vimeo.com/34534040, usar somente o seu hash 34534040">
						</div>
					</div>
										
					<div class="row">	
						<div class="col s12">
							<button class="btn blue darken-1 add-notificacao-sistema full-width" type="submit">
								Salvar notificação
								<i class="material-icons right">chevron_right</i>
							</button>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>

</main>

@stop

@section('extra-javascript')

<script type="text/javascript">

	$(document).ready(function() {
		$('input#input_text, textarea#texto').characterCounter();

	});

</script>

@stop
