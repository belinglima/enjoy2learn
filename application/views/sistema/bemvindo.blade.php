@extends('template.base-sistema')

@section('content')

<main>
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h2 class="header blue-text">
				Bem-vindo
			</h2>
			<p class="blue-text" style="margin-top: -20px;">
				<b> Data/hora {{ $data }}</b> &nbsp <b id="horario"></b>
			</p>
		</div> 
	</div>
	<div class="container" style="margin-top: -20px;">
		<div class="row">

			<div class="col s12 center">
				<div class="col s12" style="border-bottom: 1px solid #2196F3; border-bottom-width: 2px;">
					<h5 class="blue-text text-darken-1">Estatísticas</h5>
				</div>
                
				<div class="col s4 m3">
					<a href="{{ base_url('sistema/agendas') }}" style="color: inherit;">
						<div><i class="material-icons medium blue-text medium">schedule</i></div>
						<div><b>Agenda</b></div>
						<h5 style="font-weight: bold;">{{ $agendas }}</h5>
					</a>					
				</div>

				<div class="col s4 m3">
					<a href="{{ base_url('sistema/alunos') }}" style="color: inherit;">
						<div><i class="material-icons medium blue-text medium">person_pin</i></div>
						<div><b>Alunos</b></div>
						<h5 style="font-weight: bold;">{{ $alunos }}</h5>
					</a>					
				</div>

				<div class="col s4 m3">
					<a href="#" style="color: inherit;">
						<div><i class="material-icons medium blue-text medium">art_track</i></div>
						<div><b>Aulas</b></div>
						<h5 style="font-weight: bold;">{{ $aulas }}</h5>
					</a>					
				</div>

				<div class="col s4 m3">
					<a href="{{ base_url('sistema/blog') }}" style="color: inherit;">
						<div><i class="material-icons medium blue-text medium">assignment</i></div>
						<div><b>Blog</b></div>
						<h5 style="font-weight: bold;">{{ $blog }}</h5>
					</a>					
				</div>
				
				<div class="col s4 m3">
					<a href="{{ base_url('sistema/cursos') }}" style="color: inherit;">
						<div><i class="material-icons medium blue-text medium">menu_book</i></div>
						<div><b>Cursos</b></div>
						<h5 style="font-weight: bold;">{{ $cursos }}</h5>
					</a>					
				</div>

				<div class="col s4 m3">
					<a href="{{ base_url('sistema/vendas') }}" style="color: inherit;">
						<div><i class="material-icons medium blue-text medium">money</i></div>
						<div><b>Vendas</b></div>
						<h5 style="font-weight: bold;">{{ $vendas }}</h5>
					</a>					
				</div>

				<div class="col s4 m3">
					<a href="{{ base_url('sistema/duvidas') }}" style="color: inherit;">
						<div><i class="material-icons medium blue-text medium">help_center</i></div>
						<div><b>Dúvidas</b></div>
						<h5 style="font-weight: bold;">{{ $duvidas }}</h5>
					</a>					
				</div>

				<div class="col s4 m3">
					<a href="{{ base_url('sistema/certificados') }}" style="color: inherit;">
						<div><i class="material-icons medium blue-text medium">insert_drive_file</i></div>
						<div><b>Certificados</b></div>
						<h5 style="font-weight: bold;">{{ $certificados }}</h5>
					</a>					
				</div>

				<div class="col s4 m3">
					<a href="{{ base_url('sistema/notificacoes') }}" style="color: inherit;">
						<div><i class="material-icons medium blue-text medium">notifications</i></div>
						<div><b>Notificações</b></div>
						<h5 style="font-weight: bold;">{{ $notificacoes }}</h5>
					</a>					
				</div>

				<div class="col s4 m3">
					<a href="{{ base_url('sistema/contatos') }}" style="color: inherit;">
						<div><i class="material-icons medium blue-text medium">mail</i></div>
						<div><b>Contatos</b></div>
						<h5 style="font-weight: bold;">{{ $contato }}</h5>
					</a>					
				</div>

				<div class="col s4 m3">
					<a href="{{ base_url('sistema/cupons') }}" style="color: inherit;">
						<div><i class="material-icons medium blue-text medium">money</i></div>
						<div><b>Cupons</b></div>
						<h5 style="font-weight: bold;">{{ $cupom }}</h5>
					</a>					
				</div>
	            
			</div>
			
		</div>	

		<br><br><br><br>		

</main>

@stop

@section('extra-javascript')

<script type="text/javascript">
	
	setInterval(function(){
    
	    let novaHora = new Date();
	    let hora = novaHora.getHours();
	    let minuto = novaHora.getMinutes();
	    let segundo = novaHora.getSeconds();
	    
	    hora  = zero(hora);
	    minuto = zero(minuto);
	    segundo = zero(segundo);
	    horario = hora+':'+minuto+':'+segundo;
	    
	    $('#horario').html(horario);
	    
	},1000)

	function zero(x) {
	    if (x < 10) {
	        x = '0' + x;
	    } 
	    return x;
	}
	
</script>

@stop