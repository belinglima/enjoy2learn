@extends('template.base-sistema')

@section('content')

<main>

	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h1 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Editar
			</h1>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('sistema/materiaisApoio/update/'.$material['id']) }}" enctype="multipart/form-data" method="POST">
						<div class="row">
							<div class="col s12">
								<h5>Material de apoio</h5>
							</div>	
						</div>
						<div class="row">	
							<div class="col s12 m6">
								<label>Curso </label>
								<select class="browser-default curso" name="id_curso">
									<option value="" disabled selected>Escolher um curso</option>
									@foreach($cursos AS $curso)
									  @if($material['id_curso'] == $curso['id'])	
										  <option value="{{ $curso['id'] }}" selected>{{ $curso['nome'] }}</option>
									  @else
									  	  <option value="{{ $curso['id'] }}">{{ $curso['nome'] }}</option>
									  @endif
									@endforeach
								</select>
								<input type="hidden" id="curso" value="{{ $material['id_curso'] }}">
							</div>
							<div class="col s12 m6">
								<label>Aula <span style="font-weight: bold; color: red;">*</span></label>
								<select class="browser-default carregaAula" name="id_aula">
								<option value="" disabled selected></option>
								</select>
								<input type="hidden" class="aula" value="{{ $material['id_aula'] }}">
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<label>Tipo de material <span style="font-weight: bold; color: red;">*</span></label>
								<select name="tipomaterial" id="tipomaterial" class="tipomaterial browser-default" required="required">
									<option value="" disabled selected>Escolha um tipo de URL</option>
									@if($material['tipomaterial'] == 'IMAGEM')
										<option value="IMAGEM" selected>Imagem</option>
										<option value="PDF">PDF</option>
										<option value="AUDIO">Áudio</option>
									@elseif($material['tipomaterial'] == 'PDF')
										<option value="IMAGEM">Imagem</option>
										<option value="PDF" selected>PDF</option>
										<option value="AUDIO">Áudio</option>
									@elseif($material['tipomaterial'] == 'AUDIO')
										<option value="IMAGEM">Imagem</option>
										<option value="PDF">PDF</option>
										<option value="AUDIO" selected>Áudio</option>
									@endif
								</select>
							</div>
						</div>
						<div class="row urlimagem">
							<div class="col s12 center">
								<div class="col s4">
									@if (! $material['url_imagem'])
										<div class="file-field blue-field">
											<div class="btn blue darken-1">
												<span>IMAGEM</span>
												<input type="file" id="url_imagem" name="url_imagem">
											</div>
											<div class="file-path-wrapper">
												<input class="file-path validate" type="text">
											</div>
										</div>
									@else
										<img src="{{ base_url('assets/uploads/materiaisapoio/'.$material['url_imagem']) }}" class="responsive-img" style="width: 200px; height: 200px; padding: 2px; border: solid 1px #eee;">
										<div class="btn-floating blue darken-1 right remove-materialapoio-sistema-image" data-id="{{ $material['id'] }}">
											<i class="material-icons">delete</i>
										</div>
										<input type="hidden"  class="url_imagem" name="url_imagem" value="{{ $material['url_imagem'] }}" />
									@endif
								</div>
							</div>
						</div>
						<div class="row urlpdf">
							<div class="col s12">
								<label>URL PDF</label>
								<input id="url_pdf" name="url_pdf" type="text" class="url_pdf validate browser-default" placeholder="Usar a URL do PDF" value="{{ $material['url_pdf'] != 'NULL' ? $material['url_pdf'] : '' }}">
							</div>
						</div>
						<div class="row urlaudio">
							<div class="col s12">
								<label>URL áudio <span style="font-weight: bold; color: red;">*</span></label>
								<input id="url_audio" name="url_audio" type="text" class="url_audio validate browser-default" placeholder="Usar a URL do áudio" value="{{ $material['url_audio'] != 'NULL' ? $material['url_audio'] : '' }}">
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<label>Status <span style="font-weight: bold; color: red;">*</span></label>
								<select name="status" id="status" class="browser-default" required>
									@if ($material['status'] == 'SIM')
										<option value="SIM" selected>Ativo</option>
										<option value="NAO">Inativo</option>
									@else
										<option value="SIM">Ativo</option>
										<option value="NAO" selected>Inativo</option>
									@endif
								</select>
							</div> 
						</div>

						<div class="row">	
							<div class="col s12">
								<button class="btn blue darken-1 add-curso-sistema full-width" type="submit">
									Salvar material de apoio
									<i class="material-icons right">chevron_right</i>
								</button>
							</div>
						</div>
				</form>

			</div>
		</div>
	</div>

</main>

@stop

@section('extra-javascript')

<script type="text/javascript">

	let pegaDadosCurso= (selecionado) => {
      $(".loading").toggleClass('hide');
      let url = base_url('sistema/materiaisApoio/getAulaByCurso/' + selecionado );
      $.get(url, function(data){        
        $('.carregaAula').html(data);
        $(".loading").toggleClass('hide');
      }); 
    }

    $('.curso').on('change', function(){
    	let id = $(this).val();
    	pegaDadosCurso(id);
    });

	let curso = $('#curso').val();
	let aula =  $('.aula').val();
	
	if(curso) {
		$(".loading").toggleClass('hide');	
		let url = base_url('sistema/materiaisApoio/getAulaByCurso/' + curso + '/' + aula);
		$.get(url, function(data){
			$('.carregaAula').html(data);
		}); 
	}

	$(".remove-materialapoio-sistema-image").on('click', function(){
		let id = $(this).data('id');
			swal({
				title: 'Deseja excluir esta imagem?',
				text: "Esta ação não poderá ser desfeita!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#b71c1c ',
				cancelButtonColor: '#ccc',
				confirmButtonText: 'Sim',
				cancelButtonText: 'Não'
			}, function() {
				location.href = base_url('sistema/materiaisApoio/delete_image/'+id);
			});
	});

	let tipoDeMaterial = '{{ $material['tipomaterial'] }}';

	if(tipoDeMaterial == 'IMAGEM') {
		$('.urlimagem').show();
		$('.urlpdf').hide();
	    $('.urlaudio').hide();
	}
	if(tipoDeMaterial == 'PDF') {
		$('.urlpdf').show();
		$('.urlimagem').hide();
	    $('.urlaudio').hide();
	}
	if(tipoDeMaterial == 'AUDIO') {
		$('.urlaudio').show();
		$('.urlpdf').hide();
		$('.urlimagem').hide();
	}

	$('.tipomaterial').on('change', function(){
		let tipoMaterial  = $(this).val();
		if(tipoMaterial == 'IMAGEM') {
			$('.urlimagem').show();
			$('.urlpdf').hide();
	        $('.urlaudio').hide();
			$('.url_pdf').val('');
	        $('.url_audio').val('');
		}
		if(tipoMaterial == 'PDF') {
			$('.urlpdf').show();
			$('.urlimagem').hide();
	        $('.urlaudio').hide();
			$('.url_imagem').val('');
	        $('.url_audio').val('');
		}
		if(tipoMaterial == 'AUDIO') {
			$('.urlaudio').show();
			$('.urlimagem').hide();
			$('.urlpdf').hide();
			$('.url_pdf').val('');
			$('.url_imagem').val('');
		}
	});

</script>

@stop
