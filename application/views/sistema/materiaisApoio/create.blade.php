@extends('template.base-sistema')

@section('content')

<main>
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h1 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Novo
			</h1>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('sistema/materiaisApoio/insert') }}" enctype="multipart/form-data" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Material de apoio</h5>
						</div>	
					</div>
					
					<div class="row">	
						<div class="col s12 m6">
							<label>Curso <span style="font-weight: bold; color: red;">*</label>
							<select class="browser-default curso" name="id_curso" required="required">
								<option value="" disabled selected>Escolher um curso</option>
								@foreach($cursos AS $curso)
									<option value="{{ $curso['id'] }}">{{ $curso['nome'] }}</option>
								@endforeach
							</select>
						</div>
						<div class="col s12 m6">
							<label>Aula <span style="font-weight: bold; color: red;">*</span></label>
							<select class="browser-default carregaAula" name="id_aula"  required="required">
							<option value="" disabled selected></option>
							</select>
						</div>
					</div>
                    <div class="row">
						<div class="col s12">
							<label>Tipo de material <span style="font-weight: bold; color: red;">*</span></label>
							<select name="tipomaterial" id="tipomaterial" class="tipomaterial browser-default" required="required">
								<option value="" disabled selected>Escolha um tipo de URL</option>
								<option value="IMAGEM">Imagem</option>
								<option value="PDF">PDF</option>
								<option value="AUDIO">Áudio</option>
							</select>
						</div>
				    </div>
				   	<div class="row urlimagem">
				    	<div class="col s12">
							<div class="file-field input-field">
								<div class="btn blue darken-1">
									<span>IMAGEM</span>
									<input type="file" id="url_imagem" name="url_imagem">
								</div>
								<div class="file-path-wrapper">
									<input class="file-path validate" type="text">
								</div>
							</div>
						</div>
					</div>
					<div class="row urlpdf">
						<div class="col s12">
							<label>URL PDF</label>
							<input id="url_pdf" name="url_pdf" type="text" class="validate browser-default" placeholder="Usar a URL do PDF">
						</div>
					</div>
					<div class="row urlaudio">
						<div class="col s12">
							<label>URL áudio <span style="font-weight: bold; color: red;">*</span></label>
							<input id="url_audio" name="url_audio" type="text" class="validate browser-default" placeholder="Usar a URL do áudio">
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<label>Status <span style="font-weight: bold; color: red;">*</span></label>
							<select name="status" id="status" class="browser-default" required>
								<option value="SIM" selected>Ativo</option>
								<option value="NAO">Inativo</option>
							</select>
						</div>
					</div>
																			    
				    <div class="row">    
				        <div class="col s12">
							<button class="btn blue darken-1 add-curso-sistema full-width" type="submit">
								Salvar material de apoio
								<i class="material-icons right">chevron_right</i>	
						    </button>
				        </div>
					</div>
				</form>

			</div>
		</div>
	</div>
	
</main>

@stop

@section('extra-javascript')

<script type="text/javascript">
	let pegaDadosCurso= (selecionado) => {
      $(".loading").toggleClass('hide');
      let url = base_url('sistema/materiaisApoio/getAulaByCurso/' + selecionado );
      $.get(url, function(data){        
        $('.carregaAula').html(data);
        $(".loading").toggleClass('hide');
      }); 
    }

    $('.curso').on('change', function(){
    	let id = $(this).val();
    	pegaDadosCurso(id);
    });

	$('.urlimagem').hide();
	$('.urlpdf').hide();
	$('.urlaudio').hide();

	$('.tipomaterial').on('change', function(){
		let tipoMaterial  = $(this).val();
		if(tipoMaterial == 'IMAGEM') {
			$('.urlimagem').show();
			$('.urlpdf').hide();
	        $('.urlaudio').hide();
		}
		if(tipoMaterial == 'PDF') {
			$('.urlpdf').show();
			$('.urlimagem').hide();
	        $('.urlaudio').hide();
		}
		if(tipoMaterial == 'AUDIO') {
			$('.urlaudio').show();
			$('.urlpdf').hide();
			$('.urlimagem').hide();
		}
	});

</script>

@stop

