@extends('template.base-sistema')

@section('content')

<main>	
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h3 class="header center blue-text">
				@if(isset($buscar))
					<a href="{{ base_url('sistema/duvidas') }}">
						<i class="material-icons medium left blue-text" style="margin-right: -20px">chevron_left</i>
					</a>
				@else
					<a href="{{ base_url('sistema/welcome') }}">
						<i class="material-icons medium left blue-text" style="margin-right: -20p;">chevron_left</i>
					</a>
				@endif
				@if(isset($buscar))
					Dúvidas frequentes
				@else
					Dúvidas frequentes({{ $total }})
				@endif
			</h3>
			@if(isset($buscar))
				<span class="blue-text" style="font-weight: bold;">Filtrado por: <u>{{ $buscar }}</u></span>
			@endif	
		</div> 
	</div>

	<a href="{{ base_url('sistema/duvidas/create') }}" class="btn-floating btn-large waves-effect waves-light blue darken-1 right" style="margin-top: -50px; margin-right: 20px;">
	    <i class="material-icons">add</i>
	</a>
	
	<div class="container">
		<div class="card">
			<div class="card-content">
				<table class="responsive-table" id="lista">
					@if (! $duvidas)
						<tbody>
							  <tr>
								<td colspan="4">
									<i class="material-icons left">folder_open</i>
										 <b>Nenhuma dúvida cadastrada!</b>
								 <td>
							  </tr>
						</tbody>  	
					@else
						<thead>
							  <tr>
								<th>ID</th>
		              			<th>PERGUNTA</th>
		              			<th>TIPO</th>
								<th class="right">AÇÕES</th>
							  </tr>
						</thead>
						<tbody>
							@foreach($duvidas AS $duvida)
								<tr>
									<td>{{ $duvida['id'] }}</td>
									<td>{{ $duvida['pergunta'] }}</td>
									<td>{{ $duvida['tipo'] }}</td>
									<td class="right">
										<div style="margin-top: -6px;">
											<a href="{{ base_url('sistema/duvidas/edit/'.$duvida['id'])}}">
												<div class="btn blue darken-1 tooltipped" data-position="top" data-tooltip="Editar">
													<i class="material-icons">edit</i>
												</div>
											</a>
											&nbsp;
											<div class="btn blue darken-1 remove-duvida-sistema tooltipped" data-id="{{ $duvida['id'] }}" data-position="top" data-tooltip="Excluir">
												<i class="material-icons">delete</i>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					 @endif
				</table>

			</div>
		</div>
	</div>
	<div class="center">
		@php
			echo $links;
		@endphp
  	</div>
    
	<br><br><br><br>
</main>

@stop

@section('extra-javascript')
<script type="text/javascript">
	$(".remove-duvida-sistema").on('click', function(){
		var id = $(this).data('id');
		swal({
			title: 'Tem certeza?',
			text: "Esta ação não poderá ser desfeita!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#b71c1c ',
			cancelButtonColor: '#ccc',
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não'
		}, function() {
			location.href = base_url('sistema/duvidas/delete/'+id);
		});
	});
</script>
@stop
