@extends('template.base-sistema')

@section('content')

<main>
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h1 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Novo
			</h1>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('sistema/duvidas/insert') }}" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Dúvida frequente</h5>
						</div>	
					</div>
					
					<div class="row">	
						<div class="col s12">
				          	<label>Pergunta <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="pergunta" name="pergunta" type="text" class="validate browser-default" required="required">
				        </div>
				    </div>
				    <div class="row">
				    	<div class="col s12">
							<label for="resposta">Resposta <span style="font-weight: bold; color: red;">*</span></label>
							<textarea data-ls-module="charCounter" maxlength="500" id="resposta" name="resposta" class="validate browser-default" type="text" required="required" rows="20" cols="33"></textarea>
				        </div>
					</div>

					<div class="row">	
						<div class="col s12">
							<label>Tipo </label>	
							<select name="tipo" id="tipo" class="browser-default">
								<option value="" disabled selected>Escolha um Tipo</option>
								<option value="GERAL">GERAL</option>
								<option value="PAGAMENTOS">PAGAMENTOS</option>
								<option value="ALUNOS">ALUNOS</option>
							</select>
						</div>
					</div>
													    
				    <div class="row">    
				        <div class="col s12">
							<button class="btn blue darken-1 add-duvida-sistema full-width" type="submit">
								Salvar dúvida
								<i class="material-icons right">chevron_right</i>	
						    </button>
				        </div>
					</div>
				</form>

			</div>
		</div>
	</div>
	
</main>

@stop

@section('extra-javascript')

<script type="text/javascript">

	$(document).ready(function() {
		$('input#input_text, textarea#resposta').characterCounter();
		
	});	
		
</script>

@stop

