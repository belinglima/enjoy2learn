@extends('template.base-sistema')

@section('content')

<main>

	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h1 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Editar
			</h1>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('sistema/cursos/update/'.$curso['id']) }}" enctype="multipart/form-data" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Curso</h5>
						</div>	
					</div>
					
					<div class="row">	
						<div class="col s12">
				          	<label>Nome <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="nome" name="nome" type="text" class="validate browser-default" value="{{ $curso['nome'] }}" required="required">
				        </div>
				    </div>
				     <div class="row">   
				        <div class="col s12">
							<label>Descrição <span style="font-weight: bold; color: red;">*</span></label>
							<textarea data-ls-module="charCounter" maxlength="300" id="descricao" name="descricao" class="validate browser-default" type="text" required="required" rows="20" cols="33">{{ $curso['descricao'] }}</textarea>
					    </div>
					</div>
					<div class="row">
				    	<div class="col s12 center">
							<div class="col s4">
								@if (! $curso['foto'])
								   <div class="file-field blue-field">
										<div class="btn blue darken-1">
											<span>FOTO</span>
											<input type="file" id="foto" name="foto">
										</div>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text">
										</div>
									</div>
	
								@else
									<img src="{{ base_url('assets/uploads/cursos/'.$curso['foto']) }}" class="responsive-img" style="width: 200px; height: 200px; padding: 2px; border: solid 1px #eee;">
									<div class="btn-floating blue darken-1 right remove-curso-sistema-image" data-id="{{ $curso['id'] }}">
										<i class="material-icons">delete</i>
									</div>
								@endif
	
							</div>
	
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<label>Preço <span style="font-weight: bold; color: red;">*</span></label>
							<input id="preco" name="preco" type="text" class="real validate browser-default" value="{{ $curso['preco']}}">
					    </div>
					</div>
					<div class="row">
						<div class="col s12">
							<label>Status <span style="font-weight: bold; color: red;">*</span></label>
							<select name="status" id="status" class="browser-default" required>
								@if ($curso['status'] == 'ATIVO')
									<option value="ATIVO" selected>Ativo</option>
									<option value="INATIVO">Inativo</option>
								@else
									<option value="ATIVO">Ativo</option>
									<option value="INATIVO" selected>Inativo</option>
								@endif
							</select>
						</div> 
					</div>

					<div class="row">	
						<div class="col s12">	
							<label>Certificado </label>	
							<select name="id_certificado" id="id_certificado" class="browser-default">
								<option value="" disabled selected>Escolha um certificado</option>
								@foreach($certificados AS $certificado)	
									@if ($curso['id_certificado'] == $certificado['id'])
										<option value="{{ $certificado['id'] }}" selected>{{ $certificado['nome'] }}</option>
									@else
										<option value="{{ $certificado['id'] }}">{{ $certificado['nome'] }}</option>
									@endif
								@endforeach
							</select>
						</div>
										
					<div class="row">	
						<div class="col s12">
							<button class="btn blue darken-1 add-curso-sistema full-width" type="submit">
								Salvar curso
								<i class="material-icons right">chevron_right</i>
							</button>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>

	<br><br><br><br>
</main>

@stop

@section('extra-javascript')

<script type="text/javascript">

$(document).ready(function() {
	$('input#input_text, textarea#descricao').characterCounter();
});

$(".remove-curso-sistema-image").on('click', function(){
	let id = $(this).data('id');
		swal({
			title: 'Deseja excluir esta imagem?',
			text: "Esta ação não poderá ser desfeita!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#b71c1c ',
			cancelButtonColor: '#ccc',
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não'
		}, function() {
			location.href = base_url('sistema/cursos/delete_image/'+id);
		});
});

</script>

@stop
