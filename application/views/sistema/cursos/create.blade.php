@extends('template.base-sistema')

@section('content')

<main>
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h1 class="header center blue-text">
				<a href="{{ base_url('sistema/welcome') }}">
					<i class="material-icons medium left blue-text" style="margin-right: -20px;">chevron_left</i>
				</a>
			    Novo
			</h1>
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('sistema/cursos/insert') }}" enctype="multipart/form-data" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Curso</h5>
						</div>	
					</div>
					
					<div class="row">	
						<div class="col s12">
				          	<label>Nome <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="nome" name="nome" type="text" class="validate browser-default" required="required">
				        </div>
				    </div>
				    <div class="row">   
				        <div class="col s12">
				          	<label>Descrição <span style="font-weight: bold; color: red;">*</span></label>
				          	<textarea data-ls-module="charCounter" maxlength="300" id="descricao" name="descricao" class="validate browser-default" type="text" required="required" rows="20" cols="33"></textarea>
				        </div>
					</div>
					<div class="row">
				    	<div class="col s12">
							<div class="file-field input-field">
								<div class="btn blue darken-1">
									<span>FOTO</span>
									<input type="file" id="foto" name="foto">
								</div>
								<div class="file-path-wrapper">
									<input class="file-path validate" type="text">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<label>Preço <span style="font-weight: bold; color: red;">*</span></label>
							<input id="preco" name="preco" type="text" class="real validate browser-default">
					    </div>
					</div>	
					<div class="row">
						<div class="col s12">
							<label>Status <span style="font-weight: bold; color: red;">*</span></label>
							<select name="status" id="status" class="browser-default" required>
								<option value="ATIVO" selected>Ativo</option>
								<option value="INATIVO">Inativo</option>
							</select>
						</div>
					</div>
					<div class="row">	
						<div class="col s12">
							<label>Certificado </label>	
							<select name="id_certificado" id="id_certificado" class="browser-default">
								<option value="" disabled selected>Escolha um certificado</option>
								@foreach($certificados AS $certificado)	
									<option value="{{ $certificado['id'] }}">{{ $certificado['nome'] }}</option>
								@endforeach
							</select>
						</div>
					</div>
									    
				    <div class="row">    
				        <div class="col s12">
							<button class="btn blue darken-1 add-curso-sistema full-width" type="submit">
								Salvar curso
								<i class="material-icons right">chevron_right</i>	
						    </button>
				        </div>
					</div>
				</form>

			</div>
		</div>
	</div>
	
	<br><br><br><br>
</main>

@stop

@section('extra-javascript')

<script type="text/javascript">

	$(document).ready(function() {
		$('input#input_text, textarea#descricao').characterCounter();
	});
	
</script>

@stop

