@extends('template.base-sistema')

@section('content')

<main>	
	
	<div class="parallax-container valign-wrapper white">
		<div class="container">
			<h3 class="header center blue-text">
				@if(isset($buscar))
					<a href="{{ base_url('sistema/contatos') }}">
						<i class="material-icons medium left blue-text" style="margin-right: -20px">chevron_left</i>
					</a>
				@else
					<a href="{{ base_url('sistema/welcome') }}">
						<i class="material-icons medium left bluee-text" style="margin-right: -20p;">chevron_left</i>
					</a>
				@endif
				@if(isset($buscar))
					Contatos
				@else
					Contatos({{ $total }})
				@endif
			</h3>
			@if(isset($buscar))
				<span class="blue-text" style="font-weight: bold;">Filtrado por: <u>{{ $buscar }}</u></span>
			@endif	
		</div> 
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">
				<form class="form-horizontal" method="post" action="{{ base_url('sistema/contatos/index') }}">    
					<div class="row" style="margin-top: -10px;">
						<div class="col s12 m6">
							<input id="buscar" name="buscar" type="text" class="input-field validate" required="required" placeholder="Pesquisa por mensagem" autocomplete="off">
				        </div>
				        <div class="col s12 m6">
					        <button class="btn full-width blue darken-1">
					        	Buscar
					        	<i class="material-icons right">chevron_right</i>
					        </button>
				    	</div>
					</div>
										
			    </form>		
				<table class="responsive-table" id="lista">
					@if (! $contatos)
						<tbody>
							  <tr>
								  <td colspan="5">
									<i class="material-icons left">folder_open</i>
									@if(isset($buscar))
										<b>Nenhum contato encontrado!</b>
									  @else
										  <b>Nenhum contato cadastrado!</b>
									  @endif  
								</td>
							  </tr>
						</tbody>  	
					@else
						<thead>
							  <tr>
								<th>ID</th>
		              			<th>NOME</th>
								<th>TELEFONE</th>
								<th>E-MAIL</th>
								<th class="right">AÇÕES</th>
							  </tr>
						</thead>
						<tbody>
							@foreach($contatos AS $contato)
								<tr>
									<td>{{ $contato['id'] }}</td>
									<td>{{ $contato['nome'] }}</td>
									@if($contato['fone'])
										<td>{{ formataTelefone($contato['fone']) }}</td>
									@else
										<td></td>
									@endif
									<td>{{ $contato['email'] }}</td>
									<td class="right">
										<div style="margin-top: -6px;">
											<div class="btn blue darken-1 mensagem tooltipped" data-id="{{ $contato['id'] }}" data-position="top" data-tooltip="Mensagem">
												<i class="material-icons">message</i>
											</div>
	
											&nbsp;
											<div class="btn blue darken-1 remove-contato-sistema tooltipped" data-id="{{ $contato['id'] }}" data-position="top" data-tooltip="Excluir">
												<i class="material-icons">delete</i>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					 @endif
				</table>			
			</div>
		</div>
	</div>
	<div class="center">
		@php
			echo $links;
		@endphp
  	</div>
	<div id="modalMensagem" class="modal">
	    <div class="modal-content"></div>
	    <a href="#!" class="btn blue darken-1 white-text modal-close" style="position: absolute; top: 8px; right: 8px">
	    	<i class="material-icons">clear</i>
	    </a>
	</div>

	<br><br><br><br>
</main>

@stop

@section('extra-javascript')
<script type="text/javascript">
	$('.mensagem').on('click', function(){
		let id = $(this).data('id');
		let url = base_url('sistema/contatos/getMensagem/' + id);
	
		$.get(url, function(data){
			$(".modal-content").html(data);
			$('#modalMensagem').modal('open'); 
		});
	});

	$(".remove-contato-sistema").on('click', function(){
		var id = $(this).data('id');
		swal({
			title: 'Tem certeza?',
			text: "Esta ação não poderá ser desfeita!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#b71c1c ',
			cancelButtonColor: '#ccc',
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não'
		}, function() {
			location.href = base_url('sistema/contatos/delete/'+id);
		});
	});
</script>
@stop