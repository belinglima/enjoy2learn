<div class="alerta-topo">
    @include('template.show-error-success-redirect')
</div>

@php
  $CI =& get_instance();
  $selected = $CI->uri->segment(2);
  $selectedrelatorio = $CI->uri->segment(3);
  $cor = 'blue';
  $texto = 'white-text';
@endphp

<div class="navbar-fixed" style="z-index: 99999999;">
    <div class="menu-topo right-align dropdown-trigger hide-on-large-only">
      <ul id="nav-mobile" class="sidenav">
        <li class="blue center-align">
          <b class="white-text">Menu</b>
        </li>
        <li class="left-align">
            <a href="{{ base_url('sistema/blog') }}" class="{{ $selected == 'blog' ? $cor.' '.$texto: '' }}">
                <i class="material-icons {{ $selected == 'blog' ? 'white-text' : 'black-text' }}">assignment</i>
                Blog
            </a>
        </li>
        <li class="left-align">
            <a href="{{ base_url('sistema/certificados') }}" class="{{ $selected == 'certificados' ? $cor.' '.$texto: '' }}">
                <i class="material-icons {{ $selected == 'certificados' ? 'white-text' : 'black-text' }}">insert_drive_file</i>
                Certificados
            </a>
        </li>
        <li class="left-align">
            <a href="{{ base_url('sistema/contatos') }}" class="{{ $selected == 'contatos' ? $cor.' '.$texto: '' }}">
                <i class="material-icons {{ $selected == 'contatos' ? 'white-text' : 'black-text' }}">mail</i>
                Contatos
            </a>
        </li>
        <li class="left-align">
            <a href="{{ base_url('sistema/cupons') }}" class="{{ $selected == 'cupons' ? $cor.' '.$texto: '' }}">
                <i class="material-icons {{ $selected == 'cupons' ? 'white-text' : 'black-text' }} ">money</i>
                Cupons
            </a>
        </li>
        <li class="left-align">
            <a href="{{ base_url('sistema/disponibilidades') }}" class="{{ $selected == 'disponibilidades' ? $cor.' '.$texto: '' }}">
                <i class="material-icons  {{ $selected == 'disponibilidades' ? 'white-text' : 'black-text' }}">beenhere</i>
                Disponibilidade
            </a>
        </li>
        <li class="left-align">
            <a href="{{ base_url('sistema/duvidas') }}" class="{{ $selected == 'duvidas' ? $cor.' '.$texto: '' }}">
                <i class="material-icons {{ $selected == 'duvidas' ? 'white-text' : 'black-text' }}">help_center</i>
                Dúvidas Frequentes
            </a>
        </li>
        <li class="left-align">
            <a href="{{ base_url('sistema/materiaisApoio') }}" class="{{ $selected == 'materiaisApoio' ? $cor.' '.$texto: '' }}">
                <i class="material-icons  {{ $selected == 'materiaisApoio' ? 'white-text' : 'black-text' }}">menu</i>
                Materiais de apoio
            </a>
        </li>
        <li class="left-align">
            <a href="{{ base_url('sistema/notificacoes') }}" class="{{ $selected == 'notificacoes' ? $cor.' '.$texto: '' }}">
                <i class="material-icons  {{ $selected == 'notificacoes' ? 'white-text' : 'black-text' }}">notifications</i>
                Notificações
            </a>
        </li>
        <li class="left-align">
            <a href="{{ base_url('sistema/relatorios/alunos') }}" class="{{ $selectedrelatorio == 'alunos' ? $cor.' '.$texto: '' }}">
                <i class="material-icons  {{ $selectedrelatorio == 'alunos' ? 'white-text' : 'black-text' }}">person_pin</i>
                Rel alunos por período
            </a>
        </li>
        <li class="left-align">
            <a href="{{ base_url('sistema/relatorios/avaliacaodoscursos') }}" class="{{ $selectedrelatorio == 'avaliacaodoscursos' ? $cor.' '.$texto: '' }}">
                <i class="material-icons  {{ $selectedrelatorio == 'avaliacaodoscursos' ? 'white-text' : 'black-text' }}">assessment</i>
                Rel avaliação dos cursos
            </a>
        </li>  
        <li class="left-align">
            <a href="{{ base_url('sistema/relatorios/cursosmaisvendidos') }}" class="{{ $selectedrelatorio == 'cursosmaisvendidos' ? $cor.' '.$texto: '' }}">
                <i class="material-icons  {{ $selectedrelatorio == 'cursosmaisvendidos' ? 'white-text' : 'black-text' }}">menu_book</i>
                Rel cursos mais vendidos
            </a>
        </li>
        <li class="left-align">
            <a href="{{ base_url('sistema/relatorios/vendas') }}" class="{{ $selectedrelatorio == 'vendas' ? $cor.' '.$texto: '' }}">
                <i class="material-icons  {{ $selectedrelatorio == 'vendas' ? 'white-text' : 'black-text' }}">money</i>
                Rel vendas por período
            </a>
        </li>             
        <li class="left-align">
            <a href="{{ base_url('sistema/usuarios') }}" class="{{ $selected == 'usuarios' ? $cor.' '.$texto: '' }}">
                <i class="material-icons {{ $selected == 'usuarios' ? 'white-text' : 'black-text' }}">settings</i>
                Usuários
            </a>
        </li>
        <li class="left-align">
            <a href="{{ base_url('login/logout') }}" class="{{ $selected == 'logout' ? $cor.' '.$texto: '' }}">
                <i class="material-icons {{ $selected == 'logout' ? 'white-text' : 'black-text' }}">logout</i>
                Sair
            </a>
        </li>
      </ul>
      <a href="#" data-target="nav-mobile" class="blue-text sidenav-trigger">
        <i class="material-icons">menu</i>
      </a>
    </div>
</div>
