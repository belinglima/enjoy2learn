<div class="nav-bottom hide-on-large-only blue">
  <nav class="transparent blue">
    <div class="nav-wrapper blue">
  
			 <div class="row" style="padding: 10px;">
          <div class="col s2 center-align">
            <a href="{{ base_url('sistema/welcome') }}">
								<i class="material-icons white-text">home</i> 
						</a>
          </div>
          <div class="col s2 center-align">
            <a href="{{ base_url('sistema/alunos') }}">
              <i class="material-icons white-text">person_pin</i>
            </a>
          </div>
          <div class="col s4 blue center-align">
            <a class="btn-floating white" 
						style="width: 60px; height: 55px; padding: 0px 0px; border-radius: 0px;" 
						href="{{ base_url('sistema/agendas') }}">
              <i class="material-icons blue-text">schedule</i>
            </a>
          </div>
          <div class="col s2 center-align">
						<a href="{{ base_url('sistema/cursos') }}">
              <i class="material-icons white-text">menu_book</i>
            </a>
          </div>
          <div class="col s2 center-align">
           	<a href="{{ base_url('sistema/vendas') }}">
              <i class="material-icons white-text">money</i>
            </a>
        	</div>

    </div>
  </nav>
</div> 
