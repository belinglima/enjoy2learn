<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>.:: Portal do aluno ::.</title>
	
	<meta name="theme-color" content="#1a237e">
	<meta name="apple-mobile-web-app-capable" content="yes">  
	<meta name="apple-mobile-web-app-status-bar-style" content="#1a237e">
	<meta name="apple-mobile-web-app-title" content="Portal do aluno">
	<meta name="description" content="Localize entregas">

	<link rel="shortcut icon" 
		href="{{ base_url('assets/images/shortcut.png') }}" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="manifest" href="{{ base_url('manifest.json') }}" />
	@include('template.css')
</head>
<body class="pace-done">
	<div class="pace pace-inactive">
		<div class="pace-progress" data-progress-text="100%" data-progress="99" 
			style="transform: translate3d(100%, 0px, 0px);">
  			<div class="pace-progress-inner"></div>
		</div>
		<div class="pace-activity"></div>
	</div>
	
	@include('template.menu-topo')
	@include('template.menu-left')
	@yield('content')
	@include('template.footer')
</body>
<script type="text/javascript">
	function base_url(caminho) {
		return '{{ base_url() }}' + caminho;
	}
</script>
@include('template.javascript')
</html>