<script type="text/javascript" src="{{ base_url('assets/javascript/jquery-3.3.1.min.js') }}"></script>

<script type="text/javascript" src="{{ base_url('assets/javascript/materialize.js') }}"></script>

<script type="text/javascript" src="{{ base_url('assets/javascript/jquery.mask.js') }}"></script>

<script type="text/javascript" src="{{ base_url('assets/javascript/jquery.inputmask.bundle.min.js') }}"></script>

<script type="text/javascript" src="{{ base_url('assets/javascript/sweetalert.min.js') }}"></script>

<script type="text/javascript" src="{{ base_url('assets/javascript/functions.js?'.filemtime('assets/javascript/functions.js')) }}"></script>

<script type="text/javascript" src="{{ base_url('assets/javascript/global.js?'.filemtime('assets/javascript/global.js')) }}"></script>

@yield('extra-javascript')