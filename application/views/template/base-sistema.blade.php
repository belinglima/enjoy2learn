<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>.:: Sistema portal do aluno ::.</title>
	
	<meta name="theme-color" content="#1a237e">
	<meta name="apple-mobile-web-app-capable" content="yes">  
	<meta name="apple-mobile-web-app-status-bar-style" content="#1a237e">
	<meta name="apple-mobile-web-app-title" content="Portal do aluno">
	<meta name="description" content="Portal do Aluno">

	<link rel="shortcut icon" 
		href="{{ base_url('assets/images/shortcut.png') }}" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="manifest" href="{{ base_url('manifest.json') }}" />
	@include('template.css-sistema')
</head>
<body>
	@include('template.menu-topo-sistema')
	@include('template.menu-left-sistema')
	@yield('content')
	@include('template.menu-bottom-sistema')
</body>
<script type="text/javascript">
	function base_url(caminho) {
		return '{{ base_url() }}' + caminho;
	}
</script>
@include('template.javascript-sistema')
</html>