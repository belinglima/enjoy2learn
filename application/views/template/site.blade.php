<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>.:: Enjoy 2 Learn ::.</title>
	
	<meta name="theme-color" content="#1a237e">
	<meta name="apple-mobile-web-app-capable" content="yes">  
	<meta name="apple-mobile-web-app-status-bar-style" content="#1a237e">
	<meta name="apple-mobile-web-app-title" content="Portal do aluno">
	<meta name="description" content="Localize entregas">

	<link rel="shortcut icon" 
		href="{{ base_url('assets/images/shortcut.png') }}" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="manifest" href="{{ base_url('manifest.json') }}" />
	@include('template.css')
</head>
<body>
	<nav>
		<div class="nav-wrapper blue">
	  		<a href="#" class="brand-logo" style="margin-left: 12px">
	  			Enjoy <img src="{{ base_url('assets/images/glass-logo.png') }}" style="width: 50px"> learn
	  		</a>
	  		<ul id="nav-mobile" class="right hide-on-med-and-down">
	    		<li><a href="#cursos">Cursos</a></li>
	    		<li><a href="#sobre">Sobre</a></li>
	    		<li><a href="#servicos">Sobre</a></li>
	    		<li><a href="{{ base_url('login') }}">Login</a></li>
	  		</ul>
		</div>
	</nav>
	@yield('content')
</body>
<script type="text/javascript">
	function base_url(caminho) {
		return '{{ base_url() }}' + caminho;
	}
</script>
@include('template.javascript')
</html>