<ul id="slide-out" class="sidenav sidenav-fixed navColor  show-on-large-only no-shadow" 
style="height: 100%; width: 100px; overflow: hidden; z-index: 999;">
    
   <li>
      <div class="user-view center-align">
        
        <br>

      </div>
    </li>
	
    <a href="{{ base_url('welcome') }}" class="mostraHover">
        <li class="{{-- tooltipped --}}" data-position="right" data-tooltip="<b>Home</b>">
            <div class="user-view center"> 
                <i class="material-icons white-text" style="font-size: 30px">home</i>
                <p class="white-text" style="margin: -30px;">Home</p>
            </div>
        </li>
    </a>

    <a href="{{ base_url('metodology') }}" class="mostraHover">
        <li class="{{-- tooltipped --}}" data-position="right" data-tooltip="<b>Metodologia</b>">
            <div class="user-view center"> 
                <i class="material-icons white-text" style="font-size: 30px">library_books</i>
                <p class="white-text" style="margin: -30px;">Metodologia</p>
            </div>
        </li> 
    </a>

    <a href="{{ base_url('library') }}" class="mostraHover">
        <li class="{{-- tooltipped --}}" data-position="right" data-tooltip="<b>Cursos</b>">
            <div class="user-view center"> 
                <i class="material-icons white-text" style="font-size: 30px">menu_book</i>
                <p class="white-text" style="margin: -30px;">Cursos</p>
            </div>
        </li>
    </a>    

    <a href="{{ base_url('teacher') }}" class="mostraHover">
        <li class="{{-- tooltipped --}}" data-position="right" data-tooltip="<b>Professor</b>">
            <div class="user-view center"> 
                <i class="material-icons white-text" style="font-size: 30px">face_retouching_natural</i>
                <p class="white-text" style="margin: -30px;">Professor</p>
            </div>
        </li>
    </a>
        
    <a href="{{ base_url('contact') }}" class="mostraHover">
        <li class="{{-- tooltipped --}}" data-position="right" data-tooltip="<b>Contato</b>">
            <div class="user-view center"> 
                <i class="material-icons white-text" style="font-size: 30px">record_voice_over</i>
                <p class="white-text" style="margin: -30px;">Contato</p>
            </div>
        </li>
    </a> 
    
</ul>