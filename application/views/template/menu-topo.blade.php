    @php
       if(get_foto()){
          $foto = get_foto();
       } else {
          $foto = NULL;
       }
      $CI =& get_instance();
      $selected = $CI->uri->segment(1);
      $cor = 'navColor';
      $texto = 'white-text';
    @endphp


{{-- menu mobile somente celular menu hamburguer --}}
<nav class="navColor no-shadow">
  <ul id="slide-out" class="sidenav" style="z-index: 9999999999;">
    <li>
      <div class="user-view center-align">
        <div class="background">
          <img src="https://materializecss.com/images/office.jpg">
        </div>
        
        <a href="#user" style="margin-left: 35%;">
          @if ($foto)
             <img src="{{ base_url('assets/uploads/perfil/'.$foto )  }}" class="circle">
          @else
            <img src="{{ base_url('assets/images/nofoto.jpg') }}" class="circle">
          @endif
        </a>
        
        <a href="#name"><span class="white-text name">
          <b>{{ ucfirst(strtoLower(get_user_nome())) }}</b></span>
        </a>
        
        <a href="{{ base_url('profile') }}"><span class="white-text email">EDITAR PERFIL</span></a>
      </div>
    </li>
    <li class="left-align">
      <a href="{{ base_url('metodology') }}" class="{{ $selected == 'metodology' ? $cor.' '.$texto: '' }}">
          <i class="material-icons {{ $selected == 'metodology' ? 'white-text' : 'black-text' }}">library_books</i>
          Metodologia
      </a>
    </li>
    <li class="left-align">
        <a href="{{ base_url('library') }}" class="{{ $selected == 'library' ? $cor.' '.$texto: '' }}">
            <i class="material-icons {{ $selected == 'library' ? 'white-text' : 'black-text' }}">menu_book</i>
            Cursos
        </a>
    </li>
    <li class="left-align">
      <a href="{{ base_url('teacher') }}" class="{{ $selected == 'teacher' ? $cor.' '.$texto: '' }}">
          <i class="material-icons {{ $selected == 'teacher' ? 'white-text' : 'black-text' }}">face_retouching_natural</i>
          Professor
      </a>
    </li>
    <li class="left-align">
      <a href="{{ base_url('contact') }}" class="{{ $selected == 'contact' ? $cor.' '.$texto: '' }}">
        <i class="material-icons {{ $selected == 'contact' ? 'white-text' : 'black-text' }}">record_voice_over</i>
        Contato
      </a>
    </li>
    <li class="left-align">
      <a href="{{ base_url('duvidas-gerais') }}" class="{{ $selected == 'duvidas-gerais' ? $cor.' '.$texto: '' }}">
        <i class="material-icons {{ $selected == 'duvidas-gerais' ? 'white-text' : 'black-text' }}">help_center</i>
        Dúvidas frequentes
      </a>
    </li>
    <li class="left-align notify">
      <a href="javaScript:void(0);">
        <i class="material-icons black-text">notification_important</i>
        Notificações do professor
      </a>
    </li>
    <li class="left-align">
      <a href="{{ base_url('cart') }}" class="{{ $selected == 'cart' ? $cor.' '.$texto: '' }}">
        <i class="material-icons {{ $selected == 'cart' ? 'white-text' : 'black-text' }}">shopping_cart</i>
        Minhas compras
      </a>
    </li>
    <li class="left-align">
      <a href="{{ base_url('classes') }}" class="{{ $selected == 'classes' ? $cor.' '.$texto: '' }}">
        <i class="material-icons {{ $selected == 'classes' ? 'white-text' : 'black-text' }}">local_library</i>
        Meus cursos
      </a>
    </li>
    <li class="left-align">
      <a href="{{ base_url('certificates') }}" class="{{ $selected == 'certificates' ? $cor.' '.$texto: '' }}">
        <i class="material-icons {{ $selected == 'certificates' ? 'white-text' : 'black-text' }}">verified</i>
        Meus certificados
      </a>
    </li>
    <li class="left-align">
      <a href="{{ base_url('purchases') }}" class="{{ $selected == 'purchases' ? $cor.' '.$texto: '' }}">
        <i class="material-icons {{ $selected == 'purchases' ? 'white-text' : 'black-text' }}">shopping_bag</i>
        Minhas compras
      </a>
    </li>
    <li class="left-align">
      <a href="{{ base_url('login/logout') }}" class="{{ $selected == 'logout' ? $cor.' '.$texto: '' }}">
          <i class="material-icons {{ $selected == 'logout' ? 'white-text' : 'black-text' }}">logout</i>
          Sair
      </a>
    </li>
  </ul>

{{-- menu para tablet e computador --}}
  <ul class="hide-on-med-and-down center-align" style="margin-left: 12%">

    <div class="left tooltipped" data-position="bottom" data-tooltip="<b>Hello {{ ucfirst(strtoLower(get_user_nome())) }}</b>">
      <li>
        <div class="valign-wrapper">
          @if ($foto)
            <img src="{{ base_url('assets/uploads/perfil/'.$foto)  }}" class="responsive-img circle" 
              style="width: 30px;">
          @else
            <img src="{{ base_url('assets/images/nofoto.jpg') }}" class="responsive-img circle" 
              style="width: 30px;">
          @endif
        
          <span style="padding-left: 15px;"> <b>{{ ucfirst(strtoLower(get_user_nome())) }}</b> </span>
        </div>
      </li>
    </div>
    
    <div class="right">
      
      <li class="center-align tooltipped" data-position="left" data-tooltip="<b>Dúvidas frequentes</b>">
          <a href="{{ base_url('duvidas-gerais') }}">
            <i class="material-icons white-text" style="height: 65px;">help_center</i>
          </a>
      </li>

      <li class="center-align tooltipped notify" data-position="left" data-tooltip="<b>Notificações do professor</b>">
          <a href="javaScript:void(0);">
            <i class="material-icons white-text" style="height: 65px;">notification_important</i>
          </a>
      </li>

      <li class="center-align tooltipped" data-position="bottom" data-tooltip="<b>Minhas compras</b>">
        <div class="center-align" style="height: 45px;">
          
          <a  href="{{ base_url('cart') }}">
            <i class="material-icons white-text" style="height: 65px;">shopping_cart</i>
          </a>

          <span class="navText" style="position: relative; top: -74px; z-index: 0;">
            <b class="totalCarrinho">
              
            </b>
          </span>
        
        </div>
      </li>
      
      <li style="width: 250px;">
        <a class="dropdown-trigger" href="#!" data-target="dropdownUsuario">
         <b>Gerenciar</b>
        <i class="material-icons right">arrow_drop_down</i>
        </a>
      </li>

      <ul id="dropdownUsuario" class="dropdown-content right" style="z-index: 999999990;">
        <li>
          <a href="{{ base_url('profile') }}" class="blue-text">
            <i class="material-icons blue-text tiny">dashboard</i> Meu Perfil
          </a>
        </li>
        <li>
          <a href="{{ base_url('classes') }}" class="blue-text">
            <i class="material-icons blue-text tiny">local_library</i> Meus cursos
          </a>
        </li>
        <li>
          <a href="{{ base_url('certificates') }}" class="blue-text">
            <i class="material-icons blue-text tiny">verified</i> Meus certificados
          </a>
        </li>
        <li>
          <a href="{{ base_url('purchases') }}" class="blue-text">
            <i class="material-icons blue-text tiny">shopping_bag</i> Minhas compras
          </a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="{{ base_url('login/logout') }}" class="blue-text">
              <i class="material-icons blue-text tiny">logout</i> Sair
          </a>
        </li>
      </ul>
    </div>
  </ul>

  <a href="#" data-target="slide-out" class="sidenav-trigger right">
    <i class="material-icons">menu</i>
  </a>

  {{-- /* /* /* <span class="hide-on-med-and-up" style="margin-left: 35%; position: absolute; top: 5px;"> */ */ */ --}}
     <img class="hide-on-med-and-up" src="{{ base_url('assets/images/logo.png') }}" style="width: 160px; padding: 5px; margin-left: 30%; border-radius: 50px;">
  {{-- </span> --}}

  <div class="alerta-topo">
      @include('template.show-error-success-redirect')
  </div>
</nav>

<div class="notificacoes hide" style="display: block;">
  <ul class="lista-notificacoes">
    {{-- <li>
      <img src="{{base_url('assets/images/2.svg')}}" width="60" style="margin-left: 50%; transform: translate(-50%);">
    </li> --}}
  </ul>
  {{-- <a href="javaScript:void(0);" class="btt-notificacoes-lista">Aguarde...</a> --}}
</div>

<div class="orelha hide-on-med-and-down"></div>


  
