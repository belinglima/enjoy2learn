<ul id="slide-out" class="sidenav sidenav-fixed blue darken-1 menu-left show-on-large-only z-depth-3">
	 <li>
        <div class="user-view center" 
            style="margin: 0px; margin-bottom: 20px;">
            <a href="{{ base_url('sistema/welcome') }}">
                <img src="{{ base_url('assets/images/logo.png') }}" class="tooltipped" data-position="bottom" data-tooltip="Administrador" style=" width: 150px; border-radius: 50px;">
            </a>
        </div>
    </li>
    <li><div class="separador"></div></li>
    <li>
        <a href="{{ base_url('sistema/welcome') }}">
            <i class="material-icons white-text">home</i>
            <font class="white-text">Home</font>
        </a>
    </li>
    <li>
        <a href="{{ base_url('sistema/agendas') }}">
            <i class="material-icons white-text">schedule</i>
            <font class="white-text">Agenda</font>
        </a>
    </li>
    <li>
        <a href="#" data-target="slide-out-site" class="sidenav-trigger">
            <i class="material-icons white-text">menu_book</i>
            <font class="white-text">Cursos</font>
        </a>
    </li>
    <li>
        <a href="{{ base_url('sistema/disponibilidades') }}">
            <i class="material-icons white-text">beenhere</i>
            <font class="white-text truncate">Disponibilidade</font>
        </a>
    </li>
    <li>
        <a href="#" data-target="slide-out-siteNovo" class="sidenav-trigger">
            <i class="material-icons white-text">admin_panel_settings</i>
            <font class="white-text">Gerenciar</font>
        </a>
    </li>
    <li>
        <a href="{{ base_url('sistema/usuarios') }}">
            <i class="material-icons white-text">settings</i> 
            <font class="white-text">Usuários</font>
        </a>
    </li>
    <li>
    	<a href="{{ base_url('login/logout') }}">
    		<i class="material-icons white-text">logout</i> 
    		<font class="white-text">Sair</font>
    	</a>
    </li>
</ul>
<ul id="slide-out-site" class="sidenav externo no-shadow blue" style="padding-top: 64px;">
    <div class="filhoExterno">
        <li>
            <a href="{{ base_url('sistema/certificados') }}">
                <i class="material-icons white-text">insert_drive_file</i> 
                <font class="white-text">Certificados</font>
            </a>
        </li>
        <li>
            <a href="{{ base_url('sistema/cupons') }}">
                <i class="material-icons white-text">money</i> 
                <font class="white-text">Cupons</font>
            </a>
        </li>
        <li>
            <a href="{{ base_url('sistema/cursos') }}">
                <i class="material-icons white-text">menu_book</i> 
                <font class="white-text">Cursos</font>
            </a>
        </li>
        <li>
            <a href="{{ base_url('sistema/duvidas') }}">
                <i class="material-icons white-text">help_center</i> 
                <font class="white-text">Dúvidas frequentes</font>
            </a>
        </li>
        <li>
            <a href="{{ base_url('sistema/materiaisApoio') }}">
                <i class="material-icons white-text">menu</i> 
                <font class="white-text">Materiais de apoio</font>
            </a>
        </li>
    </div>
</ul>

<ul id="slide-out-siteNovo" class="sidenav externo no-shadow blue" style="padding-top: 64px;">
    <div class="filhoExterno">
        <li>
            <a href="{{ base_url('sistema/alunos') }}">
                <i class="material-icons white-text">person_pin</i> 
                <font class="white-text">Alunos</font>
            </a>
        </li>
        <li>
            <a href="{{ base_url('sistema/blog') }}">
                <i class="material-icons white-text">assignment</i> 
                <font class="white-text">Blog</font>
            </a>
        </li>
        <li>
            <a href="{{ base_url('sistema/contatos') }}">
                <i class="material-icons white-text">mail</i> 
                <font class="white-text">Contatos</font>
            </a>
        </li>
        <li>
            <a href="{{ base_url('sistema/notificacoes') }}">
                <i class="material-icons white-text">notifications</i> 
                <font class="white-text">Notificações</font>
            </a>
        </li>
        <li>
            <a href="{{ base_url('sistema/vendas') }}">
                <i class="material-icons white-text">money</i> 
                <font class="white-text">Vendas</font>
            </a>
        </li>
        <li>
            <a href="#" data-target="slide-out-relatorio" class="sidenav-trigger">
                <i class="material-icons white-text">list</i>
                <font class="white-text">Relatórios</font>
            </a>
        </li>
    </div>
</ul>

<ul id="slide-out-relatorio" class="sidenav externo no-shadow blue" style="padding-top: 64px;">
    <div class="filhoExterno">
        <li>
            <a href="{{ base_url('sistema/relatorios/alunos') }}">
                <i class="material-icons white-text">person_pin</i> 
                <font class="white-text">Alunos por período</font>
            </a>
        </li>
        <li>
            <a href="{{ base_url('sistema/relatorios/avaliacaodoscursos') }}">
                <i class="material-icons white-text">assessment</i> 
                <font class="white-text">Avaliação dos cursos</font>
            </a>
        </li>
        <li>
            <a href="{{ base_url('sistema/relatorios/cursosmaisvendidos') }}">
                <i class="material-icons white-text">menu_book</i> 
                <font class="white-text">Cursos mais vendidos</font>
            </a>
        </li>
        <li>
            <a href="{{ base_url('sistema/relatorios/vendas') }}">
                <i class="material-icons white-text">money</i> 
                <font class="white-text">Vendas por período</font>
            </a>
        </li>
    </div>
</ul>