@extends('template.base')

@php
    $CI =& get_instance();
		$codigoVideo = $CI->uri->segment(4);

    if(!!$codigoVideo){
		$decrypt = new Encryptor();
    	$urlAula = $decrypt->decrypt($codigoVideo);
    	$string = base64_encode($urlAula);
    } else {
    	$string = base64_encode('jnl0SzFOqLQ');
    }

    $i = 1;
@endphp

@section('content')

<main oncontextmenu="return false;">	
{{-- <main>	 --}}
	
	<div class="container" style="width: 95%; margin-left: 40px; margin-top: 20px;">

		<div class="row left" style="border-bottom: 1px solid #DFDFDF; width: 66%;">
			<div class="col s12 left-align">
				<div class="contorno-titulo-pagina">
					<h1>
						<strong>
							E-Learning idiomas
						</strong>
					</h1>
				</div>

				<div class="mapeamento-pagina">
		            <a href="{{ base_url('welcome') }}">Home</a> &gt;  
		            <a href="{{ base_url('classes') }}">Meus cursos</a> &gt;  
		            <strong>Player</strong>
		        </div>
			</div>
		</div>

		<div class="row">
			<div class="col s12 m8">
				<p style="font-size: 16px; font-weight: 900; padding: 10px;"> 
					{{ ucfirst(strtolower($curso->nome)) }}
				</p>

				<div id="player" class="vlite-js" data-youtube-id=""></div>

				@if($materiais)
					<div class="col s12 left" style="margin-top: 20px;">
						<p style="background: var(--dashboard); color: var(--white); right: 0px; padding: 5px 0 5px 10px;">Material de apoio</p>
					</div>

					<div class="col s12 left">
						@foreach($materiais as $material)

							<p>
								@if($material['url_imagem'] != NULL)
									<a href="{{ base_url('assets/uploads/materiaisApoio/' .$material['url_imagem']) }}"  
										target="_blank">
										{{ $i }} - IMAGEM
									</a>
								@elseif($material['url_pdf'] != NULL)
									<a href="{{ base_url('assets/uploads/materiaisApoio/' .$material['url_pdf']) }}"  
										target="_blank">
										{{ $i }} - PDF
									</a>
								@elseif($material['url_audio'] != NULL)
									<a href="{{ base_url('assets/uploads/materiaisApoio/' .$material['url_audio']) }}"  
										target="_blank">
										{{ $i }} - AUDIO
									</a>
								@endif
							</p>

							@php
								$i++;
							@endphp
						@endforeach
					</div>
				@endif
			</div>

			<div class="col s12 m4 right" style="">
				<p style="font-weight: 900;">Aulas disponíveis</p>

				<ul class="collection">
					@foreach($aulas as $aula)

						@php
							$target = base_url('player/'.$encryptor->encrypt($aula['id_curso']).'/'.$encryptor->encrypt($aula['id']).'/'.$encryptor->encrypt($aula['url']) );
						@endphp

				        <li class="collection-item">
				        	<div>
				        		
				        		@if($aula['idAula'])
				        			<i class="material-icons a">done</i>
				        		@else
				        			<i class="material-icons a"></i>
				        		@endif

				        		<a href="{{ $target }}" class="secondary-content" style="font-size: 12px;">
				        			{{ $aula['nome'] }}
				        		</a>
				        	</div>
				        </li>
					@endforeach
			    </ul>
			</div>

			
		</div>
	</div>

</main>

@stop

@section('extra-javascript')

  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/vlitejs@4/dist/vlite.css">
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/vlitejs@4"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/vlitejs@4/dist/providers/youtube.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/vlitejs@4/dist/providers/vimeo.js"></script>

  <style type="text/css">
  	 .v-vlite{
      --vlite-colorPrimary: red;
    }
  </style>


<script type="text/javascript">

	Vlitejs.registerProvider('youtube', VlitejsYoutube);

    function adw987sad987sad98sa7d97sa87a9sd87as897(data) {
        if (typeof atob === "function") {
            return atob(data);
        } else if (typeof Buffer === "function") {
            return Buffer.from(data, "base64").toString("utf-8");
        } else {
            throw new Error("Failed to determine the platform specific decoder");
        }
    }

    $('.vlite-js').attr('data-youtube-id', adw987sad987sad98sa7d97sa87a9sd87as897('<?=$string?>') + '?disablekb=1&modestbranding=1&color=white&iv_load_policy=3');
    // Vlitejs.registerProvider('vimeo', VlitejsVimeo);

    new Vlitejs('#player', {
      options: {
        controls: true,
        autoplay: false,    
        playPause: true,
        progressBar: true,
        time: true,
        volume: true,
        fullscreen: true,
        // poster: 'https://yoriiis.github.io/cdn/static/vlitejs/demo-poster.jpg',
        // poster: 'tela.png',
        bigPlay: true,
        playsinline: true,
        loop: false,
        muted: false,
        autoHide: true,
        providerParams: {}
        },
      provider: 'youtube',
      // provider: 'vimeo',
      onReady: function (player) {
        player.on('play', () => console.log('play'))
        player.on('pause', () => console.log('pause'))
        player.on('progress', () => console.log('progress'))
        player.on('timeupdate', () => console.log('timeupdate'))
        player.on('volumechange', () => console.log('volumechange'))
        player.on('enterfullscreen', () => console.log('enterfullscreen'))
        player.on('exitfullscreen', () => console.log('exitfullscreen'))
        player.on('enterpip', () => console.log('enterpip'))
        player.on('leavepip', () => console.log('leavepip'))
        player.on('trackenabled', () => console.log('trackenabled'))
        player.on('trackdisabled', () => console.log('trackdisabled'))
        player.on('ended', () => {console.log('ended'); })
        }
    });

    function rejeitaTecla(oEvent){
      var oEvent = oEvent ? oEvent : window.event;
      var tecla = (oEvent.keyCode) ? oEvent.keyCode : oEvent.which;
        if (tecla) {
          return false;
        }
    }
    document.onkeypress = rejeitaTecla;
    document.onkeydown = rejeitaTecla;
    document.oncontextmenu = rejeitaTecla;
</script>

@stop