@extends('template.base')

@section('content')

<main>
	
	<div class="parallax-container valign-wrapper {{ get_empresa_cor() }}">
		<div class="container">
			<h1 class="header center white-text truncate">
				<a href="{{ base_url('clientes') }}">
					<i class="material-icons medium left white-text" style="margin-right: -20px;">chevron_left</i>
				</a>
				Editar
			</h1>
		</div>
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('clientes/update/'.$cliente['id']) }}" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Cliente</h5>
						</div>
					</div>
					
					<div class="row">	
					    <div class="col s12 m8">
				          	<label>Nome fantasia <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="nome" name="cad[nome]" type="text" class="validate browser-default" value="{{ ucwords(strtolower($cliente['nome'])) }}"  required="required">
				        </div>
				        <div class="col s12 m4">
				          	<label>CPF/CNPJ <span style="font-weight: bold; color: red;">*</span></label> 
				          	<input id="cpfcnpj" name="cad[cpfcnpj]" data-id="" type="text" class="validate browser-default" value="{{ $cliente['cpfcnpj'] }}" required="required">
				        </div>
				    </div>

				    <div class="row">
				    	<div class="col s12">
				          	<label>Razão social <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="razao_social" name="cad[razao_social]" type="text" value="{{ ucwords(strtolower($cliente['razao_social'])) }}" class="validate browser-default" required="required">
				        </div>
					</div>

					<div class="row">
						<div class="col s12 m10">
				          	<label>Inscrição estadual <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="ie" name="cad[ie]" type="text" value="{{ $cliente['ie'] }}" class="validate browser-default" required="required">
				        </div>
				        <div class="col s12 m2">
							<label>Tem? <span style="font-weight: bold; color: red;">*</span></label>
							<select class="browser-default inscricao">
								@if ($cliente['ie'] == 0)
									<option value="1">Sim</option>
									<option value="0" selected>Não</option>
								@else
									<option value="1" selected>Sim</option>
									<option value="0">Não</option>
								@endif
							</select>
						</div>
					</div>	

				    <div class="row">
						<div class="col s12">
							<h5>Endereço</h5>
						</div>
					</div>
					
					<div class="row">	
						<div class="col s12 m4">
							<label>CEP <span style="font-weight: bold; color: red;">*</span></label>
							<input id="cep" name="cad[cep]" type="text" class="validate browser-default cep" value="{{ $cliente['cep'] }}"  onblur="pesquisacep(this.value);"
							size="10" maxlength="9" required="required">
						</div>
						<div class="col s12 m6">
							<label>Logradouro <span style="font-weight: bold; color: red;">*</span></label>
							<input id="logradouro" name="cad[logradouro]" type="text" class="validate browser-default" value="{{ ucwords(strtolower($cliente['logradouro'])) }}" required="required">
						</div>
						<div class="col s12 m2">
							<label>Nº <span style="font-weight: bold; color: red;">*</span></label>
							<input id="numero" name="cad[numero]" type="text" class="validate browser-default" value="{{ $cliente['numero'] }}" required="required">
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">
							<label>Complemento</label>
							<input id="comple" name="cad[complemento]" type="text" class="validate browser-default" value="{{ ucwords(strtolower($cliente['complemento'])) }}" >
						</div>
						<div class="col s12 m6"> 
							<label>Bairro <span style="font-weight: bold; color: red;">*</span></label>
							<input id="bairro" name="cad[bairro]" type="text" class="validate browser-default" value="{{ ucwords(strtolower($cliente['bairro'])) }}" required="required">
						</div>
					</div>
					
					<div class="row">
						<div class="col s12 m6">
							<label>Localidade <span style="font-weight: bold; color: red;">*</span></label> 
							<input id="localidade" name="cad[localidade]" type="text" class="validate browser-default" value="{{ ucwords(strtolower($cliente['localidade'])) }}" required="required">
						</div>
						<div class="col s12 m6">	
							<label>Estado <span style="font-weight: bold; color: red;">*</span></label>
							<select name="cad[estado_id]" id="estado_id" class="estado browser-default" required="required">
								<option value="" disabled selected>Escolha um estado</option>
								@foreach($estados AS $estado)	
									@if ($cliente['estado_id'] == $estado['id'])
										<option value="{{ $estado['id'] }}" selected>{{ $estado['descricao'] }}</option>
									@else
										<option value="{{ $estado['id'] }}">{{ $estado['descricao'] }}</option>
									@endif
								@endforeach
							</select>
						</div>
					</div>

					<div class="row">
                    	<div class="col s12 center">
	                      <p>
	                      	<span style="font-weight: bold; color: red;">*</span>
	                      	Em caso de não encontrar o CEP, será necessário apontar empresa no mapa.
	                      </p>
	                      <p>
	                      	<span style="font-weight: bold; color: red;">*</span>
	                      	Dica de ouro, caso não saiba exatamente onde à empresa está, aponte sobre a cidade do cliente.
	                      </p>
	                    </div>
	                </div>  
	                	
	                <div class="row">
	                  <div class="col s11">
						<div class="btn {{ get_empresa_cor() }} darken-1 getMapa full-width abre-mapa">
							Abrir mapa
						 	<i class="material-icons right">place</i>
						</div>
	                    <input id="cidade_latitude" type="hidden" value="-12.6469717" />
	                    <input id="cidade_longitude" type="hidden" value="-60.4262417" />
	                  </div>
	                  <div class="col s1">
	                      <div class="btn {{ get_empresa_cor() }}  darken-1 full-width abre-ajuda">
	                        <i class="material-icons right">help</i>
	                      </div>
	                  </div>  
	                </div>

    				<div class="row">
						<div class="col s12">
							<h5>Telefones</h5>
						</div>
					</div>

					@for ($i = 0; $i < 3; $i++)
						@if (isset($contatos[$i]['telefone']))
							@if ($contatos[$i]['id_cliente'] == $cliente['id'])
								<div class="row">
									<div class="col s12 m4">
										@if($i == 0)
											<label>Nome <span style="font-weight: bold; color: red;">*</span></label>
										@else
											<label>Nome</label>
										@endif
										<input id="nome" name="contato[nome][{{ $i }}]" type="text" class="validate browser-default" value="{{ ucwords(strtolower($contatos[$i]['nome'])) }}">
									</div>
									<div class="col s12 m4">
										@if($i == 0)
											<label>Número <span style="font-weight: bold; color: red;">*</span></label>
										@else
											<label>Número</label>
										@endif
										<input id="telefone" name="contato[telefone][{{ $i }}]" type="text" class="validate fone browser-default" value="{{ $contatos[$i]['telefone'] }}">
									</div>
									<div class="col s12 m4">
										@if($i == 0)
											<label>Tipo <span style="font-weight: bold; color: red;">*</span></label>
										@else
											<label>Tipo</label>
										@endif
										<select name="contato[tipo][{{ $i }}]" id="tipo" value="contato[{{ $contatos[$i]['tipo'] }}]" class="browser-default">
												<option value="" disabled selected>Escolha um tipo</option>
											@if($contatos[$i]['tipo'] == 'TELEFONE')
												<option value="TELEFONE" selected>Telefone</option>
												<option value="WHATSAPP">Whatsapp</option>
											@else
												<option value="TELEFONE">Telefone</option>
												<option value="WHATSAPP" selected>Whatsapp</option>
											@endif
										</select>
									</div>
									<input type="hidden" name="contato[view][{{ $i }}]" value="1">
								</div>
							@endif
						@else
							<div class="row">
								<div class="col s12 m4">
									@if($i == 0)
										<label>Nome <span style="font-weight: bold; color: red;">*</span></label>
									@else
										<label>Nome</label>
									@endif	
									<input id="nome" name="contato[nome][{{ $i }}]" type="text" class="validate browser-default">
								</div>
								<div class="col s12 m4">
									@if($i == 0)
										<label>Número <span style="font-weight: bold; color: red;">*</span></label>
									@else
										<label>Número</label>
									@endif	
									<input id="telefone" name="contato[telefone][{{ $i }}]" type="text" class="validate fone browser-default">
								</div>
								<div class="col s12 m4">
									@if($i == 0)
										<label>Tipo <span style="font-weight: bold; color: red;">*</span></label>
									@else
										<label>Tipo</label>
									@endif
									<select name="contato[tipo][{{ $i }}]" id="tipo" class="browser-default">
										<option value="" disabled selected>Escolha um tipo</option>
										<option value="TELEFONE">Telefone</option>
										<option value="WHATSAPP">Whatsapp</option>
									</select>
								</div>
								<input type="hidden" name="contato[view][{{ $i }}]" value="1">
							</div>
						@endif		
					@endfor
				
					<input id="latitude" name="cad[latitude]"  type="hidden" value="{{ $cliente['latitude'] }}">
					<input id="longitude" name="cad[longitude]" type="hidden" value="{{ $cliente['longitude'] }}">

					<div class="row">
					    <div class="col s12">
				        	<button class="btn {{ get_empresa_cor() }} darken-1 add-cliente full-width" type="submit">
				        		Salvar cliente
				        		<i class="material-icons right">chevron_right</i>
				        	</button>
				        </div>
					</div>
				</form>

			</div>
		</div>
	</div>

	<div id="modal-mapa" class="modal modal-mapa">
    	<div class="modal-content content-mapa" style="padding: 0px;">
    		<div id="mapid" style="width: 100%; height: 360px;"></div>
      		<a href="#!" class="btn-floating modal-close {{ get_empresa_cor() }} darken-1" style="position: absolute; top: 5px; right: 5px; z-index: 9999;">
      			<i class="material-icons">close</i>
      		</a>
    	</div>
  	</div>

  	<div id="modal-ajuda" class="modal modal-ajuda">
	  <div class="modal-content content-ajuda" style="padding: 0px;">
	    <div id="ajuda" style="width: 100%; height: 360px;">

	      <iframe class="videoPlay" width="100%" height="360"
	        src="https://www.youtube.com/embed/mcwldvn-v6I">
	      </iframe>
	    
	    </div>  
	    </div>
	      <a href="#!" class="btn-floating modal-close {{ get_empresa_cor() }} darken-1" style="position: absolute; top: 5px; right: 5px; z-index: 9999;">
	        <i class="material-icons">close</i>
	      </a>
	    </div>
	</div>

	<br />
	
</main>

@stop

@section('extra-javascript')

<script type="text/javascript" src="{{ base_url('assets/javascript/clientes.js?v='.Version) }}"></script>
<script type="text/javascript" src="{{ base_url('assets/javascript/consultaCep.js?v='.Version) }}"></script>

<script type="text/javascript">
	$(document).ready(function(){
		var mymap;
		var marker;
		
		$('.abre-mapa').on('click', function(){
			$('.modal-mapa').modal('open');

			let cidade_latitude = $('#cidade_latitude').val();
			let cidade_longitude = $('#cidade_longitude').val();

			let a = $("#latitude").val();
			let b = $("#longitude").val();

			setTimeout(function(){
				if (mymap != undefined) {
					return;
				}

				mymap = L.map('mapid').setView([a, b], 10);
				L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
				    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
				}).addTo(mymap);

				mymap.on('click', onMapClick);

				if (a != "" || b != "") {
					marker = L.marker([a, b]).addTo(mymap);
					mymap.setZoom(14);
				}
			}, 680);
		});	

		const onMapClick = (e) => {
			if (marker != undefined) {
				marker.remove();
			}

			marker = L.marker([e.latlng.lat, e.latlng.lng]).addTo(mymap);
			mymap.setView([e.latlng.lat, e.latlng.lng], 17);

			$("#latitude").val(e.latlng.lat);
			$("#longitude").val(e.latlng.lng);
		}

		$('#modal-ajuda').modal({
	      dismissible: false
	    });

	    $('.abre-ajuda').on('click', function(){
	      $('.modal-ajuda').modal('open');
	    });

	    $('.modal-close').on('click', function(){
	        const videos = document.querySelectorAll('iframe');
	           videos.forEach(i => {
	              let source = i.src;
	              i.src = '';
	              i.src = source;
	           });
	    });    

	    	$('.add-cliente').on('click', function(){

		if($('.produto').val() == '') {
			setTimeout(function(){ 
				swal('Alerta','Preencha o nome do produto','error');
				$('.produto').focus();
			}, 900);
			return;
		}
	
		if($('input.tipo[type="checkbox"]:checked').length == 0 ) {
      		swal('Alerta','Preencha pelo menos um veículo','error');
      		return;
      	}

      	if($('input.carroceria[type="checkbox"]:checked').length == 0 ) {
      		swal('Alerta','Preencha pelo menos uma carroceria ','error');
      		return;
      	}

      	if($('.embarcador').val() == null) {
			swal('Alerta','Selecione o embarcador','error');
			return;
		}

		if($('.destinatario').val() == '' && $('.dest').val() == null) {
			swal('Alerta','Selecione o destinatário','error');
			return;
		}

		if($('.real').val() == '') {
			setTimeout(function(){ 
				swal('Alerta','Preencha o valor','error');
				$('.real').focus();
			}, 900);
			return;

		}

		document.cadastro.submit();
      
    });     

	});
</script>

@stop