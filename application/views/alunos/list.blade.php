@extends('template.base')

@section('content')

<main>	
	<div class="parallax-container valign-wrapper {{ get_empresa_cor() }}">
		<div class="container">
			<h1 class="header center white-text">
				@if(isset($buscar))
					<a href="{{ base_url('clientes') }}">
						<i class="material-icons medium left white-text" style="margin-right: -20px">chevron_left</i>
					</a>
				@else
					<a href="{{ base_url('welcome') }}">
						<i class="material-icons medium left white-text" style="margin-right: -20px;">chevron_left</i>
					</a>
				@endif
			    Clientes
			</h1>
			@if(isset($buscar))
				<span class="white-text" style="font-weight: bold;">Filtrado por: <u>{{ $buscar }}</u></span>
			@endif		
		</div> 
	</div>

	<a href="{{ base_url('clientes/create') }}" class="btn-floating btn-large waves-effect waves-light {{ get_empresa_cor() }} darken-1 right scale-transition scale-out" style="margin-top: -50px; margin-right: 20px;">
		<i class="material-icons">add</i>
	</a>

	<div class="container">
		<div class="card">
			<div class="card-content">
				<form class="form-horizontal" action="{{ base_url('clientes/index') }}" method="POST">
					<div class="row" style="margin-top: -10px;">
						<div class="col s12 m6">
				          	<input id="buscar" name="buscar" type="text" class="input-field validate" required="required" placeholder="Pesquisa por nome fantasia, CPF ou CNPJ (somente os números)" autocomplete="off">
				        </div>
				        <div class="col s12 m6">
					        <button class="btn full-width {{ get_empresa_cor() }} darken-1">
					        	Buscar
					        	<i class="material-icons right">chevron_right</i>
					        </button>
				        </div>
					</div>
				</form>
				<table class="sortable" id="lista">
		        	<thead>
		          		<tr>
		              		<th>NOME</th>
		              		<th>CIDADE</th>
		              		<th>ESTADO</th>
		              		<th class="right">AÇÕES</th>
		          		</tr>
		        	</thead>
		        	<tbody>
			          	@if (! $clientes)
			          	<tr>
			          		<td colspan="4">
			          			<i class="material-icons left">folder_open</i> 
			          			@if(isset($buscar))
			          				<b>Nenhum cliente encontrado!</b>
			          			@else
			          				<b>Nenhum cliente cadastrado!</b>
			          			@endif
			          		</td>
			          	</tr>
			          	@endif

			          	@foreach($clientes AS $cliente)
			          	<tr>
			          		<td>{{ ucwords(strtolower($cliente['nome'])) }}</td>
			          		<td>{{ ucwords(strtolower($cliente['localidade'])) }}</td>
			          		<td>{{ ucfirst(strtoupper($cliente['sigla'])) }}</td>
			          		<td class="right">
			          			<div style="margin-top: -6px;">
				          			<a href="{{ base_url('clientes/edit/'. $encryptor->encrypt($cliente['id']) ) }}">
					          			<div class="btn {{ get_empresa_cor() }} darken-1 tooltipped" data-position="top" data-tooltip="Editar">
					          				<i class="material-icons">edit</i>
					          			</div>
					          		</a>
					          		&nbsp;
				          			<div class="btn {{ get_empresa_cor() }} darken-1 remove-cliente tooltipped" data-id="{{ $cliente['id'] }}" data-position="top" data-tooltip="Excluir">
				          				<i class="material-icons">delete</i>
				          			</div>	
			          			</div>
			          		</td>
			          	</tr>
			          	@endforeach
		        	</tbody>
		      	</table>

			</div>
		</div>
	</div>
	<div class="center">
		@php
			echo $links;
		@endphp
  	</div>

  	<br />

</main>

@stop

@section('extra-javascript')

<script type="text/javascript" src="{{ base_url('assets/javascript/clientes.js?v='.Version) }}"></script>

@stop