@extends('template.base')

@section('content')

<main>
	
	<div class="parallax-container valign-wrapper {{ get_empresa_cor() }}">
		<div class="container">
			<h1 class="header center white-text truncate">
				<a href="{{ base_url('clientes') }}">
					<i class="material-icons medium left white-text" style="margin-right: -20px;">chevron_left</i>
				</a>
				Novo
			</h1>
		</div>
	</div>

	<div class="container">
		<div class="card">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('clientes/insert') }}" method="POST">
					<div class="row">
						<div class="col s12">
							<h5>Cliente</h5>
						</div>
					</div>
					
					<div class="row">	
					    <div class="col s12 m8">
				          	<label>Nome fantasia <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="nome" name="cad[nome]" type="text" class="validate browser-default" required="required">
				        </div>
				        <div class="col s12 m4">
				          	<label>CPF/CNPJ <span style="font-weight: bold; color: red;">*</span></label> 
				          	<input id="cpfcnpj" name="cad[cpfcnpj]" data-id="" type="text" class="validate browser-default" required="required">
				        </div>
				    </div>

				      <div class="row">
				    	<div class="col s12">
				          	<label>Razão social <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="razao_social" name="cad[razao_social]" type="text" class="validate browser-default" required="required">
				        </div>
					</div>

				    <div class="row">
						<div class="col s12 m10">
				          	<label>Inscrição estadual <span style="font-weight: bold; color: red;">*</span></label>
				          	<input id="ie" name="cad[ie]" type="text" class="validate browser-default" required="required">
				        </div>
				        <div class="col s12 m2">
							<label>Tem? <span style="font-weight: bold; color: red;">*</span></label>
							<select class="browser-default inscricao">
								<option value="1">Sim</option>
                        		<option value="0">Não</option>
							</select>
						</div>
					</div>	

				    <div class="row">
						<div class="col s12">
							<h5>Endereço</h5>
						</div>
					</div>
					
					<div class="row">	
						<div class="col s12 m4">
							<label>CEP <span style="font-weight: bold; color: red;">*</span></label>
							<input id="cep" name="cad[cep]" type="text" class="validate browser-default cep" onblur="pesquisacep(this.value);"
							size="10" maxlength="9" required="required">
						</div>
						<div class="col s12 m6">
							<label>Logradouro <span style="font-weight: bold; color: red;">*</span></label>
							<input id="logradouro" name="cad[logradouro]" type="text" class="validate browser-default" required="required">
						</div>
						<div class="col s12 m2">
							<label>Nº <span style="font-weight: bold; color: red;">*</span></label>
							<input id="numero" name="cad[numero]" type="text" class="validate browser-default" required="required">
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">
							<label>Complemento</label>
							<input id="comple" name="cad[complemento]" type="text" class="validate browser-default">
						</div>
						<div class="col s12 m6"> 
							<label>Bairro <span style="font-weight: bold; color: red;">*</span></label>
							<input id="bairro" name="cad[bairro]" type="text" class="validate browser-default" required="required">
						</div>
					</div>
					
					<div class="row">
						<div class="col s12 m6">
							<label>Município <span style="font-weight: bold; color: red;">*</span></label>
							<input id="localidade" name="cad[localidade]" type="text" class="validate browser-default" required="required">
						</div>
						<div class="col s12 m6">	
							<label>Estado <span style="font-weight: bold; color: red;">*</span></label>
							<select name="cad[estado_id]" id="estado_id" class="estado browser-default" required="required">
								<option value="" disabled selected>Escolha um estado</option>
								@foreach($estados AS $estado)	
									<option value="{{ $estado['id'] }}">{{ $estado['descricao'] }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="row">
                    	<div class="col s12 center">
	                      <p>
	                      	<span style="font-weight: bold; color: red;">*</span>
	                      	Em caso de não encontrar o CEP, será necessário apontar empresa no mapa.
	                      </p>
	                      <p>
	                      	<span style="font-weight: bold; color: red;">*</span>
	                      	Dica de ouro, caso não saiba exatamente onde à empresa está, aponte sobre a cidade do cliente.
	                      </p>
	                    </div>
	                </div>  
                
	                <div class="row">
	                  <div class="col s11">
						<div class="btn {{ get_empresa_cor() }} darken-1 getMapa full-width abre-mapa">
							Abrir mapa
						 	<i class="material-icons right">place</i>
						</div>
	                    <input id="lat" type="hidden" value="-12.6469717" />
	                    <input id="lon" type="hidden" value="-60.4262417" />
	                  </div>
	                  <div class="col s1">
	                      <div class="btn {{ get_empresa_cor() }}  darken-1 full-width abre-ajuda">
	                        <i class="material-icons right">help</i>
	                      </div>
	                  </div>  
	                </div>

					<div class="row">
						<div class="col s12">
							<h5>Telefones</h5>
						</div>
					<div>

					@for ($i=0; $i<3; $i++)
						<div class="row">
							<div class="col s12 m4">
								@if($i == 0)
									<label>Nome <span style="font-weight: bold; color: red;">*</span></label>
								@else
									<label>Nome</label>
								@endif
								<input id="nome" name="contato[nome][{{ $i }}]" type="text" class="validate browser-default">
							</div>
							<div class="col s12 m4">
								@if($i == 0)
									<label>Número <span style="font-weight: bold; color: red;">*</span></label>
								@else
									<label>Número</label>
								@endif
								<input id="telefone" name="contato[telefone][{{ $i }}]" type="text" class="validate fone browser-default">
							</div>
							<div class="col s12 m4">
								@if($i == 0)
									<label>Tipo <span style="font-weight: bold; color: red;">*</span></label>
								@else
									<label>Tipo</label>
								@endif	
								<select name="contato[tipo][{{ $i }}]" id="tipo" class="browser-default">
									<option value="" disabled selected>Escolha um tipo</option>
									<option value="TELEFONE">Telefone</option>
									<option value="WHATSAPP">Whatsapp</option>
								</select>
							</div>
							<input type="hidden" name="contato[view][{{ $i }}]" value="1">
						</div>
					@endfor

					<div class="row">
						<input id="cidade_latitude" name="cad[latitude]"  type="hidden" value="">
						<input id="cidade_longitude" name="cad[longitude]" type="hidden" value="">
				        <div class="col s12">
				        	<button class="btn {{ get_empresa_cor() }} darken-1 add-cliente full-width" type="submit">
				        		Salvar cliente
				        		<i class="material-icons right">chevron_right</i>
				        	</button>
				        </div>
					</div>
				</form>

			</div>
		</div>
	</div>

	<div id="modal-mapa" class="modal modal-mapa">
    	<div class="modal-content content-mapa" style="padding: 0px;">
    		<div id="mapid" style="width: 100%; height: 360px;"></div>
      		<a href="#!" class="btn-floating modal-close {{ get_empresa_cor() }} darken-1" style="position: absolute; top: 5px; right: 5px; z-index: 9999;">
      			<i class="material-icons">close</i>
      		</a>
    	</div>
  	</div>

	<div id="modal-ajuda" class="modal modal-ajuda">
	  <div class="modal-content content-ajuda" style="padding: 0px;">
	    <div id="ajuda" style="width: 100%; height: 360px;">

	      <iframe class="videoPlay" width="100%" height="360"
	        src="https://www.youtube.com/embed/mcwldvn-v6I">
	      </iframe>
	    
	    </div>  
	    </div>
	      <a href="#!" class="btn-floating modal-close {{ get_empresa_cor() }} darken-1" style="position: absolute; top: 5px; right: 5px; z-index: 9999;">
	        <i class="material-icons">close</i>
	      </a>
	    </div>
	</div>

	<br />
	
</main>

@stop

@section('extra-javascript')

<script type="text/javascript" src="{{ base_url('assets/javascript/clientes.js?v='.Version) }}"></script>
<script type="text/javascript" src="{{ base_url('assets/javascript/ConsultaCepCliente.js?v='.Version) }}"></script>

<script type="text/javascript">
	$(document).ready(function(){
		var mymap;
		var marker;
		
		$('.abre-mapa').on('click', function(){
			$('.modal-mapa').modal('open');

			let latitude = $('#cidade_latitude').val();
			let longitude = $('#cidade_longitude').val();

			setTimeout(function(){
				if (mymap != undefined) {
					mymap.setView([
						latitude,
						longitude
					], 10);
					return;
				}

				mymap = L.map('mapid').setView([
					latitude,
					longitude
				], 10);

				L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
				    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
				}).addTo(mymap);

				mymap.on('click', onMapClick);

				if (latitude != "" || longitude != "") {
					marker = L.marker([latitude, longitude]).addTo(mymap);
					mymap.setZoom(8);
				}
			}, 680);
		});

		const onMapClick = (e) => {
			if (marker != undefined) {
				marker.remove();
			}

			marker = L.marker([e.latlng.lat, e.latlng.lng]).addTo(mymap);
			mymap.setView([e.latlng.lat, e.latlng.lng], 17);

			$("#cidade_latitude").val(e.latlng.lat);
			$("#cidade_longitude").val(e.latlng.lng);

		}

		$('#modal-ajuda').modal({
	      dismissible: false
	    });

	    $('.abre-ajuda').on('click', function(){
	      $('.modal-ajuda').modal('open');
	    });

	    $('.modal-close').on('click', function(){
	        const videos = document.querySelectorAll('iframe');
	           videos.forEach(i => {
	              let source = i.src;
	              i.src = '';
	              i.src = source;
	           });
	    });

	});
	
</script>

@stop