@extends('template.site')

@section('content')

	{{-- <main> --}}
		<div class="container">
			<div class="row center" style="
				margin-top: 40px; 
				/* height: 100vh; */
				width: 100%;
				/* -webkit-box-shadow: 0px 0px 10px 0px #000000;  */
				/* box-shadow: 0px 0px 5px 0px #000000; */
				/* border-radius: 4px; */
			">
				<div class="col s12 m4" style="padding: 10px;">
					<div class="container">
						<h5>Enjoy2Learn Idiomas</h5>
						<h5>Reunião de 1 hora com o Dennis</h5>
						
						<div class="valign-wrapper">
							<i class="material-icons">watch_later</i>
							15 min
						</div>
					</div>
				</div>
				<div class="col s8 m5">
					<h2 class="center" style="font-size: 20px; font-weight: bold;">Selecione um dia</h2>
					<br>
					<p class="left">
						<b>Escolha uma data e horário</b>
					</p>
					@if($calendar)
						@php
							echo $calendar;
						@endphp
					@endif
				</div>
				<div class="col s4 m3 right">
					<div class="_1UHdIbIBus Jqp9X3YjPb" data-container="selected-spot">
{{-- 						<button data-container="time-button" tabindex="-1" data-start-time="09:00" class="_2BzubJiZvN _2IrBWuPQTq _1byk5uh6y7 _3l-B5S_4KC _4rcXoQPLhG _11Hd4lVzFN _1Qg-rkOB2V QccgzfWCIH _2zIir_wMTE" type="button">
							<div class="_1KN9NmLOgi _33HKFhUjZR hmvLs1WTG5">
								<div class="_1dZ0hT1AE9 _2asEJ_k_27">09:00</div>
							</div>
						</button>
						<button tabindex="0" data-container="confirm-button" class="_3egpDV8hKn _2OQqVeh6S4 _1Es3W-g9AL 	_3l-B5S_4KC _4rcXoQPLhG _11Hd4lVzFN _1Qg-rkOB2V QccgzfWCIH _2zIir_wMTE confirm-button-enter-done" 	type="button">Confirmar
						</button>
 --}}
						{{-- <button data-container="time-button" tabindex="0" data-start-time="09:15" class="_2BzubJiZvN _2IrBWuPQTq _3l-B5S_4KC _4rcXoQPLhG _11Hd4lVzFN _1Qg-rkOB2V QccgzfWCIH _2zIir_wMTE" type="button"><div class="_1KN9NmLOgi _33HKFhUjZR hmvLs1WTG5"><div class="_1dZ0hT1AE9 _2asEJ_k_27">09:15</div></div></button> --}}
					</div>
				</div>
			</div>
		</div>
	{{-- </main> --}}

@stop

@section('extra-javascript')
	<script type="text/javascript">

	</script>
@stop