@extends('template.site')

@section('content')

<div class="parallax-container" style="height: 98vh">
	<div class="section">
	  	<div class="container">
	    	<div class="row">
	    		<div class="col s12 valign-wrapper" style="height: 90vh;">
		      		<h1 class="header white-text">
		        		<b>DENNIS <span class="red">GEORGE</span> VIERA</b>
		      		</h1>
		      	</div>
	    	</div>	    
	  	</div>
	</div>
	<div class="parallax">
	  	<img class="responsive-img" src="{{ base_url('assets/images/background-flag.jpg') }}">
	</div>
</div>

<main>

	<div class="container" id="cursos" style="padding-top: 18px; padding-bottom: 18px;">
		<h3>Nossos <b class="blue white-text">cursos</b></h3>
		<div class="row">
			@foreach($cursos AS $curso)
			<div class="col s12 m3">
				<div class="card">
					<div class="card-image">
						@if (isset($curso['foto']))
							<img class="activator" 
							src="{{ base_url('assets/uploads/cursos/'.$curso['foto']) }}" style="height: 150px;">
						@else
						<img class="activator" 
							src="{{base_url('assets/uploads/semfotoCinza.png')
							}}" style="height: 150px;">
						@endif
					</div>
					<div class="card-content center-align">
						<p style="margin-top: 0px;">
							{{ substr($curso['descricao'], 0, 55) }} ...
						</p>
						@if ($curso['preco'] != "0.00")
							<p style="font-weight: bold;">R$ {{ convert_double_to_BRL($curso['preco']) }}</p> <br/>
						@endif
						<div class="btn blue darken-2 full-width">
							Me interesso!
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>

	<div class="section blue" id="recursos" style="height: 100vh">
		<div class="row">
			<div class="col s12 m3 center-align white-text">
				<i class="material-icons medium">mark_chat_unread</i>
				<h5>Exercícios diários</h5>
				<p>Sempre ajudando o aluno a manter o focado e com conteudo, onde é possível aprender uma palavra nova diariamente.</p>
			</div>
			<div class="col s12 m3"></div>
			<div class="col s12 m3"></div>
			<div class="col s12 m3"></div>
		</div>
	</div>

</main>

@stop