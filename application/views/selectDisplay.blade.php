<!DOCTYPE html>
<html>
<head>
  
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

{{-- <meta name="theme-color" content="#1a237e"> --}}
<meta name="apple-mobile-web-app-capable" content="yes">  
{{-- <meta name="apple-mobile-web-app-status-bar-style" content="#1a237e">  --}}
<meta name="apple-mobile-web-app-title" content="Selflog style">
<meta name="description" content="Login do aluno enjoy2learn">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<link rel="shortcut icon" href="{{ base_url('assets/images/shortcut.svg') }}" type="image/x-icon">
<link rel="manifest" href="{{ base_url('manifest.json') }}" />
<link href="{{ base_url('assets/css/materialize.min.css') }}" rel="stylesheet" media="screen,projection"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<style>
   .parallax-container {
    height: 150px;
    margin-bottom: 20px;
  }

  .card-panel {
  border-radius: 10px;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
  }

  .negativa {
    margin-top: -60px;
    position: relative;
  }
</style>
</head>
<body>

  <div class="parallax-container blue darken-2 white-text center-align ">
    <div class="container">
      <h5 style="margin: 30px">Seja bem vindo administrador</h5>
    </div>
  </div>

  <div class="container">
    <div class="row center-align">
      
      <div class="col s12 m6 center-align {{ !$mobile ? 'negativa' : '' }} msg3">
        <a href="{{base_url('sistema/welcome')}}">
          <div class="card-panel hoverable">
            <i class="material-icons center-align large">settings</i>
            <p>Administração/Sistema</p>
          </div>
        </a>
      </div>

      <div class="col s12 m6 center-align {{ !$mobile ? 'negativa' : '' }} msg4">
        <a href="{{base_url('welcome')}}">
          <div class="card-panel hoverable">
            <i class="material-icons center-align large">person</i>
            <p>Área do aluno</p>
          </div>
        </a>
      </div>

      <div class="col s12 center-align">
        <div class="card-panel">
          
            <i class="material-icons center-align large msg1">watch_later</i>
            <p class="blue-text msg2">
              Você será desconectado em:
              <br />
              <b style="font-size: 20px;"> 
                <span class="tempo"></span>
              </b>
            </p>
            <span class="mostraMSG blue-text"></span>
            <p class="mostraLogout"></p>
    
        </div>
      </div>
    
    </div>
  </div>

<script src="{{ base_url('assets/javascript/jquery-3.3.1.min.js') }}"></script>
<script src="{{ base_url('assets/javascript/materialize.min.js') }}"></script>
<script>
  $(document).ready(function(){

    var tempo = new Number();
    if(localStorage.getItem('valorInicial') == null){
      tempo = 60;
    } else {
      tempo = localStorage.getItem('valorInicial');
    }

    function startTimeout(){
      if((tempo - 1) >= 0){
        var min = parseInt(tempo/60);
        var seg = tempo%60;
        
        if(min < 10){
            min = "0"+min;
            min = min.substr(0, 2);
        }

        if(seg <=9){
            seg = "0"+seg;
        }

        var horaImprimivel;
        if(min == 0) {
          horaImprimivel =  seg + 's';
        } else {
            horaImprimivel =  min + 'm' + '' + seg + 's';
        }
          $(".tempo").html(horaImprimivel);
          console.log(horaImprimivel);
          // localStorage.setItem('tempo', horaImprimivel)
          setTimeout(function () {
              startTimeout();
          }, 1000);
          tempo--;
          localStorage.setItem('valorInicial', tempo)
        } else {
          location.href =  base_url('login/logout');

            $('.msg1').hide();
            $('.msg2').hide();
            $('.msg3').hide();
            $('.msg4').hide();
            $('.card-panel').addClass('negativa');
            $('.mostraMSG').html('<b>Aguarde ....</b>');

          setTimeout(function(){
            $('.mostraMSG').html('<h5>Erro ao sair, favor executar manualmente <br> a saida do sistema para sua segurança.</h5>');
            $('.mostraLogout').html("<a href='{{ base_url('login/logout') }}' class='btn blue white-text'><i class='material-icons white-text tiny right'>logout</i> Sair Agora</a>");
          }, 5000);
      }
    }

    startTimeout();
    function base_url(caminho) {
      return '{{ base_url() }}' + caminho;
    }

  });
</script>
  
</body>
</html>
