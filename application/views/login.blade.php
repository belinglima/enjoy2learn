<!DOCTYPE html>
<html>
<head>
  
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

{{-- <meta name="theme-color" content="#1a237e"> --}}
<meta name="apple-mobile-web-app-capable" content="yes">  
{{-- <meta name="apple-mobile-web-app-status-bar-style" content="#1a237e">  --}}
<meta name="apple-mobile-web-app-title" content="Selflog style">
<meta name="description" content="Login do aluno enjoy2learn">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<link rel="shortcut icon" href="{{ base_url('assets/images/shortcut.svg') }}" type="image/x-icon">
<link rel="manifest" href="{{ base_url('manifest.json') }}" />
<link href="{{ base_url('assets/css/materialize.min.css') }}" rel="stylesheet" media="screen,projection"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<style>
  @import url('https://fonts.googleapis.com/css?family=Montserrat:400,800');
  .red{
    color: #FF4B2B;
  }

  .azul{
    color: #6372ff;
  }

  .button_vermelho {
    border-radius: 0 20px 20px;
    border: 1px solid #FF4B2B;
    background-color: #FF4B2B;
    color: #FFFFFF;
    font-size: 12px;
    font-weight: bold;
    padding: 12px 45px;
    letter-spacing: 1px;
    text-transform: uppercase;
    transition: transform 80ms ease-in;
    cursor: pointer;
  }

  .button_azul {
    border-radius: 0 20px 20px;
    border: 1px solid #6372ff;
    background-color: #6372ff;
    color: #FFFFFF;
    font-size: 12px;
    font-weight: bold;
    padding: 12px 45px;
    letter-spacing: 1px;
    text-transform: uppercase;
    transition: transform 80ms ease-in;
    cursor: pointer;
  }

  button {
    border-radius: 0 20px;
    border: 1px solid #6372ff;
    background-color: #6372ff;
    color: #FFFFFF;
    font-size: 12px;
    font-weight: bold;
    padding: 12px 45px;
    letter-spacing: 1px;
    text-transform: uppercase;
    transition: transform 80ms ease-in;
    cursor: pointer;
  }

  button:active {
    transform: scale(0.95);
  }

  button:focus {
    outline: none;
  }

  button.ghost {
    background-color: transparent;
    border-color: #FFFFFF;
  }

  form {
    background-color: #FFFFFF;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    padding: 0 50px;
    height: 100%;
    text-align: center;
  }

  .input_vermelho {
    border: 1px solid #FF4B2B;
    padding: 12px 15px;
    margin: 8px 0;
    width: 100%;
    color: #FF4B2B;
    outline: none;
    font-weight: bold;
  }

  .input_vermelho::-webkit-input-placeholder {
    color: #FF4B2B;
  }

  .input_azul {
    /* background: -webkit-linear-gradient(to right, #FF4B2B, #6372ff); */
    /* background: linear-gradient(to right, #FF4B2B, #6372ff); */
    border: 1px solid #6372ff;
    /* border-radius: 20px;  */
    padding: 12px 15px;
    margin: 8px 0;
    width: 100%;
    color: #6372ff;
    outline: none;
    font-weight: bold;
  }
  .input_azul::-webkit-input-placeholder {
    color: #6372ff;
  }

  .form-container {
    position: absolute;
    /*top: 5px;*/
    height: 100%;
    transition: all 0.6s ease-in-out;
  }

  .sign-in-container {
    left: 0;
    width: 50%;
    z-index: 2;
  }

  .container.right-panel-active .sign-in-container {
    transform: translateX(100%);
  }

  .sign-up-container {
    left: 0;
    width: 50%;
    opacity: 0;
    z-index: 1;
  }

  /* controle da area de login e cadastrar */
  @media only screen and (max-width: 800px) {

    html {
        font-size: 20px;
    }

    .container {
      width: 100% !important;
    } 

    .sign-in-container {
      width: 100% !important;
    }

    .sign-up-container {
      width: 100% !important;
    } 

  }

  .container.right-panel-active .sign-up-container {
    transform: translateX(100%);
    opacity: 1;
    z-index: 5;
    animation: show 0.6s;
  }

  @keyframes show {
    0%, 49.99% {
      opacity: 0;
      z-index: 1;
    }
    
    50%, 100% {
      opacity: 1;
      z-index: 5;
    }
  }

  .overlay-container {
    position: absolute;
    top: 0;
    left: 50%;
    width: 50%;
    height: 100%;
    overflow: hidden;
    transition: transform 0.6s ease-in-out;
    z-index: 100;
  }

  .container.right-panel-active .overlay-container{
    transform: translateX(-100%);
  }

  .overlay {
    background: #6372ff;
    background: -webkit-linear-gradient(to right, #FF4B2B, #6372ff);
    background: linear-gradient(to right, #FF4B2B, #6372ff);
    background-repeat: no-repeat;
    background-size: cover;
    background-position: 0 0;
    color: #FFFFFF;
    position: relative;
    left: -100%;
    height: 100%;
    width: 200%;
      transform: translateX(0);
    transition: transform 0.6s ease-in-out;
  }

  .container.right-panel-active .overlay {
      transform: translateX(50%);
  }

  .overlay-panel {
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    padding: 0 40px;
    text-align: center;
    top: 0;
    height: 100%;
    width: 50%;
    transform: translateX(0);
    transition: transform 0.6s ease-in-out;
  }

  .overlay-left {
    transform: translateX(-20%);
  }

  .container.right-panel-active .overlay-left {
    transform: translateX(0);
  }

  .overlay-right {
    right: 0;
    transform: translateX(0);
  }

  .container.right-panel-active .overlay-right {
    transform: translateX(20%);
  }

  .social-container {
    margin: 20px 0;
  }

  .social-container a {
    border: 1px solid #DDDDDD;
    border-radius: 50%;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    margin: 0 5px;
    height: 40px;
    width: 40px;
  }

  .full-width {
    margin: 5px;
    width: 100%;
  }

form .campo-termos{
  width: 100%;
  float: none;
}
form .campo-termos a{
  display: inline;
  color: #FF4B2B;
}
form .campo-termos a:hover{
  text-decoration: underline;
}
form .contorno-campo-radio{
  float: left;
  display: table;
  height: 48px;
  margin-bottom: 5px;
}
form .contorno-campo-radio + .contorno-campo-radio{
  margin-left: 35px;
}
form .contorno-campo-radio .bola-radio{
  width: 18px;
  height: 18px;
  float: left;
  position: relative;
  border: 1px solid #8C8C8C;
  border-radius: 100px;
  margin-top: 15px;
  cursor:pointer;
}
form .contorno-campo-radio .texto-radio{
  width: calc(100% - 18px);
  float: left;
  padding-left: 8px;
  font-weight: 400;
  color: #595959;
  font-size: 14px;
  line-height: 48px;
}
.campo-termos .texto-radio a{
    text-decoration: underline;
}
form .contorno-campo-radio:hover .bola-radio{
  border: 1px solid #FF4B2B;
}
form .contorno-campo-radio .bola-selecionar{
  width: 10px;
  height: 10px;
  position: absolute;
  left: 50%;
  top: 50%;
  margin-top: -5px;
  margin-left: -5px;
  background: #FF4B2B;
  border-radius: 100px;
  display: none;
}
form .contorno-campo-radio:hover .bola-selecionar{
  display: block;
}
@media (max-width:767px){
    .campo-termos .texto-radio{
        line-height:23px !important;
        padding-top:13px;
    }
}

.campo-termos .bola-selecionar, .campo-termos .bola-radio {
    border-radius: 0px !important;
}

/* COPYRIGHT */
.total-copyright{
  width: 500px;
  position: absolute;
  bottom: 32px;
  left: 90px;
}
.total-copyright .esquerda{
  color: #fff;
  font-weight: 400;
  font-size: 14px;
  text-align: left;
}
.total-copyright .esquerda span{
  display: inline-block;
}
.total-copyright .esquerda a{
  color: #fff;
  display: inline-block;
}
.total-copyright .esquerda a:hover{
  color: #fff444;
  text-decoration: underline;
}

#toast-container .toast {
    background: #6372ff !important;
    color: #fff !important;
}
</style>
</head>
<body onLoad="document.logar.email.focus();">

 
  <div class="container" id="container">

    @include('template.show-error-success-redirect')

    <div class="form-container a hide" style="overflow-x: hidden;">
      <div class="social-container">
        <img class="responsive-img social" style="margin-top: 10px; margin-bottom: -120px;" src="{{ base_url('assets/images/logo.png') }}" alt="">
      </div>

      {{-- mobile somente--}}
      <form name="cadastrarMobile" action="{{ base_url('login/insert') }}" method="POST" autocomplete="off">
        <p class="red-text"><b>Cadastro de usuário!</b></p>

        <input class="input_vermelho browser-default nomeCad1" name="nome" type="text" placeholder="Nome" autocomplete="off" required="required"/>
        <input class="input_vermelho browser-default emailCad1" name="email" type="email" placeholder="E-mail" autocomplete="off" required="required"/>
        <input class="input_vermelho browser-default senhaCad1" name="hash_senha" type="password" placeholder="Senha" autocomplete="off" required="required"/>

        <!-- TERMOS -->
        <div class="contorno-campo campo-termos" data-value="1">
          <div class="contorno-campo-radio">

            <div class="bola-radio">
              <div class="bola-selecionar"></div>
            </div>

            <div class="texto-radio">
              Li e concordo com os <a href="{{ base_url('termos/uso') }}" target="_blank">Termos de Privacidade</a>
            </div>
          </div>
          <input type="hidden" id="HdnTermos" name="HdnTermos" value="0">
        </div>

        <div class="full-width white-text" href="#">.</div>
        <button class="button_vermelho full-width cadastrarUsuario" disabled>Cadastrar</button>
        <div class="button_azul hide-on-med-and-up full-width" id="loginButton">Entrar</div>
      </form>
    </div>

    <div class="form-container sign-up-container b" style="overflow-y: hidden;">
      <div class="social-container">
        <img class="responsive-img social" style="margin-top: 20px; margin-bottom: -160px;" src="{{ base_url('assets/images/logo.png') }}" alt="">
      </div>

      {{-- Cadastro de usuario somente WEB --}}
      <form name="cadastrarWeb" action="{{ base_url('login/insert') }}" method="POST" autocomplete="off">
        <p class="red-text"><b>Cadastrar nova conta!</b></p>

        <input class="input_vermelho browser-default nomeCad2" name="nome" type="text" placeholder="Nome" autocomplete="off" required="required"/>
        <input class="input_vermelho browser-default emailCad2" name="email" type="email" placeholder="E-mail" autocomplete="off" required="required"/>
        <input class="input_vermelho browser-default senhaCad2" name="hash_senha" type="password" placeholder="Senha" autocomplete="off" required="required"/>

        <!-- TERMOS -->
        <div class="contorno-campo campo-termos" data-value="1">
          <div class="contorno-campo-radio">

            <div class="bola-radio">
              <div class="bola-selecionar"></div>
            </div>

            <div class="texto-radio">
              Li e concordo com os <a href="{{ base_url('termos/uso') }}" target="_blank">Termos de Privacidade</a>
            </div>
          </div>
          <input type="hidden" id="HdnTermos" name="HdnTermos" value="0">
        </div>
 
        <button class="button_vermelho full-width" id="cadastrarUsuario" disabled>Cadastrar</button>
      </form>
    </div>



    <div class="form-container sign-in-container c" style="overflow-y: hidden;">

      <div class="social-container">
        <img class="responsive-img social" style="margin-top: 20px; margin-bottom: -160px;" src="{{ base_url('assets/images/logo.png') }}" alt="">
      </div>  

      <form name="logar" action="{{ base_url('login') }}" method="POST">
        <p class="azul"><b>Entrar</b></p>
        <input class="input_azul browser-default" id="email" name="email" type="text" placeholder="E-mail" autocomplete="off" />
        <input class="input_azul browser-default" id="senha" name="senha" type="password" placeholder="Senha" autocomplete="off" />

        <a id="recuperar" data-target="modal1" class="full-width modal-trigger" href="#modal1">Esqueci minha senha?</a>
        <div class="button_azul full-width" id="entrar">Entrar</div>
        <div class="button_vermelho hide-on-med-and-up full-width" id="cadButton">Cadastrar</div>
        <br /><br />
      </form>

    </div>

    <div class="overlay-container hide-on-med-and-down">
      <div class="overlay">

        <div class="overlay-panel overlay-left">
          <h1>Bem vindo de volta!</h1>
          <p>Para se manter conectado conosco , entre com seus dados de login.</p>
          <button class="ghost" id="signIn">Entrar</button>

          <!-- COPYRIGHT -->
          <div class="total-copyright">
            <div class="esquerda">
              <span>
                Copyright {{ date('Y') }} © Enjoy2learn Idiomas - Desenvolvido por:
              </span>
              <a href="#tioDev" target="_blank">
                DEV TRIO
              </a>
            </div>
          </div>

        </div>

        <div class="overlay-panel overlay-right">
          <h1>Ola, Amigo!</h1>
          <p>Entre com seus dados pessoais e inicie essa jornada conosco.</p>
          <button class="ghost" id="signUp">Cadastrar</button>

          <!-- COPYRIGHT -->
          <div class="total-copyright">
            <div class="esquerda">
              <span>
                Copyright {{ date('Y') }} © Enjoy2learn Idiomas - Desenvolvido por:
              </span>
              <a href="#tioDev" target="_blank">
                DEV TRIO
              </a>
            </div>
          </div>
        </div>

      </div>
    </div>


    <div id="modal1" class="modal bottom-sheet">
      <div class="modal-content">
          <p class="azul"><b>Digite seu email.</b></p>
          <div class="input-field col s12">
              <input class="input_azul browser-default" id="recuperaEmail" name="recupera" type="text" placeholder="E-mail" autocomplete="off">
          </div>
          <p class="azul center" style="font-size: 14px">Você receberá um e-mail contendo informações de como mudar sua senha.</p>
            <a class="modal-close azul" style="position: absolute; right: 5px; top: 5px; z-index: 999;">
              <i class="material-icons">clear</i>
          </a>
      </div>
      <div class="modal-footer center">
        <div class="button_azul full-width center-align" id="enviar" style="margin-top: -38px;">Enviar</div>
      </div>
    </div>

  </div>

<script src="{{ base_url('assets/javascript/jquery-3.3.1.min.js') }}"></script>
<script src="{{ base_url('assets/javascript/materialize.min.js') }}"></script>
<script src="{{ base_url('assets/javascript/main.js') }}"></script>
<script>
  $(document).ready(function(){

    $("#recuperar").click(function(){
      $('#modal1').modal();
      $('#modal1').modal('open');
    });

    function checkMail(mail) {
			var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
			if (typeof(mail) == "string") {
				if (er.test(mail)) {
					return true;
				}
			} else if(typeof(mail) == "object") {
				if (er.test(mail.value)) {
					return true;
				}
			} else {
				return false;
			}
		}

		$("body").keypress(function(event){
			if( event.which == 13 ){
				event.preventDefault();
				submitForm();
			}
		});

		$("#enviar").click(function(){

			var url ="<?=base_url()?>forgot/validaemail";

			if ($("#recuperaEmail").val() == "") {
				M.toast({html: 'Insira um e-mail'});
				return;
			}

			if (! checkMail($("#recuperaEmail").val())) {
				M.toast({html: 'Insira um e-mail válido!'});
				return;
			}

			$(".progress").toggleClass("hide");

			$.post(url, { email: $("#recuperaEmail").val() }, function(data){
				M.toast({html: data});
				$("#recuperaEmail").val("");
				$(".progress").toggleClass("hide");
			});

		});

    $('#cadButton').on('click', function(){
      $('.c').toggleClass('hide');
      $('.b').toggleClass('hide');
      $('.a').toggleClass('hide');
    });

    $('#loginButton').on('click', function(){
      $('.c').toggleClass('hide');
      $('.b').toggleClass('hide');
      $('.a').toggleClass('hide');
    });

    function base_url(caminho) {
      return '{{ base_url() }}' + caminho;
    }

    $('.bola-radio').on('click', function(){
      $("#cadastrarUsuario").prop("disabled", false);
      $(".cadastrarUsuario").prop("disabled", false);
    });

    function submitFormCadastroWeb() {
      if ($(".nomeCad2").val() == "") {
        M.toast({html: 'Insira um nome!', classes: 'rounded'});
      }
      if ($(".emailCad2").val() == "") {
        M.toast({html: 'Insira um email!', classes: 'rounded'});
      }
      if ($(".senhaCad2").val() == "") {
        M.toast({html: 'Insira uma senha!', classes: 'rounded'});
      }
      if( $(".nomeCad2").val() != "" && $(".emailCad2").val() != "" && $(".senhaCad2").val() != "") {
        document.cadastrarWeb.submit();
      }
    };

    function submitFormCadastroMobile() {     
      if ($(".nomeCad1").val() == "") {
        M.toast({html: 'Insira um nome!', classes: 'rounded'});
      }
      if ($(".emailCad1").val() == "") {
        M.toast({html: 'Insira um email!', classes: 'rounded'});
      }
      if ($(".senhaCad1").val() == "") {
        M.toast({html: 'Insira uma senha!', classes: 'rounded'});
      }
      if( $(".nomeCad1").val() != "" && $(".emailCad1").val() != "" && $(".senhaCad1").val() != "") {
        document.cadastrarMobile.submit();
      }
    };

    $("#cadastrarUsuario").click(function(){
      console.log('cadastro web');
      submitFormCadastroWeb();
    });

    $(".cadastrarUsuario").click(function(){
      console.log('cadastro mobile');
      submitFormCadastroMobile();
    });

    $('.emailCad').on('blur', function(){
      let email = $(this).val();
      // console.log(email);
      if (email == "") {
        return;
      }

      $.post(base_url('findemail'), { email: email }, function(data){
        if (data != 0) {
          M.toast({html: 'E-mail já cadastrado!', classes: 'rounded'});
          $('.emailCad').val("");
          return;
        }
      });
    });

    if ("serviceWorker" in navigator) {
      if (navigator.serviceWorker.controller) {
          console.log("[PWA Builder] active service worker found, no need to register");
        } else {
          navigator.serviceWorker
              .register("./sw.js", {
                scope: "./"
              })
              .then(function (reg) {
                console.log("[PWA Builder] Service worker has been registered for scope: " + reg.scope);
              });
        }
    }

    $(".install-app").on('click', function(){
      installApp();
    });

    const signUpButton = document.getElementById('signUp');
    const signInButton = document.getElementById('signIn');
    const container = document.getElementById('container');

    signUpButton.addEventListener('click', () => {
      container.classList.add("right-panel-active");
    });

    signInButton.addEventListener('click', () => {
      container.classList.remove("right-panel-active");
    });

    @if (isset($msg) && !empty($msg))
      $(document).ready(function(){
        M.toast({html: '{{ $msg }}'});
      });
    @endif

    function submitForm() {
      if ($("#email").val() == "") {
        M.toast({html: 'Insira um email!', classes: 'rounded'});
      }
      if ($("#senha").val() == "") {
        M.toast({html: 'Insira uma senha!', classes: 'rounded'});
      }
      if( $("#email").val() != "" && $("#senha").val() != "") {
        document.logar.submit();
      }
    };

    $("#entrar").click(function(){
      submitForm();
    });


    $('.campo-termos').click(function () {
      value = $(this).attr('data-value');
      if($('#HdnTermos').val() == '1'){
        $('.campo-termos').find('.bola-radio').find('.bola-selecionar').css('display', 'none')
        $('#HdnTermos').val('');
      } else {
        $('#HdnTermos').val('1');
        $('.campo-termos').find('.bola-radio').find('.bola-selecionar').css('display', 'block');
      }
    });
       
  })
</script>

</body>
</html>
