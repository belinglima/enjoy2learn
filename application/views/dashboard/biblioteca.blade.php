@extends('template.base')

@section('content')

<main>	
	
	<div class="container">
		<div class="row" style="border-bottom: 1px solid #DFDFDF">
			<div class="col s12 left-align">
				<div class="contorno-titulo-pagina">
					<h1>
						<strong>
							Minha biblioteca
						</strong>
					</h1>
				</div>

				<div class="mapeamento-pagina">
		            <a href="{{ base_url('welcome') }}">Home</a> &gt;  
		            <strong>Meus cursos</strong>
		        </div>
			</div>
		</div>


      	@if($cursos)
      		<div class="row">
	      		@foreach($cursos AS $curso)
			        <div class="flip-card col s12 m4" style="margin: 20px 0 20px 0;">
					  	<div class="flip-card-inner z-depth-5">
						    <div class="flip-card-front">
						      	<div class="flip-topo">
							        <p>
										<a class="btn blue darken-3 full-width">
						        			<i class="material-icons right">queue_play_next</i> Começar
						        		</a>	
							        </p> 
							    </div>

						      	<img src="{{ base_url('assets/uploads/cursos/'.$curso['foto']) }}" alt="Avatar" style="height:300px;">
						      
							    <div class="flip-front">
							        <h4>
										{{ $curso['nome'] }}
							        </h4> 
							    </div>
						    </div>
						    <div class="flip-card-back" style="padding: 10px;">
						      	<h1></h1> 
						      	<p><b>{{ $curso['nome'] }}</b></p> 
						      	@php
									echo "<p style='color: #000000'>".nl2br($curso['descricao'])."</p>";
					            @endphp
						      
							    <div class="flip-front">
							        <h5 style="margin: 0px 5px -2px 5px;">
						        		<a href="{{ base_url('player/'.$encryptor->encrypt($curso['curso_id'])) }}" class="btn blue darken-3 full-width">
						        			<i class="material-icons right">play_circle_outline</i> Começar
						        		</a>
							        </h5>
							        <h5 style="margin: 0px 5px 10px 5px;"> 
						        		<a href="{{ base_url('classes/payment/'.$encryptor->encrypt($curso['codigo'])) }}" class="btn blue darken-3 full-width">
						        			<i class="material-icons right">more_vert</i> Dados Pagamento
						        		</a>
						        	</h5>
							    </div>
						    </div>
					  	</div>
					</div>
				@endforeach
			</div>	
		@else
			<div class="row">
	      		<div class="col s12">
				    <table class="responsive-table striped">
				        <tbody>
					      	<div class="total-lista-carrinho">
								<ul>
									<div class="nenhum-item-cadastrado">
										Você não possui nenhum curso comprado
										<p>Realize uma compra para ver os detalhes de pagento!</p>
									</div>
								</ul>
							</div>
							<br><br>
				        </tbody>
			      	</table>
			    </div>
			</div>
		@endif

	</div>

	<br><br><br>

</main>

@stop

@section('extra-javascript')

@stop