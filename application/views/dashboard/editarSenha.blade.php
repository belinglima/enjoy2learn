@extends('template.base')

@section('content')

<main>	
	
	<div class="container">

	  	<nav class="clean">
		  <div class="nav-wrapper">
		    <div class="col s12">
		      <a href="{{ base_url('profile') }}" class="breadcrumb">Perfil</a>
		      <a  class="breadcrumb" style="text-decoration: underline;">Alterar Senha</a>
		    </div>
		  </div>
		</nav>
        
        <div class="card-panel center-align">
			<div class="">
				<p class="navText" style="text-decoration: underline;"><b>ALTERAR SENHA </b></p>
				<form name="cadastro" action="{{ base_url('dashboard/alterarSenha/'.$encryptor->encrypt($usuario['id'])) }}" method="POST" enctype="multipart/form-data">
					<div class="row">	
						<div class="col s12 m6">
							<input id="senha" placeholder="Senha" name="hash_senha" class="browser-default input_azul validate" type="text" required="required">
						</div>

						<div class="col s12 m6">
							<input id="senharepeteco" placeholder="Repetir senha" class="browser-default input_azul validate" type="text" required="required">
						</div>
							
					    <div class="col s12">
				        	<button class="btn blue darken-1 alterar-senha full-width" type="submit">	
				        		Salvar senha
				        		<i class="material-icons right">chevron_right</i>
				           </button>
				        </div>
					</div>
				</form>
			
			</div>
        </div>    
	</div>

</main>

@stop

@section('extra-javascript')

<script type="text/javascript">

$('#senharepeteco').on('change', function(){

    let senha = $('#senha').val();
    let senharepeteco = $('#senharepeteco').val();
    if(senha != senharepeteco){
    swal("Atenção", " Senhas não conferem, acerte antes de enviar ", "error");
    $('#senha').focus();
    return;
    } 

});

    
</script>

@stop