@extends('template.base')

@section('content')

<main class="center-align">	

	<div class="container">
		<div class="row" style="border-bottom: 1px solid #DFDFDF">
			<div class="col s12 left-align">
				<div class="contorno-titulo-pagina">
					<h1>
						<strong>
							Contate o professor
						</strong>
					</h1>
				</div>

				<div class="mapeamento-pagina">
		            <a href="{{ base_url('welcome') }}">Home</a> &gt;  
		            <strong>Contato</strong>
		        </div>
			</div>
		</div>	
	
      	<div class="row">
            <form name="cadastro" action="{{ base_url('dashboard/insert') }}" method="POST">
				<div class="col s12 m6">
					<div class="card-panel"> 	
						<div class="row left-align">
							<div class="col s12 m6">
								<p class="navText">Nome completo</p>
								<input type="text" id="nome" name="nome" class="browser-default input_azul" placeholder="Nome completo" autocomplete="off" value="{{ ucfirst(strtoLower(get_user_nome())) }}" readonly="true" required="required"/>
							</div>

						<div class="col s12 m6">
							<p class="navText">Telefone</p>
							<input type="text" id="fone" name="fone" class="fone browser-default input_azul" placeholder="Telefone" autocomplete="off" />
						</div>

						<div class="col s12">
							<p class="navText">E-mail</p>
							<input type="email" id="email" name="email" class="browser-default input_azul" placeholder="E-mail" autocomplete="off" required="required"/>
						</div>

						<div class="col s12">
							<p class="navText">Mensagem</p>
							<textarea id="msg" name="msg" style="height: 100px; margin-top: 0px; padding: 8px; border: solid 1px #6372ff; outline: none; border-radius: 8px; margin-bottom: 8px;" autocomplete="off" class="browser-default" placeholder="Mensagem" required="required"></textarea>
						</div>

						<div class="col s12">
							<button class="btn blue darken-3 full-width navButton full-width" type="submit">	
				        		Enviar
				        		<i class="material-icons right">chevron_right</i>
				           </button>
						</div>
						</div>
					</div>
			    </form>
	      	</div>

	      	<div class="col s12 m6">
	      		<div class="card-panel">
	      			<div class="row">
			            <h5 class="center navText">Ou se preferir</h5>
			            <p class="valign-wrapper navText">
			              <i class="material-icons left">call</i> (11) 95789-8152
			            </p>
			            <p class="valign-wrapper navText">
			              <i class="material-icons left">mail</i> idiomas.enjoy2learn@gmail.com
			            </p>
			            <a class="valign-wrapper navText" href="http://www.facebook.com/Enjoy2learn.idiomas" target="_blank">
			              <i class="material-icons left">facebook</i> /Enjoy2learn.idiomas
			            </a>
			            <p class="valign-wrapper navText">
			            	<i class="material-icons left">notifications</i> (11) 95789-8152
			            </p>
		            </div>
	        	</div>
	        </div>

		</div>
	</div>

</main>

@stop

@section('extra-javascript')

@stop