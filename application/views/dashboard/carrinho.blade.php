@extends('template.base')

@section('content')

@php
	$total = 0.00;
	$totalCursos = 1;
@endphp

<main class="center-align">	
	
	<div class="container">
		
		<div class="row" style="border-bottom: 1px solid #DFDFDF">
			<div class="col s12 left-align">
				<div class="contorno-titulo-pagina">
					<h1>
						<strong>
							Carrinho
						</strong>
					</h1>
				</div>

				<div class="mapeamento-pagina">
		            <a href="{{ base_url('welcome') }}">Home</a> &gt; 
		            <a href="{{ base_url('library') }}">Cursos</a> &gt; 
		            <strong>Carrinho</strong>
		        </div>
			</div>
		</div>	

      	<div class="row">

      		<div class="col s12">
			    <table class="responsive-table striped">

			        <tbody>

			          @forelse($cursos as $curso)
			          	<tr>
				            <td class="center" style="width: 200px;">
				            	<img class="circle" src="{{ base_url('assets/uploads/cursos/'.$curso['foto']) }}" style="width: 160px; height: 160px;">
				            </td>
				            <td class="center" style="{{ $mobile ? 'width: 260px' : 'width: 450px' }};">
				            	<b>
				            		<a class="tooltipped" 
				            			data-position="top" 
				            			data-tooltip="Ver mais sobre <b>{{ $curso['nome'] }}</b>"
				            			href="{{ base_url('library/'.$encryptor->encrypt($curso['id'])) }}">{{ $curso['nome'] }}
				            		</a>
				            	</b> 

				            	@php
								  echo "<p style='color: #000000; font-size: 13px; white-space: normal'>".nl2br($curso['descricao'])."</p>";
							    @endphp

				            </td>
				            <td class="center">R$ {{ convert_double_to_BRL($curso['preco']) }}</td>
				            <td class="center">
				            	<div class="blue-text tooltipped removerCurso" data-id="{{ $encryptor->encrypt($curso['id']).'/'.$encryptor->encrypt($curso['id_venda']) }}" style="cursor: pointer;" 
				            		data-position="top" data-tooltip="Remover - {{ $curso['nome'] }}">
				            		<i class="material-icons">delete</i>
				            	</div>
				            </td>
				        </tr>

				        @php
				        	$total = ($total + $curso['preco']);
				        	$totalCursos++;
				        @endphp
				      @empty
				      	<div class="total-lista-carrinho">
							<ul>
								<div class="nenhum-item-cadastrado">
									Você não possui nenhum item em seu carrinho!
									<p>Estamos aguardando você fazer a melhor escolha da sua vida.</p>
								</div>
							</ul>
						</div>
						<br>
						<br><br><br>
			          @endforelse
			       
			        </tbody>
		      	</table>

		    </div>
	      	

		</div>
	      	<div class="row col s12 navText valign-wrapper">
	      		@if($carrinho != NULL)
		      		<div class="col s4">
		      			<p class="left" style="font-size: 20px">
		      				Desconto aplicado {{$carrinho->porcentagem}}%
		      			</p> 
		      		</div>
	      		@endif
	      		<div class="col s8 right-align">
	      			@if($carrinho != NULL)
			  			<p>
		  					<b style="text-decoration: line-through;">
		  						Total R$ {{ convert_double_to_BRL($total) }}
		  					</b>
		  					
		  					<br>
			  			
			  				<b>
			  					Valor com desconto R$ 
			  					{{ convert_double_to_BRL($total - ($total / 100 * $carrinho->porcentagem) )  }} 
			  					@php
			  						$total = $total - ($total / 100 * $carrinho->porcentagem);
			  					@endphp
			  				</b>
			  			</p>
			  		@elseif($cursos)
			  			<p>
		  					<b>
		  						Total R$ {{ convert_double_to_BRL($total) }}
		  					</b>
			  			</p>
		  			@endif
	      		</div>
	  		</div>

	  		@if($cursos)
		  		<div class="white-text blue full-width" style="padding: 1px; font-size: 18px; white-space: normal;">
			  		<p> 
						<span style="text-decoration: underline; font-size: 20px">VANTAGENS</span>
					<br>
			  			 Lembrando que ao comprar os cursos enjoy2learn você ganha acesso vitalício	  		
			  		<br>
			  			 Garantimos a devolução do seu dinheiro de volta em até 30 dias.
		  			</p>
		  		</div>
		  		
		  		@if($carrinho == NULL)
			    	<div style="border: 1px solid #2196F3; padding: 20px;">
				    	<p class="blue-text full-width white"> <b> CUPOM DE DESCONTO ? </b> </p>
				  		<div class="row valign-wrapper">
					    	<div class="col s6 valign-wrapper">
				              	<input id="cupon" type="text" class="browser-default inputCupom" 
				              		data-id="{{ $encryptor->encrypt($curso['id_venda']) }}" placeholder="Digite o codigo do cupom">
				          	</div>
					        <div class="col s6 btn blue darken-3 queroDesconto">
					          	<i class="material-icons right">add</i>
					          	Aplicar desconto agora
					        </div>
				       	</div>
				       	<p class="msgCupom red-text"></p>
			       	</div>
			    @endif

		       	<div class="row">
			        <div class="btn blue darken-3 right col s12 m4 pulse botaoFinaliza" 
			        	style="margin-bottom: 60px; margin-top: 10px;" data-total="{{$total}}">
			          	<i class="material-icons right">chevron_right</i>
			          	FINALIZAR COMPRAS
			        </div>
			    </div>
	       	@endif

	</div>

</main>

@stop

@section('extra-javascript')
 	<script type="text/javascript">
 		$(document).ready(function(){

			$(".removerCurso").on('click', function(){
			    var id = $(this).data('id');
			    swal({
				  	title: 'Tem certeza?',
				  	text: "Esta ação não poderá ser desfeita!",
				  	type: 'warning',
				  	showCancelButton: true,
				  	confirmButtonColor: '#b71c1c ',
				  	cancelButtonColor: '#ccc',
				  	confirmButtonText: 'Sim'
				}, function() {
				    location.href = base_url('cart/delete/'+id);
				});
			});

			$('.queroDesconto').on('click', function(){
				let cupom = $('.inputCupom').val();
				let idVenda = $('.inputCupom').data('id');
				if(cupom == ''){ return; }
				$(".queroDesconto").attr("disabled", true);
				$('.queroDesconto').html('AGUARDE ...');

				let url = base_url('cart/cupom/'+cupom+'/'+idVenda);
				$.get(url, function(data){

					if(data == '2'){
						$('.inputCupom').val('');
						$('.msgCupom').html('Cupom fora da validade, desculpe.');
						$('.inputCupom').focus();

						setTimeout(function() {
							$('.queroDesconto').html('<i class="material-icons right">add</i> Aplicar desconto agora');
							$(".queroDesconto").attr("disabled", false);
							$('.msgCupom').html('');
						}, 6000);
					}

					if(data == '1'){
						setTimeout(function(){
							window.location.reload();
						},5000);
					}

					if(data == '0'){
						$('.inputCupom').val('');
						$('.msgCupom').html('Cupom não encontrado, tente novamente.');
						$('.inputCupom').focus();

						setTimeout(function() {
							$('.queroDesconto').html('<i class="material-icons right">add</i> Aplicar desconto agora');
							$(".queroDesconto").attr("disabled", false);
							$('.msgCupom').html('');
						}, 6000);	
					}

				});			
			});

			$('.botaoFinaliza').on('click', function(){
				let total = $(this).data('total');
				console.log(total);
			});
			

		});
 	</script>
@stop