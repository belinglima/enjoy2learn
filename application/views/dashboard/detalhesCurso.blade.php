@extends('template.base')

@section('content')

@php
	$arrayIDS = [];
	foreach ($carrinho AS $row) :
		$arrayIDS[] = $row['curso_id'];
	endforeach;
@endphp

<main>	
	
	<div class="container">
	 
		<div class="row" style="border-bottom: 1px solid #DFDFDF">
            <div class="col s12 left-align">
              <div class="contorno-titulo-pagina">
                <h1>
                  <strong>
                    Cursos
                  </strong>
                </h1>
              </div>

              <div class="mapeamento-pagina">
                      <a href="{{ base_url('welcome') }}">Home</a> &gt;  
                      <a href="{{ base_url('library') }}">Cursos</a> &gt;  
                      <strong>{{ $curso->nome }}</strong>
                  </div>
            </div>
        </div>
	 	
		<div class="row center-align">

			<div class="col s12">
			    {{-- <h2 class="header">Horizontal Card</h2> --}}
			    <div class="card horizontal">
			      	<div class="card-image">
			        	<img src="{{ base_url('assets/uploads/cursos/'.$curso->foto) }}" style="height: 250px; width: 250px;">
			      	</div>
			      	<div class="card-stacked">
			        	<div class="card-content">
			        		<div class="chip blue darken-3 white-text " style="position: absolute; top: 10px; right: 10px; float: right;">
					    		R$ {{ convert_double_to_BRL($curso->preco) }}
							</div>

			        		<h5><b>{{ $curso->nome }}</b></h5>
							@php
								echo "<p style='color: #000000'>".nl2br($curso->descricao)."</p>";
						    @endphp
							  
			          		<br />
			          		<p><b>São {{ $totalAulas }} aulas no total este curso</b></p>
			        	</div>
			        	<div class="card-action">
					      	@if (in_array($curso->id, $arrayIDS))
					    		<a class="btn red darken-3 col s12" 
						      		href="{{ base_url('cart/') }}" style="margin-top: -10px;">
						      		CARRINHO COMPRAS
						      		<i class="material-icons white-text right">chevron_right</i> 
						      	</a>
				    		@else
					    		<a class="btn blue darken-3" href="{{ base_url('comprar/'.$encryptor->encrypt($curso->id)) }}" style="margin-top: -10px; width: 100%;">
						      		Add carrinho
						      		<i class="material-icons white-text right">attach_money</i> 
						      	</a>
				    		@endif
			        	</div>
			      	</div>
			    </div>
			</div>
	    	
		    <div class="col s12">
		    	@if($aulas != NULL)
	    			<P class="navText" style="text-decoration: underline;"><b>Aulas</b></P>
		    	@else
		    		<p class="navText"><b>Aulas ainda não foram cadastradas, aguarde.</b></p>
		    	@endif
		      	
		      	<ul class="collapsible popout" data-collapsible="accordion">
		      		@foreach($aulas as $aula)
			      		@if($loop->first)
			      			 <li class="active">
						        <div class="collapsible-header">
						        	<i class="material-icons">expand_more</i>
						            	{{ ucfirst(strtoLower($aula['nome'])) }}
						        </div>
						        <div class="collapsible-body">
									@php
										echo "<p style='color: #000000'>".nl2br($aula->descricao)."</p>";
								    @endphp

						            @if(isset($aula['url_apresentacao']))
						            	<p>
							            	<div class="btn blue darken-3 full-width abre-ajuda" data-url="{{$aula['url_apresentacao']}}">
					                        	Video demonstração
					                      	</div>
							            </p>
						            @endif
						        </div>
						    </li>	
			      		@else
			      			<li>
						        <div class="collapsible-header">
						        	<i class="material-icons">expand_more</i>
						            {{-- {{ $loop->iteration }} -  --}}{{ ucfirst(strtoLower($aula['nome'])) }}
						        </div>
						        <div class="collapsible-body">
									@php
										echo "<p style='color: #000000'>".nl2br($aula['descricao'])."</p>";
					           	    @endphp

						            @if(isset($aula['url_apresentacao']))
						            	<p>
							            	<div class="btn blue darken-3 full-width abre-ajuda" data-url="{{$aula['url_apresentacao']}}">
					                        	Video demonstração
					                      	</div>
							            </p>
						            @endif
						        </div>
						    </li>
			      		@endif		    
					@endforeach
				</ul>
		    </div>
		</div>


		<div id="modal-ajuda" class="modal modal-ajuda">
	  <div class="modal-content content-ajuda" style="padding: 0px;">
	    <div id="ajuda" style="width: 100%; height: 360px;">

	      <iframe class="videoPlay" width="100%" height="360"
	        >
	      </iframe>

	    </div>
	    </div>
	      <a href="#" class="btn-floating modal-close blue darken-3" style="position: absolute; top: 5px; right: 5px; z-index: 9999;">
	        <i class="material-icons">close</i>
	      </a>
	    </div>
	</div>

	</div>

</main>

@stop

@section('extra-javascript')
	
	<script type="text/javascript">

		$('#modal-ajuda').modal({
	      dismissible: false
	    });

	    $('.abre-ajuda').on('click', function(){
	      $('.modal-ajuda').modal('open');
	      let url = $(this).data('url');

	      urlVideo(url);
	    });

	    $('.modal-close').on('click', function(){
	        const videos = document.querySelectorAll('iframe');
	           videos.forEach(i => {
	              let source = i.src;
	              i.src = '';
	              i.src = source;
	           });
	         $('iframe').removeAttr('src','');
	    });

	    const urlVideo = (hash) => {
	    	let url = 'https://www.youtube.com/embed/'+hash;
	    	$('iframe').attr('src', url);
	    }
	</script>
@stop