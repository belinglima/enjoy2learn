@extends('template.base')

@section('content')

@php
	$CI =& get_instance();
  	$selecionado = $CI->uri->segment(1, 'duvidas-gerais');

  	if($selecionado == 'duvidas-gerais'){
  		$a = 'menu-faq-selecionado';
  		$b = 'i-no-hover-selecionado';
  		$c = 'i-selecionado';
  	} else {
  		$a = '';
  		$b = '';
  		$c = '';
  	} 
  	
  	if($selecionado == 'duvidas-alunos'){
  		$d = 'menu-faq-selecionado';
  		$e = 'i-no-hover-selecionado';
  		$f = 'i-selecionado';
  	} else {
  		$d = '';
  		$e = '';
  		$f = '';
  	}

  	if($selecionado == 'duvidas-pagamentos'){
  		$g = 'menu-faq-selecionado';
  		$h = 'i-no-hover-selecionado';
  		$i = 'i-selecionado';
  	} else {
  		$g = '';
  		$h = '';
  		$i = '';
  	}
@endphp

<main>
	
	<div class="container">
        <div class="row" style="border-bottom: 1px solid #DFDFDF">
            <div class="col s12 left-align">
                <div class="contorno-titulo-pagina">
                    <h1>
                        <strong>
                            Dúvidas frequentes
                        </strong>
                    </h1>
                </div>

                <div class="mapeamento-pagina">
                    <strong>Tire todas as suas dúvidas sobre a plataforma</strong>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div id="total-conteudo-geral">
            <div id="conteudo-estatico">
            	<div class="total-menus-faq">
            		<ul>
                        <li>
                            <a href="{{ base_url('duvidas-gerais') }}" class="{{ $a }}">
                                <div class="icone i-no-hover {{ $b }}" style="background-image: url('{{ base_url('assets/images/230620201342120000007514.svg')  }}')"></div>
                                <div class="icone i-hover {{ $c }}" style="background-image: url('{{ base_url('assets/images/23062020134212000000684323.svg')  }}')"></div>
                                Dúvidas gerais
                            </a>
                        </li>
                        <li>
                            <a href="{{ base_url('duvidas-alunos') }}"  class="{{ $d }}">
                                <div class="icone i-no-hover {{ $e }}" style="background-image: url('{{ base_url('assets/images/23062020141940000000698213.svg')  }}')"></div>
                                <div class="icone i-hover {{ $f }}" style="background-image: url('{{ base_url('assets/images/23062020141940000000429358.svg')  }}')"></div>
                                Alunos
                            </a>
                        </li>
                        <li>
                            <a href="{{ base_url('duvidas-pagamentos') }}"  class="{{ $g }}">
                                <div class="icone i-no-hover {{ $h }}" style="background-image: url('{{ base_url('assets/images/23062020142007000000482303.svg')  }}')"></div>
                                <div class="icone i-hover {{ $i }}" style="background-image: url('{{ base_url('assets/images/230620201420070000005889.svg')  }}')"></div>
                                Pagamentos
                            </a>
                        </li>
            			<div class="clearfix"></div>
            		</ul>
            	</div>

            	<div class="total-listagem-faq">
            		<ul>

            			@foreach($duvidas AS $duvida)
	                        <li>
	                            <div class="contorno-click-campo">
	                                <div class="icone-campo icone-mais"></div>
	                                <h2>
	                                    {{ $loop->iteration }}  - {{ $duvida['pergunta'] }}
	                                </h2>
	                                <div class="clearfix"></div>
	                            </div>
	                            <div class="texto-faq">
	                                @php
										echo "<p style='color: #000000'>".nl2br($duvida['resposta'])."</p>";
									@endphp
	                            </div>
	                        </li>
                        @endforeach
                        
            			<div class="clearfix"></div>
            		</ul>
            	</div>

            </div>
		</div>
    </div>

</main>

@stop

@section('extra-javascript')
 	<script type="text/javascript">
 		$('.container')
        .attr('unselectable', 'on')
        .css({
            'user-select': 'none',
            'MozUserSelect': 'none'
        })
        .on('selectstart', false)
        .on('mousedown', false);

		$(function(){
			function escondeTodos(){
				//ESCONDE TODOS E COLOCA ICONE MAIS EM TODOS
				$('.texto-faq').hide(100);
				$('.icone-campo').removeClass('icone-menos');
				$('.icone-campo').addClass('icone-mais');
			}	

			$('.contorno-click-campo').click(function(){
				escondeTodos();
				if($(this).parent().find('.texto-faq').css('display')=='none'){
					escondeTodos();

					//ABRE O TEXTO, REMOVE O ICONE MAIS E ADICIONA O ICONE MENOS NO ITEM CLICADO
					$(this).parent().find('.texto-faq').show(100);
					$(this).parent().find('.icone-campo').removeClass('icone-mais');
					$(this).parent().find('.icone-campo').addClass('icone-menos');
				} else {
					escondeTodos();
				}
			});
		});	
	</script>

@stop