@extends('template.base')

@section('content')

@php
	$arrayIDS = [];
	foreach ($carrinho AS $row) :
		$arrayIDS[] = $row['curso_id'];
	endforeach;

	$CI =& get_instance();
  	$botoes = $CI->uri->segment(1, 'library');
  	
  	if($botoes == 'library'){
  		$primeiro = 'selected';
  	} else {
  		$primeiro = '';
  	} 
  	
  	if($botoes == 'studying'){
  		$segundo = 'selected';
  	} else {
  		$segundo = '';
  	}

  	if($botoes == 'completed'){
  		$terceiro = 'selected';
  	} else {
  		$terceiro = '';
  	}
@endphp


<main>	
	
	<div class="container">
	
		<div class="row" style="border-bottom: 1px solid #DFDFDF">
			<div class="">
				<div class="col s12 m6 left">
					<div class="contorno-titulo-pagina">
						<h1>
							<strong>
								Nossos cursos
							</strong>
						</h1>
					</div>

					<div class="mapeamento-pagina hidden-mobile">
						<a href="{{ base_url('welcome') }}">Home</a> &gt; <strong>Cursos</strong>
					</div>
				</div>
				
				<div class="col s12 m6 right">
					<form action="{{ base_url('search') }}" method="post" class="formulario-busca">
						<div class="contorno-titulo-pagina">
							<h1>
								<strong style="font-size: 16px; margin-bottom: -10px;">
									Pesquisar
								</strong>
							</h1>
						</div>
						<div>
							<input type="text" id="FrmBusca" name="FrmBusca" placeholder="Ex: &quot;Cursos ENEM&quot;" value="" autocomplete="off">
							<input type="submit" class="botao-busca" value="">
						</div>
					</form>
				</div>
			</div>

			<div class="contorno-botoes-menu col s12">
				<div class="display-botoes-menu">
		            <a href="{{ base_url('library') }}" class="menu-todos {{ $primeiro }}" name="todos">
		                Ver todos
		            </a>
		            <a href="{{ base_url('studying') }}" class="menu-cursando {{ $segundo }}">
		                Cursando
		            </a>
					<a href="{{ base_url('completed') }}" class="menu-cursados {{ $terceiro }}">
						Cursados
					</a>
				</div>
			</div>
		</div>			

		<div class="row">
			@forelse($cursos as $curso)
		    	<div class="col s12 m4">

			        <div class="card sticky-action hoverable">
					    <div class="card-image waves-effect waves-block waves-light">
					    	<div class="chip white blue-text " style="position: absolute; top: 10px; right: 10px; float: right;">
					    		R$ {{ convert_double_to_BRL($curso['preco']) }}
							</div>
					      	<img class="activator" src="{{ base_url('assets/uploads/cursos/'.$curso['foto']) }}" style="height: 250px;">
					    </div>
					    <div class="card-action	">
					      <span class="card-title activator grey-text text-darken-4">
					      	{{ $curso['nome'] }}
					      </span>
					      <p>
					      	<a class="btn full-width blue darken-3" href="{{ base_url('library/'.$encryptor->encrypt($curso['id'])) }}">
					      		<i class="material-icons white-text left">add</i> 
					      			detalhes	
					      		<i class="material-icons white-text right">add</i> 
					      	</a>

					      	@if (in_array($curso['id'], $arrayIDS))
					    		<a class="btn full-width red darken-3" 
						      		href="{{ base_url('cart/') }}">
						      		CARRINHO COMPRAS
						      		<i class="material-icons white-text right">chevron_right</i> 
						      	</a>
				    		@else
				    			<a class="btn full-width blue darken-3" 
					      		href="{{ base_url('comprar/'.$encryptor->encrypt($curso['id'])) }}">
						      		Add carrinho
						      		<i class="material-icons white-text right">attach_money</i> 
						      	</a>
				    		@endif
					      </p>
					    </div>
					    <div class="card-reveal">
					      <span class="card-title grey-text text-darken-4">
					      	{{ $curso['nome'] }}
					      	<i class="material-icons right">close</i>
					      </span>
						  @php
						  	echo "<p style='color: #000000'>".nl2br($curso['descricao'])."</p>";
					  	  @endphp
					    </div>
					 </div>
			    </div>
			@empty
				<div class="col s12">
					@if(isset($busca))
						<p>
							Sua busca não encontrou nada: <b> {{ $busca }} </b>
						</p>
					@else
						<p>
							Aguardando iniciar cursos
						</p>
					@endif

					<br><br><br><br><br><br><br><br><br>
				</div>
		    @endforelse
		</div>

	</div>

</main>

@stop