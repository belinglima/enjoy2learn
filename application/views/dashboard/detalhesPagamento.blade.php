@extends('template.base')

@section('content')

@php
	const STATUS_PAGAMENTO = [
		0 => 'AGUARDANDO',
		1 => 'PENDENTE',
		2 => 'PAGO',
		3 => 'NEGADO',
		4 => 'EXPIRADO',
		5 => 'CANCELADO',
		6 => 'NÃO FINALIZADO',
		7 => 'AUTORIZADO',
		8 => 'CHARGEBACK'
	];

	const TIPO_PAGAMENTO = [
		1 => 'CARTÃO DE CRÉDITO',
		2 => 'BOLETO',
		3 => 'DÉBITO ONLINE',
		4 => 'CARTÃO DE DÉBITO',
	];

	const BANDEIRAS = [
		1 => 'VISA',
		2 => 'MASTERCARD',
		3 => 'AMERCICANEXPRESS',
		4 => 'DINERS',
		5 => 'ELO',
		6 => 'AURA',
		7 => 'JCB',
		8 => 'DISCOVER',
		9 => 'HIPERCARD',
	];
@endphp

<main>	
	
	<div class="container">
		<div class="row" style="border-bottom: 1px solid #DFDFDF">
			<div class="col s12 left-align">
				<div class="contorno-titulo-pagina">
					<h1>
						<strong>
							Detalhes pagamento
						</strong>
					</h1>
				</div>

				<div class="mapeamento-pagina">
		            <a href="{{ base_url('welcome') }}">Home</a> &gt;  
		            <a href="{{ base_url('classes') }}">Meus cursos</a> &gt;  
		            <strong>Informações</strong>
		        </div>
			</div>
		</div>

      	<div class="row">

      		@if($cursos)
      			<div class="col s12">
					<table class="tabelaDetalhes card">
						<tr>
							<td colspan="2">
								<h5>
						        	PEDIDO #{{ $venda['codigo'] }}
						      	</h5>
							</td>
						</tr>
						<tr>
							<td>Data da compra:</td>
							<td>{{ format_date_to_show($venda['data']) }}</td>
						</tr>
						@if ($cielo['payment_status'])
						<tr>
							<td>Status da tranção:</td>
							<td>{{ STATUS_PAGAMENTO[$cielo['payment_status']] }}</td>
						</tr>
						@endif
						@if ($cielo['product_id'])
						<tr>
							<td>Código Cielo:</td>
							<td>{{ $cielo['product_id'] }}</td>
						</tr>
						@endif
						@if ($cielo['customer_name'])
						<tr>
							<td>Nome:</td>
							<td>{{ $cielo['customer_name'] }}</td>
						</tr>
						@endif
						@if ($cielo['payment_method_type'])
							<tr>
								<td>Tipo de Pagamento:</td>
								<td>{{ TIPO_PAGAMENTO[$cielo['payment_method_type']] }}</td>
							</tr>
						@endif
						@if ($cielo['payment_method_brand'])
							<tr>
								<td>Bandeira do cartão:</td>
								<td>{{ BANDEIRAS[$cielo['payment_method_brand']] }}</td>
							</tr>
						@endif
						@if ($cielo['payment_maskedcreditcard'])
							<tr>
								<td>Cartão:</td>
								<td>{{ $cielo['payment_maskedcreditcard'] }}</td>
							</tr>
						@endif
						@if ($venda['total'] > 0)
						<tr>
							<td>Total:</td>
							<td>R$ {{ convert_double_to_BRL($venda['total']) }}</td>
						</tr>
						@endif
					</table>
	      		</div>

	      		@if($cursos)
	      			<h5 class="center"> 
	      				<b> Compras </b> 
	      			</h5>
	      		@endif

				@foreach ($cursos AS $curso)
					<div class="col s12 m4">
						<table class="card-panel">
							<tr>
								<td class="center">
							        <h2 class="imagemCurso">
							          	<img src="{{ base_url('assets/uploads/cursos/'.$curso['foto']) }}" style="width: 100%; height: 200px;">
							          	<div class="content">
							          		<a href="{{ base_url('library/'. $encryptor->encrypt($curso['id']) ) }}" style="color: #595959;">

								            	{{ $curso['nome'] }}
								            </a>
							        	</div>
							      	</h2>
								</td>
							</tr>
							<tr>
								<td class="center" height="150">
									@php
										echo "<p style='color: #000000'>".nl2br($curso['descricao'])."</p>";
					                @endphp	
								</td>
							</tr>
							<tr style="background-color: #595959; color: #fff">
								<td class="center">Valor R$ {{ convert_double_to_BRL($curso['preco']) }}</td>
							</tr>
						</table>
					</div>
				@endforeach

      		@else
      			<div class="row">
		      		<div class="col s12">
					    <table class="responsive-table striped">
					        <tbody>
						      	<div class="total-lista-carrinho">
									<ul>
										<div class="nenhum-item-cadastrado">
											Você não possui nenhum curso comprado
											<p>Realize uma compra para ver os detalhes de pagento!</p>
										</div>
									</ul>
								</div>
								<br><br>
					        </tbody>
				      	</table>
				    </div>
				</div>
      		@endif

		</div>
	</div>

	<br><br><br>

</main>

@stop