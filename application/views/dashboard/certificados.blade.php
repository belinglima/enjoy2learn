{{-- FONTES DO CERTIFICADO

Certificado de conclusão de curso
Nome da fonte: Source Serif Pro | tamanho da fonte 47,2
cor #114851

Certificamos que o aluno 
Nome da fonte: Lexend Deca | tamanho da fonte 23,3
cor #373737

Nome do aluno 
Nome da fonte: Amsterdam two | tamanho da fonte 55,6
cor #03989e

Concluiu com sucesso…
Nome da fonte: Lexend Deca | tamanho da fonte 15,8
cor #373737 --}}

@extends('template.base')

@section('content')

@php
	$arrayIDS = [];
	foreach ($avaliados AS $row) :
		$arrayIDS[] = $row['idCurso'];
	endforeach;
@endphp

<main>	
	
	<div class="container">
      	
      	<div class="row" style="border-bottom: 1px solid #DFDFDF">
			<div class="col s12 left-align">
				<div class="contorno-titulo-pagina">
					<h1>
						<strong>
							Meus certificados
						</strong>
					</h1>
				</div>

				<div class="mapeamento-pagina">
		            <a href="{{ base_url('welcome') }}">Home</a> &gt; 
		            <strong>Cursos livres</strong>
		        </div>
			</div>
		</div>

		<div class="row">
	        @forelse($cursos as $curso)

				<div class="col s12 m6 l3 body">
					<div class="card-container">
						<span class="pro">Liberado</span>
						<img class="img" style="height: 170px;" src="{{ base_url('assets/uploads/cursos/'.$curso['foto']) }}" alt="certificado liberado" width="200" />
						<h5>{{ $curso['nome'] }}</h5>
						<p>Parabéns você completou com sucesso este curso, <br> seu certificado está disponível</p>
						<div class="buttons">
							@if (! in_array($curso['idCurso'], $arrayIDS))
								<button class="primary avaliar" data-avaliar="{{ $encryptor->encrypt($curso['idCurso']) }}">
									Avaliar
								</button>
							@else
								<button class="primary avaliado">
									Avaliado
								</button>
							@endif
							<button class="primary ghost" data-get="{{ $encryptor->encrypt($curso['idCurso']) }}">
								Obter
							</button>
						</div>
						<br>
					</div>
					<br>
  				</div>

			@empty
      			<div class="col s12">
			      	<div class="total-lista-carrinho">
						<ul>
							<div class="nenhum-item-cadastrado">
								Você não possui nenhum certificado disponível ainda!
								<p>Conclua os cursos primeiro :)</p>
							</div>
						</ul>
					</div>
					<br>
					<br><br><br>
			    </div>
		    @endforelse
		</div>
	</div>

</main>

@stop

@section('extra-javascript')
	<script type="text/javascript">
		$('.avaliar').on('click', function(){
			let avaliar = $(this).data('avaliar');
			location.href = base_url('avaliar/' + avaliar);
		})
		
		$('.ghost').on('click', function(){
			let get = $(this).data('get');
			window.open( base_url('getcertificate/'+get), "_blank"); 
		})
	</script>
@stop