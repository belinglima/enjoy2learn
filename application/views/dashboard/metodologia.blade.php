@extends('template.base')

@section('content')

<main>	
	
	<div class="container">
      	<div class="row" style="border-bottom: 1px solid #DFDFDF">
			<div class="col s12 left-align">
				<div class="contorno-titulo-pagina">
					<h1>
						<strong>
							Método Enjoy2Learn
						</strong>
					</h1>
				</div>

				<div class="mapeamento-pagina">
		            <a href="{{ base_url('welcome') }}">Home</a> &gt;  
		            <strong>Metodologia</strong>
		        </div>
			</div>
		</div>

		<div class="row">
			<div class="total-como-funciona">
				<div class="conteudo">
					<h1>
						<p><strong>Antes de tudo, entenda a importancia do Inglês </strong>
							<br><strong> e os passos para a fluência.</strong></p>
					</h1>
					<ul>
						<li class="left">
							<div class="campo-bloco">
								<h2> 1 </h2>
								<div class="descricao">
									<div class="display">
										<p>
											O <strong> inglês </strong> se tornou a língua oficial do mundo dos negócios, tornando a comunicação mais rápida e homogênea entre empresas de todo o mundo. Ter fluência neste idioma pode significar uma oportunidade em ter salários maiores, além de que, profissionais com conhecimento na <strong> língua inglesa </strong> possuem o dobro de chances de ascensão no mercado de trabalho quando comparados com colegas que não possuem fluência.
										</p>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</li>

						<li class="right">
							<div class="campo-bloco">
								<div class="linha-2"></div>
								<h2> 2 </h2>
								<div class="descricao">
									<div class="display"><br>
										<p>
											Atualmente são inúmeros os cursos de inglês, aumentando as chances de profissionais que desejam aprender o idioma. Antes de escolher qual curso fazer, é importante verificar qual a <strong>metodologia de ensino </strong> utilizada pelo mesmo.
										</p>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</li>
					</ul>
					
					<div class="clearfix"></div>
					<h1>
						<p><strong>Metodo Enjoy2Learn.</strong></p>
					</h1>

					<ul>
						<li class="left">
							<div class="campo-bloco">
								<h2> 3 </h2>
								<div class="descricao">
									<div class="display">
										<p>
											Criamos um método que facilita a aprendizagem para um novo Idioma, onde o
											aluno aprende <strong> Inglês de maneira prática </strong>, dinâmica e integrada.
											A base de aprendizagem são as aplicações das metodologias <strong> Natural
											Approach </strong> e a <strong> coaching </strong>, bem como a aplicação da ferramenta <strong> PNL
											(Programação Neurolinguística).</strong>
										</p>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</li>

						<li class="right">
							<div class="campo-bloco">
								<div class="linha-2"></div>
								<h2> 4 </h2>
								<div class="descricao">
									<div class="display">
										<p>
											<strong> O Método aplica técnicas de PNL </strong> – Programação Neurolinguística em um passo-a-passo gradativo que foca na fluência da conversação.<br>
											<br> <strong> Alem de utilizarmos o coaching</strong>, que é um processo, uma metodologia, um conjunto de competências e habilidades que podem ser aprendidas e desenvolvidas por absolutamente qualquer pessoa pra alcançar um objetivo na vida pessoal ou profissional, até 20 vezes mais rápido, comprovadamente. 
										</p>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</li>

						<li class="left">
							<div class="campo-bloco">
								<h2> 5 </h2>
								<div class="descricao">
									<div class="display"><br>
										<p>
											Por fim utilizamos a <strong> Abordagem Natural Approach </strong> onde o aprendizado se dá através do uso natural da língua estrangeira em contato com o professor ou utilizando nosso conteudo programado. A fala surge espontaneamente no desenvolver da habilidade de comunicação.
										</p>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</li>
					</ul>

					<div class="clearfix"></div>
					<h1>
						<p><strong>Como funciona a plataforma.</strong></p>
					</h1>

					<ul>
						<li class="right">
							<div class="campo-bloco">
								<h2> 6 </h2>
								<div class="descricao">
									<div class="display"><br><br>
										<p>
											Você escolhe um plano de aulas que mais se encaixa na sua rotina;
										</p>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</li>

						<li class="left">
							<div class="campo-bloco">
								<div class="linha-2"></div>
								<h2> 7 </h2>
								<div class="descricao">
									<div class="display"><br><br>
										<p>
											Assista às aulas conforme sua disponibilidade e de acordo com sua rotina;
										</p>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</li>

						<li class="right">
							<div class="campo-bloco">
								<h2> 8 </h2>
								<div class="descricao">
									<div class="display"><br><br>
										<p>
											Participe da mentoria ao vivo 2x por semana, direto com seu professor especializado;
										</p>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</li>

						<li class="left">
							<div class="campo-bloco">
								<div class="linha-2"></div>
								<h2> 9 </h2>
								<div class="descricao">
									<div class="display"><br><br>
										<p>
											Faça os exercícios propostos e tire suas dúvidas de maneira rápida e clara por meio de nossos canais de suporte;
										</p>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</li>

						<li class="right">
							<div class="campo-bloco">
								<h2> 10 </h2>
								<div class="descricao">
									<div class="display"><br><br>
										<p>
											Nosso método trabalhará incansavelmente as 4 principais áreas de fixação da língua: listening, speaking, writing e Reading.
										</p>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</li>
					</ul>

				</div>
			</div>
		</div>
</main>

@stop

@section('extra-javascript')

@stop