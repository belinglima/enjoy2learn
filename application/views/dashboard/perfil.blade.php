@extends('template.base')

@php
   if(get_foto()){
      $foto = get_foto();
   } elseif ($usuario['foto']) {
      $foto = $usuario['foto'];
   } else {
      $foto = NULL;
   }
@endphp

@section('content')

<main>	
	
	<div class="container">
    
    <div class="row" style="border-bottom: 1px solid #DFDFDF">
      <div class="col s12 left-align">
        <div class="contorno-titulo-pagina">
          <h1>
            <strong>
              Meus Dados
            </strong>
          </h1>
        </div>

        <div class="mapeamento-pagina">
                <a href="{{ base_url('welcome') }}">Home</a> &gt;  
                <strong>Perfil</strong>
            </div>
      </div>
    </div>

    <div class="row center-align">
      <div class="col s12 m3">
        <div class="card-panel">
          <br>
          <br>
          @if ($foto)
             <img src="{{ base_url('assets/uploads/perfil/'.$foto )  }}" class="responsive-img circle" 
              style="width: 150px;">
          @else
            <img src="{{ base_url('assets/images/nofoto.jpg') }}" class="responsive-img circle"  
              style="width: 150px;">
          @endif
                        
          <a href="{{ base_url('dashboard/editarFoto') }}" 
            class="btn blue white-text tooltipped full-width" data-position="right" data-tooltip="Alterar Foto">
            <i class="material-icons right">edit</i> Alterar Foto
          </a>

          <a href="{{ base_url('dashboard/editarSenha') }}" 
            class="btn blue white-text tooltipped full-width" data-position="right" data-tooltip="Alterar Senha">
            <i class="material-icons right">fingerprint</i> Alterar Senha
          </a>
          

          <div style="margin-top: 20px;" class="blue-text">
            <b>{{ ucfirst(strtoLower($usuario['nome'])) }}</b>
          </div>
          @if($usuario['email'])
            <div class="blue-text">
              <b>{{strtoLower($usuario['email']) }}</b>
            </div>
          @endif  
          <div class="blue-text">
            <b>{{ ucfirst(strtoLower($usuario['nivel'])) }}</b>
          </div>
          
          <br>
        </div>
      </div>

      <div class="col s12 m9">
        
        <p class="navText" style="text-decoration: underline;">
          <b>Perfil do usuário</b>
        </p>
        @if($usuario['cep'] == NULL) 
           <b class="red-text">Por favor preencha os dados do seu perfil.</b>
        @endif
        
        <div class="col s12" style="margin-bottom: 20px; width: 100%;">
          <form name="cadastro" action="{{ base_url('dashboard/alterarDados/'.$encryptor->encrypt($usuario['id'])) }}" method="POST">
            
            <div class="row">
              <div class="col s12">
                <input placeholder="Nome completo" name="nome" type="text" class="browser-default input_azul" value="{{ ucfirst(strtoLower($usuario['nome'])) }}" required>
              </div>

              <div class="col s12 m4">
                <input placeholder="CPF" name="cpf" type="text" class="cpf browser-default input_azul" value="{{ $usuario['cpf'] ? $usuario['cpf'] : '' }}" required>
              </div>

              <div class="col s12 m4">
                <input placeholder="CEP" 
                  onblur="pesquisacep(this.value);"
                  type="text" name="cep" class="cep browser-default input_azul" value="{{ $usuario['cep'] ? $usuario['cep'] : '' }}" required>
              </div>

              <div class="col s12 m4">
                <input placeholder="Número" type="text" name="numero" class="browser-default input_azul" value="{{ $usuario['numero'] ? $usuario['numero'] : '' }}" required>
              </div>

              <div class="col s12">
                <input id="logradouro" placeholder="Rua" type="text" name="logradouro" class="browser-default input_azul" value="{{ $usuario['logradouro'] ? $usuario['logradouro'] : '' }}" required>
              </div>

              <div class="col s12 m6">
                <input  placeholder="Complemento/Referencia" type="text" name="complemento" class="browser-default input_azul" value="{{ $usuario['complemento'] ? $usuario['complemento'] : '' }}">
              </div>

              <div class="col s12 m6">
                <input id="bairro" placeholder="Bairro" type="text" name="bairro" class="browser-default input_azul" value="{{ $usuario['bairro'] ? $usuario['bairro'] : '' }}" required>
              </div>

              <div class="col s12 m6">
                <select class="browser-default input_azul estado" name="estado_sigla">
                  <option value="" disabled selected>Selecione o estado</option>
                  @foreach($estados as $estado) 
                    @if($estado['sigla'] == $usuario['estado_sigla'])
                    <option value="{{ $estado['sigla'] }}"selected>{{ $estado['descricao'] }}</option>
                    @else
                    <option value="{{ $estado['sigla'] }}">{{ $estado['descricao'] }}</option>
                    @endif
                  @endforeach
                </select> 
              </div>

              <div class="col s12 m6">
                <select class="browser-default input_azul carregaCidade" name="localidade">
                  <option value="" disabled selected>Selecione o estado</option>
                </select> 
                <input type="hidden" class="localidade" value="{{ $usuario['localidade'] ? $usuario['localidade'] : '' }}">
              </div>
              <div class="col s12">
                <div class="progress loading hide">
                  <div class="indeterminate"></div>
                </div>
              </div>  
              
            </div>
          </div>

          <div class="col s12">
            <button class="btn blue darken-1 alterar-dados full-width" type="submit">	
              Salvar dados
              <i class="material-icons right">chevron_right</i>
              </button>
          </div>
        </form>  
    </div>
</main>

@stop 

@section('extra-javascript')
  <script type="text/javascript">

    let pegaDadosOrigem = (selecionado) => {
        $(".loading").toggleClass('hide');
        let url = base_url('dashboard/getCidadeByEstadoSigla/' + selecionado );
        $.get(url, function(data){        
          $('.carregaCidade').html(data);
          $(".loading").toggleClass('hide');
        }); 
      }

      $('.estado').on('change', function(){
        let sigla = $(this).val();
        pegaDadosOrigem(sigla);
      });

      let estado = $('.estado').val();
      let cidade = $('.localidade').val();

      if(estado) {
          $(".loading-destino").toggleClass('hide');	
          let url = base_url('dashboard/getCidadeByEstadoSigla/' + estado + '/' + cidade);
            $.get(url, function(data){
                $('.carregaCidade').html(data);
            }); 
      }

      function pesquisacep(valor) {
        // $(".labelRua, .labelNumero, .labelBairro, .labelLocalidade, .labelUf").addClass('active');
        //Nova variável "cep" somente com dígitos.
        var cep = valor.replace(/\D/g, '');
        //Verifica se campo cep possui valor informado.
        if (cep != "") {
            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;
            //Valida o formato do CEP.
            if (validacep.test(cep)) {
                //Preenche os campos com "..." enquanto consulta webservice.
                $("#rua").val("...");
                $("#bairro").val("...");
                $(".estado").val("...");
                // $(".carregaCidade").val("...");
                //Cria um elemento javascript.
                var script = document.createElement('script');
                //Sincroniza com o callback.
                script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';
                //Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);
            } else {
                //cep é inválido.
                limpa_formulário_cep();
                swal({
                    title: "Aviso!",
                    text: "Formato de CEP inválido.",
                    closeOnConfirm: true
                });
            }
        } else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    };

    function limpa_formulário_cep() { 
        //Limpa valores do formulário de cep.
        $("#logradouro").val("");
        $("#bairro").val("");
        $(".estado").val("");
        // $(".carregaCidade").val("");
        // $("#uf").val("");
    }
        
    function meu_callback(conteudo) {
        if (!("erro" in conteudo)) {
            //Atualiza os campos com os valores.
            $("#logradouro").val(conteudo.logradouro);
            $("#bairro").val(conteudo.bairro);
            $(".estado").val(conteudo.uf);
              pegaDadosOrigem(conteudo.uf);
            setTimeout(function(){

              $(".carregaCidade").val(conteudo.localidade);
            },2000)
        } else {
            //CEP não Encontrado.
            limpa_formulário_cep();
            swal({
                title: "Aviso!",
                text: "CEP não encontrado, favor digitar os dados com referência",
                closeOnConfirm: true
            });
        }
    }
   
  </script>
@stop