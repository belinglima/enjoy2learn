@extends('template.base')

@section('content')

<main class="center-align">	

	<div id="about">
      	<div class="container"> 

          <div class="row" style="border-bottom: 1px solid #DFDFDF">
            <div class="col s12 left-align">
              <div class="contorno-titulo-pagina">
                <h1>
                  <strong>
                    O professor
                  </strong>
                </h1>
              </div>

              <div class="mapeamento-pagina">
                      <a href="{{ base_url('welcome') }}">Home</a> &gt;  
                      <strong>Quem sou eu</strong>
                  </div>
            </div>
          </div>

      		<div class="row">
      			
            <div class="col s12 m4"> 
      				<img src="{{ base_url('assets/images/professor_sem_bg.png') }}" class=" responsive-img" style="margin-top: 100px; width: 400px"> 
      			
            <span class="white-text col s12" 
              style="margin-bottom: 0px; margin-top: -5px; background-color: #1565c0;">TEACHER</span>
            </div>

      			<div class="col s12 m8">
              
              <div class="total-como-funciona">
                <div class="conteudo">
                  <h1>
                    <p><strong>Me chamo Dennis George Viera. </strong></p>
                  </h1>
                </div>
              </div>

      				<div class="">
      					<h4 class="blue-text alinhaTexto"></h4>
      					<p class="alinhaTexto">  
                    <strong> Professor de Inglês e neurolanguage coach, </strong> sócio - fundador da
                    <strong> Enjoy2learn Idiomas, </strong> a escola que nos últimos sete anos orientou e
                    direcionou alunos e clientes a alcançar suas metas pessoais e
                    profissionais no aprendizado e desenvolvimento da língua inglesa e
                    outros idiomas com mentorias e aplicações das metodologias Natural
                    Approach e a coaching, bem como a aplicação da ferramenta PNL
                    (Programação Neurolinguística).<br>
                  <br>
                    <strong>  Graduando em letras, Português/inglês. </strong><br>
                    <strong> Atuo como Life coach – Career coach – Personal coach – Positive and Language coach. Master Practitioner em PNL (Programação neurolinguística)</strong>
                  <br>
                  <br> 
                  <strong> Tenho experiência de mais de seis anos </strong> auxiliando meus alunos e clientes no aprendizado e desenvolvimento nas habilidades da língua inglesa e outros idiomas, bem como coach for career.
  	  						<br>
                  Sou filho de pai poliglota, onde absolvi está paixão em minha vida. Minha família é de origem Brasileira, Portuguesa, Uruguaia, Espanhola e Italiana. Como não ser apaixonado por idiomas?
                </p><br>
                <p class="alinhaTexto">  
                  Nível intermediário em outras quatro, o Italiano, o Francês e o Alemão, em contínuo desenvolvimento. Nível iniciante em mais quatro, Russo, Chinês Mandarin e Árabe. 
                  <br><br>
                  Hoje sou muito feliz por compartilhar está mesma paixão com minha própria família.
                  <br><br>
                  Sou um language lover (um amante das línguas), e um bookworm (um leitor havido, practitioner de PNL (Programação neurolinguística) e coaching.
                </p><br>
                <p class="alinhaTexto">  
                  Aprender outro idioma é fácil, apenas precisamos do mapa e guia correto, mas qual ou quais são os mapas e guias corretos? 
                  <br>
                  A resposta é simples; Aqueles que lhe auxiliam a extrair o máximo do seu potencial!
                </p>
                <br>
              </div>
  					</div>

            <div class="row">
              <div class="col s12 card-panel blue white-text" 
                style="
                  margin-bottom: -10px;
                  -webkit-border-top-left-radius: 10px;
                  -webkit-border-top-right-radius: 10px;
                  -moz-border-radius-topleft: 10px;
                  -moz-border-radius-topright: 10px;
                  border-top-left-radius: 10px;
                  border-top-right-radius: 10px;
                ">
               <h3>Por que me escolher?</h3>
              </div>

              <div class="col s12 m6 blue blue-text" 
                style="
                  -webkit-border-bottom-left-radius: 10px;
                  -moz-border-radius-bottomleft: 10px;
                  border-bottom-left-radius: 10px;
                ">
                <div class="card-panel">
                  <ul>
                    <li>
                      <i class="material-icons tiny blue-text left">done</i>
                      Flexibilidade de horário
                    </li>
                    <li>
                      <i class="material-icons tiny blue-text left">done</i>
                      Atendimento personalizado
                    </li>
                    <li>
                      <i class="material-icons tiny blue-text left">done</i>
                      Plataforma online 24 horas
                    </li>
                    <li>
                      <i class="material-icons tiny blue-text left">done</i>
                      Conteudo dinamico
                    </li>
                  </ul>
                </div>
              </div>

              <div class="col s12 m6 blue blue-text"
                style="
                  -webkit-border-bottom-right-radius: 10px;
                  -moz-border-radius-bottomright: 10px;
                  border-bottom-right-radius: 10px;
                ">
                <div class="card-panel">
                  <ul>
                    <li> 
                      <i class="material-icons tiny blue-text left">done</i>
                      Execicios diários</li>
                    <li> 
                      <i class="material-icons tiny blue-text left">done</i>
                      Preparação para entrevistas</li>
                    <li> 
                      <i class="material-icons tiny blue-text left">done</i>
                      Preparação IELTS/TOEFL</li>
                    <li> 
                      <i class="material-icons tiny blue-text left">done</i>
                      Pensar fora da caixa</li>
                  </ul>
                </div>
              </div>

            </div>


  				</div>
  			</div>
  		</div>
  	</div>

</main>

@stop