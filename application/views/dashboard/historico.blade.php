@extends('template.base')

@section('content')

@php
	const STATUS_PAGAMENTO = [
		0 => 'AGUARDANDO',
		1 => 'PENDENTE',
		2 => 'PAGO',
		3 => 'NEGADO',
		4 => 'EXPIRADO',
		5 => 'CANCELADO',
		6 => 'NÃO FINALIZADO',
		7 => 'AUTORIZADO',
		8 => 'CHARGEBACK'
	];
@endphp

<main>	

	<div class="container">

		<div class="row" style="border-bottom: 1px solid #DFDFDF">
			<div class="col s12 left-align">
				<div class="contorno-titulo-pagina">
					<h1>
						<strong>
							Minhas compras
						</strong>
					</h1>
				</div>

				<div class="mapeamento-pagina">
					<a href="{{ base_url('welcome') }}">Home</a> &gt; <strong>Histórico de compras</strong>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>

      	<div class="row">
			@if($compras)
				<div class="tabela-lista-minhas-compras">
					<div class="scroll-tabela-lista-minhas-compras">
						<div class="scroll-interno">
							<table class="uprightTbl">
								<thead>
									<tr class="uprightTr">
										<th width="160">
											<b>Data</b>
										</th>
										<th class="text-left">
											<b>Descrição</b>
										</th>
										<th>
											<b>Situação</b>
										</th>
										<th>
											<b>Valor</b>
										</th>
										<th class="right">
											<b>Fatura</b>
										</th>
									</tr>
								</thead>
								<tbody class="tabelaHistorico" valign="top">

									@foreach($compras AS $compra)
										<tr class="uprightTr">
			                        		<td class="">
			                        			{{ format_date_to_show($compra['data']) }} 
			                        		</td>
			                        		<td class="th-descricao movimento-{{$compra['id_venda']}}">
			                        			<b>
				                        			@foreach($movimentos AS $movimento)
				                        				@if($movimento['id_venda'] == $compra['id_venda'])
				                        					{{ $movimento['nome'] }}  <br>
				                        				@endif
				                        			@endforeach
				                        		</b>
			                        		</td>
			                        		<td class="situacao-pagamento">
			                        			{{ STATUS_PAGAMENTO[$compra['payment_status']] }} 
			                        		</td>
			                        		<td>
			                        			R$ {{ convert_double_to_BRL($compra['total']) }}
			                        		</td>
			                        		<td>
			                        			@if($compra['payment_status'] == 2)
													<div class="display-botoes-menu right">
											            <a data-id-venda="{{ $compra['id_venda'] }}" class="menu-todos btnPrintInvoice" name="todos">
											                Solicitar
											            </a>
													</div>
			                        			@else
			                        				Aguardando
			                        			@endif
			                        		</td>
										</tr>
									@endforeach

								</tbody>
							</table>

						</div>
					</div>
					<div class="aviso-deslizar hidden-md">
						&lt;&lt; Deslize para os lados nas linhas acima e veja todo o conteúdo &gt;&gt;
					</div>
				</div>

				<input type="hidden" class="movimentos" value="{{ json_encode($movimentos) }}">

			@else

				<div class="tabela-lista-minhas-compras">
					<div class="scroll-tabela-lista-minhas-compras">
						<div class="scroll-interno">
							<table class="uprightTbl">
								<tbody>
									<tr class="uprightTr">
										<th width="160">
											<b>Data</b>
										</th>
										<th class="text-left">
											<b>Descrição</b>
										</th>
										<th>
											<b>Situação</b>
										</th>
										<th>
											<b>Valor</b>
										</th>
										<th>
											<b>Fatura</b>
										</th>
									</tr>
		                        	<tr class="uprightTr">
		                        		<td class="nenhum-item-cadastrado" colspan="5">
		                        			Nenhuma compra encontrada!
		                        		</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="aviso-deslizar hidden-md">
						&lt;&lt; Deslize para os lados nas linhas acima e veja todo o conteúdo &gt;&gt;
					</div>
				</div>
			@endif
				
			<br><br>
			<br><br>
		</div>
	</div>

</main>

@stop

@section('extra-javascript')

	<script type="text/javascript" src="<?=base_url('assets/javascript/print.js');?>"></script>	
	<script type="text/javascript">
		const dataFormated = (dataUSA) => {
			let dataTotal = dataUSA;
			dataTotal = dataTotal.split(' ');

			let data = dataTotal[0];
			data = data.split('-');
			let dataBR = data[2] + "/" + data[1] + "/" + data[0];
			return dataBR;
		}

		const STATUS_PAGAMENTO = {
			0 : 'AGUARDANDO',
			1 : 'PENDENTE',
			2 : 'PAGO',
			3 : 'NEGADO',
			4 : 'EXPIRADO',
			5 : 'CANCELADO',
			6 : 'NÃO FINALIZADO',
			7 : 'AUTORIZADO',
			8 : 'CHARGEBACK'
		};

		$(".btnPrintInvoice").on('click', function(){
			var novoMovimento = [];
			var movimentos = JSON.parse($('.movimentos').val());
			let id_venda = $(this).data('id-venda');
			var geral = 0.00;

			for (const movimento of movimentos) 
			{	

				if(movimento.id_venda == id_venda){
					movimento.total = parseFloat(movimento.total).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
					movimento.data = dataFormated(movimento.data);
					movimento.situacao = STATUS_PAGAMENTO[movimento.situacao];
					geral = movimento.totalGeral;

					novoMovimento.push(movimento);
				}
			}

			printJS({
				printable: novoMovimento,
				type: 'json',
				header: 
					'<h4 class="custom-h4"> <b> Enjoy2Learn Idiomas</b> </h4>' +
					'<p class="custom-h3">Fatura</p>' + 
					'<p><b>Solicitado Dia: </b> {{ format_date_to_show(date('Y-m-d H:i:s')) }}<br>' + 
					'<b> Aluno: </b> {{ get_user_nome() }}</p>' +
					'<h2 class="custom">Total R$ ' + geral + '</h2>',
				properties: ['data', 'nome', 'situacao', 'total'],
				style: 'table { text-align: center; font-family: "Arial"; font-size: 17px; position: relative;} .custom-h4 { text-align: center; font-family: "Arial"; font-size: 35px; } .custom-h3 { text-align: center; font-family: "Arial"; font-size: 19px; } .custom { margin-bottom: 80%; bottom: 0; right: 0; position: absolute; } @page { size: A4; margin: 15mm;} body {margin: 15; height: 50%;} h4 {margin: 15}'
			});
		});
	</script>

@stop