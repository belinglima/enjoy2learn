@extends('template.base')

@section('content')

<main class="center-align">
		
	<div class="container">
			
		<div class="row" style="border-bottom: 1px solid #DFDFDF">
			<div class="col s12 left-align">
				<div class="contorno-titulo-pagina">
					<h1>
						<strong>
							Area do Aluno
						</strong>
					</h1>
				</div>

				<div class="mapeamento-pagina">  
		            <strong>Home</strong>
		        </div>
			</div>
		</div>

		@if($alerta['cep'] == NULL) 
			<b class="red-text">Por favor preencha os dados do seu perfil.</b>
		@endif	

	
		<div class="row">
			<div class="col s12 center">
				<div class="col s12" style="border-bottom: 1px solid #2196F3; border-bottom-width: 2px;">
					<h5 class="blue-text text-darken-1">Estatísticas</h5>
				</div>

				<div class="col s12 m6">
					{{-- <a href="#" style="color: inherit;"> --}}
						<div><i class="material-icons medium blue-text medium">art_track</i></div>
						<div><b>Aulas</b></div>
						<h5 style="font-weight: bold;">{{ $aulas }}</h5>
					{{-- </a>					 --}}
				</div>
				
				<div class="col s12 m6">
					{{-- <a href="#" style="color: inherit;"> --}}
						<div><i class="material-icons medium blue-text medium">menu_book</i></div>
						<div><b>Cursos</b></div>
						<h5 style="font-weight: bold;">{{ $cursos }}</h5>
					{{-- </a>					 --}}
				</div>

			</div>
		</div>			
		<p>..</p>
		<br><br><br><br>
</main>

@stop

@section('extra-javascript')

<script type="text/javascript">
	
	/*setInterval(function(){
    
	    let novaHora = new Date();
	    let hora = novaHora.getHours();
	    let minuto = novaHora.getMinutes();
	    let segundo = novaHora.getSeconds();
	    
	    hora  = zero(hora);
	    minuto = zero(minuto);
	    segundo = zero(segundo);
	    horario = hora+':'+minuto+':'+segundo;
	    
	    $('#horario').html(horario);
	    
	},1000)*/

	/*function zero(x) {
	    if (x < 10) {
	        x = '0' + x;
	    } 
	    return x;
	}*/

</script>

@stop