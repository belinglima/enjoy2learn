@extends('template.base')

@section('content')

<main>	
	
	<div class="container">
	   	<nav class="clean">
		  <div class="nav-wrapper">
		    <div class="col s12">
		      <a href="{{ base_url('profile') }}" class="breadcrumb">Perfil</a>
		      <a  class="breadcrumb" style="text-decoration: underline;">Alterar Foto</a>
		    </div>
		  </div>
		</nav>
        
        <div class="card center-align">
			<div class="card-content">

				<form name="cadastro" action="{{ base_url('dashboard/alterarFoto/'.$encryptor->encrypt($usuario['id'])) }}" method="POST" enctype="multipart/form-data">
					<div class="row">	
						<div class="col s12 center">
							@if (! $usuario['foto'])
														
							<div class="file-field input-field">
								<div class="btn blue darken-1">
									<span>FOTO</span>
									<input type="file" id="foto" name="foto" value="fileupload">
								</div>
								<div class="file-path-wrapper">
									<input class="file-path validate" type="text">
								</div>
							</div>
							@else
								<img src="{{ base_url('assets/uploads/perfil/'.$usuario['foto']) }}" class="responsive-img" style="padding: 2px; border: solid 1px #eee;">
								<div class="btn-floating blue darken-1 right remove-foto-perfil" data-id="{{ $usuario['id'] }}">
									<i class="material-icons">delete</i>
								</div>
							@endif
						</div>
					</div>

					<div class="row">							
					    <div class="col s12">
				        	<button class="btn blue darken-1 alterar-foto full-width" type="submit">	
				        		Salvar foto
				        		<i class="material-icons right">chevron_right</i>
				           </button>
				        </div>
					</div>
				</form>
			
			</div>
        </div>    
	</div>

</main>

@stop

@section('extra-javascript')

<script type="text/javascript">
    
	$(".remove-foto-perfil").on('click', function(){
		let id = $(this).data('id');
			 swal({
				 title: 'Deseja excluir esta imagem?',
				 text: "Esta ação não poderá ser desfeita!",
				 type: 'warning',
				 showCancelButton: true,
				 confirmButtonColor: '#b71c1c ',
				 cancelButtonColor: '#ccc',
				 confirmButtonText: 'Sim, deletar!'
			 }, function() {
				 location.href = base_url('dashboard/deletarFoto/'+id);
			 });
	 });
 
 </script>
 

@stop