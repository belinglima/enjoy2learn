@extends('template.site')

@section('content')

<section class="showcase center">
  <div class="container">
    
    <div class="pb-2 mt-4 mb-2 border-bottom">
        <!-- <h2>Implement Calendar Library in CodeIgniter</h2> -->
    </div>

    <div id="my-calendar">
      <div class="text-center">

        <div class="preloader-wrapper active">
          <div class="spinner-layer spinner-red-only">
            <div class="circle-clipper left">
              <div class="circle"></div>
            </div><div class="gap-patch">
              <div class="circle"></div>
            </div><div class="circle-clipper right">
              <div class="circle"></div>
            </div>
          </div>
        </div>
        
      </div>
    </div>

  </div>
</section>    

@stop

@section('extra-javascript')

<script type="text/javascript" src="{{ base_url('assets/javascript/common.js?'.filemtime('assets/javascript/common.js')) }}"></script>

<script type="text/javascript">
  
</script>

@stop