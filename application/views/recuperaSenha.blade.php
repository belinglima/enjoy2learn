<!DOCTYPE html>
<html>
<head>
  
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

{{-- <meta name="theme-color" content="#1a237e"> --}}
<meta name="apple-mobile-web-app-capable" content="yes">  
{{-- <meta name="apple-mobile-web-app-status-bar-style" content="#1a237e">  --}}
<meta name="apple-mobile-web-app-title" content="Selflog style">
<meta name="description" content="Login do aluno enjoy2learn">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<link rel="shortcut icon" href="{{ base_url('assets/images/shortcut.svg') }}" type="image/x-icon">
<link rel="manifest" href="{{ base_url('manifest.json') }}" />
<link href="{{ base_url('assets/css/materialize.min.css') }}" rel="stylesheet" media="screen,projection"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<style>
  @import url('https://fonts.googleapis.com/css?family=Montserrat:400,800');
  .red{
    color: #FF4B2B;
  }

  .azul{
    color: #6372ff;
  }

  .button_vermelho {
    border-radius: 0 20px 20px;
    border: 1px solid #FF4B2B;
    background-color: #FF4B2B;
    color: #FFFFFF;
    font-size: 12px;
    font-weight: bold;
    padding: 12px 45px;
    letter-spacing: 1px;
    text-transform: uppercase;
    transition: transform 80ms ease-in;
    cursor: pointer;
  }

  .button_azul {
    border-radius: 0 20px 20px;
    border: 1px solid #6372ff;
    background-color: #6372ff;
    color: #FFFFFF;
    font-size: 12px;
    font-weight: bold;
    padding: 12px 45px;
    letter-spacing: 1px;
    text-transform: uppercase;
    transition: transform 80ms ease-in;
    cursor: pointer;
  }

  button {
    border-radius: 0 20px;
    border: 1px solid #6372ff;
    background-color: #6372ff;
    color: #FFFFFF;
    font-size: 12px;
    font-weight: bold;
    padding: 12px 45px;
    letter-spacing: 1px;
    text-transform: uppercase;
    transition: transform 80ms ease-in;
    cursor: pointer;
  }

  button:active {
    transform: scale(0.95);
  }

  button:focus {
    outline: none;
  }

  button.ghost {
    background-color: transparent;
    border-color: #FFFFFF;
  }

  form {
    background-color: #FFFFFF;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    padding: 0 50px;
    height: 100%;
    text-align: center;
  }

  .input_vermelho {
    border: 1px solid #FF4B2B;
    padding: 12px 15px;
    margin: 8px 0;
    width: 100%;
    color: #FF4B2B;
    outline: none;
    font-weight: bold;
  }

  .input_vermelho::-webkit-input-placeholder {
    color: #FF4B2B;
  }

  .input_azul {
    /* background: -webkit-linear-gradient(to right, #FF4B2B, #6372ff); */
    /* background: linear-gradient(to right, #FF4B2B, #6372ff); */
    border: 1px solid #6372ff;
    /* border-radius: 20px;  */
    padding: 12px 15px;
    margin: 8px 0;
    width: 100%;
    color: #6372ff;
    outline: none;
    font-weight: bold;
  }
  .input_azul::-webkit-input-placeholder {
    color: #6372ff;
  }

  .form-container {
    position: absolute;
    /*top: 5px;*/
    height: 100%;
    transition: all 0.6s ease-in-out;
  }

  .sign-in-container {
    width: 50%;
    z-index: 2;
  }

  .container.right-panel-active .sign-in-container {
    transform: translateX(100%);
  }

  .sign-up-container {
    left: 0;
    width: 50%;
    opacity: 0;
    z-index: 1;
  }

  /* controle da area de login e cadastrar */
  @media only screen and (max-width: 800px) {

    html {
        font-size: 20px;
    }

    .container {
      width: 100% !important;
    } 

    .sign-in-container {
      width: 100% !important;
    }

    .sign-up-container {
      width: 100% !important;
    } 

  }

  .container.right-panel-active .sign-up-container {
    transform: translateX(100%);
    opacity: 1;
    z-index: 5;
    animation: show 0.6s;
  }

  @keyframes show {
    0%, 49.99% {
      opacity: 0;
      z-index: 1;
    }
    
    50%, 100% {
      opacity: 1;
      z-index: 5;
    }
  }

  .overlay-container {
    position: absolute;
    top: 0;
    height: 100%;
    overflow: hidden;
    transition: transform 0.6s ease-in-out;
    z-index: 100;
  }

  .container.right-panel-active .overlay-container{
    transform: translateX(-100%);
  }

  .overlay {
    background: #6372ff;
    background: -webkit-linear-gradient(to right, #FF4B2B, #6372ff);
    background: linear-gradient(to right, #FF4B2B, #6372ff);
    background-repeat: no-repeat;
    background-size: cover;
    background-position: 0 0;
    color: #FFFFFF;
    position: relative;
    height: 100%;
    width: 200%;
      transform: translateX(0);
    transition: transform 0.6s ease-in-out;
  }

  .container.right-panel-active .overlay {
      transform: translateX(50%);
  }

  .overlay-panel {
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    padding: 0 40px;
    text-align: center;
    top: 0;
    height: 100%;
    width: 50%;
    transform: translateX(0);
    transition: transform 0.6s ease-in-out;
  }

  .overlay-left {
    transform: translateX(-20%);
  }

  .container.right-panel-active .overlay-left {
    transform: translateX(0);
  }

  .overlay-right {
    right: 0;
    transform: translateX(0);
  }

  .container.right-panel-active .overlay-right {
    transform: translateX(20%);
  }

  .social-container {
    margin: 20px 0;
  }

  .social-container a {
    border: 1px solid #DDDDDD;
    border-radius: 50%;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    margin: 0 5px;
    height: 40px;
    width: 40px;
  }

  .full-width {
    margin: 5px;
    width: 100%;
  }


  form .campo-termos{
  width: 100%;
  float: none;
}
form .campo-termos a{
  display: inline;
  color: #FF4B2B;
}
form .campo-termos a:hover{
  text-decoration: underline;
}
form .contorno-campo-radio{
  display: table;
  height: 48px;
  margin-bottom: 5px;
}
form .contorno-campo-radio + .contorno-campo-radio{
  margin-left: 35px;
}
form .contorno-campo-radio .bola-radio{
  width: 18px;
  height: 18px;
  float: left;
  position: relative;
  border: 1px solid #8C8C8C;
  border-radius: 100px;
  margin-top: 15px;
  cursor:pointer;
}
form .contorno-campo-radio .texto-radio{
  width: calc(100% - 18px);
  float: left;
  padding-left: 8px;
  font-weight: 400;
  color: #595959;
  font-size: 14px;
  line-height: 48px;
}
.campo-termos .texto-radio a{
    text-decoration: underline;
}
form .contorno-campo-radio:hover .bola-radio{
  border: 1px solid #FF4B2B;
}
form .contorno-campo-radio .bola-selecionar{
  width: 10px;
  height: 10px;
  position: absolute;
  left: 50%;
  top: 50%;
  margin-top: -5px;
  margin-left: -5px;
  background: #FF4B2B;
  border-radius: 100px;
  display: none;
}
form .contorno-campo-radio:hover .bola-selecionar{
  display: block;
}
@media (max-width:767px){
    .campo-termos .texto-radio{
        line-height:23px !important;
        padding-top:13px;
    }
}

.campo-termos .bola-selecionar, .campo-termos .bola-radio {
    border-radius: 0px !important;
}

</style>
</head>
<body>

 
  <div class="container" id="container">

    @include('template.show-error-success-redirect')

   
    <div class="form-container sign-in-container">

      <div class="social-container">
        <img class="responsive-img social" style="margin-top: 20px; margin-bottom: -160px;" src="{{ base_url('assets/images/logo.png') }}" alt="">
      </div>  

      <form>
        <p class="azul"><b>Recuperar a senha</b></p>
        <input class="input_azul browser-default" id="senha" type="password" placeholder="Insira a senha" autocomplete="off" required="required" />
        <input type="hidden" id="id" value="{{ $id }}" />
        <input type="hidden" id="email" value="{{ $email }}" />
        <input class="input_azul browser-default" id="repita-senha" type="password" placeholder="Repetir a senha" autocomplete="off" required="required" />

        <div class="button_azul full-width salvaSenha">Salvar</div>
        <br /><br />
      </form>

    </div>
   
  </div>

<script src="{{ base_url('assets/javascript/jquery-3.3.1.min.js') }}"></script>
<script src="{{ base_url('assets/javascript/materialize.min.js') }}"></script>
<script src="{{ base_url('assets/javascript/main.js') }}"></script>
<script>
  $(document).ready(function(){

    function base_url(caminho) {
		  return '{{ base_url() }}' + caminho;
	  }
    
    document.getElementById('senha').focus();
 
    $(".salvaSenha").on('click', function(){

        var id = ($("#id").length > 0) ? $("#id").val() : "" ;
        var email = ($("#email").length > 0) ? $("#email").val() : "" ;

        if (! email || ! id) {
          M.toast({html: 'Erro no envio das informações!'});

          return;
        }

        if ($("#senha").val() == "" || $("#repita-senha").val() == "") {
          M.toast({html: 'Preencha a nova senha e repita-a novamente!'});
          
          return;
        }

        if ($("#senha").val() != $("#repita-senha").val()) {
          M.toast({html: 'As senhas informadas devem ser iguais!'});
          
          return;
        }

        $(".progress").toggleClass("hide");
        url = base_url('forgot/confirmasenha');

        $.post(url, { email: email, senha: $("#senha").val(), id: id }, function(data){
          M.toast({html: data}, 5000);
          $(".progress").toggleClass("hide");
          
          setTimeout(function(){
            location.href = base_url('login');
          }, 4800);
        });

    });
})
</script>

</body>
</html>
