@extends('template.base')

@section('content')

<main>	
	
	<div class="container">
      	
      	<div class="row" style="border-bottom: 1px solid #DFDFDF">
			<div class="col s12 left-align">
				<div class="contorno-titulo-pagina">
					<h1>
						<strong>
							Avaliações
						</strong>
					</h1>
				</div>

				<div class="mapeamento-pagina">
		            <a href="{{ base_url('welcome') }}">Home</a> &gt; 
		            <a href="{{ base_url('certificates') }}">Certificados</a> &gt; 
		            <strong>Avaliação do curso</strong>
		        </div>
			</div>
		</div>

			<br>
		<div class="row">
	      	<div class="col s12">
		      	<div class="card-panel">

		      		<div class="col s12 center">
						<h5>Avaliação do curso - <b> {{ $curso['nome'] }} </b> </h5>
					</div>

		      		<br>

					<form name="cadastro" class="cadastro" action="{{ base_url('save_evaluation') }}" method="POST">
						<input type="hidden" name="hash" value="{{ $encryptor->encrypt($curso['id']) }}" >
						<div class="row center">
							<div class="col s12">								
								<p>* valem notas de 0 - 10, onde sendo 0 como muito ruim e 10 como muito satisfeito,
									<br> tambem lembramos que esta avaliação é feita sempre ao finalizar <br> um curso em nossa plataforma</p>
							</div>
						</div>

						<br><br>

						<div class="row">

			                <div class="form-input col s12 m4">
			                    <p for="conteudo" class="required">Conteudo apresentado!</p>
			                    <select name="conteudo" class="browser-default conteudo" required="required">
			                       <option value="0" selected="selected" disabled="disabled">Selecione</option>
			                       <option value="10">10</option>
			                       <option value="9">9</option>
			                       <option value="8">8</option>
			                       <option value="7">7</option>
			                       <option value="6">6</option>
			                       <option value="5">5</option>
			                       <option value="4">4</option>
			                       <option value="3">3</option>
			                       <option value="2">2</option>
			                       <option value="1">1</option>
			                    </select>
			                </div> 

			                <div class="form-input col s12 m4">
			                    <p for="audio" class="required">Audio do video!</p>
			                    <select name="audio" class="browser-default audio" required="required">
			                       <option value="0" selected="selected" disabled="disabled">Selecione</option>
			                       <option value="10">10</option>
			                       <option value="9">9</option>
			                       <option value="8">8</option>
			                       <option value="7">7</option>
			                       <option value="6">6</option>
			                       <option value="5">5</option>
			                       <option value="4">4</option>
			                       <option value="3">3</option>
			                       <option value="2">2</option>
			                       <option value="1">1</option>
			                    </select>
			                </div> 

			                <div class="form-input col s12 m4">
			                    <p for="video" class="required">Qualidade do video!</p>
			                    <select name="video" class="browser-default video" required="required">
			                       <option value="0" selected="selected" disabled="disabled">Selecione</option>
			                       <option value="10">10</option>
			                       <option value="9">9</option>
			                       <option value="8">8</option>
			                       <option value="7">7</option>
			                       <option value="6">6</option>
			                       <option value="5">5</option>
			                       <option value="4">4</option>
			                       <option value="3">3</option>
			                       <option value="2">2</option>
			                       <option value="1">1</option>
			                    </select>
			                </div> 

			                <div class="form-input col s12 m4">
			                    <p for="material" class="required">Material apresentado!</p>
			                    <select name="material" class="browser-default material" required="required">
			                       <option value="0" selected="selected" disabled="disabled">Selecione</option>
			                       <option value="10">10</option>
			                       <option value="9">9</option>
			                       <option value="8">8</option>
			                       <option value="7">7</option>
			                       <option value="6">6</option>
			                       <option value="5">5</option>
			                       <option value="4">4</option>
			                       <option value="3">3</option>
			                       <option value="2">2</option>
			                       <option value="1">1</option>
			                    </select>
			                </div> 

			                <div class="form-input col s12 m4">
			                    <p for="dinamica" class="required">Dinamica do curso!</p>
			                    <select name="dinamica" class="browser-default dinamica" required="required">
			                       <option value="0" selected="selected" disabled="disabled">Selecione</option>
			                       <option value="10">10</option>
			                       <option value="9">9</option>
			                       <option value="8">8</option>
			                       <option value="7">7</option>
			                       <option value="6">6</option>
			                       <option value="5">5</option>
			                       <option value="4">4</option>
			                       <option value="3">3</option>
			                       <option value="2">2</option>
			                       <option value="1">1</option>
			                    </select>
			                </div> 

			                <div class="form-input col s12 m4">
			                    <p for="dificuldade" class="required">Dificuldade do material?</p>
			                    <select name="dificuldade" class="browser-default dificuldade" required="required">
			                       <option value="0" selected="selected" disabled="disabled">Selecione</option>
			                       <option value="10">10</option>
			                       <option value="9">9</option>
			                       <option value="8">8</option>
			                       <option value="7">7</option>
			                       <option value="6">6</option>
			                       <option value="5">5</option>
			                       <option value="4">4</option>
			                       <option value="3">3</option>
			                       <option value="2">2</option>
			                       <option value="1">1</option>
			                    </select>
			                </div> 

			                <div class="form-input col s12 m6">
			                    <p for="professor" class="required">Avalie o professor!</p>
			                    <select name="professor" class="browser-default professor" required="required">
			                       <option value="0" selected="selected" disabled="disabled">Selecione</option>
			                       <option value="10">10</option>
			                       <option value="9">9</option>
			                       <option value="8">8</option>
			                       <option value="7">7</option>
			                       <option value="6">6</option>
			                       <option value="5">5</option>
			                       <option value="4">4</option>
			                       <option value="3">3</option>
			                       <option value="2">2</option>
			                       <option value="1">1</option>
			                    </select>
			                </div> 

			                <div class="form-input col s12 m6">
			                    <p for="nota" class="required">Nota para o curso em geral!</p>
			                    <select name="nota" class="browser-default nota" required="required">
			                       <option value="0" selected="selected" disabled="disabled">Selecione</option>
			                       <option value="10">10</option>
			                       <option value="9">9</option>
			                       <option value="8">8</option>
			                       <option value="7">7</option>
			                       <option value="6">6</option>
			                       <option value="5">5</option>
			                       <option value="4">4</option>
			                       <option value="3">3</option>
			                       <option value="2">2</option>
			                       <option value="1">1</option>
			                    </select>
			                </div> 

			                <div class="form-input col s12">
			                    <p for="obs" class="required">
			                    	Observações que gostaria de compartilhar conosco! 
			                    </p>
			                    <textarea id="texto" name="obs" class="materialize-textarea verifica" data-length="500" placeholder="Um breve texto" required></textarea>
			                </div>

			                <div class="col s12" style="margin-top: 20px;">
				              <div class="btn-large indigo darken-4 full-width btnEnvia">
				                <i class="material-icons white-text right">chevron_right</i> Avaliar Curso
				              </div>
				            </div>

			            </div>
		               
					</form>
				</div>
			</div>

		</div>
	</div>

</main>

@stop

@section('extra-javascript')

<script type="text/javascript">
	$('input#input_text, textarea#texto').characterCounter();

	$('.btnEnvia').on('click', function(){
		let conteudo = $('.conteudo').val();
		let audio = $('.audio').val();
		let video = $('.video').val();
		let material = $('.material').val();
		let dinamica = $('.dinamica').val();
		let dificuldade = $('.dificuldade').val();
		let professor = $('.professor').val();
		let nota = $('.nota').val();

		if(!conteudo || ! audio || ! video || !material || !dinamica || !dificuldade || !professor || ! nota){
			return false;
		}

		$("form[name='cadastro']").submit();
	})
</script>

@stop