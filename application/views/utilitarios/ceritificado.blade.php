<style type="text/css">
	main {
		display: none;
	}

	@media print {
		@page{
     size: landscape;
     margin: -10mm -10mm -10mm -10mm;
     -webkit-print-color-adjust: exact !important;
		  color-adjust: exact !important;   
    }

    main {
    	display: block !important;
    	-webkit-print-color-adjust: exact;
    }
	
		
	}

	.alunoNome {
		position: absolute;
		top: 45%;
		left: 29%;
		font-size: 40px;
		font-weight: 900;
		font-family: monospace;
		color: #1565c0;
	}

		.alunoEmail {
			position: absolute;
			top: 6%;
			right: 6%;
			font-size: 16px;
			font-family: monospace;
			color: #1565c0;
		}

		.cursoNome {
			position: absolute;
			top: 30%;
			left: 42%;
			font-size: 20px;
			font-weight: 900;
			font-family: monospace;
			color: #1565c0;
		}

		.cursoHash {
			position: absolute;
			bottom: 8%;
			right: 10%;
			font-size: 13px;
			font-weight: 200;
			font-family: monospace;
			color: #1565c0;
		}

		.cursoValidade {
			position: absolute;
			bottom: 6%;
			right: 10%;
			font-size: 13px;
			font-weight: 200;
			font-family: monospace;
			color: #1565c0;
		}

		img {
			height: 100%; 
			width: 100%; 
			-webkit-print-color-adjust: exact;
			margin: -10px;
			background-repeat: no-repeat; 
			background-size: 100% 100%;
			position: absolute; 
		}
</style>

<script type="text/javascript">
 	setTimeout(function(){
 		window.print();
 	}, 2000)
</script>


<main>	
	<img src="../assets/uploads/certificados/{{ $certificado['foto']  }}" />
	<p class="alunoNome">{{ $aluno->nome }}</p>
	<p class="alunoEmail">{{ $aluno->email }}</p>
	<p class="cursoNome">{{ $curso->nome }}</p>
	<p class="cursoHash">Certificado: <b> {{ hash('md5', $curso->id) }} </b></p>
	<p class="cursoValidade">Verificar certificado em www.enjoy2learnidiomas.com.br/verificar </p>
</main>

<p>Se o certificado não for gerado favor atualizar a pagina</p>
