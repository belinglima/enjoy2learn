@extends('template.site')

@section('content')

  <style type="text/css">
    input {
      font-family: 'Montserrat', sans-serif;
    }
  </style>

  <div class="parallax-container parallax-container-inside valign-wrapper">
    <div class="section" style="width: 100%;">
      <div class="container">
        <div class="row">
          <div class="col s12">
            <h1 class="header white-text">
            	Recuperar a senha
            </h1>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s12">
          <div class="card-panel">
            <div class="col s12">
              <div class="col s12">
                <label>E-mail</label>
                <input class="browser-default" id="recuperaEmail" name="recupera" type="text" placeholder="E-mail" autocomplete="off">
                <p>Por motivos de segurança sua senha é criptografada, não há como recupera-lá senão gerar um nova senha.</p>
                <p>Você receberá um e-mail contendo informações de como mudar sua senha.</p>
              </div>
              <div class="col s12">
                <div class="btn-large indigo darken-3" id="enviar" style="width: 100%; margin-top: 12px;">
                  Enviar
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col s12">
                <div class="progress loading hide">
                  <div class="indeterminate"></div>
                </div>
              </div>
            </div>
          </div>            
        </div>    
    </div>
  </div>
</div>

@stop

@section('extra-javascript')

<script type="text/javascript">

  function checkMail(mail) {
      var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
      if (typeof(mail) == "string") {
        if (er.test(mail)) {
          return true;
        }
      } else if(typeof(mail) == "object") {
        if (er.test(mail.value)) {
          return true;
        }
      } else {
        return false;
      }
    }

    $("#enviar").click(function(){

      var url ="<?=base_url()?>forgot/validaemail";

      if ($("#recuperaEmail").val() == "") {
        M.toast({html: 'Insira um e-mail'});
        return;
      }

      if (! checkMail($("#recuperaEmail").val())) {
        M.toast({html: 'Insira um e-mail válido!'});
        return;
      }

      $(".progress").toggleClass("hide");

      $.post(url, { email: $("#recuperaEmail").val() }, function(data){
        M.toast({html: data});
        $("#recuperaEmail").val("");
        $(".progress").toggleClass("hide");
      });

    });

    $("#recuperar").click(function(){
       $("#recuperaEmail").val($("#email").val());
    });

    @if (isset($msg) && !empty($msg))
      $(document).ready(function(){
        M.toast({html: '{{ $msg }}'});
      });
    @endif

    $('#recuperaEmail').on('input', function(){
      $(this).val($(this).val().toLowerCase());
    });
  
</script>

@stop