<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Fuso
{
	function __construct()
	{
		$this->ci = & get_instance();
	}

	public function hrAjustes($dataBanco)
	{   
		$fuso = $_SESSION["fusoSecao"];
	    // date_default_timezone_set('America/Sao_Paulo');
	    $horarioVerao = date('I', strtotime(date('Y-m-d H:i:s'))); 

	    // verifica o fuso da secao
	    if ($fuso == '-3') {
	        $remove = '-3';
	    }

	    // verifica o fuso da secao
	    if ($fuso == '-4') {
	        // se tiver horario de verao adicionao  mais 1 hora
	        if ($horarioVerao) {
	            $remove = '-2';
	        } else {
	            $remove = '-1';
	        }
	    }

	    // verifica o fuso da secao
	    if ($fuso == '-5') {
	        // se tiver horario de verao adicionao  mais 1 hora
	        if ($horarioVerao) {
	            $remove = '-3';
	        } else {
	            $remove = '-2';
	        }
	    }

	    return date('Y-m-d H:i:s', strtotime( $dataBanco. $remove.' hour'));
	}
}