<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class FotoAulas
{
	protected $ci;
    protected $config;

	function __construct()
	{
		$this->ci = & get_instance();
		$this->config['upload_path']     = FCPATH . 'assets/uploads/aulas';
        $this->config['allowed_types']   = 'gif|jpg|png|jpeg';
        $this->config['encrypt_name']    = TRUE;

        $this->ci->load->library('upload', $this->config);
	}

    public function do_upload($name)
    {
		return $this->ci->upload->do_upload($name);
	}

	public function data($indice=null)
	{
		if (!$indice) {
			return $this->ci->upload->data();
		}
		return $this->ci->upload->data($indice);
	}

    public function delete_img($foto)
    {
        if(file_exists($this->config['upload_path'].'/'.$foto)) {
            if (!unlink($this->config['upload_path'] . '/' . $foto)) {
                return false;
            }
        }
        return true;
    }

	public function display_errors()
	{
		return $this->ci->upload->display_errors();
	}

	public function do_resize() 
	{
        $this->config['image_library'] = 'gd2';
        $this->config['source_image'] = $this->data()["full_path"];
        $this->config['maintain_ratio'] = TRUE;
        $this->config['width'] = 600;
        $this->config['height'] = 600;

        $this->ci->load->library('image_lib', $this->config);

        if (!$this->ci->image_lib->resize()) {
            return false;
        }
	}
}
