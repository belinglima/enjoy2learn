<?php //if (!defined('BASEPATH')) exit('No direct script access allowed');

class CreateImage {
    
   // Matriz para criar o texto para imagem
   var $str    = "0123456789";
   var $width  = 68;//Largura da imagem
   var $height = 38;//Altura da imagem

   // Cores no formato hexadecimal
   var $hexcolors = array("#ffff00", "#000000", "#ff0000", "#ff00ff", "#808080", "#008000", "#00ff00", "#000080", "#800000", "#008080", "#800080", "#0000ff", "#c0c0c0", "#808000", "#00ffff");

   var $image;

   // Gera uma semente para ser utilizada pela função srand
   function make_seed() {
       list($usec, $sec) = explode(' ', microtime());
       return (float) $sec + ((float) $usec * 100000);
   }

   // Converte hexadecimal para rgb
   function hex2rgb($hex) {
       $hex = str_replace('#','',$hex);
       $rgb = array('r' => hexdec(substr($hex,0,2)),
                    'g' => hexdec(substr($hex,2,2)),
                    'b' => hexdec(substr($hex,4,2)));
       return $rgb;
   }

   // Aloca uma cor para imagem
   function color($value){
       $rgb = $this->hex2rgb($value);
       return imagecolorallocate($this->image, $rgb['r'], $rgb['g'], $rgb['b']);
   }

   // Cria uma linha em  posição e cor aleatória
   function randline(){
       imagesetthickness ($this->image, 2);
       imageline($this->image,rand(2, $this->width - 2), 2,rand(2, $this->width - 2), $this->height-2,$this->color("#ccccccc"));
   }

   // Cria uma imagem com texto aleatório e retorno o texto
  function output(){
    $ret="";
    $this->image = imagecreate($this->width,$this->height);
    $background = $this->color('#ffffff');
    imagefilledrectangle($this->image, 0, 0, $this->width -1 , $this->height -1, $background);
    imagerectangle($this->image , 0, 0 ,$this->width - 1, $this->height - 1, $this->color('#999999'));
    srand($this->make_seed());
    for ($k=0;$k < 3;$k++){
      $this->randline();
    }

    $font = __DIR__.'/arial.ttf';

    for ($i=0; $i < 5; $i++) {
      $this->str = str_shuffle($this->str);
      $char = $this->str[0];
      $ret .= $char;
      imagettftext($this->image, 14 , 0, ($i == 0 ? 14 : ($i+2) *9+1)  - 5 , rand(25,25), $this->color("#000000"), $font, $char);
    }

    imagepng($this->image);
    imagedestroy($this->image);

    return $ret;
  }
}
