<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Library extends CI_Controller 
{
    protected $aluno_id;

	public function __construct() {
        parent::__construct();

        $this->load->model('Vendas_Model');
        $this->load->model('Carrinho_Model');
        $this->load->model('Cursos_Model');
        // $this->load->model('Cielo_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();

        if (! get_user_session()) {
            return $this->redirect->url('login/logout')
                ->withSuccess('Necessário estar logado, para entrar no sistema.')
                ->send();
        }

        $this->aluno_id = get_user_session();
    }

    // public function index()
    // {
    //     $info['vendas'] = $this->Vendas_Model->allFrom($this->cliente_id);
    //     $this->template->view('portal.biblioteca', $info);
    // }

	public function insert($codigo = '')
    {
        if (! $codigo) {
            return $this->redirect->url('library')
                ->withError('Erro na compra do curso')
                ->send();
        }

        $decrypt = new Encryptor();
        $id = $decrypt->decrypt($codigo);

        $curso = $this->Cursos_Model->find_by_code($id)->row_array();

        if (! $curso) {
            return $this->redirect->url('library')
                ->withError('Curso não encontrado!')
                ->send();
        }

        $carrinho = $this->Carrinho_Model->getVendasAguardando();

        // CRIA UM CARRINHO //
        if ($carrinho->num_rows() == 0) {
            $dados = [
                'aluno_id' => $this->aluno_id,
                'data' => date('Y-m-d'),
                'total' => $curso['preco'],
                'codigo' => substr(str_shuffle(MD5(microtime())), 0, 10)
            ];

            if (! $venda_id = $this->Vendas_Model->save($dados)) {
                return $this->redirect->url('portal/biblioteca')
                    ->withError('Erro na compra, tente mais tarde!')
                    ->send();
            }

            $this->Carrinho_Model->insereAtividade($venda_id, $curso);


            return $this->redirect->url('library')
                ->withSuccess('Curso adicionado no carrinho!')
                ->send();
        }


        // INSERE ATIVIDADE NO CARRINHO //
        if ($carrinho->num_rows() > 0) {
            $venda_id = $carrinho->row()->id_venda;
            $novoTotal = ($carrinho->row()->total + $curso['preco']);

            if ($this->Carrinho_Model->verificaExistencia($venda_id, $curso['id']) == 0) {
                if ($this->Carrinho_Model->insereAtividade($venda_id, $curso)) {
                    $this->Carrinho_Model->atualizaTotal($venda_id, $novoTotal);
                }
            } else {
                return $this->redirect->url('library')
                    ->withError('Esse curso já está em seu carrinho!')
                    ->send();
            }

            return $this->redirect->url('library')
                ->withSuccess('Curso adicionado ao carrinho')
                ->send();
        }
    }

    // Remove atividade do Pedido //
    // public function remover($id, $codigo)
    // {
    //     $id = base64_decode($id);
        
    //     if (! $id) {
    //         return $this->redirect->url('portal/biblioteca')
    //             ->withError('Atividade não encontrada!')
    //             ->send();
    //     }

    //     $venda = $this->Carrinho_Model->getVendaByPdfId($id)->row();
    //     $novoTotal = ($venda->total - $venda->valor);

    //     $this->Carrinho_Model->removeAtividade($id);
    //     $this->Carrinho_Model->atualizaTotal($venda->id_venda, $novoTotal);

    //     return $this->redirect->url('portal/pagamento/'.$codigo)
    //         ->withSuccess('Atividade removida!')
    //         ->send();
    // }

    // // Deleta Pedido
    // public function delete($codigo)
    // {
    //     $id = $this->Carrinho_Model->getIdByCodigo($codigo);
        
    //     if (! $id) {
    //         return $this->redirect->url('portal/biblioteca')
    //             ->withError('Pedido não encontrado!')
    //             ->send();
    //     }

    //     if (! $this->Carrinho_Model->delete($id)) {
    //         return $this->redirect->url('portal/biblioteca/')
    //             ->withError('Erro na remoção do pedido!')
    //             ->send();
    //     }
        
    //     return $this->redirect->url('portal/biblioteca/')
    //         ->withSuccess('Pedido removido!')
    //         ->send();
    // }

    // public function pagamento($codigo)
    // {
    //     $id_venda = $this->Carrinho_Model->getIdByCodigo($codigo);

    //     if (! $id_venda) {
    //         return $this->redirect->url('portal/biblioteca')
    //             ->withError('Compra não encontrada!')
    //             ->send();
    //     }

    //     $this->template->view('portal.pagamento', 
    //         array(
    //             'venda' => $this->Vendas_Model->find($id_venda)->row_array(),
    //             'pdfs' => $this->Carrinho_Model->getPdfsVenda($id_venda),
    //             'cielo' => $this->Cielo_Model->find($id_venda)->row_array()
    //         )
    //     );
    // }

    // // Compensa pagamentos com valor R$ 0,00 => Grátis
    // public function buyFree($codigo)
    // {
    //     $id = $this->Carrinho_Model->getIdByCodigo($codigo);

    //     if (! $codigo) {
    //         return $this->redirect->url('portal/pagamento/'.$codigo)
    //             ->withError('* Erro nos dados passados!')
    //             ->send();
    //     }

    //     $venda = $this->Vendas_Model->getVenda($id)->row();

    //     if ($venda->total > 0) {
    //         return $this->redirect->url('portal/pagamento/'.$codigo)
    //             ->withError('* Venda não autorizada, valor indevido!')
    //             ->send();
    //     }

    //     if ($this->Cielo_Model->find($id)->num_rows() == 0) 
    //     {
    //         $cielo['venda_id'] = $id;
    //         $cielo['payment_status'] = 2;
    //         $cielo['product_id'] = 'VENDA GRATIS';
    //         $cielo['data'] = date('Y-m-d');

    //         if ($this->Cielo_Model->save($cielo)) {
    //             $this->Carrinho_Model->updateStatusVenda($id, 'EFETIVADA');
    //             return $this->redirect->url('portal/atividades/')
    //                 ->withSuccess('Pedido liberado!')
    //                 ->send();
    //         }
    //     }

    //     return $this->redirect->url('portal/pagamento/'.$codigo)
    //         ->withError('* Venda não autorizada!')
    //         ->send();
    // }

    // // public function cielo()
    // // {
    // //     $this->load->library('Cielo');
    // //     $codigo = $this->input->post('codigo');
    // //     $id_venda = base64_decode($codigo);

    // //     $VENDA = $this->Vendas_Model->getVenda($id_venda)->row();

    // //     if (! $VENDA) {
    // //         return $this->redirect->url('portal/biblioteca/')
    // //             ->withError('Erro no envio dos dados!')
    // //             ->send();
    // //     }

    // //     $obj = new StdClass;
    // //     $obj->Type = "Digital";
    // //     $obj->name = "Pedido #".str_pad($VENDA->id_venda , 5 , '0' , STR_PAD_LEFT);
    // //     $obj->description = "Pedido #".str_pad($VENDA->id_venda , 5 , '0' , STR_PAD_LEFT);
    // //     $obj->price = (int) $VENDA->total * 100;
    // //     $obj->maxNumberOfInstallments = 1;

    // //     $obj->shipping = new StdClass;
    // //     $obj->shipping->type = "WithoutShipping";

    // //     $obj->SoftDescriptor = "CBELEM".str_pad($VENDA->id_venda , 5 , '0' , STR_PAD_LEFT);

    // //     $this->cielo->setToken();
    // //     $requestCielo = $this->cielo->createRequestCielo($obj);

    // //     if (isset($requestCielo->shortUrl)) 
    // //     {
    // //         $cielo = $this->Cielo_Model->find($VENDA->id_venda);

    // //         $saveOrder = new StdClass;
    // //         $saveOrder->venda_id = $VENDA->id_venda;
    // //         $saveOrder->product_id = $requestCielo->id;
    // //         $saveOrder->data = date('Y-m-d');

    // //         if ($cielo->num_rows() > 0) {
    // //             $this->Cielo_Model->updateByIdVenda($VENDA->id_venda, $saveOrder);
    // //         } else {
    // //             $this->Cielo_Model->save($saveOrder);
    // //         }

    // //         $this->Carrinho_Model->updateStatusVenda($VENDA->id_venda, 'PENDENTE');

    // //         header("Location: ".$requestCielo->shortUrl);
    // //         return;
    // //     }
    // // }

    // public function paymentCielo()
    // {
    //     $this->load->library('Cielo');
    //     $this->load->model('Clientes_Model');

    //     $scape = array('-', '.', '(', ')', ' ');

    //     $codigo = $this->input->post('codigo');
    //     $id_venda = base64_decode($codigo);

    //     $VENDA = $this->Vendas_Model->getVenda($id_venda)->row();

    //     if (! $VENDA) {
    //         return $this->redirect->url('portal/biblioteca/')
    //             ->withError('Erro no envio dos dados!')
    //             ->send();
    //     }

    //     $CLI = $this->Clientes_Model->find($this->cliente_id)->row();

    //     $obj = new StdClass;
    //     $obj->OrderNumber = $VENDA->id_venda;
    //     $obj->SoftDescriptor = "CBELEM".str_pad($VENDA->id_venda , 5 , '0' , STR_PAD_LEFT);

    //     $obj->Cart = new StdClass;

    //     $obj->Cart->Discount = new StdClass;

    //     $obj->Cart->Items = [];

    //     $obj->Cart->Items[0] = new StdClass;

    //     $obj->Cart->Items[0]->Name = "Pedido #".str_pad($VENDA->id_venda , 5 , '0' , STR_PAD_LEFT);
    //     $obj->Cart->Items[0]->Description = "Pedido #".str_pad($VENDA->id_venda , 5 , '0' , STR_PAD_LEFT);
    //     $obj->Cart->Items[0]->UnitPrice = (int) $VENDA->total * 100;
    //     $obj->Cart->Items[0]->Quantity = 1;
    //     $obj->Cart->Items[0]->Type = "Digital";

    //     $obj->Shipping = new StdClass;
    //     $obj->Shipping->Type = "WithoutShipping";
    //     // $obj->Shipping->targetZipCode = str_replace($scape, "", $CLI->cep);

    //     // $obj->Shipping->Address = new StdClass;
    //     // $obj->Shipping->Address->Street = $CLI->endereco;
    //     // $obj->Shipping->Address->Number = $CLI->numero;
    //     // $obj->Shipping->Address->Complement = "";
    //     // $obj->Shipping->Address->District = $CLI->bairro;
    //     // $obj->Shipping->Address->City = $CLI->cidade;
    //     // $obj->Shipping->Address->State = $CLI->estado;

    //     $obj->Payment = new StdClass;

    //     $obj->Customer = new StdClass;
    //     $obj->Customer->Identity = str_replace($scape, "", $CLI->cpf);
    //     $obj->Customer->FullName = $CLI->nome;
    //     $obj->Customer->Email = $CLI->email;
    //     $obj->Customer->Phone = str_replace($scape, "", $CLI->telefone);

    //     $obj->Options = new StdClass;
    //     $obj->Settings = null;

    //     $requestCielo = $this->cielo->createPaymentRequest($obj);

    //     if (isset($requestCielo->settings->checkoutUrl)) 
    //     {
    //         $cielo = $this->Cielo_Model->find($VENDA->id_venda);

    //         $saveOrder = new StdClass;
    //         $saveOrder->venda_id = $VENDA->id_venda;
    //         $saveOrder->order_number = $requestCielo->orderNumber;
    //         $saveOrder->data = date('Y-m-d');

    //         if ($cielo->num_rows() > 0) {
    //             $this->Cielo_Model->updateByIdVenda($VENDA->id_venda, $saveOrder);
    //         } else {
    //             $this->Cielo_Model->save($saveOrder);
    //         }

    //         $this->Carrinho_Model->updateStatusVenda($VENDA->id_venda, 'PENDENTE');

    //         header("Location: ".$requestCielo->settings->checkoutUrl);
    //         return;
    //     }
    // }
}