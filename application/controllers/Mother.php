<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mother extends CI_Controller 
{
    public function __construct() {
        parent::__construct();

        $this->load->model('Main_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();

        if (! get_user_session()) {
            return $this->redirect->url('login/logout')
                ->withSuccess('Necessário estar logado, para entrar no sistema.')
                ->send();
        }

    }

    public function deleteNotificacao()
    {
        $this->Main_Model->deleteNotificacao();
    }
    
}