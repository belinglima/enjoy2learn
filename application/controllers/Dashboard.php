<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('Encryptor');
        $this->load->library('UploadFiles/FotoPerfil', '', 'Foto');
        $this->load->model('Contato_Model');
        $this->load->model('Cursos_Model');
        $this->load->model('Perfil_Model');
        $this->load->model('sistema/Usuarios_Model');
        $this->load->model('Vendas_Model');
        $this->load->model('Notificacao_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();

        if (! get_user_session()) {
            return $this->redirect->url('login/logout')
                ->withError('Necessário estar logado, para entrar no sistema.')
                ->send();
        }

    }

    public function contato()
    {  
        $this->template->view('dashboard.contato');
    }

    public function cursos($num)
    {
        $info['encryptor'] = new Encryptor();
        $info['cursos'] = $this->Cursos_Model->all($num)->result_array();
        $info['carrinho'] = $this->Cursos_Model->getVendasAguardando()->result_array();

        $this->template->view('dashboard.cursos', $info);
    }

    public function show($id)
    {
        $decrypt = new Encryptor();
        $id = $decrypt->decrypt($id);

        $info['encryptor'] = new Encryptor();

        $info['curso'] = $this->Cursos_Model->find($id)->row();
        $info['aulas'] = $this->Cursos_Model->aulasCurso($id)->result_array();
        $info['totalAulas'] = $this->Cursos_Model->aulasCurso($id)->num_rows();
        $info['carrinho'] = $this->Cursos_Model->getVendasAguardando()->result_array();
        
        $this->template->view('dashboard.detalhesCurso', $info);
    }

    public function professor()
    {
        $this->template->view('dashboard.professor');
    }

    public function metodologia()
    {
        $this->template->view('dashboard.metodologia');
    }

    public function entrada()
    {
        $info['alerta'] = $this->Perfil_Model->findUser()->row_array();
        $info['cursos'] = $this->Vendas_Model->cursos()->num_rows();
        $info['aulas'] = $this->Vendas_Model->aulas()->num_rows();

        $this->template->view('dashboard.bemvindo', $info);
    }

    public function perfil()
    {   
        $info['encryptor'] = new Encryptor();
        $info['usuario'] = $this->Perfil_Model->findUser()->row_array();;
        $info['estados'] = $this->Perfil_Model->estadosAll()->result_array();

        $this->template->view('dashboard.perfil', $info);
    }

    public function meusCursos()
    {
        $info['encryptor'] = new Encryptor();
        $info['cursos'] = $this->Vendas_Model->allFrom()->result_array();

        $this->template->view('dashboard.biblioteca', $info); 
    }

    public function seleciona()
    {   
        $this->load->library('user_agent');
        $info['mobile'] = $this->agent->is_mobile();
        
        $this->template->view('selectDisplay', $info);
    }

    public function atualizaCarrinho()
    {
        echo $total = $this->Cursos_Model->getVendasAguardando()->num_rows();
    }

    public function getNotification()
    {
        $encryptor = new Encryptor();
        $notificacoes = $this->Notificacao_Model->all()->result_array();

        $mostrar = [];
        if (!empty($notificacoes)) {
                for ($i=0; $i < 1; $i++) { 
                    if ($i == 1 && count($notificacoes) > 2){
                        array_push($mostrar, 
                            '<li class="notificacao nao-lida">
                                <a href="/enjoy2learn/popup-notificacoes/'.$encryptor->encrypt($notificacoes[$i]["id"]).'">
                                    <span>
                                        '.format_datetime_to_show($notificacoes[$i]["data"]).'<br />
                                        '.$notificacoes[$i]["titulo"].'
                                    </span>
                                    <p>
                                        '.$notificacoes[$i]["texto"].'
                                    </p>
                                  </a>
                            </li>
                            <a href="'.base_url("notificacoes").'" class="btt-notificacoes-lista">Ver Todas</a>
                        '); 
                    } else {
                         array_push($mostrar, 
                            '<li class="notificacao nao-lida">
                                <a href="/enjoy2learn/popup-notificacoes/'.$encryptor->encrypt($notificacoes[$i]["id"]).'">
                                    <span>
                                        '.format_datetime_to_show($notificacoes[$i]["data"]).'<br />
                                        '.$notificacoes[$i]["titulo"].'
                                    </span>
                                    <p>
                                        '.$notificacoes[$i]["texto"].'
                                    </p>
                                  </a>
                            </li>
                        '); 
                    }
                }

        } else {   
            array_push($mostrar, 
                '<li class="notificacao nao-lida">
                    <a href="#!">
                        <span>
                            '.format_datetime_to_show(date("Y-m-d H:i:s")).'<br />
                            Aguardando...
                        </span>
                        <p>
                            <br>
                                Você ainda não
                            <br>
                                recebeu nunhuma notificação!
                        </p>
                      </a>
                </li>
            '); 
        }
        echo json_encode($mostrar);
    }


    public function editarFoto()
    {   
       $info['encryptor'] = new Encryptor();
       $info['usuario'] = $this->Usuarios_Model->find(get_user_session())->row_array();
    
       $this->template->view('dashboard.editarFoto', $info);
    }

    public function alterarFoto()
    {   
        $data = $this->input->post();

        // Upload da foto peril
            if (isset($_FILES['foto']['name']) && 
            ! empty($_FILES['foto']['name'])) {
            
            if ($this->Foto->do_upload('foto')) {
                $this->Foto->do_resize();
                $data['foto'] = $this->Foto->data('file_name');
            } else {
                echo $this->Foto->display_errors();
            }
        } else {
            $data['foto'] = NULL;
        }
        
        if (! $this->Usuarios_Model->updatePerfil($data)) {
            return $this->redirect->url('dashboard/editarFoto')
                ->withError('Erro na alteração!')
                ->send();
        }

        $_SESSION['foto'] = $data['foto'];

        return $this->redirect->url('profile')
            ->withSuccess('Foto alterada!')
            ->send();
    }

    public function deletarFoto()
    {
        $usuario = $this->Usuarios_Model->findUser()->row_array();

        if ($this->Usuarios_Model->deletarFoto()) {

            $this->Foto->delete_img($usuario['foto']);

        } else {
             return $this->redirect->url('dashboard/editarFoto')
                ->withError('Erro ao excluir a foto!')
                ->send();
        }
        
        $_SESSION['foto'] = NULL;

        return $this->redirect->url('dashboard/editarFoto')
                ->withSuccess('Foto excluída!')
                ->send();
    }

    public function alterarDados()
    {   
        $data = $this->input->post();

        $cpf = preg_replace('/[^0-9]/', '', $data['cpf']);
        $data['cpf'] = $cpf; 

        $cep = preg_replace('/[^0-9]/', '', $data['cep']);
        $data['cep'] = $cep;

        if (! $data ) {
            return $this->redirect->url('perfil')
                ->withError('Erro nos dados passados!')
                ->send();
        }
        
        if (! $this->Usuarios_Model->updatePerfil($data)) {
            return $this->redirect->url('perfil')
                ->withError('Erro na alteração!')
                ->send();
        }

        return $this->redirect->url('welcome')
            ->withSuccess('Dados alterados!')
            ->send(); 
    }

    public function editarSenha()
    {   
       $info['encryptor'] = new Encryptor();
       $info['usuario'] = $this->Usuarios_Model->find(get_user_session())->row_array();
    
       $this->template->view('dashboard.editarSenha', $info);
    }

    public function alterarSenha()
    {   
        $data = $this->input->post();

        if (! $data ) {
            return $this->redirect->url('dashboard/editarSenha')
                ->withError('Erro nos dados passados!')
                ->send();
        }
        
        $this->load->library('bcrypt');

        if (! empty($data['hash_senha'])) {
            $data['hash_senha'] = $this->bcrypt->hash($data['hash_senha']);
        } else {
            unset($data['hash_senha']);
        }

       if (! $this->Perfil_Model->update($data)) {
            return $this->redirect->url('dashboard/editarSenha')
                ->withError('Erro na alteração!')
                ->send();
        }

        return $this->redirect->url('profile')
            ->withSuccess('Senha alterada!')
            ->send();
    }

    public function getCidadeByEstadoSigla($sigla, $cidade = "")
    {   
        $municipios = $this->Perfil_Model->getMunicipios($sigla)->result_array();
        
        $dados = '<option value="false" selected>Escolha uma cidade</option>';
        foreach ($municipios as $municipio) {
            if($municipio['nome'] == $cidade) {
                $dados .= "<option value='" . $municipio['nome'] . "' selected>" . $municipio['nome'] . "</option>";
            } else {
                $dados .= "<option value='" . $municipio['nome'] . "'>" . $municipio['nome'] . "</option>";
            }
        }

        echo $dados;
    }

    public function insert()
    {
        $data = $this->input->post();

        // caso não venha dados do formulario retorna um erro para o site.
        if (! $data) {
            return $this->redirect->url('contact')
                ->withError('Nenhum dado foi passado!')
                ->send();
        }

        $dados['nome'] = $data['nome'];
        $remover = array("(",")"," ","-");
        $telefone = str_replace($remover, "", $data['fone']);
        $data['fone'] = $telefone;
        $dados['fone'] = $data['fone'];
        $dados['email'] = $data['email'];
        $dados['msg'] = $data['msg'];

        if ($dados['id'] = $this->Contato_Model->save($data)) {

            // envio de email após o cadastro no banco de dados
            $this->load->library('SendEmail');
            $body = $this->load->view('emails/contato', $dados, true);
            
            $this->sendemail->enviar(
                $data['email'],
                'E-mail de contato ', 
                $body
            );

            return $this->redirect->url('welcome')
                ->withSuccess('Contato enviado')
                ->send();            
        }
    }

    public function search()
    {   
        $data = $this->input->post();
        $info['encryptor'] = new Encryptor();
        $info['cursos'] = $this->Cursos_Model->search($data['FrmBusca'])->result_array();
        $info['carrinho'] = $this->Cursos_Model->getVendasAguardando()->result_array();
        $info['busca'] = $data['FrmBusca'];

        $this->template->view('dashboard.cursos', $info); 
    }  

    public function certificados()
    {
        $info['encryptor'] = new Encryptor();
        $info['cursos'] = $this->Perfil_Model->getCursos()->result_array();
        $info['avaliados'] = $this->Cursos_Model->getAvaliados()->result_array();

        $this->template->view('dashboard.certificados', $info); 
    }  

    public function compras()
    {
        $info['encryptor'] = new Encryptor();
        $info['compras'] = $this->Vendas_Model->all()->result_array();
        $info['movimentos'] = $this->Vendas_Model->allMovimentos()->result_array();

        $this->template->view('dashboard.historico', $info); 
    }  

    public function duvidas($num)
    {
        $info['duvidas'] = $this->Cursos_Model->duvidas($num)->result_array();
        $this->template->view('dashboard.duvidas', $info); 
    }

    public function notificacoes($num)
    {   
        $decrypt = new Encryptor();
        $num = $decrypt->decrypt($num);
        $url = $this->Notificacao_Model->pegaUrlNotificacao($num);
        if($url){
            echo "<script>window.open('".$url."', '_blank');</script>";

            echo "Aguarde.. você esta sendo redirecionado..";
            header("refresh: 3; http://localhost/enjoy2learn/welcome");
            return true;
        }

        return $this->redirect->url('welcome')
            ->withError('Notificação sem url cadastrado no momento!')
            ->send();
    }  

    public function avaliar($idCurso)
    {
        $decrypt = new Encryptor();
        $id = $decrypt->decrypt($idCurso); 

        if (! $id) {
            return $this->redirect->url('certificates')
                ->withError('Necessário selecionar curso antes!')
                ->send();
        }

        if($this->Cursos_Model->verificaAvalicao($id) == 'true'){
            return $this->redirect->url('certificates')
                ->withError('Curso ja foi avaliado, termine outro para avaliar novamente!')
                ->send();   
        }
        
        $this->template->view(
            'utilitarios.avaliar', 
            array(
                'curso' => $this->Cursos_Model->find($id)->row_array(),
                'encryptor' => new Encryptor()
            )
        ); 
    }

    public function saveEvaluation()
    {
        $data = $this->input->post();

        if (! $data) {
            return $this->redirect->url('certificates')
                ->withError('Necessário selecionar curso antes!')
                ->send();
        }

        $decrypt = new Encryptor();
        $data['hash'] = $decrypt->decrypt($data['hash']);

        if(!$this->Cursos_Model->saveEvaluation($data)){
             return $this->redirect->url('certificates')
                ->withError('Erro ao salvar avaliação, tente novamente!')
                ->send();
        }

        return $this->redirect->url('certificates')
            ->withSuccess('Avaliação cadastrada com sucesso, obrigado!')
            ->send();        
    }

    public function getcertificate($idCurso)
    {
        $decrypt = new Encryptor();
        $id = $decrypt->decrypt($idCurso); 

        if (! $id) {
            return $this->redirect->url('certificates')
                ->withError('Necessário selecionar curso antes!')
                ->send();
        }

        $this->template->view(
            'utilitarios.ceritificado', 
            array(
                'certificado' => $this->Cursos_Model->getCertificado($id)->row_array(),
                'aluno' => $this->Perfil_Model->findUser()->row(),
                'curso' => $this->Cursos_Model->find_by_code($id)->row()
            )
        ); 
    }
}