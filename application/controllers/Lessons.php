<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lessons extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Cursos_Model');

        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();
    }

    public function player($idCurso = '', $idAula = '', $url = '')
    {
    	$decrypt = new Encryptor();
        $idCurso = $decrypt->decrypt($idCurso); 

        if ($idAula) {
	        $idAula = $decrypt->decrypt($idAula); 
	        $this->Cursos_Model->saveVisualizacao($idAula, $idCurso);
        }

        if (! $idCurso) {
            return $this->redirect->url('classes')
                ->withError('Necessário selecionar curso antes!')
                ->send();
        }

        $info['encryptor'] = new Encryptor();
        $info['url'] = $url; 

    	$info['curso'] = $this->Cursos_Model->find_by_code($idCurso)->row();
    	$info['materiais'] = $this->Cursos_Model->getMaterialApoio($idCurso, $idAula)->result_array();
    	$info['aulas'] = $this->Cursos_Model->getAulas($idCurso)->result_array();
        
        $this->template->view('lessons.player',$info);
    }
    
}