<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('sistema/Usuarios_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();
    }

    public function index()
    {
        $this->template->view('forgot.index');
    }

    public function validaemail()
    {
        $email = $this->input->post('email');

        echo "$email";

        if (! $email) {
            echo "Nenhum e-mail informado!";
            return;
        }

        $usuario = $this->Usuarios_Model->getUsuarioPorEmail($email);

        if (empty($usuario)) {
            echo " Este e-mail não consta em nossa base!";
            return;
        }

        $this->load->library('SendEmail');

        $endereco = base_url('forgot/recuperaSenha/').base64_encode($email)."/".base64_encode($usuario['id']);

        $view = $this->template->view('emails/recuperaSenha', array('nome' => $usuario['nome'], 'endereco' => $endereco), true);

        if ($this->sendemail->enviar($email, "Esqueceu sua senha?", $view)) {
            echo " Enviamos instruções para o seu e-mail!";
        } else {
            echo " Erro no envio de e-mail, contate o suporte!";
        }

        return;
    }

    public function recuperasenha($email="", $id="") 
    {
        if (!$email || !$id) {
            header("Location: ".base_url());
            return;
        }

        $info['email'] = $email;
        $info['id'] = $id;

        $this->template->view('recuperaSenha', $info);
    }

    public function confirmaSenha()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        header("Access-Control-Allow-Headers: X-Requested-With");
        
        $this->load->library('bcrypt');

        $id = $this->input->post('id');
        $email = $this->input->post('email');
        $senha = $this->input->post('senha');

        if (! $id || ! $email || ! $senha) {
            header("Location: ".base_url());
            return;
        }

        $id = base64_decode($id);
        $email = base64_decode($email);
        $hashSenha = $this->bcrypt->hash($senha);
        
        $usuario = $this->Usuarios_Model->getUsuarioPorEmail($email);

        if (! $this->Usuarios_Model->updateSenha($id, $email, $hashSenha)) {
            echo "Erro na alteração de senha, contate o suporte!";
            return;
        }
        
        $this->load->library('SendEmail');
        $view = $this->template->view('emails/alteracaoSenha', array('nome' => $usuario['nome']), true);
        $this->sendemail->enviar($email, "Senha alterada!", $view);
        
        echo "Senha modificada com sucesso! aguarde...";
        return;
    }
}