<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller 
{
	public function __construct()
    { 
        parent::__construct();
        $this->load->model('Cursos_Model');
		$this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();
    }

	public function index()
	{
		$this->template->view(
			'site.homepage',
			array(
				'cursos' => $this->Cursos_Model->all(0)->result_array()
			)
		);
	}

	public function appointment()
	{
		 $data = array();
        $data['metaDescription'] = 'Calendar';
        $data['metaKeywords'] = 'Calendar';
        $data['title'] = "Calendar - Techarise";
        $data['breadcrumbs'] = array('Calendar' => '#');
        $this->template->view('calendar.index', $data);	
	}

	 // getCalendar method
    public function getCalendar() { 
            
            $data = array();
            $curentDate = date('Y-m-d', time());
            if ($this->input->post('page') !== null) {
                $malestr = str_replace("?", "", $this->input->post('page'));
                $navigation = explode('/', $malestr);
                $getYear = $navigation[1];
                $getMonth = $navigation[2];
            } else {
                $getYear = date('Y');
                $getMonth = date('m');
            }
            if ($this->input->post('year') !== null) {
                $getYear = $this->input->post('year');
            }
            if ($this->input->post('month') !== null) {
                $getMonth = $this->input->post('month'); 
            }

            $already_selected_value = $getYear;
            $earliest_year = 1950;
            // $startYear = '';
            $googleEventArr = array();
            $calendarData = array();

            $class = 'href="javascript:void(0);" data-days="{day}"';

        //     $startYear .= '<div class="col s6"><div class="select-control"><select name="year" id="setYearVal" class="browser-default">';
        // foreach (range
        //         (date
        //                 ('Y') + 50, $earliest_year) as $x) {
        //     $startYear .= '<option value="' . $x . '"' . ($x == $already_selected_value ? ' selected="selected"' : '') . '>' . $x . '</option>';
        // }
        // $startYear .= '</select></div></div>';
        $startMonth = '<div class="col s12"><div class="select-control">
        	<select name="mont h" id="setMonthVal" class="browser-default center-align"><option value="0" disabled>Selecione o mês</option>
            <option value="01" ' . ('01' == $getMonth ? ' selected="selected"' : '') . '>Janeiro</option>
            <option value="02" ' . ('02' == $getMonth ? ' selected="selected"' : '') . '>Fevereiro</option>
            <option value="03" ' . ('03' == $getMonth ? ' selected="selected"' : '') . '>Março</option>
            <option value="04" ' . ('04' == $getMonth ? ' selected="selected"' : '') . '>Abril</option>
            <option value="05" ' . ('05' == $getMonth ? ' selected="selected"' : '') . '>Maio</option>
            <option value="06" ' . ('06' == $getMonth ? ' selected="selected"' : '') . '>Junho</option>
            <option value="07" ' . ('07' == $getMonth ? ' selected="selected"' : '') . '>Julho</option>
            <option value="08" ' . ('08' == $getMonth ? ' selected="selected"' : '') . '>Agosto</option>
            <option value="09" ' . ('09' == $getMonth ? ' selected="selected"' : '') . '>Setembro</option>
            <option value="10" ' . ('10' == $getMonth ? ' selected="selected"' : '') . '>Outubro</option>
            <option value="11" ' . ('11' == $getMonth ? ' selected="selected"' : '') . '>Novembro</option>
            <option value="12" ' . ('12' == $getMonth ? ' selected="selected"' : '') . '>Dezembro</option>
    </select></div></div>';
    
        $prefs['template'] = '        

        {table_open}<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" class="calendar">{/table_open}

        {heading_row_start}<tr style="border:none;">{/heading_row_start}

        {heading_previous_cell}<th style="border:none;" class="padB"><a class="calnav" data-calvalue="{previous_url}" href="javascript:void(0);"><i class="material-icons">chevron_left</i></a></th>{/heading_previous_cell}
        {heading_title_cell}<th style="border:none;" colspan="{colspan}"><div class="row">' . $startMonth . '</div></th>{/heading_title_cell}
        {heading_next_cell}<th style="border:none;" class="padB"><a class="calnav" data-calvalue="{next_url}" href="javascript:void(0);"><i class="material-icons">chevron_right</i></a></th>{/heading_next_cell}

        {heading_row_end}</tr>{/heading_row_end}

        {week_row_start}<tr>{/week_row_start}
        {week_day_cell}<th>{week_day}</th>{/week_day_cell}
        {week_row_end}</tr>{/week_row_end}

        {cal_row_start}<tr>{/cal_row_start}
        {cal_cell_start}<td>{/cal_cell_start}
        {cal_cell_start_today}<td>{/cal_cell_start_today}
        {cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}

        {cal_cell_content}<a ' . $class . '>{day}</a>{content}{/cal_cell_content}
        {cal_cell_content_today}<a ' . $class . '>{day}</a>{content}<div class="highlight"></div>{/cal_cell_content_today}

        {cal_cell_no_content}<a ' . $class . '>{day}</a>{/cal_cell_no_content}
        {cal_cell_no_content_today}<a ' . $class . '>{day}</a><div class="highlight"></div>{/cal_cell_no_content_today}

        {cal_cell_blank}&nbsp;{/cal_cell_blank}

        {cal_cell_other}{day}{/cal_cel_other}

        {cal_cell_end}</td>{/cal_cell_end}
        {cal_cell_end_today}</td>{/cal_cell_end_today}
        {cal_cell_end_other}</td>{/cal_cell_end_other}
        {cal_row_end}</tr>{/cal_row_end}

        {table_close}</table>{/table_close}';
        $prefs['start_day'] = 'monday';
        $prefs['day_type'] = 'short';
        $prefs['show_next_prev'] = TRUE;
        $prefs['next_prev_url'] = '?';

        $this->load->library('calendar', $prefs);
        $data['calendar'] = $this->calendar->generate($getYear, $getMonth, $calendarData, $this->uri->segment(3), $this->uri->segment(4));
        echo $data['calendar'];
    }

}