<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendas extends CI_Controller 
{
	public function __construct()
    { 
        parent::__construct();
        $this->load->library('Encryptor');
        $this->load->model('Carrinho_Model');
        $this->load->model('Vendas_Model');
        $this->load->model('Cielo_Model');
        $this->load->library('user_agent');
		$this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();
    }

	public function lista()
	{
		$info['encryptor'] = new Encryptor();
		$info['cursos'] = $this->Carrinho_Model->getVendasAguardando()->result_array();
		$info['mobile'] = $this->agent->mobile();
		$info['carrinho'] = $this->Carrinho_Model->verificaCodigo()->row();

		$this->template->view('dashboard.carrinho', $info);
	}

	public function delete($id, $idVenda)
	{
		$decrypt = new Encryptor();
        $id = $decrypt->decrypt($id);
        $idVenda = $decrypt->decrypt($idVenda);

        $qnt = $this->Vendas_Model->verificaCursos($idVenda)->num_rows();
        if($qnt > 1){
        	$this->Vendas_Model->deleteCurso($id);
        	
        	return $this->redirect->url('cart')
                ->withSuccess('Curso removido do carrinho!')
                ->send();
        } 

        if ($qnt == 1) {
        	$this->Vendas_Model->deleteCurso($id);
        	$this->Vendas_Model->deletaVenda($idVenda);

        	return $this->redirect->url('library')
                ->withSuccess('Cursos apagados do carrinho!')
                ->send();
        }
	}

	public function cupom($cupom, $idVenda)
	{	

		$dados = $this->Vendas_Model->verificaCupom($cupom)->row();
		if($dados == NULL){
			echo '0';
			return; 
		}

		if(date('Y-m-d') > $dados->validade){
			echo '2';
			return;
		}
		
		$decrypt = new Encryptor();
        $idVenda = $decrypt->decrypt($idVenda);

		$cadastroCupom = $this->Vendas_Model->insereCupom($dados->id, $idVenda);
		if(isset($cadastroCupom)) {
			echo '1';
			return;
		}

		echo '0';
		return;
	}

	public function getPagamento($codigo)
	{
		$decrypt = new Encryptor();
        $codigo = $decrypt->decrypt($codigo);
        $id_venda = $this->Carrinho_Model->getIdByCodigo($codigo);

        if (! $id_venda) {
            return $this->redirect->url('classes')
                ->withError('Detalhes curso não encontrado!')
                ->send();
        }
   
        $this->template->view('dashboard.detalhesPagamento', 
            array(
                'venda' => $this->Vendas_Model->find($id_venda)->row_array(),
                'cursos' => $this->Carrinho_Model->getCursosVenda($id_venda)->result_array(),
                'cielo' => $this->Cielo_Model->find($id_venda)->row_array(),
                'encryptor' => new Encryptor()
            )
        );
	}
}