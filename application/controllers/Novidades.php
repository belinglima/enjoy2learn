<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Novidades extends CI_Controller 
{
	public function __construct()
    { 
        parent::__construct();
		$this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();
    }

	public function index()
	{
		$this->template->view(
			'site.novidades'
		);
	}
}