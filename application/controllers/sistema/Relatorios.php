<?php

use phpDocumentor\Reflection\DocBlock\Tags\Var_;

defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorios extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
       
        $this->load->model('Carrinho_Model');
        $this->load->model('sistema/Alunos_Model');
        $this->load->model('sistema/Cursos_Model');
        $this->load->model('sistema/Vendas_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();

        if (! get_system_user_session()) {
            return $this->redirect->url('login/logout')
                ->send();
        }
    }

    public function alunos()
    {
        $info = [];
        $alunos = null;
        $msg = null;
        
        if ($dados = $this->input->post()) {
            $alunos = $this->Alunos_Model->getRelatorioAlunos($dados)->result_array();
            if (empty($alunos)) {
                $msg = true;
            } else {
                $msg = false;
            }
        }    
        
        $info = [
            'dados' => $dados, 
            'alunos' => $alunos, 
            'msg' => $msg
        ];

        $this->template->view('sistema.relatorios.alunos', $info);
    }

    public function avaliacaodoscursos()
    {
        $info = [];
        $cursos = $this->Cursos_Model->all()->result_array();
        $avaliacoes = null;
        $msg = null;
        
        if ($dados = $this->input->post()) {
            $avaliacoes = $this->Cursos_Model->getRelatorioAvaliacoesCursos($dados)->result_array();
            if (empty($avaliacoes)) {
                $msg = true;
            } else {
                $msg = false;
            }
        }    
        
        $info = [
            'dados' => $dados, 
            'cursos' => $cursos, 
            'avaliacoes' => $avaliacoes, 
            'msg' => $msg
        ];

        $this->template->view('sistema.relatorios.avaliacaodoscursos', $info);
    }

    public function cursosmaisvendidos()
    {
        $info = [];
        $cursos = null;
        $msg = null;
        
        if ($dados = $this->input->post()) {
            $cursos = $this->Cursos_Model->getRelatorioCursosMaisVendidos($dados)->result_array();
            if (empty($cursos)) {
                $msg = true;
            } else {
                $msg = false;
            }
        }    
        
        $info = [
            'dados' => $dados, 
            'cursos' => $cursos, 
            'msg' => $msg
        ];

        $this->template->view('sistema.relatorios.cursosmaisvendidos', $info);
    }

    public function vendas()
    {
        $info = [];
        $vendas = null;
        $msg = null;
        
        if ($dados = $this->input->post()) {
            $vendas = $this->Vendas_Model->getRelatorioVendas($dados)->result_array();
            if (empty($vendas)) {
                $msg = true;
            } else {
                $msg = false;
            }
        }    
        
        $info = [
            'dados' => $dados, 
            'vendas' => $vendas, 
            'msg' => $msg
        ];

       $this->template->view('sistema.relatorios.vendas', $info);
    }

    public function create()
    {
        $info['clientes'] = $this->Clientes_Model->all()->result_array();
        $info['pdfs'] = $this->Pdf_Model->all()->result_array();

        $this->template->view('vendas.create', $info);
    }

    // public function insert()
    // {
    //     $data = $this->input->post('cad');

    //     if (! $data) {
    //         return $this->redirect->url('vendas')
    //             ->withError('Nenhum dado passado!')
    //             ->send();
    //     }

    //     $venda['data'] = date('Y-m-d');
    //     $venda['total'] = convert_BRL_to_double(str_replace(",", ".", $data['total']));
        
    //     if ($id = $this->Vendas_Model->save($venda)) {

    //         return $this->redirect->url('vendas')
    //             ->withSuccess('Venda cadastrada!')
    //             ->send();
    //     }

    //     return $this->redirect->url('vendas')
    //             ->withError('Erro no cadastro!')
    //             ->send();
    // }

    public function edit($id)
    {
        $this->load->model('Cielo_Model');

        $this->template->view('vendas.edit', 
            array(
                'venda' => $this->Vendas_Model->find($id)->row_array(), 
                'cielo' => $this->Cielo_Model->find($id)->row_array(), 
                'pdfs' => $this->Vendas_Model->getPdfsVenda($id)->result_array()
            )
        );
    }

    public function update($id)
    {
        $data = $this->input->post('cad');

        if (! $data || ! $id) {
            return $this->redirect->url('vendas/edit/'.$id)
                ->withError('Erro nos dados!')
                ->send();
        }

        $data['total'] = convert_BRL_to_double(str_replace(",", ".", $data['total']));

        if (! $this->Vendas_Model->update($id, $data)) {
            return $this->redirect->url('vendas/edit/'.$id)
                ->withError('Erro na alteração!')
                ->send();
        }

        return $this->redirect->url('vendas')
            ->withSuccess('Registro alterado!')
            ->send();
    }

    public function delete($id)
    {
        $this->load->model('Cielo_Model');

        if (! $id) {
            return $this->redirect->url('vendas')
                ->withError('Informe um ID!')
                ->send();
        }

        if (! $this->Vendas_Model->removePedido($id)) {
            return $this->redirect->url('vendas')
                ->withError('Erro na exclusão!')
                ->send();
        }

        return $this->redirect->url('vendas')
            ->withSuccess('Pedido excluído!')
            ->send();
    }

}
