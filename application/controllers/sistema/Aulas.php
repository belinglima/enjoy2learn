<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aulas extends CI_Controller 
{
	public function __construct()
    {
        parent::__construct();
        $this->load->library('UploadFiles/FotoCursos', '', 'Foto');
        $this->load->model('sistema/Cursos_Model');
        $this->load->model('sistema/Aulas_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();
               
        if (! get_system_user_session()) {
        	return $this->redirect->url('sistema/login/logout')
                ->send();
        }
    }

    public function create($idCurso)
    {
        $info['idCurso'] = $idCurso;
        $info['curso'] = $this->Cursos_Model->find($idCurso)->row_array();
     
        $this->template->view('sistema.aulas.create', $info);
    }

    public function insert($idCurso)
    {
        $data = $this->input->post();

        // Upload da foto aula
        if (isset($_FILES['foto']['name']) && 
         ! empty($_FILES['foto']['name'])) {
            if ($this->Foto->do_upload('foto')) {
                $this->Foto->do_resize();
                $data['foto'] = $this->Foto->data('file_name');
            } else {
                echo $this->Foto->display_errors();
            }
        } else {
            $data['foto'] = NULL;
        }

        if (! $data) {
            return $this->redirect->url('sistema/aulas/create'.$idCurso)
                ->withError('Nenhum dado foi passado!')
                ->send();
        }

        $this->load->library('bcrypt'); 
        
        $data['id_curso'] = $idCurso;

        if (! $this->Aulas_Model->save($data)) {
            return $this->redirect->url('sistema/aulas/create/'.$idCurso)
                ->withError('Erro no cadastro!')
                ->send();
        }

        return $this->redirect->url('sistema/aulas/list/'.$idCurso)
                ->withSuccess('Nova aula cadastrada!')
                ->send();
    }

    public function edit($id)
    {
        $info['aula'] = $this->Aulas_Model->find($id)->row_array();

        $this->template->view('sistema.aulas.edit', $info);
    }

    public function update($id)
    {
        $data = $this->input->post();

        $idCurso = $this->Aulas_Model->find($id)->row()->id_curso;

        // Upload da foto curso
        if (isset($_FILES['foto']['name']) && 
            ! empty($_FILES['foto']['name'])) {
            
            if ($this->Foto->do_upload('foto')) {
                $this->Foto->do_resize();
                $data['foto'] = $this->Foto->data('file_name');
            } else {
               echo $this->Foto->display_errors();
            }
        }    
           
        if (! $data || ! $id) {
            return $this->redirect->url('sistema/aulas/edit/'.$id)
                ->withError('Erro nos dados passados!')
                ->send();
        }

        if (! $this->Aulas_Model->update($id, $data)) {
            return $this->redirect->url('sistema/aulas/edit/'.$id)
                ->withError('Erro na alteração!')
                ->send();
        }

        return $this->redirect->url('sistema/aulas/list/'.$idCurso)
                ->withSuccess('Aula alterada!')
                ->send();
    }

	public function lista($id, $page = '', $index = 0) 
	{
        $this->load->library('pagination');

        $data = $this->input->post();

        $config['base_url'] = base_url('sistema/aulas/list/'. $id .'/paginate/');
        $config['total_rows'] = $this->Aulas_Model->countFindCurso($id)->num_rows();
        $config['per_page'] = 10;
        
        $this->pagination->initialize($config);
        
        $info["links"] = $this->pagination->create_links();
        $info['idCurso'] = $id;
        $info['total'] = $this->Aulas_Model->totalRegistros($id);

        if($data) {
            $info['aulas'] = $this->Aulas_Model->findCurso($id, $index, $data)->result_array();
            $info['buscar'] = $data['buscar'];
        } else {
            $info['aulas'] = $this->Aulas_Model->findCurso($id, $index)->result_array();
        }

        $this->template->view('sistema.aulas.list', $info);
    }

    public function delete($id)
    {
        $idCurso = $this->Aulas_Model->find($id)->row()->id_curso;

        $aula = $this->Aulas_Model->find($id)->row_array();
        $this->Foto->delete_img($aula['foto']);

        if (! $id) {
            return $this->redirect->url('sistema/aulas/list/'.$idCurso)
                ->withError('Informe um ID!')
                ->send();
        }

        if (! $this->Aulas_Model->delete($id)) {
            return $this->redirect->url('sistema/aulas/list/'.$idCurso)
                ->withError('Erro na exclusão!')
                ->send();
        }

        return $this->redirect->url('sistema/aulas/list/'.$idCurso)
            ->withSuccess('Aula excluída!')
            ->send();
    }

    public function delete_image($id)
    {
        $aula = $this->Aulas_Model->find($id)->row_array();

        if ($this->Aulas_Model->delete_imagem($id)) {

            $this->Foto->delete_img($aula['foto']);

        } else {
             return $this->redirect->url($_SERVER['HTTP_REFERER'])
                ->withError('Erro ao excluir a foto!')
                ->send();
        }
        
        return $this->redirect->url($_SERVER['HTTP_REFERER'])
                ->withSuccess('Foto excluída!')
                ->send();
    }
}