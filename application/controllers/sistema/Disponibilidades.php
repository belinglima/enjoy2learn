<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disponibilidades extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sistema/Disponibilidades_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();

        if (! get_system_user_session()) {
            return $this->redirect->url('sistema/login/logout')
                ->send();
        }
    }

    public function index($index = 0)
    {
        $this->load->library('pagination');
        
        $data = $this->input->post();
            
        $config['base_url'] = base_url('sistema/disponibilidades/');
        $config['total_rows'] = $this->Disponibilidades_Model->countAll()->num_rows();
        $config['per_page'] = 10;
        
        $this->pagination->initialize($config);

        $info["links"] = $this->pagination->create_links();
        $info['total'] = $this->Disponibilidades_Model->totalRegistros();

        if($data) {
            $info['disponibilidades'] = $this->Disponibilidades_Model->all($index, $data)->result_array();
            $info['buscar'] = $data['buscar'];
        } else {
            $info['disponibilidades'] = $this->Disponibilidades_Model->all($index)->result_array();
        }

        $this->template->view('sistema.disponibilidades.list', $info);
    }

    public function create()
    {
        $this->template->view('sistema.disponibilidades.create');
    }

    public function insert()
    {
        $dia = $this->input->post('dia');
        $horainicio = $this->input->post('horainicio');
        $horafim = $this->input->post('horafim');
        
        for($i=0; $i<count($dia); $i++) {
            $data[] = [
                'dia' => $dia[$i],
                'horainicio' => $horainicio[$i],
                'horafim' => $horafim[$i]
            ];
        }

        if (! $data) {
            return $this->redirect->url('sistema/disponibilidades/create')
                ->withError('Nenhum dado foi passado!')
                ->send();
        }

        if ($id = $this->Disponibilidades_Model->save($data)) {
            return $this->redirect->url('sistema/disponibilidades')
                ->withSuccess('Nova disponibilidade cadastrada!')
                ->send();
        }

        return $this->redirect->url('sistema/disponibilidades')
                ->withError('Erro no cadastro!')
                ->send();   
    }

    public function edit($id)
    {
        $info['disponibilidade'] = $this->Disponibilidades_Model->find($id)->row_array();
        
        $this->template->view('sistema.disponibilidades.edit', $info);
    }

    public function update($id)
    {
        $data = $this->input->post();

        if (! $data || ! $id) {
            return $this->redirect->url('sistema/disponibilidades/edit/'.$id)
                ->withError('Erro nos dados passados!')
                ->send();
        }

        if (! $this->Disponibilidades_Model->update($id, $data)) {
            return $this->redirect->url('sistema/disponibilidades/edit/'.$id)
                ->withError('Erro na alteração!')
                ->send();
        }

        return $this->redirect->url('sistema/disponibilidades')
            ->withSuccess('Disponibilidade alterada!')
            ->send();
    }

    public function delete($id)
    {
        if (! $id) {
            return $this->redirect->url('sistema/disponibilidades')
                ->withError('Informe um ID!')
                ->send();
        }
 
        if (! $this->Disponibilidades_Model->delete($id)) {
            return $this->redirect->url('sistema/disponibilidades')
                ->withError('Erro na exclusão!')
                ->send();
        }

        return $this->redirect->url('sistema/disponibilidades')
            ->withSuccess('Disponibilidade excluída!')
            ->send();
    }

}