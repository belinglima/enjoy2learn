<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sistema/Usuarios_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();

        if (! get_system_user_session()) {
            return $this->redirect->url('sistema/login/logout')
                ->send();
        }
    }

    public function index($index = 0)
    {
        $this->load->library('pagination');
        
        $config['base_url'] = base_url('sistema/usuarios/');
        $config['total_rows'] = $this->Usuarios_Model->countAll()->num_rows();
        $config['per_page'] = 10;
        
        $this->pagination->initialize($config);

        $info["links"] = $this->pagination->create_links();
        $info['total'] = $this->Usuarios_Model->totalRegistros();
        $info['usuarios'] = $this->Usuarios_Model->all($index)->result_array();
 
        $this->template->view('sistema.usuarios.list', $info);
    }

    public function create()
    {
        $this->template->view('sistema.usuarios.create');
    }

    public function insert()
    {
        $data = $this->input->post();

        if (! $data) {
            return $this->redirect->url('sistema/usuarios')
                ->withError('Nenhum dado foi passado!')
                ->send();
        }

        $this->load->library('bcrypt');

        $data['hash_senha'] = (! empty($data['hash_senha'])) ? $this->bcrypt->hash($data['hash_senha']) : "";

        if ($id = $this->Usuarios_Model->save($data)) {
            return $this->redirect->url('sistema/usuarios')
                ->withSuccess('Novo usuário cadastrado!')
                ->send();
        }

        return $this->redirect->url('sistema/usuarios')
                ->withError('Erro no cadastro!')
                ->send();   
    }

    public function edit($id)
    {
        $info['usuario'] = $this->Usuarios_Model->find($id)->row_array();
        $this->template->view('sistema.usuarios.edit', $info);
    }

    public function update($id)
    {
        $data = $this->input->post();
        
        if (! $data || ! $id) {
            return $this->redirect->url('sistema/usuarios/edit/'.$id)
                ->withError('Erro nos dados passados!')
                ->send();
        }

        $this->load->library('bcrypt');

        if (! empty($data['hash_senha'])) {
            $data['hash_senha'] = $this->bcrypt->hash($data['hash_senha']);
        } else {
            unset($data['hash_senha']);
        }

        if (! $this->Usuarios_Model->update($id, $data)) {
            return $this->redirect->url('sistema/usuarios/edit/'.$id)
                ->withError('Erro na alteração!')
                ->send();
        }

        return $this->redirect->url('sistema/usuarios')
            ->withSuccess('Usuário alterado!')
            ->send();
    }

    public function delete($id)
    {
        if (! $id) {
            return $this->redirect->url('sistema/usuarios')
                ->withError('Informe um ID!')
                ->send();
        }
 
        if (! $this->Usuarios_Model->delete($id)) {
            return $this->redirect->url('sistema/usuarios')
                ->withError('Erro na exclusão!')
                ->send();
        }

        return $this->redirect->url('sistema/usuarios')
            ->withSuccess('Usuário excluído!')
            ->send();
    }

    public function deletarFoto()
    {
        $data['foto'] = null;
        $this->db->where('id', $this->aluno_id);
        return $this->db->update('usuarios', $data);
    }

}