<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('UploadFiles/FotoBlog', '', 'Foto');
        $this->load->model('sistema/Blog_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();

        if (! get_system_user_session()) {
            return $this->redirect->url('sistema/login/logout')
                ->send();
        }
    }

    public function index($index = 0)
    {
        $this->load->library('pagination');
    
        $data = $this->input->post();
            
        $config['base_url'] = base_url('sistema/blog/');
        $config['total_rows'] = $this->Blog_Model->countAll()->num_rows();
        $config['per_page'] = 10;
        
        $this->pagination->initialize($config);

        $info["links"] = $this->pagination->create_links();
        $info['total'] = $this->Blog_Model->totalRegistros();

        if($data) {
            $info['blogs'] = $this->Blog_Model->all($index, $data)->result_array();
            $info['buscar'] = $data['buscar'];
        } else {
            $info['blogs'] = $this->Blog_Model->all($index)->result_array();
        }

        $this->template->view('sistema.blog.list', $info);
    }

    public function create()
    {
        $this->template->view('sistema.blog.create');
    }

    public function insert()
    {
        $data = $this->input->post();

        // Upload da foto blog
        if (isset($_FILES['foto']['name']) && 
            ! empty($_FILES['foto']['name'])) {
            
            if ($this->Foto->do_upload('foto')) {
                $this->Foto->do_resize();
                $data['foto'] = $this->Foto->data('file_name');
            } else {
               echo $this->Foto->display_errors();
            }
        } else {
            $data['foto'] = NULL;
        }

        if (! $data) {
            return $this->redirect->url('sistema/blog')
                ->withError('Nenhum dado foi passado!')
                ->send();
        }

        if ($id = $this->Blog_Model->save($data)) {
            return $this->redirect->url('sistema/blog')
                ->withSuccess('Novo blog cadastrado!')
                ->send();
        }

        return $this->redirect->url('sistema/blog')
                ->withError('Erro no cadastro!')
                ->send();   
    }

    public function edit($id)
    {
        $info['blog'] = $this->Blog_Model->find($id)->row_array();
        $this->template->view('sistema.blog.edit', $info);
    }

    public function update($id)
    {
        $data = $this->input->post();

        // Upload da foto blog
        if (isset($_FILES['foto']['name']) && 
            ! empty($_FILES['foto']['name'])) {
            
            if ($this->Foto->do_upload('foto')) {
                $this->Foto->do_resize();
                $data['foto'] = $this->Foto->data('file_name');
            } else {
               echo $this->Foto->display_errors();
            }
        }    

        if (! $data || ! $id) {
            return $this->redirect->url('sistema/blog/edit/'.$id)
                ->withError('Erro nos dados passados!')
                ->send();
        }

        if (! $this->Blog_Model->update($id, $data)) {
            return $this->redirect->url('sistema/blog/edit/'.$id)
                ->withError('Erro na alteração!')
                ->send();
        }

        return $this->redirect->url('sistema/blog')
            ->withSuccess('Blog alterado!')
            ->send();
    }

    public function delete($id)
    {
        if (! $id) {
            return $this->redirect->url('sistema/blog')
                ->withError('Informe um ID!')
                ->send();
        }
        
        $blog = $this->Blog_Model->find($id)->row_array();
        $this->Foto->delete_img($blog['foto']);

        if (! $this->Blog_Model->delete($id)) {
            return $this->redirect->url('sistema/blog')
                ->withError('Erro na exclusão!')
                ->send();
        }

        return $this->redirect->url('sistema/blog')
            ->withSuccess('Blog excluído!')
            ->send();
    }

    public function delete_image($id)
    {
        $blog = $this->Blog_Model->find($id)->row_array();

        if ($this->Blog_Model->delete_imagem($id)) {

            $this->Foto->delete_img($blog['foto']);

        } else {
             return $this->redirect->url($_SERVER['HTTP_REFERER'])
                ->withError('Erro ao excluir a foto!')
                ->send();
        }
        
        return $this->redirect->url($_SERVER['HTTP_REFERER'])
                ->withSuccess('Foto excluída!')
                ->send();
    }
}