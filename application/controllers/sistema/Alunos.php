<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alunos extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sistema/Alunos_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();

        if (! get_system_user_session()) {
            return $this->redirect->url('sistema/login/logout')
                ->send();
        }
    }

    public function index($index = 0)
    {
        
        $this->load->library('pagination');
        
        $data = $this->input->post();
            
        $config['base_url'] = base_url('sistema/alunos/');
        $config['total_rows'] = $this->Alunos_Model->countAll()->num_rows();
        $config['per_page'] = 10;
        
        $this->pagination->initialize($config);

        $info["links"] = $this->pagination->create_links();
        $info['total'] = $this->Alunos_Model->totalRegistros();

        if($data) {
            $info['alunos'] = $this->Alunos_Model->all($index, $data)->result_array();
            $info['buscar'] = $data['buscar'];
        } else {
            $info['alunos'] = $this->Alunos_Model->all($index)->result_array();
        }

        $this->template->view('sistema.alunos.list', $info);
    }

    public function create()
    {
        $this->template->view('sistema.alunos.create');
    }

    public function insert()
    {
        $data = $this->input->post();

        if (! $data) {
            return $this->redirect->url('sistema/alunos')
                ->withError('Nenhum dado foi passado!')
                ->send();
        }

        $this->load->library('bcrypt');

        $data['hash_senha'] = (! empty($data['hash_senha'])) ? $this->bcrypt->hash($data['hash_senha']) : "";

        if ($id = $this->Alunos_Model->save($data)) {
            return $this->redirect->url('sistema/alunos')
                ->withSuccess('Novo aluno cadastrado!')
                ->send();
        }

        return $this->redirect->url('sistema/alunos')
                ->withError('Erro no cadastro!')
                ->send();   
    }

    public function edit($id)
    {
        $info['aluno'] = $this->Alunos_Model->find($id)->row_array();
        $this->template->view('sistema.alunos.edit', $info);
    }

    public function update($id)
    {
        $data = $this->input->post();
        
        if (! $data || ! $id) {
            return $this->redirect->url('sistema/alunos/edit/'.$id)
                ->withError('Erro nos dados passados!')
                ->send();
        }

        $this->load->library('bcrypt');

        if (! empty($data['hash_senha'])) {
            $data['hash_senha'] = $this->bcrypt->hash($data['hash_senha']);
        } else {
            unset($data['hash_senha']);
        }

        if (! $this->Alunos_Model->update($id, $data)) {
            return $this->redirect->url('sistema/alunos/edit/'.$id)
                ->withError('Erro na alteração!')
                ->send();
        }

        return $this->redirect->url('sistema/alunos')
            ->withSuccess('Aluno alterado!')
            ->send();
    }

    public function delete($id)
    {
        if (! $id) {
            return $this->redirect->url('sistema/alunos')
                ->withError('Informe um ID!')
                ->send();
        }
 
        if (! $this->Alunos_Model->delete($id)) {
            return $this->redirect->url('sistema/alunos')
                ->withError('Erro na exclusão!')
                ->send();
        }

        return $this->redirect->url('sistema/alunos')
            ->withSuccess('Aluno excluído!')
            ->send();
    }

}