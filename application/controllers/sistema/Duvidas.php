<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Duvidas extends CI_Controller 
{
	public function __construct()
    {
        parent::__construct();
        $this->load->model('sistema/Duvidas_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();
         
        if (! get_system_user_session()) {
        	return $this->redirect->url('sistema/login/logout')
                ->send();
        }
    }

	public function index($index = 0)
	{        
        $this->load->library('pagination');
        
        $config['base_url'] = base_url('sistema/duvidas/');
        $config['total_rows'] = $this->Duvidas_Model->countAll()->num_rows();
        $config['per_page'] = 10;
        
        $this->pagination->initialize($config);

        $info["links"] = $this->pagination->create_links();
        $info['total'] = $this->Duvidas_Model->totalRegistros();
        $info['duvidas'] = $this->Duvidas_Model->all($index)->result_array();
       
        $this->template->view('sistema.duvidas.list', $info);
    }   

    public function create()
    {
        $this->template->view('sistema.duvidas.create');
    }

    public function insert()
    {
         $data = $this->input->post();

         if (! $data) {
             return $this->redirect->url('sistema/duvidas')
                 ->withError('Nenhum dado foi passado!')
                 ->send();
         }

        if ($id = $this->Duvidas_Model->save($data)) {
             return $this->redirect->url('sistema/duvidas')
                 ->withSuccess('Nova dúvida cadastrada!')
                 ->send();
        }
        
        return $this->redirect->url('sistema/duvidas')
                 ->withError('Erro no cadastro!')
                 ->send();   
    }

    public function edit($id)
    {
        $info['duvida'] = $this->Duvidas_Model->find($id)->row_array();
        $this->template->view('sistema.duvidas.edit', $info);
    }

    public function update($id)
    {
        $data = $this->input->post();

        if (! $data || ! $id) {
             return $this->redirect->url('sistema/duvidas/edit/'.$id)
                 ->withError('Erro nos dados passados!')
                 ->send();
        }

        if (! $this->Duvidas_Model->update($id, $data)) {
             return $this->redirect->url('sistema/duvidas/edit/'.$id)
                 ->withError('Erro na alteração!')
                 ->send();
        }

        return $this->redirect->url('sistema/duvidas')
            ->withSuccess('Dúvida alterada!')
             ->send();
    }

    public function delete($id)
    {
        if (! $id) {
             return $this->redirect->url('sistema/duvidas')
                 ->withError('Informe um ID!')
                 ->send();
        }
 
        if (! $this->Duvidas_Model->delete($id)) {
            return $this->redirect->url('sistema/duvidas')
                ->withError('Erro na exclusão!')
                ->send();
        }

        return $this->redirect->url('sistema/duvidas')
             ->withSuccess('Dúvida excluída!')
             ->send();
    }
}