<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contatos extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sistema/Contatos_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();

        if (! get_system_user_session()) {
            return $this->redirect->url('sistema/login/logout')
                ->send();
        }
    }

    public function index($index = 0)
    {
        
        $this->load->library('pagination');
        
        $data = $this->input->post();
            
        $config['base_url'] = base_url('sistema/contatos/');
        $config['total_rows'] = $this->Contatos_Model->countAll()->num_rows();
        $config['per_page'] = 10;
        
        $this->pagination->initialize($config);

        $info["links"] = $this->pagination->create_links();
        $info['total'] = $this->Contatos_Model->totalRegistros();

        if($data) {
            $info['contatos'] = $this->Contatos_Model->all($index, $data)->result_array();
            $info['buscar'] = $data['buscar'];
        } else {
            $info['contatos'] = $this->Contatos_Model->all($index)->result_array();
        }

        $this->template->view('sistema.contatos.list', $info);
    }

    public function getMensagem($id)
    {
        $mensagem = $this->Contatos_Model->getMensagem($id)->row_array();
        echo $this->load->view('sistema/contatos/mensagem', [ 'mensagem' => $mensagem], true);
    }

    public function delete($id)
    {
        if (! $id) {
            return $this->redirect->url('sistema/contatos')
                ->withError('Informe um ID!')
                ->send();
        }
 
        if (! $this->Contatos_Model->delete($id)) {
            return $this->redirect->url('sistema/contatos')
                ->withError('Erro na exclusão!')
                ->send();
        }

        return $this->redirect->url('sistema/contatos')
            ->withSuccess('Contato excluído!')
            ->send();
    }

}