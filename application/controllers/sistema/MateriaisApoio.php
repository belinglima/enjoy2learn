<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MateriaisApoio extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('UploadFiles/FotoMateriaisApoio', '', 'Foto');
        $this->load->model('sistema/Cursos_Model');
        $this->load->model('sistema/MateriaisApoio_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();

        if (! get_system_user_session()) {
            return $this->redirect->url('sistema/login/logout')
                ->send();
        }
    }

    public function index($index = 0)
    {
        $this->load->library('pagination');
    
        $data = $this->input->post();
            
        $config['base_url'] = base_url('sistema/materiaisApoio/');
        $config['total_rows'] = $this->MateriaisApoio_Model->countAll()->num_rows();
        $config['per_page'] = 10;
        
        $this->pagination->initialize($config);

        $info["links"] = $this->pagination->create_links();
        $info['total'] = $this->MateriaisApoio_Model->totalRegistros();

        if($data) {
            $info['materiais'] = $this->MateriaisApoio_Model->all($index, $data)->result_array();
            $info['buscar'] = $data['buscar'];
        } else {
            $info['materiais'] = $this->MateriaisApoio_Model->all($index)->result_array();
        }

        $this->template->view('sistema.materiaisApoio.list', $info);
    }

    public function getAulaByCurso($id, $idAula = "") 
    {   
        $aulas = $this->Cursos_Model->getAulaByCurso($id)->result_array();

        $dados = '<option value="false" selected>Escolha uma aula</option>';
        foreach ($aulas AS $aula) {
            if($aula['id'] == $idAula) {
                $dados .= "<option value='" . $aula['id'] . "' selected>" . $aula['nome'] . "</option>";
            } else {
                $dados .= "<option value='" . $aula['id'] . "'>" . $aula['nome'] . "</option>";
            }
        }

        echo $dados;
    }

    public function create()
    {
        $info['cursos'] = $this->Cursos_Model->all()->result_array();
        $this->template->view('sistema.materiaisApoio.create', $info);
    }

    public function insert()
    {
        $data = $this->input->post();

        // Upload da foto material apoio
        if (isset($_FILES['url_imagem']['name']) && 
            ! empty($_FILES['url_imagem']['name'])) {
            
            if ($this->Foto->do_upload('url_imagem')) {
                $this->Foto->do_resize();
                $data['url_imagem'] = $this->Foto->data('file_name');
            } else {
               echo $this->Foto->display_errors();
            }
        } else {
            $data['url_imagem'] = NULL;
        }

        if (! $data) {
            return $this->redirect->url('sistema/materiaisApoio')
                ->withError('Nenhum dado foi passado!')
                ->send();
        }

        if ($id = $this->MateriaisApoio_Model->save($data)) {
            return $this->redirect->url('sistema/materiaisApoio')
                ->withSuccess('Novo material de apoio cadastrado!')
                ->send();
        }

        return $this->redirect->url('sistema/materiaisApoio')
                ->withError('Erro no cadastro!')
                ->send();   
    }

    public function edit($id)
    {
        $info['material'] = $this->MateriaisApoio_Model->find($id)->row_array();
        $info['cursos'] = $this->Cursos_Model->all()->result_array();
        $this->template->view('sistema.materiaisApoio.edit', $info);
    }

    public function update($id)
    {
        $data = $this->input->post();

        if($data['tipomaterial'] == 'IMAGEM') {
            $data['url_audio'] == NULL;
            $data['url_pdf'] == NULL;
        }

        if($data['tipomaterial'] == 'PDF') {
            $data['url_imagem'] == NULL;
            $data['url_audio'] == NULL;
        }

        if($data['tipomaterial'] == 'AUDIO') {
            $data['url_imagem'] == NULL;
            $data['url_pdf'] == NULL;
        }

        // Upload da foto material apoio
        if (isset($_FILES['url_imagem']['name']) && 
            ! empty($_FILES['url_imagem']['name'])) {
            
            if ($this->Foto->do_upload('url_imagem')) {
                $this->Foto->do_resize();
                $data['url_imagem'] = $this->Foto->data('file_name');
            } else {
               echo $this->Foto->display_errors();
            }
        }    

        if (! $data || ! $id) {
            return $this->redirect->url('sistema/materiaisApoio/edit/'.$id)
                ->withError('Erro nos dados passados!')
                ->send();
        }

        if (! $this->MateriaisApoio_Model->update($id, $data)) {
            return $this->redirect->url('sistema/materiaisApoio/edit/'.$id)
                ->withError('Erro na alteração!')
                ->send();
        }

        return $this->redirect->url('sistema/materiaisApoio')
            ->withSuccess('Material de apoio alterado!')
            ->send();
    }

    public function delete($id)
    {
        if (! $id) {
            return $this->redirect->url('sistema/materiaisApoio')
                ->withError('Informe um ID!')
                ->send();
        }
        
        $material = $this->MateriaisApoio_Model->find($id)->row_array();
        $this->Foto->delete_img($material['url_imagem']);

        if (! $this->MateriaisApoio_Model->delete($id)) {
            return $this->redirect->url('sistema/materiaisApoio')
                ->withError('Erro na exclusão!')
                ->send();
        }

        return $this->redirect->url('sistema/materiaisApoio')
            ->withSuccess('Material de apoio excluído!')
            ->send();
    }

    public function delete_image($id)
    {
        $material = $this->MateriaisApoio_Model->find($id)->row_array();

        if ($this->MateriaisApoio_Model->delete_imagem($id)) {

            $this->Foto->delete_img($material['url_imagem']);

        } else {
             return $this->redirect->url($_SERVER['HTTP_REFERER'])
                ->withError('Erro ao excluir o material!')
                ->send();
        }
        
        return $this->redirect->url($_SERVER['HTTP_REFERER'])
                ->withSuccess('Material de excluído!')
                ->send();
    }
}