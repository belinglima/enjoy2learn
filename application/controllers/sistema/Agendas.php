<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agendas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sistema/Agendas_Model');
        $this->load->model('sistema/Alunos_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();

        if (! get_system_user_session()) {
            return $this->redirect->url('sistema/login/logout')
                ->send();
        }
    }

    public function index($index = 0)
    {
        $this->load->library('pagination');
        
        $data = $this->input->post();
            
        $config['base_url'] = base_url('sistema/agendas/');
        $config['total_rows'] = $this->Agendas_Model->countAll()->num_rows();
        $config['per_page'] = 10;
        
        $this->pagination->initialize($config);

        $info["links"] = $this->pagination->create_links();
        $info['total'] = $this->Agendas_Model->totalRegistros();

        if($data) {
            $info['agendas'] = $this->Agendas_Model->all($index, $data)->result_array();
            $info['buscar'] = $data['buscar'];
        } else {
            $info['agendas'] = $this->Agendas_Model->all($index)->result_array();
        }

        $this->template->view('sistema.agendas.list', $info);
    }

    public function create()
    {
        $info['alunos'] = $this->Alunos_Model->all($start = 0, $data = '')->result_array();

        $this->template->view('sistema.agendas.create', $info);
    }

    public function insert()
    {
        $data = $this->input->post();

        $dataAgenda = format_date_to_bd($data['data']);
        $data['data'] = $dataAgenda;

        unset($data['nome']);
       
        $idAluno = $data['aluno_id'];

        if(empty($idAluno) || $idAluno == 0) {
            $data['aluno_id'] = NULL;
        }

        $agendas = $this->Agendas_Model->verificaAgenda($data);
        
        if($agendas) {
            return $this->redirect->url('sistema/agendas/create')
                    ->withError('Já existe aula marcada esse horário!')
                    ->send();
        }

        if (! $data) {
            return $this->redirect->url('sistema/agendas')
                ->withError('Nenhum dado foi passado!')
                ->send();
        }

        if ($id = $this->Agendas_Model->save($data)) {
            return $this->redirect->url('sistema/agendas')
                ->withSuccess('Novo agenda cadastrada!')
                ->send();
        }

        return $this->redirect->url('sistema/agendas')
                ->withError('Erro no cadastro!')
                ->send();   
    }

    public function edit($id)
    {
        $info['agenda'] = $this->Agendas_Model->find($id)->row_array();
        $info['alunos'] = $this->Alunos_Model->all($start = 0, $data = '')->result_array();

        $this->template->view('sistema.agendas.edit', $info);
    }

    public function update($id)
    {
        $data = $this->input->post();

        $dataAgenda = format_date_to_bd($data['data']);
        $data['data'] = $dataAgenda;

        unset($data['nome']);

        if(empty($data['aluno_id']) || $data['aluno_id'] == 0) {
            $data['aluno_id'] = NULL;
        } 
        
        if (! $data || ! $id) {
            return $this->redirect->url('sistema/agendas/edit/'.$id)
                ->withError('Erro nos dados passados!')
                ->send();
        }

        $agendas = $this->Agendas_Model->verificaAgendaAluno($data);
        
        if($agendas) {
            return $this->redirect->url('sistema/agendas/create')
                    ->withError('Já existe aula marcada esse horário!')
                    ->send();
        }

        if (! $this->Agendas_Model->update($id, $data)) {
            return $this->redirect->url('sistema/agendas/edit/'.$id)
                ->withError('Erro na alteração!')
                ->send();
        }

        return $this->redirect->url('sistema/agendas')
            ->withSuccess('Agenda alterada!')
            ->send();
    }

    public function delete($id)
    {
        if (! $id) {
            return $this->redirect->url('sistema/agendas')
                ->withError('Informe um ID!')
                ->send();
        }
 
        if (! $this->Agendas_Model->delete($id)) {
            return $this->redirect->url('sistema/agendas')
                ->withError('Erro na exclusão!')
                ->send();
        }

        return $this->redirect->url('sistema/agendas')
            ->withSuccess('Agenda excluída!')
            ->send();
    }

}