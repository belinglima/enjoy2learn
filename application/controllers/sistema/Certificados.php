<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Certificados extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('UploadFiles/FotoCertificados', '', 'Foto');
        $this->load->model('sistema/Certificados_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();

        if (! get_system_user_session()) {
            return $this->redirect->url('sistema/login/logout')
                ->send();
        }
    }

    public function index($index = 0)
    {
        $this->load->library('pagination');
    
        $data = $this->input->post();
            
        $config['base_url'] = base_url('sistema/certificados/');
        $config['total_rows'] = $this->Certificados_Model->countAll()->num_rows();
        $config['per_page'] = 10;
        
        $this->pagination->initialize($config);

        $info["links"] = $this->pagination->create_links();
        $info['total'] = $this->Certificados_Model->totalRegistros();

        if($data) {
            $info['certificados'] = $this->Certificados_Model->all($index, $data)->result_array();
            $info['buscar'] = $data['buscar'];
        } else {
            $info['certificados'] = $this->Certificados_Model->all($index)->result_array();
        }

        $this->template->view('sistema.certificados.list', $info);
    }

    public function create()
    {
        $this->template->view('sistema.certificados.create');
    }

    public function insert()
    {
        $data = $this->input->post();

        // Upload da foto certificados
        if (isset($_FILES['foto']['name']) && 
            ! empty($_FILES['foto']['name'])) {
            
            if ($this->Foto->do_upload('foto')) {
                $this->Foto->do_resize();
                $data['foto'] = $this->Foto->data('file_name');
            } else {
               echo $this->Foto->display_errors();
            }
        } else {
            $data['foto'] = NULL;
        }

        if (! $data) {
            return $this->redirect->url('sistema/certificados')
                ->withError('Nenhum dado foi passado!')
                ->send();
        }

        if ($id = $this->Certificados_Model->save($data)) {
            return $this->redirect->url('sistema/certificados')
                ->withSuccess('Novo certificado cadastrado!')
                ->send();
        }

        return $this->redirect->url('sistema/certificados')
                ->withError('Erro no cadastro!')
                ->send();   
    }

    public function edit($id)
    {
        $info['certificado'] = $this->Certificados_Model->find($id)->row_array();
        $this->template->view('sistema.certificados.edit', $info);
    }

    public function update($id)
    {
        $data = $this->input->post();

        // Upload da foto certificado
        if (isset($_FILES['foto']['name']) && 
            ! empty($_FILES['foto']['name'])) {
            
            if ($this->Foto->do_upload('foto')) {
                $this->Foto->do_resize();
                $data['foto'] = $this->Foto->data('file_name');
            } else {
               echo $this->Foto->display_errors();
            }
        }    

        if (! $data || ! $id) {
            return $this->redirect->url('sistema/certificados/edit/'.$id)
                ->withError('Erro nos dados passados!')
                ->send();
        }

        if (! $this->Certificados_Model->update($id, $data)) {
            return $this->redirect->url('sistema/certificados/edit/'.$id)
                ->withError('Erro na alteração!')
                ->send();
        }

        return $this->redirect->url('sistema/certificados')
            ->withSuccess('Certificado alterado!')
            ->send();
    }

    public function delete($id)
    {
        if (! $id) {
            return $this->redirect->url('sistema/certificados')
                ->withError('Informe um ID!')
                ->send();
        }
        
        $certificado = $this->Certificados_Model->find($id)->row_array();
        $this->Foto->delete_img($certificado['foto']);

        if (! $this->Certificados_Model->delete($id)) {
            return $this->redirect->url('sistema/certificados')
                ->withError('Erro na exclusão!')
                ->send();
        }

        return $this->redirect->url('sistema/certificados')
            ->withSuccess('Certificado excluído!')
            ->send();
    }

    public function delete_image($id)
    {
        $certificado = $this->Certificados_Model->find($id)->row_array();

        if ($this->Certificados_Model->delete_imagem($id)) {

            $this->Foto->delete_img($certificado['foto']);

        } else {
             return $this->redirect->url($_SERVER['HTTP_REFERER'])
                ->withError('Erro ao excluir a foto!')
                ->send();
        }
        
        return $this->redirect->url($_SERVER['HTTP_REFERER'])
                ->withSuccess('Foto excluída!')
                ->send();
    }
}