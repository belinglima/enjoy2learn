<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sistema/Vendas_Model');
        $this->load->model('Carrinho_Model');
        $this->load->model('sistema/Cursos_Model');
        $this->load->model('sistema/Alunos_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();

        if (! get_system_user_session()) {
            return $this->redirect->url('login/logout')
                ->send();
        }
    }

    public function index($index = 0)
    {
        $this->load->library('pagination');
        
        $config['base_url'] = base_url('sistema/vendas/');
        $config['total_rows'] = $this->Vendas_Model->countAll()->num_rows();
        $config['per_page'] = 10;
    
        $this->pagination->initialize($config);

        $info["links"] = $this->pagination->create_links();
        $info['total'] = $this->Vendas_Model->totalRegistros();
        $info['vendas'] = $this->Vendas_Model->all($index)->result_array();
 
        $this->template->view('sistema.vendas.list', $info);
    }

    public function filtro()
    {
        $data = $this->input->post();
        
        $info['vendas'] = $this->Vendas_Model->filter($data)->result_array();
        $info['data'] = $data;

        $this->template->view('vendas.list', $info);
    }

    public function create()
    {
        $info['clientes'] = $this->Clientes_Model->all()->result_array();
        $info['pdfs'] = $this->Pdf_Model->all()->result_array();

        $this->template->view('vendas.create', $info);
    }

    // public function insert()
    // {
    //     $data = $this->input->post('cad');

    //     if (! $data) {
    //         return $this->redirect->url('vendas')
    //             ->withError('Nenhum dado passado!')
    //             ->send();
    //     }

    //     $venda['data'] = date('Y-m-d');
    //     $venda['total'] = convert_BRL_to_double(str_replace(",", ".", $data['total']));
        
    //     if ($id = $this->Vendas_Model->save($venda)) {

    //         return $this->redirect->url('vendas')
    //             ->withSuccess('Venda cadastrada!')
    //             ->send();
    //     }

    //     return $this->redirect->url('vendas')
    //             ->withError('Erro no cadastro!')
    //             ->send();
    // }

    public function edit($id)
    {
        $this->load->model('Cielo_Model');

        $this->template->view('vendas.edit', 
            array(
                'venda' => $this->Vendas_Model->find($id)->row_array(), 
                'cielo' => $this->Cielo_Model->find($id)->row_array(), 
                'pdfs' => $this->Vendas_Model->getPdfsVenda($id)->result_array()
            )
        );
    }

    public function update($id)
    {
        $data = $this->input->post('cad');

        if (! $data || ! $id) {
            return $this->redirect->url('vendas/edit/'.$id)
                ->withError('Erro nos dados!')
                ->send();
        }

        $data['total'] = convert_BRL_to_double(str_replace(",", ".", $data['total']));

        if (! $this->Vendas_Model->update($id, $data)) {
            return $this->redirect->url('vendas/edit/'.$id)
                ->withError('Erro na alteração!')
                ->send();
        }

        return $this->redirect->url('vendas')
            ->withSuccess('Registro alterado!')
            ->send();
    }

    public function delete($id)
    {
        $this->load->model('Cielo_Model');

        if (! $id) {
            return $this->redirect->url('vendas')
                ->withError('Informe um ID!')
                ->send();
        }

        if (! $this->Vendas_Model->removePedido($id)) {
            return $this->redirect->url('vendas')
                ->withError('Erro na exclusão!')
                ->send();
        }

        return $this->redirect->url('vendas')
            ->withSuccess('Pedido excluído!')
            ->send();
    }

}
