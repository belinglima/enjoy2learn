<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cupons extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sistema/Cupons_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();

        if (! get_system_user_session()) {
            return $this->redirect->url('sistema/login/logout')
                ->send();
        }
    }

    public function index($index = 0)
    {
        $this->load->library('pagination');
        
        $config['base_url'] = base_url('sistema/cupons/');
        $config['total_rows'] = $this->Cupons_Model->countAll()->num_rows();
        $config['per_page'] = 10;
        
        $this->pagination->initialize($config);

        $info["links"] = $this->pagination->create_links();
        $info['total'] = $this->Cupons_Model->totalRegistros();
        $info['cupons'] = $this->Cupons_Model->all($index)->result_array();
 
        $this->template->view('sistema.cupons.list', $info);
    }

    public function create()
    {
        $this->template->view('sistema.cupons.create');
    }

    public function insert()
    {
        $data = $this->input->post();

        $validade = format_date_to_bd($data['validade']);
        $data['validade'] = $validade;

        $porcentagem = str_replace(',','.', str_replace('.','', $data['porcentagem']));
        $data['porcentagem'] = $porcentagem;

        if (! $data) {
            return $this->redirect->url('sistema/cupons')
                ->withError('Nenhum dado foi passado!')
                ->send();
        }

        if ($id = $this->Cupons_Model->save($data)) {
            return $this->redirect->url('sistema/cupons')
                ->withSuccess('Novo cupom, cadastrado!')
                ->send();
        }

        return $this->redirect->url('sistema/cupons')
                ->withError('Erro no cadastro!')
                ->send();   
    }

    public function edit($id)
    {
        $info['cupom'] = $this->Cupons_Model->find($id)->row_array();
        $this->template->view('sistema.cupons.edit', $info);
    }

    public function update($id)
    {
        $data = $this->input->post();

        $validade = format_date_to_bd($data['validade']);
        $data['validade'] = $validade;

        $porcentagem = str_replace(',','.', str_replace('.','', $data['porcentagem']));
        $data['porcentagem'] = $porcentagem;
        
        if (! $data || ! $id) {
            return $this->redirect->url('sistema/cupons/edit/'.$id)
                ->withError('Erro nos dados passados!')
                ->send();
        }

        if (! $this->Cupons_Model->update($id, $data)) {
            return $this->redirect->url('sistema/cupons/edit/'.$id)
                ->withError('Erro na alteração!')
                ->send();
        }

        return $this->redirect->url('sistema/cupons')
            ->withSuccess('Cupom alterado!')
            ->send();
    }

    public function delete($id)
    {
        if (! $id) {
            return $this->redirect->url('sistema/cupons')
                ->withError('Informe um ID!')
                ->send();
        }
 
        if (! $this->Cupons_Model->delete($id)) {
            return $this->redirect->url('sistema/cupons')
                ->withError('Erro na exclusão!')
                ->send();
        }

        return $this->redirect->url('sistema/cupons')
            ->withSuccess('Cupom excluído!')
            ->send();
    }

}