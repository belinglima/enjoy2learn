<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cursos extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('UploadFiles/FotoCursos', '', 'Foto');
        $this->load->model('sistema/Aulas_Model');
        $this->load->model('sistema/Certificados_Model');
        $this->load->model('sistema/Cursos_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();

        if (! get_system_user_session()) {
            return $this->redirect->url('sistema/login/logout')
                ->send();
        }
    }

    public function index($index = 0)
    {
        $this->load->library('pagination');
    
        $data = $this->input->post();
            
        $config['base_url'] = base_url('sistema/cursos/');
        $config['total_rows'] = $this->Cursos_Model->countAll()->num_rows();
        $config['per_page'] = 10;
        
        $this->pagination->initialize($config);

        $info["links"] = $this->pagination->create_links();
        $info['total'] = $this->Cursos_Model->totalRegistros();

        if($data) {
            $info['cursos'] = $this->Cursos_Model->all($index, $data)->result_array();
            $info['buscar'] = $data['buscar'];
        } else {
            $info['cursos'] = $this->Cursos_Model->all($index)->result_array();
        }

        $this->template->view('sistema.cursos.list', $info);
    }

    public function create()
    {
        $info['certificados'] = $this->Certificados_Model->all()->result_array();
        $this->template->view('sistema.cursos.create', $info);
    }

    public function insert()
    {
        $data = $this->input->post();

        $preco = str_replace(',','.', str_replace('.','', $data['preco']));
        $data['preco'] = $preco;

        // Upload da foto curso
        if (isset($_FILES['foto']['name']) && 
            ! empty($_FILES['foto']['name'])) {
            
            if ($this->Foto->do_upload('foto')) {
                $this->Foto->do_resize();
                $data['foto'] = $this->Foto->data('file_name');
            } else {
               echo $this->Foto->display_errors();
            }
        } else {
            $data['foto'] = NULL;
        }

        if (! $data) {
            return $this->redirect->url('sistema/cursos')
                ->withError('Nenhum dado foi passado!')
                ->send();
        }

        if ($id = $this->Cursos_Model->save($data)) {
            return $this->redirect->url('sistema/cursos')
                ->withSuccess('Novo curso cadastrado!')
                ->send();
        }

        return $this->redirect->url('sistema/cursos')
                ->withError('Erro no cadastro!')
                ->send();   
    }

    public function edit($id)
    {
        $info['curso'] = $this->Cursos_Model->find($id)->row_array();
        $info['certificados'] = $this->Certificados_Model->all()->result_array();
        $this->template->view('sistema.cursos.edit', $info);
    }

    public function update($id)
    {
        $data = $this->input->post();

        $preco = str_replace(',','.', str_replace('.','', $data['preco']));
        $data['preco'] = $preco;

        // Upload da foto curso
        if (isset($_FILES['foto']['name']) && 
            ! empty($_FILES['foto']['name'])) {
            
            if ($this->Foto->do_upload('foto')) {
                $this->Foto->do_resize();
                $data['foto'] = $this->Foto->data('file_name');
            } else {
               echo $this->Foto->display_errors();
            }
        }    

        if (! $data || ! $id) {
            return $this->redirect->url('sistema/cursos/edit/'.$id)
                ->withError('Erro nos dados passados!')
                ->send();
        }

        if (! $this->Cursos_Model->update($id, $data)) {
            return $this->redirect->url('sistema/cursos/edit/'.$id)
                ->withError('Erro na alteração!')
                ->send();
        }

        return $this->redirect->url('sistema/cursos')
            ->withSuccess('Curso alterado!')
            ->send();
    }

    public function delete($id)
    {
        if (! $id) {
            return $this->redirect->url('sistema/cursos')
                ->withError('Informe um ID!')
                ->send();
        }
        
        $curso = $this->Cursos_Model->find($id)->row_array();
        $this->Foto->delete_img($curso['foto']);

        $this->Aulas_Model->deleteAulas($id);
 
        if (! $this->Cursos_Model->delete($id)) {
            return $this->redirect->url('sistema/cursos')
                ->withError('Erro na exclusão!')
                ->send();
        }

        return $this->redirect->url('sistema/cursos')
            ->withSuccess('Curso excluído!')
            ->send();
    }

    public function delete_image($id)
    {
        $curso = $this->Cursos_Model->find($id)->row_array();

        if ($this->Cursos_Model->delete_imagem($id)) {

            $this->Foto->delete_img($curso['foto']);

        } else {
             return $this->redirect->url($_SERVER['HTTP_REFERER'])
                ->withError('Erro ao excluir a foto!')
                ->send();
        }
        
        return $this->redirect->url($_SERVER['HTTP_REFERER'])
                ->withSuccess('Foto excluída!')
                ->send();
    }
}