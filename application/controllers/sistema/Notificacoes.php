<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificacoes extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sistema/Notificacoes_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();

        if (! get_system_user_session()) {
            return $this->redirect->url('sistema/login/logout')
                ->send();
        }
    }

    public function index($index = 0)
    {
        $this->load->library('pagination');
        
        $config['base_url'] = base_url('sistema/notificacoes/');
        $config['total_rows'] = $this->Notificacoes_Model->countAll()->num_rows();
        $config['per_page'] = 10;
        
        $this->pagination->initialize($config);

        $info["links"] = $this->pagination->create_links();
        $info['total'] = $this->Notificacoes_Model->totalRegistros();
        $info['notificacoes'] = $this->Notificacoes_Model->all($index)->result_array();
 
        $this->template->view('sistema.notificacoes.list', $info);
    }

    public function create()
    {
        $this->template->view('sistema.notificacoes.create');
    }

    public function insert()
    {
        $data = $this->input->post();

        if (! $data) {
            return $this->redirect->url('sistema/notificacoes')
                ->withError('Nenhum dado foi passado!')
                ->send();
        }

        if ($id = $this->Notificacoes_Model->save($data)) {
            return $this->redirect->url('sistema/notificacoes')
                ->withSuccess('Nova notificação cadastrada!')
                ->send();
        }

        return $this->redirect->url('sistema/notificacoes')
                ->withError('Erro no cadastro!')
                ->send();   
    }

    public function edit($id)
    {
        $info['notificacao'] = $this->Notificacoes_Model->find($id)->row_array();
        $this->template->view('sistema.notificacoes.edit', $info);
    }

    public function update($id)
    {
        $data = $this->input->post();

        if (! $data || ! $id) {
            return $this->redirect->url('sistema/notificacoes/edit/'.$id)
                ->withError('Erro nos dados passados!')
                ->send();
        }

        if (! $this->Notificacoes_Model->update($id, $data)) {
            return $this->redirect->url('sistema/notificacoes/edit/'.$id)
                ->withError('Erro na alteração!')
                ->send();
        }

        return $this->redirect->url('sistema/notificacoes')
            ->withSuccess('Notificação alterada!')
            ->send();
    }

    public function delete($id)
    {
        if (! $id) {
            return $this->redirect->url('sistema/notificacoes')
                ->withError('Informe um ID!')
                ->send();
        }
 
        if (! $this->Notificacoes_Model->delete($id)) {
            return $this->redirect->url('sistema/notificacoes')
                ->withError('Erro na exclusão!')
                ->send();
        }

        return $this->redirect->url('sistema/notificacoes')
            ->withSuccess('Notificação excluída!')
            ->send();
    }

}
