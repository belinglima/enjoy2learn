<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller 
{
	public function __construct()
    {
        parent::__construct();
        $this->load->model('sistema/Welcome_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();
         
        if (! get_system_user_session()) {
        	return $this->redirect->url('sistema/login/logout')
                ->send();
        }
    }

	public function index()
	{
        $info['agendas'] = $this->Welcome_Model->agendas()->num_rows();
        $info['alunos'] = $this->Welcome_Model->alunos()->num_rows();
        $info['aulas'] = $this->Welcome_Model->aulas()->num_rows();
        $info['blog'] = $this->Welcome_Model->blog()->num_rows();
        $info['cursos'] = $this->Welcome_Model->cursos()->num_rows();
        $info['vendas'] = $this->Welcome_Model->vendas()->num_rows();
        $info['duvidas'] = $this->Welcome_Model->duvidas()->num_rows();
        $info['certificados'] = $this->Welcome_Model->certificados()->num_rows();
        $info['notificacoes'] = $this->Welcome_Model->notificacoes()->num_rows();
        $info['contato'] = $this->Welcome_Model->contato()->num_rows();
        $info['cupom'] = $this->Welcome_Model->cupom()->num_rows();
        $info['data']  = date('d/m/Y');
        
        $this->template->view('sistema/bemvindo', $info);
    }   
}