<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mother_Model');
        $this->load->model('sistema/Usuarios_Model');
        $this->load->driver('cache',
                array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_')
        );
        $this->cache->clean();
    }

    public function index()
    {
        if ($this->session->has_userdata('id')) {
            return $this->redirect->url('welcome')
                ->withSuccess('Usuário logado!')
                ->send();
        }

        $info['msg'] = "";
        $emailLogin = $this->input->post('email');
        $senha = $this->input->post('senha');

        if ($emailLogin && $senha) {
            $usuario = $this->verifica($emailLogin, $senha);

            if(!$usuario){
                $usuario = $this->verificaSistema($emailLogin, $senha);                
            }

            if ($usuario) {
                set_user_session($usuario);
                $foto = $this->Usuarios_Model->getFoto();
                set_foto($foto);
                $this->Mother_Model->deleteNotificacao();
            } else {
                $info['msg'] = 'Login ou senha incorretos!';
            }
        }
        
        if (empty($info['msg']) && $this->session->has_userdata('id')) {

            if($usuario['nivel'] == 'Administrador'){
                return $this->redirect->url('select')
                    ->withSuccess('Usuário logado!')
                    ->send();
            }

            return $this->redirect->url('welcome')
                ->withSuccess('Bem vindo aluno!')
                ->send();

        }
        
        $this->template->view('login', $info);
    }

    public function verifica($email, $senha)
    {
        $this->load->library('bcrypt');

        $usuario = $this->Usuarios_Model->findLoginEmail($email);

        if (! empty($usuario)) {
            if ($this->bcrypt->compare($senha, $usuario['hash_senha'])) {
                return $usuario;
            }
        }

        return false;
    }

    public function verificaSistema($login, $senha)
    {
        $this->load->library('bcrypt');

        $usuario = $this->Usuarios_Model->findLogin($login);

        if (! empty($usuario)) {
            if ($this->bcrypt->compare($senha, $usuario['hash_senha'])) {
                return $usuario;
            }
        }

        return false;
    }

    public function logout()
    {   
        echo "<script type='text/javascript'>localStorage.clear();</script>";
        $this->session->sess_destroy();
        $info['msg'] = "Logout realizado!";
        $this->template->view('login', $info);
	}

    public function insert()
    {
        $data = $this->input->post();

        // caso não venha dados do formulario retorna um erro para o site.
        if (! $data) {
            return $this->redirect->url('usuarios')
                ->withError('Nenhum dado foi passado!')
                ->send();
        }

        // dados serão enviados por email para o cliente ativar a conta
        // $dados['link'] = 'http://www.enjoy2learn.com.br/activate/dsf987lkjso7s8987s987s98s7';
        $dados['nome'] = $data['nome'];
        $dados['email'] = $data['email'];
        $dados['senha'] = $data['hash_senha'];

        $this->load->library('bcrypt');
        $data['hash_senha'] = 
        (! empty($data['hash_senha'])) ? $this->bcrypt->hash($data['hash_senha']) : "";

        if($data['HdnTermos'] == 1){
            $data['termos'] = $data['HdnTermos'];
            $data['termo_id'] = $this->Usuarios_Model->getUltimoIdTermo();
        }
        unset($data['HdnTermos']);

        if ($dados['idcliente'] = $this->Usuarios_Model->save($data)) {

            // envio de email após o cadastro no banco de dados
            $this->load->library('SendEmail');
            $body = $this->load->view('emails/cliente', $dados, true);
            
            $this->sendemail->enviar(
                $data['email'],
                'Ativação de usuario', 
                $body
            );

            // retorna uma mensagem após o usuario ser registrado no banco de dados.
            return $this->redirect->url('login')
                ->withSuccess('Cadastro realizado, e-mail enviado, ative sua conta para entrar no sistema')
                ->send();            
        }

        // retorna um erro, caso o cadastro não seja realizado.
        return $this->redirect->url('login')
                ->withError('Erro no cadastro! tente novamente.')
                ->send();   
    }

    public function consultaEmail()
    {
        $email = $this->input->post('email');

        if (! $email) {
            echo 0;
            return;
        }

        echo $this->Usuarios_Model->findEmail($email)->num_rows();
        return;
    }
}