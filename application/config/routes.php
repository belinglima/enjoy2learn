<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Novidades';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// dashborad do aluno
$route['contact'] = 'Dashboard/contato';
$route['teacher'] = 'Dashboard/professor';
$route['metodology'] = 'Dashboard/metodologia';
$route['classes'] = 'Dashboard/meusCursos';
$route['profile'] = 'Dashboard/perfil';
$route['certificates'] = 'Dashboard/certificados';
$route['purchases'] = 'Dashboard/compras';

// meus cursos e pagamento do curso
$route['player'] = 'Lessons/player';
$route['player/(:any)'] = 'Lessons/player/$1';
$route['player/(:any)/(:any)'] = 'Lessons/player/$1/$2';
$route['player/(:any)/(:any)/(:any)'] = 'Lessons/player/$1/$2/$3';
$route['classes/payment/(:any)'] = 'Vendas/getPagamento/$1';

// avaliar
$route['avaliar/(:any)'] = 'Dashboard/avaliar/$1';
$route['save_evaluation'] = 'Dashboard/saveEvaluation';

// gerar certificado
$route['getcertificate/(:any)'] = 'Dashboard/getcertificate/$1';

// duvidas frequentes
$route['duvidas-gerais'] = 'Dashboard/duvidas/1';
$route['duvidas-alunos'] = 'Dashboard/duvidas/2';
$route['duvidas-pagamentos'] = 'Dashboard/duvidas/3';

// notificações
$route['popup-notificacoes/(:any)'] = 'Dashboard/notificacoes/$1';

// busca de cursos
$route['search'] = 'Dashboard/search';

// menu lista de cursos
$route['library'] = 'Dashboard/cursos/0';
$route['studying'] = 'Dashboard/cursos/1';
$route['completed'] = 'Dashboard/cursos/2';
$route['library/(:any)'] = 'Dashboard/show/$1';
$route['studying/(:any)'] = 'Dashboard/show/$1';
$route['completed/(:any)'] = 'Dashboard/show/$1';

// vendas
$route['cart'] = 'Vendas/lista';
$route['cart/delete/(:any)/(:any)'] = 'Vendas/delete/$1/$2';
$route['cart/cupom/(:any)/(:any)'] = 'Vendas/cupom/$1/$2';

// compras
$route['comprar'] = "Library/insert";
$route['comprar/(:any)'] = "Library/insert/$1";

// select witch screen to enter / System Adminitration or Student Classes.
$route['welcome'] = 'Dashboard/entrada';
$route['select'] = 'Dashboard/seleciona';
$route['findemail'] = 'Login/consultaEmail';

//Sistema
$route['sistema/agendas'] = 'sistema/Agendas/index';
$route['sistema/agendas/(:num)'] = 'sistema/Agendas/index/$1';
$route['sistema/alunos'] = 'sistema/Alunos/index';
$route['sistema/alunos/(:num)'] = 'sistema/Alunos/index/$1';
$route['sistema/aulas/lista/(:num)/paginate/(:num)'] = 'sistema/Aulas/lista/$1/p/$2';
$route['sistema/blog'] = 'sistema/Blog/index';
$route['sistema/blog/(:num)'] = 'sistema/Blog/index/$1';
$route['sistema/certificados'] = 'sistema/Certificados/index';
$route['sistema/certificados/(:num)'] = 'sistema/Certificados/index/$1';
$route['sistema/contatos'] = 'sistema/Contatos/index';
$route['sistema/contatos/(:num)'] = 'sistema/Contatos/index/$1';
$route['sistema/contatos'] = 'sistema/Contatos/index';
$route['sistema/cupons/(:num)'] = 'sistema/Cupons/index/$1';
$route['sistema/cursos'] = 'sistema/Cursos/index';
$route['sistema/cursos/(:num)'] = 'sistema/Cursos/index/$1';
$route['sistema/disponibilidades'] = 'sistema/Disponibilidades/index';
$route['sistema/disponibilidades/(:num)'] = 'sistema/Disponibilidades/index/$1';
$route['sistema/duvidas'] = 'sistema/Duvidas/index';
$route['sistema/duvidas/(:num)'] = 'sistema/Duvidas/index/$1';
$route['sistema/materiaisApoio'] = 'sistema/MateriaisApoio/index';
$route['sistema/materiaisApoio/(:num)'] = 'sistema/MateriaisApoio/index/$1';
$route['sistema/notificacoes'] = 'sistema/Notificacoes/index';
$route['sistema/notificacoes/(:num)'] = 'sistema/Notificacoes/index/$1';
$route['sistema/usuarios'] = 'sistema/Usuarios/index';
$route['sistema/usuarios/(:num)'] = 'sistema/Usuarios/index/$1';
$route['sistema/vendas'] = 'sistema/Vendas/index';
$route['sistema/vendas/(:num)'] = 'sistema/Vendas/index/$1';
